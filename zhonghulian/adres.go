package zhonghulian

type adres struct {
	Id      string
	Bibdid  string
	Nbr     int
	Seatbid []*seatbid
}

type seatbid struct {
	Bid  []*bid
	Seat string
}

type bid struct {
	Impid   string
	Admtype int
	Price   int
	Adid    string
	Nurl    string
	Adm     string
	Adw     int
	Adh     int
	Iurl    string
	Curl    string
	Cturl   []string
	Aurl    string
	Cid     string
	Crid    string
	Ctype   int
	Cbundle string
	Attr    []string
	Adomain string
	Ext     *ext
	Native  *resNative
}
type ext struct {
	Instl   int
	Adt     int
	Ade     string
	Apkname string
}
type resNative struct {
	Tid      string
	Resource resource
}

type resource struct {
	Title string
	Desc  string
	Path  string
	Ldm   string
}
