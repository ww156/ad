package zhonghulian

import (
	"bytes"
	// "encoding/json"
	// "fmt"
	"io/ioutil"
	// "log"
	"net/http"
	// "net/url"
	"strconv"
	// "strings"
	// "time"
	// "unsafe"
	// "regexp"

	"ad/mongo"
	"ad/util"
	"github.com/pquerna/ffjson/ffjson"
)

const (
	PRO_URL = "http://ecook.adtree.cn:4010"
	AID     = "04c45f83e9f98e381cadfd43e5d10706"
	TID     = "WSCF001"
)

func Zhonghulian(getReq *util.ReqMsg, funcName string) util.ResMsg {
	return base(getReq, funcName, 640, 100, 3000, AID, "2")
}
func ZhonghulianFlow(getReq *util.ReqMsg, funcName string) util.ResMsg {
	return base(getReq, funcName, 640, 360, 6000, AID, "7")
}
func ZhonghulianStartup(getReq *util.ReqMsg, funcName string) util.ResMsg {
	return base(getReq, funcName, 640, 960, 12000, AID, "2")
}

func base(getReq *util.ReqMsg, funcName string, w, h, price int, aid, adtype string) util.ResMsg {
	carrier := "46000"
	switch getReq.Imsi {
	case "1":
		carrier = "46000"
	case "2":
		carrier = "46001"
	case "3":
		carrier = "46002"
	default:
		carrier = "0"
	}
	network := 1
	switch getReq.Network {
	case "wifi":
		network = 1
	case "2g":
		network = 3
	case "3g":
		network = 4
	case "4g":
		network = 5
	default:
		network = 0
	}
	width, err := strconv.Atoi(getReq.Screenwidth)
	if err != nil {
		width = 0
	}
	height, err := strconv.Atoi(getReq.Screenheight)
	if err != nil {
		height = 0
	}
	postData := adreq{
		Id: util.GetRandom(),
		Imp: []*Imp{
			&Imp{
				Impid:       util.GetRandom(),
				Bidfloor:    price,
				Bidfloorcur: "CNY",
				W:           w,
				H:           h,
				Adtype:      adtype,
				Native: &native{
					Tid: TID,
				},
			},
		},
		App: &App{
			Aid:      aid,
			Name:     "网上厨房",
			Ver:      getReq.Appversion,
			Bundle:   "cn.ecook",
			Storeurl: "http://a.app.qq.com/o/simple.jsp?pkgname=cn.ecook",
		},
		Device: &Device{
			Did:            util.Md5(getReq.Imei),
			Dpid:           getReq.Androidid,
			Mac:            util.Md5(getReq.Mac),
			Ua:             getReq.Ua,
			Ip:             getReq.Ip,
			Country:        "CN",
			Carrier:        carrier,
			Language:       "zh",
			Make:           getReq.Vendor,
			Model:          getReq.Model,
			Os:             "ANDROID",
			Osv:            getReq.Osversion,
			Connectiontype: network,
			Devicetype:     2,
			Sw:             width,
			Sh:             height,
		},
	}
	if getReq.Os == "2" {
		postData.Device.Os = "IOS"
		postData.Device.Devicetype = 1
		postData.App.Bundle = "cn.ecook.ecook"
		postData.App.Storeurl = "https://itunes.apple.com/cn/app/id12345678?mt=8"
	}

	ma, err := ffjson.Marshal(&postData)
	if err != nil {
		return util.ResMsg{}
	}
	// ma = []byte(`{"id":"d6e608bcbe5344d4b5135f710ed65fb9","imp":[{"impid":"483ceb489810452187f72667509b7b0d","bidfloor":6000,"bidfloorcur":"CNY","w":640,"h":960,"instl":0}],"app":{"aid":"9eded84882b9441583f8f623aff2fe8e","name":"App Name","cat":["2"],"ver":"1.0","bundle":"com.company.appname","paid":0,"storeurl":"https://itunes.apple.com/cn/app/id12345678?mt=8 "},"device":{"did":"75184389584b32a37f4f4570ca85112da1463707","dpid":"e1ef08c816e8e1604c7b4c5ddad8cdaf2edfc843","ua":"Mozilla%2F5.0+%28iPhone%3B+U%3B+CPU+like+Mac+OS+X%3B +en%29+AppleWebKit%2F420% 2B+%28KHTML","ip":"123.123.123.123 ","country":"CN","carrier":"46000","language":"zh","make":"Apple","model":"iPhone5,1","os":"iOS","osv":"7.0","connectiontype":2,"devicetype":1,"loc":"38.04165,114.50884"}}`)
	// 请求广告
	req, err := http.NewRequest("POST", PRO_URL, bytes.NewBuffer(ma))
	if err != nil {
		return util.ResMsg{}
	}
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("x-SSPrtb-version", "1.3")

	resp, err := util.Client.Do(req)
	mongo.LogReq(funcName, getReq.Os)
	if err != nil {
		// fmt.Println(err.Error())
		mongo.LogTimeout(funcName, getReq.Os)
		return util.ResMsg{}
	}
	data, err := ioutil.ReadAll(resp.Body)
	resp.Body.Close()
	if err != nil {
		mongo.LogNo(funcName, getReq.Os)
		return util.ResMsg{}
	}
	// fmt.Println(resp.StatusCode)
	if resp.StatusCode != 200 {
		mongo.LogTimeout(funcName, getReq.Os)
		return util.ResMsg{}
	}

	return FormateRes(data, getReq.Os, funcName, adtype)
}

// 根据广告平台的返回生成客户端返回
func FormateRes(res []byte, os, funcName, adtype string) util.ResMsg {
	resData := adres{}
	ffjson.Unmarshal(res, &resData)
	// fmt.Println(string(res))
	if len(resData.Seatbid) == 0 {
		mongo.LogNo(funcName, os)
		util.HttpRequestCount.WithLabelValues(funcName + "noerr").Inc()
		return util.ResMsg{}
	}
	if len(resData.Seatbid[0].Bid) == 0 {
		// fmt.Println(3)
		mongo.LogNo(funcName, os)
		return util.ResMsg{}
	}

	ad := resData.Seatbid[0].Bid[0]
	if ad.Admtype == 0 {
		mongo.LogNo(funcName, os)
		return util.ResMsg{}
	}
	if ad.Adm == "" {
		mongo.LogNo(funcName, os)
		return util.ResMsg{}
	}

	postData := util.ResMsg{
		Id:       "0",
		Weight:   0,
		State:    0,
		Title:    "",
		Content:  "",
		ImageUrl: ad.Adm,
		Uri:      ad.Curl,
	}
	if adtype == "7" {
		native := ad.Native.Resource
		postData.Title = native.Title
		postData.Content = native.Desc
		postData.ImageUrl = native.Path
	}
	// fmt.Println(ad.Curl)
	if ad.Nurl != "" {
		postData.Displayreport = append(postData.Displayreport, ad.Nurl)
	}
	if ad.Iurl != "" {
		postData.Displayreport = append(postData.Displayreport, ad.Iurl)
	}
	postData.Displayreport = append(postData.Displayreport, "http://120.26.247.23/ad/report.php?method="+funcName+"&event=displayreport&os="+os+"&t="+util.GetRandom())
	postData.Clickreport = ad.Cturl
	postData.Clickreport = append(postData.Clickreport, "http://120.26.247.23/ad/report.php?method="+funcName+"&event=clickreport&os="+os+"&t="+util.GetRandom())

	return postData
}
