package zhonghulian

type adreq struct {
	Id     string   `json:"id"`
	Imp    []*Imp   `json:"imp,omitempty"`
	App    *App     `json:"app"`
	Device *Device  `json:"device"`
	Bcat   []string `json:"bcat,omitempty"`
	Badv   []string `json:"badv,omitempty"`
}

type Imp struct {
	Impid       string   `json:"impid"`
	Bidfloor    int      `json:"bidfloor"`
	Bidfloorcur string   `json:"bidfloorcur"`
	W           int      `json:"w"`
	H           int      `json:"h"`
	Pos         int      `json:"int,omitempty"`
	Btype       []string `json:"btype,omitempty"`
	Battr       []string `json:"battr,omitempty"`
	Instl       int      `json:"instl,omitempty"`
	Splash      int      `json:"splash,omitempty"`
	Adtype      string   `json:"adtype"`
	Native      *native  `json:"native,omitempty"`
}

type native struct {
	Tid string `json:"tid"`
}

type App struct {
	Aid      string   `json:"aid"`
	Name     string   `json:"name"`
	Cat      []string `json:"cat,omitempty"`
	Ver      string   `json:"ver"`
	Bundle   string   `json:"bundle"`
	Itid     string   `json:"itid,omitempty"`
	Paid     int      `json:"paid,omitempty"`
	Storeurl string   `json:"storeurl"`
	Keywords string   `json:"keywords,omitempty"`
	Pid      string   `json:"pid,omitempty"`
	Pub      string   `json:"pub,omitempty"`
}

type Device struct {
	Did            string  `json:"did"`
	Dpid           string  `json:"dpid"`
	Mac            string  `json:"mac"`
	Ua             string  `json:"ua"`
	Ip             string  `json:"ip"`
	Country        string  `json:"country"`
	Carrier        string  `json:"carrier"`
	Language       string  `json:"language"`
	Make           string  `json:"make"`
	Model          string  `json:"model"`
	Os             string  `json:"os"`
	Osv            string  `json:"osv"`
	Connectiontype int     `json:"connectiontype"`
	Devicetype     int     `json:"devicetype"`
	Ioc            string  `json:"ioc,omitempty"`
	Density        float64 `json:"density,omitempty"`
	Sw             int     `json:"sw"`
	Sh             int     `json:"sh"`
	Orientation    int     `json:"orientation,omitempty"`
}
