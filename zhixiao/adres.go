package zhixiao

type adres struct {
	Status   string
	Errcode  int
	Type     string
	Creative []Ad
}

type Ad struct {
	Type     string
	W        int
	H        int
	Url      string
	Action   []Action
	Tracking []Tracking
}

type Action struct {
	Et int
	Eu string
}

type Tracking struct {
	Et  int
	Tku []string
}
