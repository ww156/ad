package zhixiao

import (
	"encoding/json"
	// "fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"strconv"
	"time"

	"ad/mongo"
	"ad/util"
)

const (
	URL                 = "http://b.bla01.com/mad.htm"
	CLID                = "fARMUMMceYBlmQsRkzAwbBOG8sUnzp4M"
	AID_ANDROID         = "1041"
	AID_IOS             = "1040"
	AID_ANDROID_STARTUP = "1039"
	AID_IOS_STARTUP     = "1038"
)

func Zhixiao(getReq *util.ReqMsg, funcName string) util.ResMsg {
	if getReq.Os == "1" {
		return base(getReq, funcName, AID_ANDROID, "640", "100")
	}
	return base(getReq, funcName, AID_IOS, "640", "100")
}

func ZhixiaoStartup(getReq *util.ReqMsg, funcName string) util.ResMsg {
	if getReq.Os == "1" {
		return base(getReq, funcName, AID_ANDROID_STARTUP, "640", "960")
	}
	return base(getReq, funcName, AID_IOS_STARTUP, "640", "960")
}

func base(getReq *util.ReqMsg, funcName, aid, w, h string) util.ResMsg {
	timestamp := strconv.Itoa(int(time.Now().Unix()))
	v := url.Values{}
	v.Set("aid", aid)
	v.Set("bn", getReq.Vendor)
	v.Set("mn", getReq.Model)
	v.Set("os", "Android "+getReq.Osversion)
	v.Set("rs", getReq.Screenwidth+"x"+getReq.Screenheight)
	// network := 0
	// switch getReq.Network {
	// case "wifi":
	// 	network = 1
	// case "2g":
	// 	network = 2
	// case "3g":
	// 	carrier = 3
	// case "4g":
	// 	carrier = 4
	// default:
	// 	carrier = 0
	// }
	v.Set("net", getReq.Network)
	carrier := "00"
	switch getReq.Imsi {
	case "1":
		carrier = "00"
	case "2":
		carrier = "01"
	case "3":
		carrier = "03"
	default:
		carrier = "unknown"
	}
	v.Set("mnc", carrier)
	v.Set("ut", "imei")
	v.Set("imei", getReq.Imei)
	v.Set("lt", "1")
	v.Set("ip", getReq.Ip)
	v.Set("ua", getReq.Ua)
	v.Set("aw", w)
	v.Set("ah", h)
	v.Set("fmt", "json")
	v.Set("ts", timestamp)
	v.Set("ver", "1.0.0")

	vk := util.Md5(CLID + "|" + getReq.Imei + "|" + timestamp)
	if getReq.Os == "2" {
		v.Set("os", "ios "+getReq.Osversion)
		v.Set("ut", "idfa")
		v.Set("idfa", getReq.Idfa)
		vk = util.Md5(CLID + "|" + getReq.Idfa + "|" + timestamp)
	}
	// client := http.Client{Timeout: util.Timeout}
	req, err := http.NewRequest("GET", URL+"?"+v.Encode(), nil)
	req.Header.Set("Connection", "keep-alive")
	req.Header.Set("vk", vk)
	if err != nil {
		mongo.LogTimeout(funcName, getReq.Os)
		return util.ResMsg{}
	}
	resp, err := util.Client.Do(req)
	mongo.LogReq(funcName, getReq.Os)
	if err != nil {
		mongo.LogTimeout(funcName, getReq.Os)
		return util.ResMsg{}
	}
	data, err := ioutil.ReadAll(resp.Body)
	resp.Body.Close()
	if err != nil {
		mongo.LogNo(funcName, getReq.Os)
		return util.ResMsg{}
	}
	if resp.StatusCode != 200 {
		mongo.LogNo(funcName, getReq.Os)
		return util.ResMsg{}
	}

	return FormateRes(data, getReq.Os, funcName)
}

func FormateRes(res []byte, os, funcName string) util.ResMsg {
	resData := adres{}
	json.Unmarshal(res, &resData)
	// fmt.Println(string(res))
	if resData.Status != "success" {
		mongo.LogNo(funcName, os)
		return util.ResMsg{}
	}
	ads := resData.Creative
	if len(ads) == 0 {
		mongo.LogNo(funcName, os)
		util.HttpRequestCount.WithLabelValues(funcName + "noerr").Inc()
		return util.ResMsg{}
	}
	if ads[0].Url == "" {
		mongo.LogNo(funcName, os)
		util.HttpRequestCount.WithLabelValues(funcName + "noerr").Inc()
		return util.ResMsg{}
	}
	postData := util.ResMsg{
		Id:       "0",
		Weight:   0,
		State:    0,
		Title:    "",
		Content:  "",
		ImageUrl: ads[0].Url,
		Uri:      "",
	}
	for _, value := range ads[0].Action {
		if value.Et == 11 {
			postData.Uri = value.Eu
		}
	}
	for _, value := range ads[0].Tracking {
		if value.Et == 2 {
			postData.Displayreport = value.Tku
		}
		if value.Et == 11 {
			postData.Clickreport = value.Tku
		}
	}
	postData.Displayreport = append(postData.Displayreport, "http://120.26.247.23/ad/report.php?method="+funcName+"&event=displayreport&os="+os+"&t="+util.GetRandom())
	postData.Clickreport = append(postData.Clickreport, "http://120.26.247.23/ad/report.php?method="+funcName+"&event=clickreport&os="+os+"&t="+util.GetRandom())

	return postData
}
