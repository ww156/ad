package zhixiao

type adreq struct {
	Aid    int    `json:"aid"`
	Bn     string `json:"bn"`
	Mn     string `json:"mn"`
	Os     string `json:"os"`
	Rs     string `json:"rs"`
	Net    string `json:"net"`
	Mnc    int    `json:"mnc"`
	Lct    string `json:"lct"`
	Ut     string `json:"ut"`
	Imei   string `json:"imei"`
	Anid   string `json:"anid"`
	Mac1   string `json:"mac1"`
	Mac2   string `json:"mac2"`
	Idfa   string `json:"idfa"`
	Idfv   string `json:"idfv"`
	Ctmid  string `json:"ctmid"`
	Lt     int    `json:"lt"`
	Ua     string `json:"ua"`
	Aw     int    `json:"aw"`
	Ah     int    `json:"ah"`
	Fmt    string `json:"fmt"`
	Mnd    int    `json:"mnd"`
	Mxd    int    `json:"mxd"`
	Ts     int64  `json:"ts"`
	Ver    string `json:"ver"`
	Istest bool   `json:"istest"`
	Isping bool   `json:"isping"`
}
