package yuemeng

type adres struct {
	ResultCode    string
	ResultMessage string
	Ads           []Ad
}

type Ad struct {
	AdHotActionType  string `json:"ad-hot-action-type"`
	AdHotActionParam string `json:"ad-hot-action-param"`
	Text_title       string
	Content          string
	Desc             string
	Ad_pic           string
	BannerType       int
	Showurl          []string
	Clickurl         []string
}

type param struct {
	Target          int
	Url             string
	Cpd_report_urls []string
	Cpa_report_urls []string
	App_name        string
	App_size        int64
	App_package     string
}
