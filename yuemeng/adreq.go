package yuemeng

type adreq struct {
	Code         string  `json:"code"`
	Extra        string  `json:"extra"`
	Ip           string  `json:"ip"`
	Os           int     `json:"os"`
	AdType       int     `json:"adType"`
	BannerType   int     `json:"bannerType"`
	Imei         string  `json:"imei"`
	Idfa         string  `json:"idfa"`
	Androidid    string  `json:"androidid"`
	Mac          string  `json:"mac"`
	TimeStamp    string  `json:"timeStamp"`
	ValidateCode string  `json:"validateCode"`
	Imsi         string  `json:"imsi"`
	StyleId      float64 `json:"styleId"`
	Num          int     `json:"num"`
	Network      string  `json:"network"`
	Model        string  `json:"model"`
	Dt           string  `json:"dt"`
	Width        int     `json:"width"`
	Height       int     `json:"height"`
	Release      string  `json:"release"`
	Appversion   string  `json:"appversion"`
	UserObj      *User   `json:"userObj"`
	ScreenWidth  string  `json:"screenWidth"`
	ScreenHeight string  `json:"screenHeight"`
}

type User struct {
	Lat      string `json:"lat"`
	Lon      string `json:"lon"`
	Gender   string `json:"gender"`
	Age      int    `json:"age"`
	Keywords string `json:"keywords"`
	Country  string `json:"country"`
	City     string `json:"city"`
	Ext      string `json:"ext"`
}

type Extra struct {
	BookName string `json:"bookName"`
	Author   string `json:"author"`
	Category string `json:"category"`
}
