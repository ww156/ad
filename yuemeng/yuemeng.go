package yuemeng

import (
	"encoding/json"
	// "fmt"
	"io/ioutil"
	"net/http"
	"strconv"
	"time"

	"ad/mongo"
	"ad/util"
)

const (
	URL          = "http://ad.ipadview.com/dspapi/ad/getAd/"
	CODE_ANDROID = "20015"
	CODE_IOS     = "20016"
)

func Yuemeng(getReq *util.ReqMsg, funcName string) util.ResMsg {
	return base(getReq, funcName, 2, 640, 100, 0)
}

func YuemengFlow(getReq *util.ReqMsg, funcName string) util.ResMsg {
	return base(getReq, funcName, 2, 640, 360, 1)
}

func YuemengStartup(getReq *util.ReqMsg, funcName string) util.ResMsg {
	return base(getReq, funcName, 4, 640, 960, 0)
}

func base(getReq *util.ReqMsg, funcName string, adtype int, w, h, bannertype int) util.ResMsg {
	network := "1"
	switch getReq.Network {
	case "wifi":
		network = "1"
	case "2g":
		network = "2"
	default:
		network = "3"
	}
	postData := adreq{
		Code:         CODE_ANDROID,
		Ip:           getReq.Ip,
		Os:           1,
		AdType:       adtype,
		BannerType:   bannertype,
		Imei:         getReq.Imei,
		Idfa:         getReq.Idfa,
		Androidid:    getReq.Androidid,
		Mac:          getReq.Mac,
		TimeStamp:    strconv.Itoa(int(time.Now().Unix())),
		Imsi:         getReq.Imsi,
		Network:      network,
		Model:        getReq.Model,
		Dt:           "phone",
		Width:        w,
		Height:       h,
		Release:      getReq.Osversion,
		Appversion:   getReq.Appversion,
		ScreenWidth:  getReq.Screenwidth,
		ScreenHeight: getReq.Screenheight,
		UserObj: &User{
			Lat: "0",
			Lon: "0",
		},
	}
	postData.ValidateCode = util.Md5(postData.TimeStamp + CODE_ANDROID)
	if getReq.Os == "2" {
		postData.Code = CODE_IOS
		postData.Os = 2
		postData.Imei = getReq.Idfa
		postData.Idfa = getReq.Idfa
		postData.ValidateCode = util.Md5(postData.TimeStamp + CODE_IOS)
	}

	ma, err := json.Marshal(&postData)
	if err != nil {
		return util.ResMsg{}
	}
	header := http.Header{}
	header.Set("Connection", "keep-alive")
	header.Set("Content-Type", "application/json")
	header.Set("User-Agent", getReq.Ua)
	resp, err := util.HttpPOST(URL, &header, ma)
	mongo.LogReq(funcName, getReq.Os)
	if err != nil {
		mongo.LogTimeout(funcName, getReq.Os)
		util.HttpRequestCount.WithLabelValues(funcName + "timeouterr").Inc()
		return util.ResMsg{}
	}
	data, err := ioutil.ReadAll(resp.Body)
	resp.Body.Close()
	if err != nil {
		mongo.LogNo(funcName, getReq.Os)
		util.HttpRequestCount.WithLabelValues(funcName + "readresperr").Inc()
		return util.ResMsg{}
	}
	if resp.StatusCode != 200 {
		mongo.LogTimeout(funcName, getReq.Os)
		util.HttpRequestCount.WithLabelValues(funcName + "not200err").Inc()
		return util.ResMsg{}
	}

	return FormateRes(data, getReq.Os, funcName)
}

func FormateRes(res []byte, os, funcName string) util.ResMsg {
	resData := &adres{}
	json.Unmarshal(res, resData)
	// fmt.Printf("%+v", resData)
	if resData.ResultCode != "0" {
		mongo.LogNo(funcName, os)
		util.HttpRequestCount.WithLabelValues(funcName + "noerr").Inc()
		return util.ResMsg{}
	}
	if len(resData.Ads) == 0 {
		mongo.LogNo(funcName, os)
		util.HttpRequestCount.WithLabelValues(funcName + "noerr").Inc()
		return util.ResMsg{}
	}
	ad := resData.Ads[0]
	if ad.Ad_pic == "" {
		mongo.LogNo(funcName, os)
		util.HttpRequestCount.WithLabelValues(funcName + "noerr").Inc()
		return util.ResMsg{}
	}
	target := param{}
	json.Unmarshal([]byte(ad.AdHotActionParam), &target)
	postData := util.ResMsg{
		Id:       "0",
		Weight:   0,
		State:    0,
		Title:    ad.Text_title,
		Content:  ad.Content,
		ImageUrl: ad.Ad_pic,
		Uri:      target.Url,
	}
	postData.Displayreport = ad.Showurl
	postData.Displayreport = append(postData.Displayreport, "http://120.26.247.23/ad/report.php?method="+funcName+"&event=displayreport&os="+os+"&t="+util.GetRandom())
	postData.Clickreport = ad.Clickurl
	postData.Clickreport = append(postData.Clickreport, "http://120.26.247.23/ad/report.php?method="+funcName+"&event=clickreport&os="+os+"&t="+util.GetRandom())

	return postData
}
