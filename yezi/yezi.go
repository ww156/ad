package yezi

import (
	"bytes"
	// "encoding/json"
	// "fmt"
	"io/ioutil"
	// "log"
	"net/http"
	"strconv"
	"strings"
	// "time"
	// "unsafe"
	"math/rand"
	// "regexp"

	"github.com/golang/protobuf/proto"

	"ad/mongo"
	"ad/util"
)

const (
	URL            = "http://api.ssp.securecloud.com.cn/api/def" // "http://120.92.44.245/api/def"
	BANNER_WIDTH   = 640
	BANNER_HEIGHT  = 100
	FLOW_WIDTH     = 1280
	FLOW_HEIGHT    = 720
	STARTUP_WIDTH  = 640
	STARTUP_HEIGHT = 960
	// 新增加广告位
	// 网上厨房(新)-I	uevexsm2	网上厨房(新)-I_原生开屏	lhohekpx	信息流
	// 网上厨房(新)-I	uevexsm2	网上厨房(新)-I_信息流	uasyavax	信息流
	// 网上厨房(新)-A	6ck1jw9m	网上厨房(新)-A_原生开屏	ilhlqytc	信息流
	// 网上厨房(新)-A	6ck1jw9m	网上厨房(新)-A_信息流	chduqdxb	信息流
	// IOS_N_APPID            = "uevexsm2"
	// IOS_N_ADID_STARTUP     = "lhohekpx"
	// IOS_N_ADID_FLOW        = "uasyavax"
	// ANDROID_N_APPID        = "6ck1jw9m"
	// ANDROID_N_ADID_STARTUP = "ilhlqytc"
	// ANDROID_N_ADID_FLOW    = "chduqdxb"
)

var (
	IOS_APPID            = []string{"apvzmppc", "uevexsm2"} // "j0g2phg7"
	IOS_ADID             = "jqcsmgfd"                       // "mfyrrfeb"
	IOS_ADID_FLOW        = []string{"x0yqwngi", "uasyavax"} // "vzbyzvth"
	IOS_ADID_STARTUP     = []string{"ku7fmcc9", "lhohekpx"} // "julx2mnw"
	IOS_ADID_STORY       = "kxsdkw5h"
	ANDROID_APPID        = []string{"pyeyuwha", "6ck1jw9m"} // "6bq3bpop"
	ANDROID_ADID         = "bodok0oz"                       // "vtt8ro1o"
	ANDROID_ADID_FLOW    = []string{"e6ihvhbh", "chduqdxb"} // "fdej1xay"
	ANDROID_ADID_STARTUP = []string{"mfbvctzi", "ilhlqytc"} // "sx8d0lkj"
	ANDROID_ADID_STORY   = "vkkupb52"
)

func Yezi(getReq *util.ReqMsg, funcName string) util.ResMsg {
	if getReq.Os == "1" {
		return base(getReq, funcName, ANDROID_APPID[0], ANDROID_ADID, BANNER_WIDTH, BANNER_HEIGHT)
	}
	return base(getReq, funcName, IOS_APPID[0], IOS_ADID, BANNER_WIDTH, BANNER_HEIGHT)
}

func YeziStartUp(getReq *util.ReqMsg, funcName string) util.ResMsg {
	r := rand.Intn(2)
	// fmt.Println(r)
	if getReq.Os == "1" {
		return base(getReq, funcName, ANDROID_APPID[r], ANDROID_ADID_STARTUP[r], STARTUP_WIDTH, STARTUP_HEIGHT)
	}
	return base(getReq, funcName, IOS_APPID[r], IOS_ADID_STARTUP[r], STARTUP_WIDTH, STARTUP_HEIGHT)
}

func YeziFlow(getReq *util.ReqMsg, funcName string) util.ResMsg {
	r := rand.Intn(2)
	// fmt.Println(r)
	if getReq.Os == "1" {
		return base(getReq, funcName, ANDROID_APPID[r], ANDROID_ADID_FLOW[r], FLOW_WIDTH, FLOW_HEIGHT)
	}
	return base(getReq, funcName, IOS_APPID[r], IOS_ADID_FLOW[r], FLOW_WIDTH, FLOW_HEIGHT)
}
func YeziFlow2(getReq *util.ReqMsg, funcName string) util.ResMsg {
	r := rand.Intn(2)
	// fmt.Println(r)
	if getReq.Os == "1" {
		return base(getReq, funcName, ANDROID_APPID[r], ANDROID_ADID_FLOW[r], FLOW_WIDTH, FLOW_HEIGHT)
	}
	return base(getReq, funcName, IOS_APPID[r], IOS_ADID_FLOW[r], FLOW_WIDTH, FLOW_HEIGHT)
}
func YeziFlow3(getReq *util.ReqMsg, funcName string) util.ResMsg {
	r := rand.Intn(2)
	// fmt.Println(r)
	if getReq.Os == "1" {
		return base(getReq, funcName, ANDROID_APPID[r], ANDROID_ADID_FLOW[r], FLOW_WIDTH, FLOW_HEIGHT)
	}
	return base(getReq, funcName, IOS_APPID[r], IOS_ADID_FLOW[r], FLOW_WIDTH, FLOW_HEIGHT)
}
func YeziFlow4(getReq *util.ReqMsg, funcName string) util.ResMsg {
	r := rand.Intn(2)
	// fmt.Println(r)
	if getReq.Os == "1" {
		return base(getReq, funcName, ANDROID_APPID[r], ANDROID_ADID_FLOW[r], FLOW_WIDTH, FLOW_HEIGHT)
	}
	return base(getReq, funcName, IOS_APPID[r], IOS_ADID_FLOW[r], FLOW_WIDTH, FLOW_HEIGHT)
}
func YeziFlow5(getReq *util.ReqMsg, funcName string) util.ResMsg {
	r := rand.Intn(2)
	// fmt.Println(r)
	if getReq.Os == "1" {
		return base(getReq, funcName, ANDROID_APPID[r], ANDROID_ADID_FLOW[r], FLOW_WIDTH, FLOW_HEIGHT)
	}
	return base(getReq, funcName, IOS_APPID[r], IOS_ADID_FLOW[r], FLOW_WIDTH, FLOW_HEIGHT)
}
func YeziFlow6(getReq *util.ReqMsg, funcName string) util.ResMsg {
	r := rand.Intn(2)
	// fmt.Println(r)
	if getReq.Os == "1" {
		return base(getReq, funcName, ANDROID_APPID[r], ANDROID_ADID_FLOW[r], FLOW_WIDTH, FLOW_HEIGHT)
	}
	return base(getReq, funcName, IOS_APPID[r], IOS_ADID_FLOW[r], FLOW_WIDTH, FLOW_HEIGHT)
}
func YeziFlow7(getReq *util.ReqMsg, funcName string) util.ResMsg {
	r := rand.Intn(2)
	// fmt.Println(r)
	if getReq.Os == "1" {
		return base(getReq, funcName, ANDROID_APPID[r], ANDROID_ADID_FLOW[r], FLOW_WIDTH, FLOW_HEIGHT)
	}
	return base(getReq, funcName, IOS_APPID[r], IOS_ADID_FLOW[r], FLOW_WIDTH, FLOW_HEIGHT)
}
func YeziFlow8(getReq *util.ReqMsg, funcName string) util.ResMsg {
	r := rand.Intn(2)
	// fmt.Println(r)
	if getReq.Os == "1" {
		return base(getReq, funcName, ANDROID_APPID[r], ANDROID_ADID_FLOW[r], FLOW_WIDTH, FLOW_HEIGHT)
	}
	return base(getReq, funcName, IOS_APPID[r], IOS_ADID_FLOW[r], FLOW_WIDTH, FLOW_HEIGHT)
}
func YeziFlow9(getReq *util.ReqMsg, funcName string) util.ResMsg {
	r := rand.Intn(2)
	// fmt.Println(r)
	if getReq.Os == "1" {
		return base(getReq, funcName, ANDROID_APPID[r], ANDROID_ADID_FLOW[r], FLOW_WIDTH, FLOW_HEIGHT)
	}
	return base(getReq, funcName, IOS_APPID[r], IOS_ADID_FLOW[r], FLOW_WIDTH, FLOW_HEIGHT)
}

func YeziStoryFlow(getReq *util.ReqMsg, funcName string) util.ResMsg {
	if getReq.Os == "1" {
		return base(getReq, funcName, ANDROID_APPID[0], ANDROID_ADID_STORY, FLOW_WIDTH, FLOW_HEIGHT)
	}
	return base(getReq, funcName, IOS_APPID[0], IOS_ADID_STORY, FLOW_WIDTH, FLOW_HEIGHT)
}

func base(getReq *util.ReqMsg, funcName, appid, adid string, w, h uint32) util.ResMsg {
	postData := MobadsRequest{
		RequestId: proto.String(util.GetRandom()),
		ApiVersion: &Version{
			Major: proto.Uint32(5),
			Minor: proto.Uint32(4),
			Micro: proto.Uint32(1),
		},
		App: &App{
			AppId:     proto.String(appid),
			ChannelId: proto.String(""),
			AppVersion: &Version{
				Major: proto.Uint32(13),
				Minor: proto.Uint32(3),
				Micro: proto.Uint32(0),
			},
			AppPackage: proto.String("cn.ecook"),
		},
		Device: &Device{
			DeviceType: Device_PHONE.Enum(),
			OsType:     Device_ANDROID.Enum(),
			Vendor:     []byte(getReq.Vendor),
			Model:      []byte(getReq.Model),
			Udid: &UdId{
				Idfa:      proto.String(getReq.Idfa),
				Imei:      proto.String(getReq.Imei),
				Mac:       proto.String(getReq.Mac),
				AndroidId: proto.String(getReq.Androidid),
			},
		},
		Network: &Network{
			Ipv4: proto.String(getReq.Ip),
		},
		Adslot: &AdSlot{
			AdslotId: proto.String(adid),
			AdslotSize: &Size{
				Width:  proto.Uint32(w),
				Height: proto.Uint32(h),
			},
		},
	}
	// screensize
	width, err := strconv.Atoi(getReq.Screenwidth)
	if err != nil {
		width = 0
	}
	height, err := strconv.Atoi(getReq.Screenheight)
	if err != nil {
		height = 0
	}
	screenSize := &Size{
		Width:  proto.Uint32(uint32(width)),
		Height: proto.Uint32(uint32(height)),
	}
	postData.Device.ScreenSize = screenSize
	// appversion
	var n [3]int
	appversion := strings.Split(getReq.Appversion, ".")
	for index, value := range appversion {
		if index < 3 {
			n[index], _ = strconv.Atoi(value)
		}
	}
	appVersion := &Version{
		Major: proto.Uint32(uint32(n[0])),
		Minor: proto.Uint32(uint32(n[1])),
		Micro: proto.Uint32(uint32(n[2])),
	}
	postData.App.AppVersion = appVersion
	// fmt.Printf("%+v", postData)
	// fmt.Printf("%+v", postData.Device.ScreenSize)
	// osversion
	var m [3]int
	osversion := strings.Split(getReq.Osversion, ".")
	for index, value := range osversion {
		if index < 3 {
			m[index], _ = strconv.Atoi(value)
		}
	}
	osVersion := &Version{
		Major: proto.Uint32(uint32(m[0])),
		Minor: proto.Uint32(uint32(m[1])),
		Micro: proto.Uint32(uint32(m[2])),
	}
	postData.Device.OsVersion = osVersion
	// 网络类型
	// operator := postData.Network.OperatorType
	switch getReq.Imsi {
	case "1":
		postData.Network.OperatorType = Network_CHINA_MOBILE.Enum()
	case "2":
		postData.Network.OperatorType = Network_CHINA_UNICOM.Enum()
	case "3":
		postData.Network.OperatorType = Network_CHINA_TELECOM.Enum()
	}
	// conn := postData.Network.ConnectionType
	switch getReq.Network {
	case "以太网":
		postData.Network.ConnectionType = Network_ETHERNET.Enum()
	case "2g":
		postData.Network.ConnectionType = Network_CELL_2G.Enum()
	case "3g":
		postData.Network.ConnectionType = Network_CELL_3G.Enum()
	case "4g":
		postData.Network.ConnectionType = Network_CELL_4G.Enum()
	default:
		postData.Network.ConnectionType = Network_NEW_TYPE.Enum()
	}

	if getReq.Os == "2" {
		postData.App.AppPackage = proto.String("cn.ecook.ecook")
		postData.Device.OsType = Device_IOS.Enum()
	}
	ma, err := proto.Marshal(&postData)
	if err != nil {
		mongo.LogTimeout(funcName, getReq.Os)
		return util.ResMsg{}
	}

	// 请求广告
	// client := &http.Client{Timeout: util.Timeout}
	req, err := http.NewRequest("POST", URL, bytes.NewBuffer(ma))
	if err != nil {
		mongo.LogTimeout(funcName, getReq.Os)
		return util.ResMsg{}
		// log.Fatal("NewRequest error:", err)
	}
	// 请求头设置
	req.Header.Set("Connection", "keep-alive")
	req.Header.Set("Content-Type", "x-protobuf;charset=utf-8")
	resp, err := util.Client.Do(req)
	mongo.LogReq(funcName, getReq.Os)
	if err != nil {
		mongo.LogTimeout(funcName, getReq.Os)
		return util.ResMsg{}
	}
	data, err := ioutil.ReadAll(resp.Body)
	resp.Body.Close()
	if resp.StatusCode != 200 {
		mongo.LogNo(funcName, getReq.Os)
		return util.ResMsg{}
	}
	if err != nil {
		// log.Fatal("ReadResponse error:", err)
		mongo.LogNo(funcName, getReq.Os)
		return util.ResMsg{}
	}
	// res := &MobadsResponse{}
	// proto.Unmarshal(data, res)
	// fmt.Printf("res:%+v", res)
	return FormateRes(data, getReq.Os, funcName)
}

func FormateRes(res []byte, os, funcName string) util.ResMsg {
	// re := regexp.MustCompile(`\$\{[^\}]*\}`)
	resData := MobadsResponse{}
	err := proto.Unmarshal(res, &resData)
	if err != nil {
		mongo.LogNo(funcName, os)
		return util.ResMsg{}
	}
	// fmt.Println(&resData)
	// fmt.Println(*resData.ErrorCode)
	// if *resData.ErrorCode != 0 {
	// 	mongo.LogNo(funcName, os)
	// 	return util.ResMsg{}
	// }
	if len(resData.Ads) == 0 {
		mongo.LogNo(funcName, os)
		util.HttpRequestCount.WithLabelValues(funcName + "noerr").Inc()
		return util.ResMsg{}
	}
	if len(resData.Ads[0].MetaGroup) == 0 {
		mongo.LogNo(funcName, os)
		util.HttpRequestCount.WithLabelValues(funcName + "noerr").Inc()
		return util.ResMsg{}
	}
	adTracking := resData.Ads[0].AdTracking
	info := resData.Ads[0].MetaGroup[0]
	postData := util.ResMsg{
		Id:       "0",
		Weight:   0,
		State:    1,
		Title:    string(info.Title),
		Content:  "",
		ImageUrl: "",
		Uri:      "", //re.ReplaceAllString(strings.Replace(*info.ClickUrl, "${ACCT_TYPE}", "1", -1), "-999"),
	}
	if len(info.Description) > 0 {
		postData.Content = string(info.Description[0])
	}
	// fmt.Println(info.ImageSrc)
	// fmt.Println(len(info.ImageSrc))
	if len(info.ImageSrc) > 0 && info.ImageSrc[0] != "" {
		postData.ImageUrl = info.ImageSrc[0]
	} else {
		mongo.LogNo(funcName, os)
		util.HttpRequestCount.WithLabelValues(funcName + "noerr").Inc()
		return util.ResMsg{}
	}

	if *info.ClickUrl == "" {
		mongo.LogNo(funcName, os)
		util.HttpRequestCount.WithLabelValues(funcName + "noerr").Inc()
		return util.ResMsg{}
	}
	uri := *info.ClickUrl
	uri = strings.Replace(uri, "${DOWN_X}", "IT_CLK_PNT_DOWN_X", -1)
	uri = strings.Replace(uri, "${DOWN_Y}", "IT_CLK_PNT_DOWN_Y", -1)
	uri = strings.Replace(uri, "${UP_X}", "IT_CLK_PNT_UP_X", -1)
	uri = strings.Replace(uri, "${UP_Y}", "IT_CLK_PNT_UP_Y", -1)
	postData.Uri = uri
	postData.Displayreport = info.WinNoticeUrl
	postData.Displayreport = append(postData.Displayreport, "http://120.26.247.23/ad/report.php?method="+funcName+"&event=displayreport&os="+os+"&t="+util.GetRandom())
	var clickurl []string
	for _, track := range adTracking {
		if track.TrackingEvent.String() == "AD_CLICK" {
			clickurl = track.TrackingUrl
		}
	}
	// fmt.Printf("%s", re)
	for _, trackurl := range clickurl {
		trackurl = strings.Replace(trackurl, "${DOWN_X}", "IT_CLK_PNT_DOWN_X", -1)
		trackurl = strings.Replace(trackurl, "${DOWN_Y}", "IT_CLK_PNT_DOWN_Y", -1)
		trackurl = strings.Replace(trackurl, "${UP_X}", "IT_CLK_PNT_UP_X", -1)
		trackurl = strings.Replace(trackurl, "${UP_Y}", "IT_CLK_PNT_UP_Y", -1)
		postData.Clickreport = append(postData.Clickreport, trackurl)
	}
	postData.Clickreport = append(postData.Clickreport, "http://120.26.247.23/ad/report.php?method="+funcName+"&event=clickreport&os="+os+"&t="+util.GetRandom())
	// data, err := json.Marshal(&postData)
	// if err != nil || len(data) == 0 {
	// 	// log.Fatal(err)
	// 	mongo.LogNo(funcName, os)
	// 	return nil
	// }
	// util.HttpRequestCount.WithLabelValues(funcName + "yes").Inc()
	return postData
}
