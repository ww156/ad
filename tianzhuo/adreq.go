package tianzhuo

type adreq struct {
	Posid     string `json:"posid"`
	Postype   string `json:"postype"`
	Posw      string `json:"posw"`
	Posh      string `json:"posh"`
	Appsmsg   app    `json:"appmsg"`
	Devicemsg device `json:"devicemsg"`
}

type app struct {
	Appid       string `json:"appid"`
	Appname     string `json:"appname"`
	Packagename string `json:"packagename"`
}
type device struct {
	Imsi           string `json:"imsi"`
	Imei           string `json:"imei"`
	Mac            string `json:"mac"`
	Ip             string `json:"ip"`
	Mobile         string `json:"mobile"`
	Ua             string `json:"ua"`
	Screenwidth    string `json:"screenwidth"`
	Screenheight   string `json:"screenheight"`
	Androidversion string `json:"androidversion"`
	Brand          string `json:"brand"`
	Model          string `json:"model"`
	Density        string `json:"density"`
}
