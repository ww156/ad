package tianzhuo

import (
	"bytes"
	// "encoding/json"
	// "fmt"
	"io/ioutil"
	// "log"
	"net/http"
	// "net/url"
	"strconv"
	// "strings"
	"math/rand"
	// "time"
	// "unsafe"
	// "regexp"

	"ad/mongo"
	"ad/util"
	"github.com/pquerna/ffjson/ffjson"
)

const (
	URL             = "http://www.tz-dsp.com/api/app/index.jsp"
	APPID           = "30100002"
	ADID            = "10100004"
	ADID_FLOW       = "10100006"
	ADID_STARTUP    = "10100005"
	ADID_STORY_FLOW = "10100007"
)

func Tianzhuo(getReq *util.ReqMsg, funcName string) util.ResMsg {
	return base(getReq, funcName, "2", "640", "100", ADID)
}

func TianzhuoFlow(getReq *util.ReqMsg, funcName string) util.ResMsg {
	return base(getReq, funcName, "1", "640", "360", ADID_FLOW)
}

func TianzhuoStartup(getReq *util.ReqMsg, funcName string) util.ResMsg {
	return base(getReq, funcName, "3", "640", "960", ADID_STARTUP)
}
func TianzhuoStoryFlow(getReq *util.ReqMsg, funcName string) util.ResMsg {
	return base(getReq, funcName, "3", "232", "132", ADID_STORY_FLOW)
}

func base(getReq *util.ReqMsg, funcName, adtype, w, h, adid string) util.ResMsg {
	if getReq.Os == "2" {
		return util.ResMsg{}
	}
	// 请求参数
	imsi := ""
	switch getReq.Imsi {
	case "1":
		getReq.Imsi = "1"
		imsi = "46000" + strconv.Itoa(10000+rand.Intn(90000)) + strconv.Itoa(10000+rand.Intn(90000))
	case "2":
		getReq.Imsi = "2"
		imsi = "46001" + strconv.Itoa(10000+rand.Intn(90000)) + strconv.Itoa(10000+rand.Intn(90000))
	case "3":
		getReq.Imsi = "3"
		imsi = "46002" + strconv.Itoa(10000+rand.Intn(90000)) + strconv.Itoa(10000+rand.Intn(90000))
	default:
		getReq.Imsi = "0"
	}

	postData := adreq{
		Posid:   adid,
		Postype: adtype,
		Posw:    w,
		Posh:    h,
		Appsmsg: app{
			Appid:       APPID,
			Appname:     "网上厨房",
			Packagename: "cn.ecook",
		},
		Devicemsg: device{
			Imsi:           imsi,
			Imei:           getReq.Imei,
			Mac:            getReq.Mac,
			Ip:             getReq.Ip,
			Mobile:         getReq.Imsi,
			Ua:             getReq.Ua,
			Screenheight:   getReq.Screenheight,
			Screenwidth:    getReq.Screenwidth,
			Androidversion: getReq.Osversion,
			Brand:          getReq.Vendor,
			Model:          getReq.Model,
			Density:        getReq.Sd,
		},
	}

	ma, err := ffjson.Marshal(postData)
	if err != nil {
		// fmt.Println(err.Error())
		return util.ResMsg{}
	}
	req, err := http.NewRequest("POST", URL, bytes.NewReader(ma))
	if err != nil {
		// fmt.Println(err.Error())
		return util.ResMsg{}
	}
	req.Header.Set("Content-Type", "application/json;charset=utf-8")
	req.Header.Set("Connection", "keep-alive")
	resp, err := util.Client.Do(req)
	mongo.LogReq(funcName, getReq.Os)
	if err != nil {
		// fmt.Println(err.Error())
		mongo.LogTimeout(funcName, getReq.Os)
		return util.ResMsg{}
	}
	data, err := ioutil.ReadAll(resp.Body)
	resp.Body.Close()
	if err != nil {
		mongo.LogNo(funcName, getReq.Os)
		return util.ResMsg{}
	}
	// fmt.Println(resp.StatusCode)
	if resp.StatusCode != 200 {
		mongo.LogTimeout(funcName, getReq.Os)
		return util.ResMsg{}
	}
	return FormateRes(data, getReq.Os, funcName)
}

func FormateRes(res []byte, os, funcName string) util.ResMsg {
	resData := &adres{}
	ffjson.Unmarshal(res, resData)
	// fmt.Println(string(res))
	if resData.Code != "200" {
		mongo.LogNo(funcName, os)
		return util.ResMsg{}
	}
	ad := resData.Data
	if ad.Imageurl == "" {
		mongo.LogNo(funcName, os)
		return util.ResMsg{}
	}
	postData := util.ResMsg{
		Id:       "0",
		Weight:   0,
		State:    0,
		Title:    ad.Adname,
		Content:  ad.Adremark,
		ImageUrl: ad.Imageurl,
		Uri:      ad.Loadpage,
	}

	postData.Displayreport = ad.Traceshow
	postData.Displayreport = append(postData.Displayreport, "http://120.26.247.23/ad/report.php?method="+funcName+"&event=displayreport&os="+os+"&t="+util.GetRandom())
	postData.Clickreport = ad.Traceclick
	postData.Clickreport = append(postData.Clickreport, "http://120.26.247.23/ad/report.php?method="+funcName+"&event=clickreport&os="+os+"&t="+util.GetRandom())

	return postData
}
