package tianzhuo

type adres struct {
	Code string
	Msg  string
	Data ad
}
type ad struct {
	Adname             string
	Adremark           string
	Adpackagename      string
	Adtype             string
	Imageurl           string
	Iconurl            string
	Apkurl             string
	Linkurl            string
	Traceshow          []string
	Traceclick         []string
	Tracebegindownload []string
	Traceenddownload   []string
	Traceinstall       []string
	Loadpage           string
}
