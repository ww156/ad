package addata

type Adres struct {
	Apid      string   `json:"apid"`
	Sourceurl string   `json:"sourceurl"`
	Action    int32    `json:"action"`
	Iurl      []string `json:"iurl"`
	Curl      []string `json:"curl"`
	Ldp       string   `json:"ldp"`
	Adm       string   `json:"adm"`
}
