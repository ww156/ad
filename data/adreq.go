package addata

type Site struct {
	Name string `json:"name"`
	Page string `json:"page"`
	Ref  string `json:"ref"`
}

type App struct {
	Name     string `json:"name"`
	Bundle   string `json:"bundle"`
	Keywords string `json:"keywords"`
}

type Geo struct {
	Lat     float32 `json:"lat"`
	Lon     float32 `json:"lon"`
	Prov    string  `josn:"prov"`
	City    string  `josn:"city"`
	Country string  `json:"country"`
}

type Device struct {
	Devicetype     int32  `json:"devicetype"`
	Os             int32  `json:"os"`
	Osv            string `json:"osv"`
	Openudid       string `json:"openudid"`
	Idfa           string `json:"idfa"`
	Androidid      string `josn:"androidid"`
	Imei           string `json:"imei"`
	Mac            string `json:"mac"`
	Ip             string `json:"ip"`
	Ua             string `json:"ua"`
	W              int32  `json:"w"`
	H              int32  `josn:"h"`
	Make           string `josn:"make"`
	Model          string `josn:"model"`
	Carrier        int32  `json:"carrier"`
	Connectiontype int32  `json:"connectiontype"`
	Ts             int32  `json:"ts"`
	Geo            Geo    `json:"geo"`
}

type User struct {
	Id       string `json:"id"`
	Yob      int32  `json:"yob"`
	Gender   string `json:"gender"`
	Keywords string `json:"keywords"`
}

type Adreq struct {
	Apid   string `json:"apid"`
	Site   Site   `json:"site"`
	App    App    `json:"app"`
	Device Device `json:"device"`
	User   User   `json:"user"`
}
