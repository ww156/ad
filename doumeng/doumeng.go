package doumeng

import (
	"encoding/json"
	// "fmt"
	"io/ioutil"
	// "log"
	"net/http"
	"strconv"
	// "strings"
	"bytes"
	// "time"

	"ad/mongo"
	"ad/util"
)

const (
	ANDROID_KEY = "2f52dc78dbbc843b19cf2f260b04812f"
	IOS_KEY     = "4abf74b5f69378e8b609e6f3d53ac760"
	BANNER      = "2"
	FLOW        = "3"
	STARTUP     = "6"
)

func Doumeng(getReq *util.ReqMsg, funcName string) util.ResMsg {
	return base(getReq, BANNER, funcName)
}
func DoumengFlow(getReq *util.ReqMsg, funcName string) util.ResMsg {
	return base(getReq, FLOW, funcName)
}
func DoumengStartUp(getReq *util.ReqMsg, funcName string) util.ResMsg {
	return base(getReq, STARTUP, funcName)
}

func base(getReq *util.ReqMsg, adtype, funcName string) util.ResMsg {
	// android
	reqData := Adreq{
		Ad: Ad{
			AdType: adtype,
			Ip:     getReq.Ip,
		},
		Device: Device{
			AdType:        adtype,
			AndroidID:     getReq.Androidid,
			Appkey:        ANDROID_KEY, // "a5771bce93e200c36f7cd9dfd0e5deaa",
			Brand:         getReq.Vendor,
			Model:         getReq.Model,
			DevAppPackage: "cn.ecook",
			Imei:          getReq.Imei,
			// Imsi:       getReq.Imsi,
			Language:      "zh",
			Mac:           getReq.Mac,
			MobileSystem:  "android",
			PhoneNumber:   "",
			Resolution:    getReq.Screenwidth + "*" + getReq.Screenheight,
			Version:       "3.5.0",
			SimserialNum:  "",
			SystemVersion: getReq.Osversion,
		},
	}

	device := &reqData.Device
	switch getReq.Imsi {
	case "1":
		device.Imsi = "46000"
	case "2":
		device.Imsi = "46001"
	case "3":
		device.Imsi = "46002"
	default:
		device.Imsi = "46000"
	}
	sd, err := strconv.Atoi(getReq.Sd)
	if err != nil {
		device.ScreenDensity = "3.0"
	}

	device.ScreenDensity = strconv.Itoa(sd / 160)
	// ios
	if getReq.Os == "2" {
		device.Appkey = IOS_KEY
		device.Imei = getReq.Idfa
		device.MobileSystem = "ios"
		device.DevAppPackage = "cn.ecook.ecook"
	}
	// network
	switch getReq.Network {
	case "2g":
		device.Network = "2g"
	case "3g":
		device.Network = "3g"
	case "wifi":
		device.Network = "wifi"
	default:
		device.Network = "other"
	}
	// 运营商名称
	switch getReq.Imsi {
	case "1":
		device.Operator = "00"
	case "2":
		device.Operator = "01"
	case "3":
		device.Operator = "03"
	default:
		device.Operator = "01"
	}
	// fmt.Printf("device: %+v", device)
	// 编码请求数据
	ma, err := json.Marshal(reqData)
	if err != nil {
		return util.ResMsg{}
	}
	// 请求广告
	// client := &http.Client{Timeout: util.Timeout}
	req, err := http.NewRequest("POST", "http://client.bayimob.com/terminal/adRequest/getAd.do?t="+adtype, bytes.NewBuffer(ma))
	if err != nil {
		return util.ResMsg{}
	}
	// 请求头设置
	req.Header.Set("Connection", "keep-alive")
	req.Header.Set("Content-Type", "application/json")
	resp, err := util.Client.Do(req)
	mongo.LogReq(funcName, getReq.Os)
	// 请求错误，日志记录为timeout
	if err != nil {
		mongo.LogTimeout(funcName, getReq.Os)
		// util.HttpRequestCount.WithLabelValues(funcName + "timeouterr").Inc()
		return util.ResMsg{}
	}
	data, err := ioutil.ReadAll(resp.Body)
	resp.Body.Close()
	if err != nil {
		mongo.LogNo(funcName, getReq.Os)
		// util.HttpRequestCount.WithLabelValues(funcName + "readresperr").Inc()
		return util.ResMsg{}
	}
	// 请求返回失败，日志记录为timeout
	if resp.StatusCode != 200 {
		mongo.LogTimeout(funcName, getReq.Os)
		// util.HttpRequestCount.WithLabelValues(funcName + "not200err").Inc()
		return util.ResMsg{}
	}
	return FormateRes(data, getReq.Os, funcName)
}

// 根据广告平台的返回生成客户端返回
func FormateRes(res []byte, os, funcName string) util.ResMsg {
	resData := Adres{}
	json.Unmarshal(res, &resData)
	// fmt.Printf("doumeng:%+v\n", resData)
	if resData.Rc == "100002" {
		mongo.LogNo(funcName, os)
		// util.HttpRequestCount.WithLabelValues(funcName + "noerr").Inc()
		return util.ResMsg{}
	}
	if len(resData.Ads) == 0 {
		mongo.LogNo(funcName, os)
		// util.HttpRequestCount.WithLabelValues(funcName + "noerr").Inc()
		return util.ResMsg{}
	}
	info := resData.Ads[0]
	postData := util.ResMsg{
		Id:       "0",
		Weight:   0,
		State:    1,
		Title:    info.Title,
		Content:  info.Recommend,
		ImageUrl: info.Image,
		Uri:      info.Url,
	}
	postData.Displayreport = info.ShowurlTrack
	postData.Displayreport = append(postData.Displayreport, "http://120.26.247.23/ad/report.php?method="+funcName+"&event=displayreport&os="+os+"&t="+util.GetRandom())
	postData.Clickreport = info.ClickurlTrack
	postData.Clickreport = append(postData.Clickreport, "http://120.26.247.23/ad/report.php?method="+funcName+"&event=clickreport&os="+os+"&t="+util.GetRandom())

	return postData
}
