package doumeng

type Adinfo struct {
	Adtype        string   // 2 banner ,3 插屏 ,6 全屏
	Type          string   // 交互类型 1 跳转网页 2 下载
	Image         string   // 图片地址
	Url           string   // url
	Title         string   // 广告
	Recommend     string   // 提示语
	Icon          string   // 图标 url
	CacheExpires  int      // 单条广告缓存时间
	ShowurlTrack  []string // 曝光地址要求全部提交，只提交一次
	ClickurlTrack []string // 点击回调地址要求全部提交
	InstallTrack  []string // 安装回调地址要求全部提交
}

type Adres struct {
	Rc  string //返回代码 成功 100001, 无填充 100002
	Ads []Adinfo
}
