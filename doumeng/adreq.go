package doumeng

type Ad struct {
	AdType string `json:"adType"` // 2 banner ,3 插屏 ,6 全屏
	Ip     string `json:"ip"`     // ip地址
}

type Device struct {
	AdType        string `json:"adType"`        // 2 banner ,3 插屏 ,6 全屏
	AndroidID     string `json:"androidID"`     // 安卓 id
	Appkey        string `json:"appKey"`        // appkey 由豆盟提供
	Brand         string `json:"brand"`         // 手机品牌
	Model         string `json:"model"`         // 手机型号
	DevAppPackage string `json:"devAppPackage"` // app 包名
	Imei          string `json:"imei"`          // imei 号
	Imsi          string `json:"imsi"`          // imsi 号
	Language      string `json:"language"`      // 语言
	Mac           string `json:"mac"`           // mac
	MobileSystem  string `json:"mobileSystem"`  // 手机系统
	Network       string `json:"network"`       // 网络状况 wifi 、3g、2g、other
	Operator      string `json:"operator"`      // 运营商 00 移动 01 联通 03 电信
	PhoneNumber   string `json:"phoneNumber"`   // 手机号
	Resolution    string `json:"resolution"`    // 分辨率 1080*1920
	ScreenDensity string `json:"screenDensity"` // 屏幕密度
	Version       string `json:"version"`       // 版本号
	SimserialNum  string `json:"simSerialNum"`  // sim 卡序列号
	SystemVersion string `json:"systemVersion"` // 系统版本
}

type Adreq struct {
	Ad     Ad     `json:"ad"`
	Device Device `json:"device"`
}
