package eztmob

import (
	// "encoding/json"
	// "fmt"
	"io/ioutil"
	"math/rand"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"time"

	"ad/mongo"
	"ad/util"
	"github.com/pquerna/ffjson/ffjson"
)

const (
	// URL          = "http://119.23.238.229:9000/postgg.aspx"
	// URL          = "http://119.23.238.229:9000/postggs.aspx"
	URL          = "http://119.23.238.229:9090/postggs.aspx"
	ADID         = "8DB6738425A2EC87"
	ADID_STARTUP = "BB85961C7F212385"
	UID          = "zyxk05"
	POSTTYPE     = "1"
)

// var UID = [2]string{"zyxk05", "zyxk06"}
// var POSTTYPE = [2]string{"1", "3"}

func Eztmob(getReq *util.ReqMsg, funcName string) util.ResMsg {
	return base(getReq, funcName, "2", ADID, "640", "100")
}

func EztmobStartup(getReq *util.ReqMsg, funcName string) util.ResMsg {
	return base(getReq, funcName, "4", ADID_STARTUP, "640", "960")
}

func base(getReq *util.ReqMsg, funcName, adtype, adid, w, h string) util.ResMsg {
	// return util.ResMsg{}
	if getReq.Os == "2" {
		return util.ResMsg{}
	}
	network := "unknown"
	switch getReq.Network {
	case "wifi":
		network = "1"
	case "2g":
		network = "2"
	case "3g":
		network = "3"
	case "4g":
		network = "4"
	default:
		network = "unknown"
	}
	sd, err := strconv.Atoi(getReq.Sd)
	ppi := sd / 160
	carrier := "0"
	imsi := "46000"
	switch getReq.Imsi {
	case "1":
		carrier = "1"
		imsi = "46000"
	case "2":
		carrier = "2"
		imsi = "46001"
	case "3":
		carrier = "4"
		imsi = "46002"
	}
	v := url.Values{}
	// uid := UID[rand.Intn(2)]
	// v.Set("uid", uid)
	v.Set("uid", UID)
	// posttype := POSTTYPE[rand.Intn(2)]
	// v.Set("posttype", posttype)
	v.Set("posttype", POSTTYPE)
	// fmt.Println(uid, posttype)
	v.Set("bid", adid+strconv.Itoa(int(time.Now().Unix()))+"0001")
	v.Set("adtype", adtype)
	v.Set("pkgname", "cn.ecook")
	v.Set("appname", "网上厨房")
	v.Set("appv", getReq.Appversion)
	v.Set("sdkuid", getReq.Androidid)
	v.Set("conn", network)
	v.Set("carrier", carrier)
	v.Set("os", "0")
	v.Set("osv", getReq.Osversion)
	v.Set("imei", getReq.Imei)
	v.Set("wma", strings.ToUpper(strings.Replace(getReq.Mac, ":", "", -1)))
	v.Set("aid", getReq.Androidid)
	v.Set("aaid", "")
	v.Set("idfa", getReq.Idfa)
	v.Set("oid", getReq.Openudid)
	v.Set("device", getReq.Model)
	v.Set("ua", getReq.Ua)
	v.Set("ip", getReq.Ip)
	v.Set("width", w)
	v.Set("height", h)
	v.Set("pw", getReq.Screenwidth)
	v.Set("ph", getReq.Screenheight)
	v.Set("density", strconv.Itoa(ppi))
	v.Set("lon", getReq.Lng)
	v.Set("lat", getReq.Lat)
	v.Set("imsi", imsi+strconv.Itoa(10000+rand.Intn(90000))+strconv.Itoa(10000+rand.Intn(90000)))

	if getReq.Os == "2" {
		v.Set("os", "1")
	}

	req, err := http.NewRequest("POST", URL, strings.NewReader(v.Encode()))
	if err != nil {
		return util.ResMsg{}
	}
	req.Header.Set("Connection", "keep-alive")
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	resp, err := util.Client.Do(req)
	mongo.LogReq(funcName, getReq.Os)
	// 请求错误，日志记录为timeout
	if err != nil {
		mongo.LogTimeout(funcName, getReq.Os)
		return util.ResMsg{}
	}
	data, err := ioutil.ReadAll(resp.Body)
	resp.Body.Close()
	if err != nil {
		mongo.LogNo(funcName, getReq.Os)
		return util.ResMsg{}
	}
	// 请求返回失败，日志记录为timeout
	if resp.StatusCode != 200 {
		mongo.LogTimeout(funcName, getReq.Os)
		return util.ResMsg{}
	}
	return FormateRes(data, getReq.Os, funcName)
}

func FormateRes(res []byte, os, funcName string) util.ResMsg {
	resData := adres{}
	ffjson.Unmarshal(res, &resData)
	// fmt.Printf("%+v", resData)
	// fmt.Println(string(res))
	ad := resData.Info
	if ad.Returncode != 200 {
		mongo.LogNo(funcName, os)
		return util.ResMsg{}
	}

	postData := util.ResMsg{
		Id:       "0",
		Weight:   0,
		State:    1,
		Title:    ad.Title,
		Content:  ad.Description,
		ImageUrl: ad.Imgurl,
		Uri:      ad.Clickurl,
	}
	postData.Displayreport = ad.Imgtracking
	postData.Displayreport = append(postData.Displayreport, "http://120.26.247.23/ad/report.php?method="+funcName+"&event=displayreport&os="+os+"&t="+util.GetRandom())
	postData.Clickreport = ad.Thclkurl
	postData.Clickreport = append(postData.Clickreport, "http://120.26.247.23/ad/report.php?method="+funcName+"&event=clickreport&os="+os+"&t="+util.GetRandom())

	return postData
}
