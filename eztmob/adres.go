package eztmob

type adres struct {
	Info Ad
}

// type adres map[string]Ad

type Ad struct {
	Channelid   string
	Adspaceid   string
	Returncode  int
	Adwidth     int
	Adheight    int
	Adtype      int
	Showtime    int
	Clickurl    string
	Imgtracking []string
	Systime     int
	Thclkurl    []string
	Cid         string
	Bid         string
	Imgurl      string
	Title       string
	Description string
	Deeplink    string
}
