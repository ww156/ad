package baicheng

import (
	"bytes"
	// "encoding/json"
	// "fmt"
	"io/ioutil"
	// "log"
	"net/http"
	"strconv"
	"strings"
	// "time"
	// "unsafe"

	"github.com/golang/protobuf/proto"

	"ad/mongo"
	"ad/util"
)

// 安卓：
// 包名:cn.ecook
// 网上厨房 android 开屏 640*960:
// app_key： 97469b5f834547c1890f7df86f9cea91 adspace_key： 5cda06e524744318b0380d77fd5131d7
// 网上厨房 android 一图两文 640*360:
// app_key： 97469b5f834547c1890f7df86f9cea91 adspace_key： 688e3b999baa4334a956e56d45a5ccb3
// 网上厨房 android banner 640*100:
// app_key： 97469b5f834547c1890f7df86f9cea91 adspace_key： 2f004bf96e6f46bb8a6e17f60b2c327c

// ios：
// 包名:cn.ecook
// 网上厨房 ios 开屏 640*960:
// app_key： 6dc2756b450d4eb5921fb063028d382c adspace_key： 65ef7679501749dcabee0f341eddb131
// 网上厨房 ios 一图两文 640*360:
// app_key： 6dc2756b450d4eb5921fb063028d382c adspace_key： a6d792a0723e469a9d1073f47aa51d38
// 网上厨房 ios banner 640*100:
// app_key： 6dc2756b450d4eb5921fb063028d382c adspace_key： c6000e1172de43c8b3439f6f723d4f3b
const (
	URL                        = "http://180.97.75.192:16800/madx"
	PRO_URL                    = "http://adx.bcadx.com:6011/madx"
	APPKEY_ANDROID             = "97469b5f834547c1890f7df86f9cea91"
	ADSPACEKEY_ANDROID_STARTUP = "5cda06e524744318b0380d77fd5131d7"
	ADSPACEKEY_ANDROID_FLOW    = "688e3b999baa4334a956e56d45a5ccb3"
	ADSPACEKEY_ANDROID         = "2f004bf96e6f46bb8a6e17f60b2c327c"
	APPKEY_IOS                 = "6dc2756b450d4eb5921fb063028d382c"
	ADSPACEKEY_IOS_STARTUP     = "65ef7679501749dcabee0f341eddb131"
	ADSPACEKEY_IOS_FLOW        = "a6d792a0723e469a9d1073f47aa51d38"
	ADSPACEKEY_IOS             = "c6000e1172de43c8b3439f6f723d4f3b"
)

func Baicheng(getReq *util.ReqMsg, funcName string) util.ResMsg {
	if getReq.Os == "1" {
		return base(getReq, funcName, ADSPACEKEY_ANDROID)
	}
	return base(getReq, funcName, ADSPACEKEY_IOS)
}

func BaichengFlow(getReq *util.ReqMsg, funcName string) util.ResMsg {
	if getReq.Os == "1" {
		return base(getReq, funcName, ADSPACEKEY_ANDROID_FLOW)
	}
	return base(getReq, funcName, ADSPACEKEY_IOS_FLOW)
}

func BaichengStartup(getReq *util.ReqMsg, funcName string) util.ResMsg {
	if getReq.Os == "1" {
		return base(getReq, funcName, ADSPACEKEY_ANDROID_STARTUP)
	}
	return base(getReq, funcName, ADSPACEKEY_IOS_STARTUP)
}

func base(getReq *util.ReqMsg, funcName, adkey string) util.ResMsg {
	postData := MobadsRequest{
		Adspacekey: proto.String(adkey),
		SdkVersion: &Version{
			Major: proto.Uint32(1),
			Minor: proto.Uint32(0),
			Micro: proto.Uint32(0),
		},
		App: &App{
			AppVersion: &Version{
				Major: proto.Uint32(1),
				Minor: proto.Uint32(1),
				Micro: proto.Uint32(1),
			},
			AppKey:         proto.String(APPKEY_ANDROID),
			AppPackageName: proto.String("cn.ecook"),
		},
		Device: &Device{
			DeviceType: Device_PHONE.Enum(),
			OsType:     Device_ANDROID.Enum(),
			OsVersion: &Version{
				Major: proto.Uint32(1),
				Minor: proto.Uint32(1),
				Micro: proto.Uint32(1),
			},
			Vendor: []byte(getReq.Vendor),
			Model:  []byte(getReq.Model),
			Udid: &UdId{
				Idfa:      proto.String(getReq.Idfa),
				Imei:      proto.String(getReq.Imei),
				Mac:       proto.String(getReq.Mac),
				ImeiMd5:   proto.String(util.Md5(getReq.Imei)),
				AndroidId: proto.String(getReq.Androidid),
			},
			ScreenSize: &Size{},
		},
		Network:  &Network{},
		ClientIp: proto.String(getReq.Ip),
		// ClientIp:       proto.String("116.228.41.194"),
		DbName:         proto.String("bc"),
		NoDefaultAds:   proto.Int32(1),
		FeedsSdkRender: proto.Int32(1),
	}
	if getReq.Os == "2" {
		postData.App.AppPackageName = proto.String("cn.ecook.ecook")
		postData.App.AppKey = proto.String(APPKEY_IOS)
		postData.Device.OsType = Device_IOS.Enum()
	}
	// screen_size
	screenw, err := strconv.Atoi(getReq.Screenwidth)
	if err != nil {
		screenw = 0
	}
	screenh, err := strconv.Atoi(getReq.Screenheight)
	if err != nil {
		screenh = 0
	}
	postData.Device.ScreenSize = &Size{
		Width:  proto.Uint32(uint32(screenw)),
		Height: proto.Uint32(uint32(screenh)),
	}
	var m [3]uint32
	appversion := strings.Split(getReq.Appversion, ".")
	for index, value := range appversion {
		if index < 3 {
			v, err := strconv.Atoi(value)
			if err == nil {
				m[index] = uint32(v)
			}
		}
	}
	postData.App.AppVersion = &Version{
		Major: &m[0],
		Minor: &m[1],
		Micro: &m[2],
	}

	var n [3]uint32
	osversion := strings.Split(getReq.Osversion, ".")
	for index, value := range osversion {
		if index < 3 {
			v, err := strconv.Atoi(value)
			if err == nil {
				n[index] = uint32(v)
			}
		}
	}
	postData.Device.OsVersion = &Version{
		Major: &n[0],
		Minor: &n[1],
		Micro: &n[2],
	}

	network := postData.Network
	switch getReq.Network {
	case "以太网":
		network.ConnectionType = Network_ETHERNET.Enum()
	case "wifi":
		network.ConnectionType = Network_WIFI.Enum()
	case "2g":
		network.ConnectionType = Network_CELL_2G.Enum()
	case "3g":
		network.ConnectionType = Network_CELL_3G.Enum()
	case "4g":
		network.ConnectionType = Network_CELL_4G.Enum()
	default:
		network.ConnectionType = Network_CONNCTION_UNKNOWN.Enum()
	}
	// 运营商
	switch getReq.Imsi {
	case "1":
		network.OperatorType = Network_CHINA_MOBILE.Enum()
	case "2":
		network.OperatorType = Network_CHINA_UNICOM.Enum()
	case "3":
		network.OperatorType = Network_CHINA_TELECOM.Enum()
	default:
		network.OperatorType = Network_UNKNOWN_OPERATOR.Enum()
	}

	ma, err := proto.Marshal(&postData)
	if err != nil {
		return util.ResMsg{}
	}

	req, err := http.NewRequest("POST", PRO_URL, bytes.NewReader(ma))
	if err != nil {
		return util.ResMsg{}
	}
	req.Header.Set("Connection", "keep-alive")
	req.Header.Set("Content-Type", "octet-stream")
	resp, err := util.Client.Do(req)
	mongo.LogReq(funcName, getReq.Os)
	if err != nil {
		mongo.LogTimeout(funcName, getReq.Os)
		util.HttpRequestCount.WithLabelValues(funcName + "timeouterr").Inc()
		return util.ResMsg{}
	}
	data, err := ioutil.ReadAll(resp.Body)
	resp.Body.Close()
	if err != nil {
		mongo.LogNo(funcName, getReq.Os)
		util.HttpRequestCount.WithLabelValues(funcName + "readresperr").Inc()
		return util.ResMsg{}
	}
	if resp.StatusCode != 200 {
		mongo.LogTimeout(funcName, getReq.Os)
		util.HttpRequestCount.WithLabelValues(funcName + "not200err").Inc()
		return util.ResMsg{}
	}
	return FormateRes(data, getReq.Os, funcName)
}

func FormateRes(res []byte, os, funcName string) util.ResMsg {
	resData := &MobadsResponse{}
	proto.Unmarshal(res, resData)
	// fmt.Printf("%+v", resData)
	if *resData.ErrorCode != 0 {
		mongo.LogNo(funcName, os)
		util.HttpRequestCount.WithLabelValues(funcName + "noerr").Inc()
		return util.ResMsg{}
	}
	if len(resData.Ads) == 0 {
		mongo.LogNo(funcName, os)
		util.HttpRequestCount.WithLabelValues(funcName + "noerr").Inc()
		return util.ResMsg{}
	}
	ad := resData.Ads[0].CreativeInfo
	postData := util.ResMsg{
		Id:       "0",
		Weight:   0,
		State:    0,
		Title:    *ad.Title,
		Content:  *ad.AdText,
		ImageUrl: ad.SourceUrls[0],
		Uri:      *ad.ClickUrl,
	}
	postData.Displayreport = ad.ImpressionUrl
	postData.Displayreport = append(postData.Displayreport, "http://120.26.247.23/ad/report.php?method="+funcName+"&event=displayreport&os="+os+"&t="+util.GetRandom())
	postData.Clickreport = ad.ClickTrackUrl
	postData.Clickreport = append(postData.Clickreport, "http://120.26.247.23/ad/report.php?method="+funcName+"&event=clickreport&os="+os+"&t="+util.GetRandom())

	return postData
}
