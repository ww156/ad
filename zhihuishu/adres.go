package zhihuishu

type adres struct {
	Code  int
	Msg   string
	Pvid  string
	Count int
	Ads   []ad
}

type ad struct {
	Action         int
	Apn            string
	Aps            float64
	Logourl        string
	Title          string
	Desc           string
	Src            string
	Icon           string
	Link           string
	Clickreport    []string
	Displayreport  []displayreport
	Trackingevents []tracking
}
type displayreport struct {
	Reporttime int
	Reporturl  string
}
type tracking struct {
	Eventtype string
	Tracking  []string
}
