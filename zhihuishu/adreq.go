package zhihuishu

type adreq struct {
	Ver            string `json:"ver"`
	Appid          string `json:"appid"`
	Lid            string `json:"lid"`
	Os             string `json:"os"`
	Osversion      string `json:"osversion"`
	Appversion     string `json:"appversion"`
	Androidid      string `json:"android"`
	Imei           string `json:"imei"`
	Mac            string `json:"mac"`
	Idfv           string `json:"idfv"`
	Idfa           string `json:"idfa"`
	Udid           string `json:"udid"`
	Idv            string `json:"idv"`
	Appname        string `json:"appname"`
	Apppackagename string `json:"apppackagename"`
	Imsi           string `json:"imsi"`
	Ip             string `json:"ip"`
	Ua             string `json:"ua"`
	Network        string `json:"network"`
	Time           string `json:"time"`
	Screenwidth    string `json:"screenwidth"`
	Screenheight   string `json:"screenheight"`
	Width          string `json:"width"`
	Height         string `json:"height"`
	Manufacturer   string `json:"manufacturer"`
	Brand          string `json:"brand"`
	Model          string `json:"model"`
	Language       string `json:"language"`
	Wifissid       string `json:"wifissid"`
	Token          string `json:"token"`
	IsGP           int    `json:"isGP"`
}
