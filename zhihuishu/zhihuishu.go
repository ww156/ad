package zhihuishu

import (
	// "bytes"
	"encoding/json"
	// "fmt"
	"io/ioutil"
	// "log"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"time"
	// "unsafe"
	// "regexp"

	"ad/mongo"
	"ad/util"
)

// 网上厨房_IOS_TZL
// AppId：4DFBB5D3C35544D8ACCAA9F55ABC2670
// APIKey：CFE26BB2849141EC0B696F0A938E54A0

// 网上厨房_IOS_开屏_TZL
// lid:A95CC1BADB2444CFB15A16116D9C2348
// 网上厨房_IOS_信息流_TZL
// lid：0F08C67D9F1B411A832F420A67C84C3D
// 网上厨房_IOS_banner_TZL
// lid：4A1F2B2C55674DE1B79D4155A84830C7

// 网上厨房_安卓_TZL
// AppId：559B3CEF2CAA455FA9BCC83452A98D3F
// APIKey：48C2FCDCF01806097A688B5053CDBBE9

// 网上厨房_安卓_开屏_TZL
// lid:D2072DF53DF64E3391ACF5C6A646FC36
// 网上厨房_安卓_信息流_TZL
// lid:A553C53A4C184DE6976F7B48348681B3
// 网上厨房_安卓_banner_TZL
// lid：E92360AD0CDA4AFBACEF21B3E2B913AA

const (
	URL = "https://api.snmi.cn/v10/getad"

	APPID_IOS       = "4DFBB5D3C35544D8ACCAA9F55ABC2670"
	KEY_IOS         = "CFE26BB2849141EC0B696F0A938E54A0"
	LID_IOS         = "4A1F2B2C55674DE1B79D4155A84830C7"
	LID_IOS_FLOW    = "0F08C67D9F1B411A832F420A67C84C3D"
	LID_IOS_STARTUP = "A95CC1BADB2444CFB15A16116D9C2348"

	APPID_ANDROID       = "559B3CEF2CAA455FA9BCC83452A98D3F"
	KEY_ANDROID         = "48C2FCDCF01806097A688B5053CDBBE9"
	LID_ANDROID         = "E92360AD0CDA4AFBACEF21B3E2B913AA"
	LID_ANDROID_FLOW    = "A553C53A4C184DE6976F7B48348681B3"
	LID_ANDROID_STARTUP = "D2072DF53DF64E3391ACF5C6A646FC36"
)

func Zhihuishu(getReq *util.ReqMsg, funcName string) util.ResMsg {
	if getReq.Os == "1" {
		return base(getReq, funcName, APPID_ANDROID, KEY_ANDROID, LID_ANDROID, "640", "100")
	}
	return base(getReq, funcName, APPID_IOS, KEY_IOS, LID_IOS, "640", "100")
}

func ZhihuishuFlow(getReq *util.ReqMsg, funcName string) util.ResMsg {
	if getReq.Os == "1" {
		return base(getReq, funcName, APPID_ANDROID, KEY_ANDROID, LID_ANDROID_FLOW, "640", "360")
	}
	return base(getReq, funcName, APPID_IOS, KEY_IOS, LID_IOS_FLOW, "640", "360")
}

func ZhihuishuStartup(getReq *util.ReqMsg, funcName string) util.ResMsg {
	if getReq.Os == "1" {
		return base(getReq, funcName, APPID_ANDROID, KEY_ANDROID, LID_ANDROID_STARTUP, "640", "960")
	}
	return base(getReq, funcName, APPID_IOS, KEY_IOS, LID_IOS_STARTUP, "640", "960")
}

func base(getReq *util.ReqMsg, funcName, appid, key, lid, w, h string) util.ResMsg {
	carrier := "46000"
	switch getReq.Imsi {
	case "1":
		carrier = "46000"
	case "2":
		carrier = "46001"
	case "3":
		carrier = "46003"
	default:
		carrier = "unknown"
	}
	ts := strconv.Itoa(int(time.Now().UnixNano() / 1e6))
	postData := adreq{
		Ver:            "1.7.4",
		Appid:          appid,
		Lid:            lid,
		Os:             getReq.Os,
		Osversion:      getReq.Osversion,
		Appversion:     getReq.Appversion,
		Androidid:      getReq.Androidid,
		Imei:           getReq.Imei,
		Mac:            getReq.Mac,
		Idfa:           getReq.Idfa,
		Udid:           getReq.Openudid,
		Appname:        "网上厨房",
		Apppackagename: "cn.ecook",
		Imsi:           carrier,
		Ip:             getReq.Ip,
		Ua:             getReq.Ua,
		Network:        getReq.Network,
		Time:           ts,
		Screenwidth:    getReq.Screenwidth,
		Screenheight:   getReq.Screenheight,
		Width:          w,
		Height:         h,
		Manufacturer:   getReq.Vendor,
		Brand:          getReq.Vendor,
		Model:          getReq.Model,
		Language:       "zh-CN",
		Token:          util.Md5(appid + key + lid + ts),
		IsGP:           0,
	}
	if getReq.Os == "2" {
		postData.Apppackagename = "cn.ecook.ecook"
	}
	ma, err := json.Marshal(&postData)
	if err != nil {
		return util.ResMsg{}
	}
	v := url.Values{}
	v.Set("data", string(ma))
	// 请求广告
	req, err := http.NewRequest("POST", URL, strings.NewReader(v.Encode()))
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Set("Connection", "keep-alive")
	if err != nil {
		return util.ResMsg{}
	}
	resp, err := util.Client.Do(req)
	mongo.LogReq(funcName, getReq.Os)
	if err != nil {
		mongo.LogTimeout(funcName, getReq.Os)
		util.HttpRequestCount.WithLabelValues(funcName + "timeouterr").Inc()
		return util.ResMsg{}
	}
	data, err := ioutil.ReadAll(resp.Body)
	resp.Body.Close()
	if err != nil {
		mongo.LogNo(funcName, getReq.Os)
		return util.ResMsg{}
	}
	if resp.StatusCode != 200 {
		mongo.LogTimeout(funcName, getReq.Os)
		return util.ResMsg{}
	}

	return FormateRes(data, getReq.Os, funcName)
}

// 根据广告平台的返回生成客户端返回
func FormateRes(res []byte, os, funcName string) util.ResMsg {
	resData := adres{}
	json.Unmarshal(res, &resData)
	// fmt.Printf("%+v", resData)
	// fmt.Println(string(res))
	if resData.Code != 0 {
		mongo.LogNo(funcName, os)
		util.HttpRequestCount.WithLabelValues(funcName + "noerr").Inc()
		return util.ResMsg{}
	}
	if len(resData.Ads) == 0 {
		mongo.LogNo(funcName, os)
		util.HttpRequestCount.WithLabelValues(funcName + "noerr").Inc()
		return util.ResMsg{}
	}

	ad := resData.Ads[0]
	if ad.Src == "" {
		if ad.Icon == "" {
			mongo.LogNo(funcName, os)
			util.HttpRequestCount.WithLabelValues(funcName + "noerr").Inc()
			return util.ResMsg{}
		} else {
			ad.Src = ad.Icon
		}
	}
	postData := util.ResMsg{
		Id:       "0",
		Weight:   0,
		State:    0,
		Title:    ad.Title,
		Content:  ad.Desc,
		ImageUrl: ad.Src,
		Uri:      ad.Link,
	}
	for _, v := range ad.Displayreport {
		if v.Reporturl != "" {
			postData.Displayreport = append(postData.Displayreport, v.Reporturl)
		}
	}
	postData.Displayreport = append(postData.Displayreport, "http://120.26.247.23/ad/report.php?method="+funcName+"&event=displayreport&os="+os+"&t="+util.GetRandom())
	postData.Clickreport = ad.Clickreport
	postData.Clickreport = append(postData.Clickreport, "http://120.26.247.23/ad/report.php?method="+funcName+"&event=clickreport&os="+os+"&t="+util.GetRandom())

	return postData
}
