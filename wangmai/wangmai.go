package wangmai

import (
	// "bytes"
	// "encoding/json"
	"fmt"
	"io/ioutil"
	// "log"
	"net/http"
	// "strconv"
	"strings"
	// "time"
	// "unsafe"
	// "regexp"

	"ad/mongo"
	"ad/util"
	// "github.com/PuerkitoBio/goquery"
	"github.com/pquerna/ffjson/ffjson"
)

// key:b745a026e4da3b7f
// token-IOS                skpwvpaaip
// 网上厨房-IOS-开屏        代码位ID：50072
// 网上厨房-IOS-信息流      代码位ID：50071
// 网上厨房-IOS-横幅        代码位ID：50070

// token-安卓               arwa9ucfjk
// 网上厨房-安卓-信息流     代码位ID：50069
// 网上厨房-安卓-开屏       代码位ID：50068
// 网上厨房-安卓-横幅       代码位ID：50067

const (
	// URL = "http://debug.mssp.adunite.com/"
	// KEY              = "9a2061117f975a56"
	// APPTOKEN_ANDROID = "9nc45mowpz"
	// ADID_ANDROID     = "10110080"
	// APPTOKEN_IOS     = "ctcqf6be2a"
	// ADID_IOS         = "10110081"

	URL                  = "http://api.mssp.adwangmai.com/v1.api"
	KEY                  = "b745a026e4da3b7f"
	APPTOKEN_ANDROID     = "arwa9ucfjk"
	ADID_BANNER_ANDROID  = "50067"
	ADID_FLOW_ANDROID    = "50069"
	ADID_STARTUP_ANDROID = "50068"
	APPTOKEN_IOS         = "skpwvpaaip"
	ADID_BANNER_IOS      = "50070"
	ADID_FLOW_IOS        = "50071"
	ADID_STARTUP_IOS     = "50072"
)

// banner  是  640*270
func Wangmai(getReq *util.ReqMsg, funcName string) util.ResMsg {
	if getReq.Os == "1" {
		return base(getReq, funcName, "640", "720", ADID_BANNER_ANDROID)
	}
	return base(getReq, funcName, "640", "720", ADID_BANNER_IOS)
	// return []byte("")
}

func WangmaiFlow(getReq *util.ReqMsg, funcName string) util.ResMsg {
	if getReq.Os == "1" {
		return base(getReq, funcName, "640", "100", ADID_FLOW_ANDROID)
	}
	return base(getReq, funcName, "640", "100", ADID_FLOW_IOS)
	// return []byte("")
}

func WangmaiStartup(getReq *util.ReqMsg, funcName string) util.ResMsg {
	if getReq.Os == "1" {
		return base(getReq, funcName, "640", "960", ADID_STARTUP_ANDROID)
	}
	return base(getReq, funcName, "640", "960", ADID_STARTUP_IOS)
	// return []byte("")
}

func base(getReq *util.ReqMsg, funcName, w, h, adid string) util.ResMsg {
	postData := adreq{
		Sign:     util.Md5(KEY + APPTOKEN_ANDROID),
		Apptoken: APPTOKEN_ANDROID,
		Data: &Data{
			App: &App{
				App_version: &Version{
					Major: "1",
					Minor: "1",
					Micro: "1",
				},
			},
			Adslot: &Adslot{
				Adslot_id: adid,
				Adslot_size: &Size{
					Width:  w,
					Height: h,
				},
			},
			Device: &Device{
				Device_type: "1",
				Os_type:     "1",
				Os_version: &Version{
					Major: "1",
					Minor: "1",
					Micro: "1",
				},
				Vendor: getReq.Vendor,
				Model:  getReq.Model,
				Screen_size: &Size{
					Width:  getReq.Screenwidth,
					Height: getReq.Screenheight,
				},
				Udid: &Udid{
					Idfa:       getReq.Idfa,
					Imei:       getReq.Imei,
					Android_id: getReq.Androidid,
					Mac:        getReq.Mac,
				},
			},
			Network: &Network{
				Ipv4:            getReq.Ip,
				Connection_type: "4",
				Operator_type:   "3",
			},
		},
	}
	if getReq.Os == "2" {
		postData.Sign = util.Md5(KEY + APPTOKEN_IOS)
		postData.Apptoken = APPTOKEN_IOS
		postData.Data.Device.Os_type = "2"
	}
	var m = [3]string{"1", "1", "1"}
	appversion := strings.Split(getReq.Appversion, ".")
	for index, value := range appversion {
		if index < 3 {
			m[index] = value
		}
	}
	postData.Data.App.App_version = &Version{
		Major: m[0],
		Minor: m[1],
		Micro: m[2],
	}

	var n = [3]string{"1", "1", "1"}
	osversion := strings.Split(getReq.Osversion, ".")
	for index, value := range osversion {
		if index < 3 {
			n[index] = value
		}
	}
	postData.Data.Device.Os_version = &Version{
		Major: n[0],
		Minor: n[1],
		Micro: n[2],
	}

	network := postData.Data.Network
	switch getReq.Network {
	case "以太网":
		network.Connection_type = "101"
	case "wifi":
		network.Connection_type = "100"
	case "2g":
		network.Connection_type = "2"
	case "3g":
		network.Connection_type = "3"
	case "4g":
		network.Connection_type = "4"
	default:
		network.Connection_type = "999"
	}
	// 运营商
	switch getReq.Imsi {
	case "1":
		network.Operator_type = "1"
	case "2":
		network.Operator_type = "3"
	case "3":
		network.Operator_type = "2"
	default:
		network.Operator_type = "0"
	}

	ma, err := ffjson.Marshal(&postData)
	if err != nil {
		return util.ResMsg{}
	}
	header := http.Header{}
	header.Set("Connection", "keep-alive")
	header.Set("Content-Type", "application/json")
	resp, err := util.HttpPOST(URL, &header, ma)
	mongo.LogReq(funcName, getReq.Os)
	if err != nil {
		mongo.LogTimeout(funcName, getReq.Os)
		return util.ResMsg{}
	}
	data, err := ioutil.ReadAll(resp.Body)
	resp.Body.Close()
	if err != nil {
		mongo.LogNo(funcName, getReq.Os)
		return util.ResMsg{}
	}
	if resp.StatusCode != 200 {
		mongo.LogTimeout(funcName, getReq.Os)
		return util.ResMsg{}
	}
	return FormateRes(data, getReq.Os, funcName)
}

func FormateRes(res []byte, os, funcName string) util.ResMsg {
	resData := &adres{}
	ffjson.Unmarshal(res, resData)
	fmt.Printf("%+v", resData)
	fmt.Println(string(res))
	if resData.Error_code != 0 {
		mongo.LogNo(funcName, os)
		return util.ResMsg{}
	}
	ad := resData.Wxad
	if ad.Image_src == "" {
		mongo.LogNo(funcName, os)
		return util.ResMsg{}
	}
	postData := util.ResMsg{
		Id:       "0",
		Weight:   0,
		State:    0,
		Title:    ad.Ad_title,
		Content:  ad.Description,
		ImageUrl: ad.Image_src,
		Uri:      ad.Click_url,
	}
	postData.Displayreport = append(postData.Displayreport, ad.Win_notice_url, "http://120.26.247.23/ad/report.php?method="+funcName+"&event=displayreport&os="+os+"&t="+util.GetRandom())
	postData.Clickreport = append(postData.Clickreport, "http://120.26.247.23/ad/report.php?method="+funcName+"&event=clickreport&os="+os+"&t="+util.GetRandom())

	return postData
}
