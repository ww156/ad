package wangmai

type adreq struct {
	Sign     string `json:"sign"`
	Apptoken string `json:"apptoken"`
	Data     *Data  `json:"data"`
}

type Data struct {
	App     *App     `json:"app"`
	Adslot  *Adslot  `json:"adslot"`
	Device  *Device  `json:"device"`
	Network *Network `json:"network"`
	Gps     *Gps     `json:"gps"`
}

type App struct {
	App_version *Version `json:"app_version"`
}

type Version struct {
	Major string `json:"major"`
	Minor string `json:"minor"`
	Micro string `json:"micro"`
}

type Adslot struct {
	Adslot_id   string `json:"adslot_id"`
	Adslot_size *Size  `json:"adslot_size"`
}

type Size struct {
	Width  string `json:"width"`
	Height string `json:"height"`
}

type Device struct {
	Device_type string   `json:"device_type"`
	Os_type     string   `json:"os_type"`
	Os_version  *Version `json:"os_version"`
	Vendor      string   `json:"vendor"`
	Model       string   `json:"model"`
	Screen_size *Size    `json:"screen_size"`
	Udid        *Udid    `json:"udid"`
}

type Udid struct {
	Idfa       string `json:"idfa"`
	Imei       string `json:"imei"`
	Android_id string `json:"android_id"`
	Mac        string `json:"mac"`
}

type Network struct {
	Ipv4            string `json:"ipv4"`
	Connection_type string `json:"connection_type"`
	Operator_type   string `json:"operator_type"`
}

type Gps struct {
	Coordinate_type string `json:"coordinate_type"`
	Longitude       string `json:"longitude"`
	Latitude        string `json:"latitude"`
	Timestamp       int    `json:"timestamp"`
}
