package wangmai

type adres struct {
	Error_code int
	Request_id string
	Wxad       Ad
}

type Ad struct {
	Title            string
	Ad_title         string
	Brand_name       string
	Description      string
	Image_src        string
	Material_width   int
	Material_height  int
	Icon_src         string
	Creative_type    int
	Interaction_type int
	App_package      string
	App_size         float64
	Win_notice_url   string
	Click_url        string
}
