package youzi

type adres struct {
	Reqid string
	Ads   []Adinfo
}

type Adinfo struct {
	Yzid            string
	Html_snippet    string
	Title           string
	Description     string
	Sub_description string
	Images          []Imageinfo
	Icons           []Imageinfo
	Logos           []Imageinfo
	Snapshots       []Imageinfo
	Click_url       string
	Datas           []Datainfo
	Act_type        int
	Type            int
	Expiration      int
	App_name        string
	Ad_trackers     []Adtracker
}
type Imageinfo struct {
	Url  string
	Size Size
}
type Datainfo struct {
	Type  int
	Value string
}

type Adtracker struct {
	Type int
	Urls []string
}
