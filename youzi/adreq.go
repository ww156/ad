package youzi

type adreq struct {
	Reqid   string  `json:"reqid,omitempty"`
	Version string  `json:"version,omitempty"`
	App     *App    `json:"app,omitempty"`
	Device  *Device `json:"device,omitempty"`
	Geo     *Geo    `json:"geo,omitempty"`
	Slots   []*Ad   `json:"slots,omitempty"`
}

type App struct {
	Name    string `json:"name,omitempty"`
	Bundle  string `json:"bundle,omitempty"`
	Version string `json:"version,omitempty"`
}

type Device struct {
	Ipv4        string `json:"ipv4,omitempty"`
	Type        int    `json:"type,omitempty"`
	Os          int    `json:"os,omitempty"`
	Osv         string `json:"osv,omitempty"`
	Vendor      string `json:"vendor,omitempty"`
	Model       string `json:"model,omitempty"`
	Imei        string `json:"imei,omitempty"`
	Android_id  string `json:"android_id,omitempty"`
	Idfa        string `json:"idfa,omitempty"`
	Mac         string `json:"mac,omitempty"`
	Screen_size *Size  `json:"screen_size,omitempty"`
	Ppi         int    `json:"ppi,omitempty"`
	Carrier     string `json:"carrier,omitempty"`
	Conn_type   int    `json:"conn_type,omitempty"`
}

type Size struct {
	Width  int `json:"width,omitempty"`
	Height int `json:"height,omitempty"`
}

type Geo struct {
	Type      int    `json:"type,omitempty"`
	Lon       int    `json:"lon,omitempty"`
	Lat       int    `json:"lat,omitempty"`
	Timestamp int    `json:"timestamp,omitempty"`
	Country   string `json:"country,omitempty"`
	Region    string `json:"region,omitempty"`
}

type Ad struct {
	Yzid string `json:"yzid,omitempty"`
	Size *Size  `json:"size,omitempty"`
}
