package youzi

import (
	// "bytes"
	"encoding/json"
	// "fmt"
	"io/ioutil"
	// "log"
	"net/http"
	// "net/url"
	"strconv"
	"strings"
	// "time"
	// "unsafe"
	// "regexp"

	"ad/mongo"
	"ad/util"
)

const (
	URL          = "http://cpro.08ld.com:8085/api/v1"
	YZID         = "ad411644"
	YZID_FLOW    = "ad411652"
	YZID_STARTUP = "ad411647"
)

func Youzi(getReq *util.ReqMsg, funcName string) util.ResMsg {
	return base(getReq, funcName, YZID, 640, 100)
}
func YouziFlow(getReq *util.ReqMsg, funcName string) util.ResMsg {
	return base(getReq, funcName, YZID_FLOW, 640, 360)
}
func YouziStartup(getReq *util.ReqMsg, funcName string) util.ResMsg {
	return base(getReq, funcName, YZID_STARTUP, 640, 960)
}
func base(getReq *util.ReqMsg, funcName, yzid string, w, h int) util.ResMsg {
	width, err := strconv.Atoi(getReq.Screenwidth)
	if err != nil {
		width = 0
	}
	height, err := strconv.Atoi(getReq.Screenheight)
	if err != nil {
		height = 0
	}
	carrier := "46000"
	switch getReq.Imsi {
	case "1":
		carrier = "46000"
	case "2":
		carrier = "46001"
	case "3":
		carrier = "46002"
	default:
		carrier = "unknown"
	}
	network := 0
	switch getReq.Network {
	case "以太网":
		network = 1
	case "wifi":
		network = 2
	case "2g":
		network = 4
	case "3g":
		network = 5
	case "4g":
		network = 6
	default:
		network = 0
	}
	sd, err := strconv.Atoi(getReq.Sd)
	dpi := sd / 160
	postData := adreq{
		Reqid:   util.GetRandom(),
		Version: "1.2",
		App: &App{
			Name:    "网上厨房",
			Bundle:  "cn.ecook",
			Version: getReq.Appversion,
		},
		Device: &Device{
			Ipv4:       getReq.Ip,
			Type:       4,
			Os:         1,
			Osv:        getReq.Osversion,
			Vendor:     getReq.Vendor,
			Model:      getReq.Model,
			Imei:       getReq.Imei,
			Android_id: getReq.Androidid,
			Idfa:       getReq.Idfa,
			Mac:        getReq.Mac,
			Screen_size: &Size{
				Width:  width,
				Height: height,
			},
			Ppi:       dpi,
			Carrier:   carrier,
			Conn_type: network,
		},
		Slots: []*Ad{
			{
				Yzid: yzid,
				Size: &Size{
					Width:  w,
					Height: h,
				},
			},
		},
	}
	if getReq.Os == "2" {
		postData.Device.Os = 2
	}

	ma, err := json.Marshal(&postData)
	if err != nil {
		return util.ResMsg{}
	}

	header := http.Header{}
	header.Set("Connection", "keep-alive")
	header.Set("Content-Type", "application/json;charset=utf-8")
	resp, err := util.HttpPOST(URL, &header, ma)
	mongo.LogReq(funcName, getReq.Os)
	if err != nil {
		mongo.LogTimeout(funcName, getReq.Os)
		util.HttpRequestCount.WithLabelValues(funcName + "timeouterr").Inc()
		return util.ResMsg{}
	}
	data, err := ioutil.ReadAll(resp.Body)
	resp.Body.Close()
	if err != nil {
		mongo.LogNo(funcName, getReq.Os)
		util.HttpRequestCount.WithLabelValues(funcName + "readresperr").Inc()
		return util.ResMsg{}
	}
	// fmt.Println(resp.StatusCode)
	if resp.StatusCode != 200 {
		mongo.LogTimeout(funcName, getReq.Os)
		util.HttpRequestCount.WithLabelValues(funcName + "not200err").Inc()
		return util.ResMsg{}
	}

	return FormateRes(data, getReq.Os, funcName)
}

func FormateRes(res []byte, os, funcName string) util.ResMsg {
	resData := &adres{}
	json.Unmarshal(res, resData)
	// fmt.Printf("%+v", resData)
	if len(resData.Ads) == 0 {
		mongo.LogNo(funcName, os)
		util.HttpRequestCount.WithLabelValues(funcName + "noerr").Inc()
		return util.ResMsg{}
	}

	ad := resData.Ads[0]
	if len(ad.Images) == 0 {
		mongo.LogNo(funcName, os)
		util.HttpRequestCount.WithLabelValues(funcName + "noerr").Inc()
		return util.ResMsg{}
	}
	image := ad.Images[0]
	uri := strings.Replace(ad.Click_url, "%%CLK_DOWN_X%%", "-999", -1)
	uri = strings.Replace(ad.Click_url, "%%CLK_DOWN_X%%", "-999", -1)
	uri = strings.Replace(ad.Click_url, "%%CLK_DOWN_X%%", "-999", -1)
	uri = strings.Replace(ad.Click_url, "%%CLK_DOWN_X%%", "-999", -1)
	postData := util.ResMsg{
		Id:       "0",
		Weight:   0,
		State:    0,
		Title:    ad.Title,
		Content:  ad.Description,
		ImageUrl: image.Url,
		Uri:      uri,
	}
	adtracker := ad.Ad_trackers
	for _, v := range adtracker {
		if v.Type == 1 {
			for _, url := range v.Urls {
				url = strings.Replace(url, "%%CLK_DOWN_X%%", "-999", -1)
				url = strings.Replace(url, "%%CLK_DOWN_X%%", "-999", -1)
				url = strings.Replace(url, "%%CLK_DOWN_X%%", "-999", -1)
				url = strings.Replace(url, "%%CLK_DOWN_X%%", "-999", -1)
				postData.Displayreport = append(postData.Displayreport, url)
			}
		}
		if v.Type == 2 {
			for _, url := range v.Urls {
				url = strings.Replace(url, "%%CLK_DOWN_X%%", "-999", -1)
				url = strings.Replace(url, "%%CLK_DOWN_X%%", "-999", -1)
				url = strings.Replace(url, "%%CLK_DOWN_X%%", "-999", -1)
				url = strings.Replace(url, "%%CLK_DOWN_X%%", "-999", -1)
				postData.Clickreport = append(postData.Clickreport, url)
			}
		}
	}
	postData.Displayreport = append(postData.Displayreport, "http://120.26.247.23/ad/report.php?method="+funcName+"&event=displayreport&os="+os+"&t="+util.GetRandom())
	postData.Clickreport = append(postData.Clickreport, "http://120.26.247.23/ad/report.php?method="+funcName+"&event=clickreport&os="+os+"&t="+util.GetRandom())

	return postData
}
