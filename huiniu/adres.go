package huiniu

type adres struct {
	Height         int
	Width          int
	Html           string
	MonitorUrl     []string
	ClickUrl       []string
	SrcUrls        []string
	DUrl           []string
	Pid            string
	Video_duration int
	Cid            string
	Ader_id        string
	Target_type    int
	App_name       string
	Package_name   string
	Down_url       string
	Title          string
	Content        string
	Icon           string
	Icon_title     string
	From           string
}
