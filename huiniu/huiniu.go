package huiniu

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"

	"ad/mongo"
	"ad/util"
)

// 线上环境：
// appid     100242

// 网上厨房banner-ANDROID:1000934
// 网上厨房banner-IOS:1000933
// 网上厨房信息流-ANDROID:1000932
// 网上厨房信息流-IOS:1000931

// 请求地址：http://ssp.1rtb.com/req_ad
const (
	URL               = "http://ssp.1rtb.com/req_ad"
	APPID             = "100242"
	ADID_ANDROID      = "1000934"
	ADID_ANDROID_FLOW = "1000932"
	ADID_IOS          = "1000933"
	ADID_IOS_FLOW     = "1000931"
)

func Huiniu(getReq *util.ReqMsg, funcName string) util.ResMsg {
	if getReq.Os == "1" {
		return base(getReq, funcName, ADID_ANDROID, "1", "640", "100")
	}
	return base(getReq, funcName, ADID_IOS, "1", "640", "100")
}
func HuiniuFlow(getReq *util.ReqMsg, funcName string) util.ResMsg {
	if getReq.Os == "1" {
		return base(getReq, funcName, ADID_ANDROID_FLOW, "2", "640", "360")
	}
	return base(getReq, funcName, ADID_IOS_FLOW, "2", "640", "360")
}

func base(getReq *util.ReqMsg, funcName, adid, adtype, w, h string) util.ResMsg {
	v := url.Values{}
	v.Set("pid", adid)
	v.Set("type", "api")
	v.Set("ad_type", adtype)
	v.Set("ad_w", w)
	v.Set("ad_h", h)

	v.Set("app_package", "cn.ecook")
	v.Set("app_id", APPID)
	v.Set("app_name", "网上厨房")
	v.Set("device_imei", getReq.Imei)
	v.Set("device_adid", getReq.Androidid)
	v.Set("device_os", "Android")
	if getReq.Os == "2" {
		v.Set("app_package", "cn.ecook.ecook")
		v.Set("device_adid", getReq.Idfa)
		v.Set("device_os", "iOS")
	}
	v.Set("device_mac", getReq.Mac)
	v.Set("device_type_os", getReq.Osversion)
	v.Set("device_brand", getReq.Vendor)
	v.Set("device_model", getReq.Model)
	v.Set("device_width", getReq.Screenwidth)
	v.Set("device_height", getReq.Screenheight)
	carrier := "46000"
	switch getReq.Imsi {
	case "1":
		carrier = "46000"
	case "2":
		carrier = "46001"
	case "3":
		carrier = "46003"
	}
	v.Set("device_imsi", carrier)
	v.Set("device_network", getReq.Network)
	v.Set("device_density", getReq.Sd)
	v.Set("device_ip", getReq.Ip)
	v.Set("device_ua", getReq.Ua)
	v.Set("device_lan", "zh-CN")
	v.Set("device_type", "0")
	v.Set("is_mobile", "1")
	header := http.Header{}
	header.Set("Connection", "keep-alive")
	header.Set("Content-Type", "application/x-www-form-urlencoded")
	resp, err := util.HttpPOST(URL, &header, []byte(v.Encode()))
	mongo.LogReq(funcName, getReq.Os)
	if err != nil {
		fmt.Println("[huiniu.go-70]", err.Error())
		mongo.LogTimeout(funcName, getReq.Os)
		util.HttpRequestCount.WithLabelValues(funcName + "timeouterr").Inc()
		return util.ResMsg{}
	}
	data, err := ioutil.ReadAll(resp.Body)
	resp.Body.Close()
	if err != nil {
		mongo.LogNo(funcName, getReq.Os)
		util.HttpRequestCount.WithLabelValues(funcName + "readresperr").Inc()
		return util.ResMsg{}
	}
	if resp.StatusCode != 200 {
		mongo.LogTimeout(funcName, getReq.Os)
		util.HttpRequestCount.WithLabelValues(funcName + "not200err").Inc()
		return util.ResMsg{}
	}
	return FormateRes(data, getReq.Os, funcName)
}

func FormateRes(res []byte, os, funcName string) util.ResMsg {
	resData := &adres{}
	json.Unmarshal(res, resData)
	// fmt.Println(resData)
	// fmt.Println(string(res))
	if len(resData.SrcUrls) == 0 || len(resData.DUrl) == 0 {
		mongo.LogNo(funcName, os)
		util.HttpRequestCount.WithLabelValues(funcName + "noerr").Inc()
		return util.ResMsg{}
	}
	postData := util.ResMsg{
		Id:       "0",
		Weight:   0,
		State:    0,
		Title:    resData.Title,
		Content:  resData.Content,
		ImageUrl: resData.SrcUrls[0],
		Uri:      resData.DUrl[0],
	}
	postData.Displayreport = resData.MonitorUrl
	postData.Displayreport = append(postData.Displayreport, "http://120.26.247.23/ad/report.php?method="+funcName+"&event=displayreport&os="+os+"&t="+util.GetRandom())
	postData.Clickreport = resData.ClickUrl
	postData.Clickreport = append(postData.Clickreport, "http://120.26.247.23/ad/report.php?method="+funcName+"&event=clickreport&os="+os+"&t="+util.GetRandom())

	return postData
}
