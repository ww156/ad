package cache

import (
	"ad/util"
	"bytes"

	"github.com/bradfitz/gomemcache/memcache"
)

// 初始化memcache连接
var mc = memcache.New("116.62.14.43:11211")

func LogReq(name, os string) error {
	return logState(name, os, "request")
}
func LogYes(name, os string) error {
	return logState(name, os, "yes")
}
func LogNo(name, os string) error {
	return logState(name, os, "no")
}
func LogTimeout(name, os string) error {
	return logState(name, os, "timeout")
}
func logState(name, os, state string) error {
	time := util.FormateYMDH()
	// memcache的key
	key := name + "_" + os + "_" + time + "_" + state
	// memcache的标示key
	keyStr := time + "_Adstr"
	//	获取存储key的字符串*_Adstr
	adstrItem, err := mc.Get(keyStr)
	if err != nil {
		return err
	}
	adstr := adstrItem.Value
	if util.IsExist(keyStr) || bytes.Contains(adstrV, []byte(key)) {
		return mc.Increment(key, 1)
	} else {
		item := memcache.Item{Key: key, Value: []byte("1"), Flags: 1, Expiration: 604800}
		return mc.Set(item)
	}
	return nil
}

// wget -qNO /root/go/bin/ad http://120.26.6.239:12300/sync
