package wangxiang

import (
	// "bytes"
	"encoding/json"
	// "fmt"
	"io/ioutil"
	// "log"
	"net/http"
	// "strconv"
	"strings"
	// "time"
	// "unsafe"
	// "regexp"

	"ad/mongo"
	"ad/util"
)

// key  	373540e9883e881b
// 应用标识:iOS   si8dfasp6y     android   awj4fjwoiz
// 广告位ID:
// banner   iOS      10270092
//          android  10270091
// 信息流   iOS      10270094
//          android  10270093
// 开屏     iOS      10270096
//          android  10270095

const (
	// URL = "http://debug.mssp.adunite.com/"
	// KEY              = "9a2061117f975a56"
	// APPTOKEN_ANDROID = "9nc45mowpz"
	// ADID_ANDROID     = "10110080"
	// APPTOKEN_IOS     = "ctcqf6be2a"
	// ADID_IOS         = "10110081"

	URL                  = "http://api.mssp.adunite.com/v1.api"
	KEY                  = "373540e9883e881b"
	APPTOKEN_ANDROID     = "awj4fjwoiz"
	ADID_BANNER_ANDROID  = "10270091"
	ADID_FLOW_ANDROID    = "10270093"
	ADID_STARTUP_ANDROID = "10270095"
	APPTOKEN_IOS         = "si8dfasp6y"
	ADID_BANNER_IOS      = "10270092"
	ADID_FLOW_IOS        = "10270094"
	ADID_STARTUP_IOS     = "10270096"
)

// banner  是  640*270
func Wangxiang(getReq *util.ReqMsg, funcName string) util.ResMsg {
	if getReq.Os == "1" {
		return base(getReq, funcName, "640", "720", ADID_BANNER_ANDROID)
	}
	return base(getReq, funcName, "640", "720", ADID_BANNER_IOS)
	// return util.ResMsg("")
}

func WangxiangFlow(getReq *util.ReqMsg, funcName string) util.ResMsg {
	if getReq.Os == "1" {
		return base(getReq, funcName, "640", "100", ADID_FLOW_ANDROID)
	}
	return base(getReq, funcName, "640", "100", ADID_FLOW_IOS)
	// return util.ResMsg("")
}

func WangxiangStartup(getReq *util.ReqMsg, funcName string) util.ResMsg {
	if getReq.Os == "1" {
		return base(getReq, funcName, "640", "960", ADID_STARTUP_ANDROID)
	}
	return base(getReq, funcName, "640", "960", ADID_STARTUP_IOS)
	// return util.ResMsg("")
}

func base(getReq *util.ReqMsg, funcName, w, h, adid string) util.ResMsg {
	postData := adreq{
		Sign:     util.Md5(KEY + APPTOKEN_ANDROID),
		Apptoken: APPTOKEN_ANDROID,
		Data: &Data{
			App: &App{
				App_version: &Version{
					Major: "1",
					Minor: "1",
					Micro: "1",
				},
			},
			Adslot: &Adslot{
				Adslot_id: adid,
				Adslot_size: &Size{
					Width:  w,
					Height: h,
				},
			},
			Device: &Device{
				Device_type: "1",
				Os_type:     "1",
				Os_version: &Version{
					Major: "1",
					Minor: "1",
					Micro: "1",
				},
				Vendor: getReq.Vendor,
				Model:  getReq.Model,
				Screen_size: &Size{
					Width:  getReq.Screenwidth,
					Height: getReq.Screenheight,
				},
				Udid: &Udid{
					Idfa:       getReq.Idfa,
					Imei:       getReq.Imei,
					Android_id: getReq.Androidid,
					Mac:        getReq.Mac,
				},
			},
			Network: &Network{
				Ipv4:            getReq.Ip,
				Connection_type: "4",
				Operator_type:   "3",
			},
		},
	}
	if getReq.Os == "2" {
		postData.Sign = util.Md5(KEY + APPTOKEN_IOS)
		postData.Apptoken = APPTOKEN_IOS
		postData.Data.Device.Os_type = "2"
	}
	var m = [3]string{"1", "1", "1"}
	appversion := strings.Split(getReq.Appversion, ".")
	for index, value := range appversion {
		if index < 3 {
			m[index] = value
		}
	}
	postData.Data.App.App_version = &Version{
		Major: m[0],
		Minor: m[1],
		Micro: m[2],
	}

	var n = [3]string{"1", "1", "1"}
	osversion := strings.Split(getReq.Osversion, ".")
	for index, value := range osversion {
		if index < 3 {
			n[index] = value
		}
	}
	postData.Data.Device.Os_version = &Version{
		Major: n[0],
		Minor: n[1],
		Micro: n[2],
	}

	network := postData.Data.Network
	switch getReq.Network {
	case "以太网":
		network.Connection_type = "101"
	case "wifi":
		network.Connection_type = "100"
	case "2g":
		network.Connection_type = "2"
	case "3g":
		network.Connection_type = "3"
	case "4g":
		network.Connection_type = "4"
	default:
		network.Connection_type = "999"
	}
	// 运营商
	switch getReq.Imsi {
	case "1":
		network.Operator_type = "1"
	case "2":
		network.Operator_type = "3"
	case "3":
		network.Operator_type = "2"
	default:
		network.Operator_type = "0"
	}

	ma, err := json.Marshal(&postData)
	if err != nil {
		return util.ResMsg{}
	}
	header := http.Header{}
	header.Set("Connection", "keep-alive")
	header.Set("Content-Type", "application/json")
	resp, err := util.HttpPOST(URL, &header, ma)
	mongo.LogReq(funcName, getReq.Os)
	if err != nil {
		mongo.LogTimeout(funcName, getReq.Os)
		util.HttpRequestCount.WithLabelValues(funcName + "timeouterr").Inc()
		return util.ResMsg{}
	}
	data, err := ioutil.ReadAll(resp.Body)
	resp.Body.Close()
	if err != nil {
		mongo.LogNo(funcName, getReq.Os)
		util.HttpRequestCount.WithLabelValues(funcName + "readresperr").Inc()
		return util.ResMsg{}
	}
	if resp.StatusCode != 200 {
		mongo.LogTimeout(funcName, getReq.Os)
		util.HttpRequestCount.WithLabelValues(funcName + "not200err").Inc()
		return util.ResMsg{}
	}
	return FormateRes(data, getReq.Os, funcName)
}

func FormateRes(res []byte, os, funcName string) util.ResMsg {
	resData := &adres{}
	json.Unmarshal(res, resData)
	if resData.Error_code != 0 {
		mongo.LogNo(funcName, os)
		util.HttpRequestCount.WithLabelValues(funcName + "noerr").Inc()
		return util.ResMsg{}
	}
	ad := resData.Wxad
	if ad.Image_src == "" {
		mongo.LogNo(funcName, os)
		util.HttpRequestCount.WithLabelValues(funcName + "noerr").Inc()
		return util.ResMsg{}
	}
	postData := util.ResMsg{
		Id:       "0",
		Weight:   0,
		State:    0,
		Title:    ad.Ad_title,
		Content:  ad.Description,
		ImageUrl: ad.Image_src,
		Uri:      ad.Click_url,
	}
	if ad.Image_src == "" {
		mongo.LogNo(funcName, os)
		util.HttpRequestCount.WithLabelValues(funcName + "noerr").Inc()
		return util.ResMsg{}
	}
	postData.Displayreport = append(postData.Displayreport, ad.Win_notice_url, "http://120.26.247.23/ad/report.php?method="+funcName+"&event=displayreport&os="+os+"&t="+util.GetRandom())
	postData.Clickreport = append(postData.Clickreport, "http://120.26.247.23/ad/report.php?method="+funcName+"&event=clickreport&os="+os+"&t="+util.GetRandom())

	return postData
}
