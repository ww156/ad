package youmi

import (
	"encoding/json"
	// "fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"strconv"
	"time"

	"ad/mongo"
	"ad/util"
)

const (
	URL_ANDROID            = "https://native.umapi.cn/aos/v1/oreq"
	URL_IOS                = "https://native.umapi.cn/ios/v1/oreq"
	APPID_IOS              = "a4d3334f6a1c29bf"
	APPID_ANDROID          = "40c1eea8baacf13f"
	SLOTID_ANDROID         = "8006"
	SLOTID_ANDROID_FLOW    = "8002"
	SLOTID_ANDROID_STARTUP = "8005"
	SLOTID_IOS             = "8004"
	SLOTID_IOS_FLOW        = "8001"
	SLOTID_IOS_STARTUP     = "8003"
)

func Youmi(getReq *util.ReqMsg, funcName string) util.ResMsg {
	if getReq.Os == "1" {
		return base(getReq, funcName, SLOTID_ANDROID)
	}
	return base(getReq, funcName, SLOTID_IOS)
}

func YoumiFlow(getReq *util.ReqMsg, funcName string) util.ResMsg {
	if getReq.Os == "1" {
		return base(getReq, funcName, SLOTID_ANDROID_FLOW)
	}
	return base(getReq, funcName, SLOTID_IOS_FLOW)
}

func YoumiStartup(getReq *util.ReqMsg, funcName string) util.ResMsg {
	if getReq.Os == "1" {
		return base(getReq, funcName, SLOTID_ANDROID_STARTUP)
	}
	return base(getReq, funcName, SLOTID_IOS_STARTUP)
}

func base(getReq *util.ReqMsg, funcName, slotid string) util.ResMsg {
	URL := URL_ANDROID
	APPID := APPID_ANDROID
	network := "1"
	switch getReq.Network {
	case "wifi":
		network = "1"
	case "2g":
		network = "2"
	case "3g":
		network = "3"
	case "4g":
		network = "4"
	case "5g":
		network = "5"
	default:
		network = "0"
	}
	carrier := "2"
	switch getReq.Imsi {
	case "1":
		carrier = "2"
	case "2":
		carrier = "3"
	case "3":
		carrier = "4"
	default:
		carrier = "0"

	}
	value := url.Values{}
	value.Set("reqtime", strconv.Itoa(int(time.Now().Unix())))
	value.Set("slotid", slotid)
	value.Set("adcount", "1")
	value.Set("reqid", util.GetRandom())
	value.Set("idfa", getReq.Idfa)
	value.Set("brand", getReq.Vendor)
	value.Set("model", getReq.Model)
	value.Set("mac", getReq.Mac)
	value.Set("imei", getReq.Imei)
	value.Set("androidid", getReq.Androidid)
	value.Set("imsi", getReq.Imsi)
	value.Set("ip", getReq.Ip)
	value.Set("ua", getReq.Ua)
	value.Set("os", "android")
	value.Set("osv", getReq.Osversion)
	value.Set("appversion", getReq.Appversion)
	value.Set("conntype", network)
	value.Set("carrier", carrier)
	value.Set("pk", "cn.ecook")
	value.Set("countrycode", "CN")
	value.Set("language", "zh")
	if getReq.Os == "2" {
		value.Set("os", "ios")
		URL = URL_IOS
		APPID = APPID_IOS
	}

	// client := &http.Client{Timeout: util.Timeout}
	// fmt.Println(URL + "?" + value.Encode())
	req, err := http.NewRequest("GET", URL+"?"+value.Encode(), nil)
	if err != nil {
		return util.ResMsg{}
	}
	req.Header.Set("Connection", "keep-alive")
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", "Bearer "+APPID)
	// fmt.Println(APPID)
	resp, err := util.Client.Do(req)
	mongo.LogReq(funcName, getReq.Os)
	if err != nil {
		mongo.LogTimeout(funcName, getReq.Os)
		util.HttpRequestCount.WithLabelValues(funcName + "timeouterr").Inc()
		return util.ResMsg{}
	}
	data, err := ioutil.ReadAll(resp.Body)
	resp.Body.Close()
	if resp.StatusCode != 200 {
		mongo.LogTimeout(funcName, getReq.Os)
		util.HttpRequestCount.WithLabelValues(funcName + "not200err").Inc()
		return util.ResMsg{}
	}
	if err != nil {
		mongo.LogNo(funcName, getReq.Os)
		util.HttpRequestCount.WithLabelValues(funcName + "readresperr").Inc()
		return util.ResMsg{}
	}
	return FormateRes(data, getReq.Os, funcName)
}

func FormateRes(res []byte, os, funcName string) util.ResMsg {
	resData := &adres{}
	json.Unmarshal(res, resData)
	// fmt.Printf("%+v", resData)
	if resData.C != 0 {
		mongo.LogNo(funcName, os)
		util.HttpRequestCount.WithLabelValues(funcName + "noerr").Inc()
		return util.ResMsg{}
	}
	if len(resData.Ad) == 0 {
		mongo.LogNo(funcName, os)
		util.HttpRequestCount.WithLabelValues(funcName + "noerr").Inc()
		return util.ResMsg{}
	}
	ad := resData.Ad[0]
	if len(ad.Pic) == 0 {
		mongo.LogNo(funcName, os)
		util.HttpRequestCount.WithLabelValues(funcName + "noerr").Inc()
		return util.ResMsg{}
	}
	pic := ad.Pic[0]
	if pic.Url == "" {
		mongo.LogNo(funcName, os)
		util.HttpRequestCount.WithLabelValues(funcName + "noerr").Inc()
		return util.ResMsg{}
	}
	postData := util.ResMsg{
		Id:       "0",
		Weight:   0,
		State:    0,
		Title:    ad.Name,
		Content:  ad.Slogan,
		ImageUrl: pic.Url,
		Uri:      ad.Url,
	}
	if ad.Uri != "" {
		postData.Uri = ad.Uri
	}
	track := ad.Track
	postData.Displayreport = track.Show
	postData.Displayreport = append(postData.Displayreport, "http://120.26.247.23/ad/report.php?method="+funcName+"&event=displayreport&os="+os+"&t="+util.GetRandom())
	postData.Clickreport = track.Click
	postData.Clickreport = append(postData.Clickreport, "http://120.26.247.23/ad/report.php?method="+funcName+"&event=clickreport&os="+os+"&t="+util.GetRandom())

	return postData
}
