package youmi

type adres struct {
	C   int
	Rsd string
	Ad  []Ad
}
type Ad struct {
	Id        string
	Slotid    string
	Name      string
	Icon      string
	Pic       []Pic
	Slogan    string
	Subslogan string
	Url       string
	Uri       string
	Pt        int
	Track     Track
	App       App
	Ext       Ext
}

type Pic struct {
	Url string
	W   int
	H   int
}
type Track struct {
	Show     []string
	Click    []string
	Download []string
	Install  []string
}

type App struct {
	Storeid     string
	Bid         string
	Description string
	Size        string
	Screenshot  []string
	Score       float64
	Category    string
}

type Ext struct {
	Io    int
	Delay int
	sal   int
	Pl    int
}
