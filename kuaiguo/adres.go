package kuaiguo

type adres struct {
	Id         string
	Seatbid    []*seatbid
	Bidid      string
	Cur        string
	Customdata string
	Nbr        int
	Ext        *ext
}
type seatbid struct {
	Bid   []*ad
	Seat  string
	Group int
	Ext   *ext
}

type ad struct {
	Id      string
	Impid   string
	Price   float64
	Adid    string
	Nurl    string
	Adm     string
	Adomain []string
	Bundle  string
	Iurl    string
	Cid     string
	Crid    string
	Cat     []string
	Attr    []int
	Dealid  string
	H       int
	W       int
	Ext     *bidext
}

type bidext struct {
	Type     int
	Stype    int
	Aurl     []string
	Title    string
	Text     string
	Curl     string
	Cmurl    []string
	Murl     []string
	Durl     string
	Ddesc    string
	Dmurl    []string
	Req_uuid int
}

type ext struct{}
