package kuaiguo

import (
	"bytes"
	// "encoding/json"
	// "fmt"
	"io/ioutil"
	// "log"
	"net/http"
	// "net/url"
	// "math/rand"
	"strconv"
	"strings"
	// "time"
	// "unsafe"
	// "regexp"

	"ad/mongo"
	"ad/util"
	"github.com/pquerna/ffjson/ffjson"
)

const (
	URL = "http://ecook.api.idsie.com/api.htm?pid=310829"
)

func Kuaiguo(getReq *util.ReqMsg, funcName string) util.ResMsg {
	return base(getReq, funcName)
}

func KuaiguoFlow(getReq *util.ReqMsg, funcName string) util.ResMsg {
	if getReq.Os == "2" {
		return util.ResMsg{}
	}
	return base(getReq, funcName)
}

func KuaiguoStartup(getReq *util.ReqMsg, funcName string) util.ResMsg {
	return base(getReq, funcName)
}

func base(getReq *util.ReqMsg, funcName string) util.ResMsg {
	width, err := strconv.Atoi(getReq.Screenwidth)
	if err != nil {
		width = 0
	}
	height, err := strconv.Atoi(getReq.Screenheight)
	if err != nil {
		height = 0
	}
	sd, _ := strconv.Atoi(getReq.Sd)
	dpi := sd / 160
	operator := ""
	switch getReq.Imsi {
	case "1":
		operator = "46000"
	case "2":
		operator = "46001"
	case "3":
		operator = "46002"
	default:
		operator = ""
	}
	network := 0
	switch getReq.Network {
	case "以太网":
		network = 1
	case "wifi":
		network = 2
	case "2g":
		network = 4
	case "3g":
		network = 5
	case "4g":
		network = 6
	default:
		network = 0
	}
	lat, _ := strconv.ParseFloat(getReq.Lat, 64)
	lon, _ := strconv.ParseFloat(getReq.Lng, 64)
	postData := adreq{
		Id:      util.GetRandom(),
		Package: "cn.ecook",
		Version: "1",
		Device: &device{
			Geo: &geo{
				Lat:  lat,
				Lon:  lon,
				Type: 1,
			},
			Ip:                 getReq.Ip,
			Devicetype:         4,
			Os:                 "Android",
			Osv:                getReq.Osversion,
			Did:                getReq.Imei,
			Didmd5:             util.Md5(getReq.Imei),
			Dpid:               getReq.Androidid,
			Dpidmd5:            util.Md5(getReq.Androidid),
			Mac:                getReq.Mac,
			Macmd5:             util.Md5(getReq.Mac),
			Ua:                 getReq.Ua,
			Make:               getReq.Vendor,
			Model:              getReq.Model,
			W:                  width,
			H:                  height,
			Ppi:                dpi,
			Carrier:            operator,
			Connectiontype:     network,
			Screen_orientation: 1,
		},
	}
	device := postData.Device
	if getReq.Os == "2" {
		postData.Package = "cn.ecook.ecook"
		device.Os = "IOS"
		device.Dpid = getReq.Idfa
		device.Dpidmd5 = util.Md5(getReq.Idfa)
	}

	ma, err := ffjson.Marshal(&postData)
	if err != nil {
		return util.ResMsg{}
	}
	// 请求广告
	req, err := http.NewRequest("POST", URL, bytes.NewReader(ma))
	req.Header.Set("Connection", "keep-alive")
	req.Header.Set("Content-Type", "application/json;charset=utf-8")

	if err != nil {
		return util.ResMsg{}
	}
	resp, err := util.Client.Do(req)
	mongo.LogReq(funcName, getReq.Os)
	if err != nil {
		mongo.LogTimeout(funcName, getReq.Os)
		return util.ResMsg{}
	}
	data, err := ioutil.ReadAll(resp.Body)
	resp.Body.Close()
	if err != nil {
		mongo.LogNo(funcName, getReq.Os)
		return util.ResMsg{}
	}
	// fmt.Println(resp.StatusCode)
	if resp.StatusCode != 200 {
		mongo.LogTimeout(funcName, getReq.Os)
		return util.ResMsg{}
	}
	return FormateRes(data, getReq.Os, funcName, getReq.Lat, getReq.Lng)
}

// 根据广告平台的返回生成客户端返回
func FormateRes(res []byte, os, funcName, lat, lon string) util.ResMsg {
	resData := adres{}
	ffjson.Unmarshal(res, &resData)
	// fmt.Printf("%+v", resData)
	// fmt.Println(string(res))
	if resData.Nbr != 0 {
		mongo.LogNo(funcName, os)
		return util.ResMsg{}
	}

	if len(resData.Seatbid) == 0 {
		mongo.LogNo(funcName, os)
		return util.ResMsg{}
	}
	seatbid := resData.Seatbid[0]
	if len(seatbid.Bid) == 0 {
		mongo.LogNo(funcName, os)
		return util.ResMsg{}
	}
	ad := seatbid.Bid[0].Ext

	if len(ad.Aurl) == 0 {
		mongo.LogNo(funcName, os)
		return util.ResMsg{}
	}

	postData := util.ResMsg{
		Id:       "0",
		Weight:   0,
		State:    1,
		Title:    ad.Title,
		Content:  ad.Text,
		ImageUrl: ad.Aurl[0],
		Uri:      "",
	}
	postData.Uri = replace(ad.Curl, lat, lon)

	postData.Displayreport = ad.Murl
	postData.Displayreport = append(postData.Displayreport, "http://120.26.247.23/ad/report.php?method="+funcName+"&event=displayreport&os="+os+"&t="+util.GetRandom())
	for _, v := range ad.Cmurl {
		if v != "" {
			postData.Clickreport = append(postData.Clickreport, replace(v, lat, lon))
		}
	}
	postData.Clickreport = append(postData.Clickreport, "http://120.26.247.23/ad/report.php?method="+funcName+"&event=clickreport&os="+os+"&t="+util.GetRandom())

	return postData
}

func replace(str, lat, lon string) string {
	str = strings.Replace(str, "%%LAT%%", lat, -1)
	str = strings.Replace(str, "%%LON%%", lon, -1)
	str = strings.Replace(str, "%%DOWNX%%", "IT_CLK_PNT_DOWN_X", -1)
	str = strings.Replace(str, "%%DOWNY%% ", "IT_CLK_PNT_DOWN_Y", -1)
	str = strings.Replace(str, "%%UPX%%", "IT_CLK_PNT_UP_X", -1)
	str = strings.Replace(str, "%%UPY%% %", "IT_CLK_PNT_UP_Y", -1)
	return str
}
