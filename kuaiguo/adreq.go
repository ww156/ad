package kuaiguo

type adreq struct {
	Id      string  `json:"id"`
	Package string  `json:"package"`
	Version string  `json:"version"`
	Device  *device `json:"device"`
	User    *user   `json:"user"`
}

type device struct {
	Geo                *geo   `json:"geo"`
	Ip                 string `json:"ip"`
	Devicetype         int    `json:"devicetype"`
	Os                 string `json:"os"`
	Osv                string `json:"osv"`
	Did                string `json:"did"`
	Didmd5             string `json:"didmd5"`
	Dpid               string `json:"dpid"`
	Dpidmd5            string `json:"dpidmd5"`
	Mac                string `json:"mac"`
	Macmd5             string `json:"macmd5"`
	Ua                 string `json:"ua"`
	Make               string `json:"make"`
	Model              string `json:"model"`
	W                  int    `json:"w"`
	H                  int    `json:"h"`
	Ppi                int    `json:"ppi"`
	Carrier            string `json:"carrier"`
	Connectiontype     int    `json:"connectiontype"`
	Screen_orientation int    `json:"screen_orientation"`
}
type geo struct {
	Lat  float64 `json:"lat"`
	Lon  float64 `json:"lon"`
	Type int     `json:"type"`
}

type user struct {
	Id     string `json:"id"`
	Yob    int    `json:"yob"`
	Gender string `json:"gender"`
}
