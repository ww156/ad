package zhizun

import (
	// "bytes"
	"crypto/md5"
	"encoding/base64"
	"encoding/hex"
	"encoding/json"
	// "fmt"
	"io/ioutil"
	// "log"
	"net/http"
	"net/url"
	"strconv"
	// "os"
	"time"

	// "ad/zhizun"
	"ad/mongo"
	"ad/util"
)

const (
	// amdroid
	ZHIZUNFLOW_ANDROID_KEY   = "quZJes" // "NMFozD"
	ZHIZUNFLOW_ANDROID_SAID  = "186"    // "125"
	ZHIZUNFLOW_ANDROID_SCALE = "20.5"
	ZHIZUN_ANDROID_KEY       = "KvQYPX"
	ZHIZUN_ANDROID_SAID      = "187"
	ZHIZUN_ANDROID_SCALE     = "32.5"
	// ios
	ZHIZUNFLOW_IOS_KEY   = "ysxigB" // "NMFozD"
	ZHIZUNFLOW_IOS_SAID  = "189"    // "125"
	ZHIZUNFLOW_IOS_SCALE = "32.5"

	KEY_ANDROID_FLOW   = "mABeRj"
	SAID_ANDROID_FLOW  = "197"
	SCALE_ANDROID_FLOW = "16.9"
)

func Zhizun(getReq *util.ReqMsg, funcName string) util.ResMsg {
	if getReq.Os == "1" {
		return base(getReq, funcName, ZHIZUN_ANDROID_KEY, ZHIZUN_ANDROID_SAID, ZHIZUN_ANDROID_SCALE)
	}
	return util.ResMsg{}
}

func ZhizunFlow(getReq *util.ReqMsg, funcName string) util.ResMsg {
	if getReq.Os == "1" {
		return base(getReq, funcName, KEY_ANDROID_FLOW, SAID_ANDROID_FLOW, SCALE_ANDROID_FLOW)
	}
	return base(getReq, funcName, ZHIZUNFLOW_IOS_KEY, ZHIZUNFLOW_IOS_SAID, ZHIZUNFLOW_IOS_SCALE)
}

func base(getReq *util.ReqMsg, funcName, key, said, scale string) util.ResMsg {
	reqData := Adreq{
		Key:         key,
		Said:        said,
		Time:        time.Now().Unix(),
		Scale:       "20.5",
		Device_type: 1, // 设备类型固定值1：手机
		Screen_size: getReq.Screenwidth + "*" + getReq.Screenheight,
		Version:     getReq.Osversion,
		Phone_model: getReq.Model,
		Network:     getReq.Network,
		Ip:          getReq.Ip,
	}
	//         | getReq | reqData
	// android:| 1      | 2
	// ios:    | 2      | 3
	reqData.Os = 2 // android
	reqData.Imei = getReq.Androidid
	switch getReq.Imsi {
	case "1":
		reqData.Operator = url.QueryEscape("移动")
	case "2":
		reqData.Operator = url.QueryEscape("联通")
	case "3":
		reqData.Operator = url.QueryEscape("电信")
	}
	if getReq.Os == "2" { // ios
		reqData.Os = 3
		reqData.Imei = getReq.Idfa
	}
	reqData.Encryption = getMd5(reqData.Key, reqData.Time, reqData.Said)
	ma, err := json.Marshal(reqData)
	str := base64.StdEncoding.EncodeToString(ma)
	// fmt.Println(url.QueryEscape(str))
	// 请求广告
	req, err := http.NewRequest("POST", "http://openapi.zzad.com/index.php/api?keys="+url.QueryEscape(str), nil)
	if err != nil {
		return util.ResMsg{}
	}
	req.Header.Set("Connection", "keep-alive")
	resp, err := util.Client.Do(req)
	mongo.LogReq(funcName, getReq.Os)
	if err != nil {
		mongo.LogTimeout(funcName, getReq.Os)
		util.HttpRequestCount.WithLabelValues(funcName + "timeouterr").Inc()
		return util.ResMsg{}
	}
	data, err := ioutil.ReadAll(resp.Body)
	resp.Body.Close()
	if err != nil {
		mongo.LogNo(funcName, getReq.Os)
		return util.ResMsg{}
	}
	// fmt.Println(resp.StatusCode)
	if resp.StatusCode != 200 {
		mongo.LogTimeout(funcName, getReq.Os)
		return util.ResMsg{}
	}
	return FormateRes(data, getReq.Os, funcName)
}

// 根据广告平台的返回生成客户端返回
func FormateRes(res []byte, os, funcName string) util.ResMsg {
	resData := Adres{}
	json.Unmarshal(res, &resData)
	if resData.Code == 400 {
		mongo.LogNo(funcName, os)
		util.HttpRequestCount.WithLabelValues(funcName + "noerr").Inc()
		return util.ResMsg{}
	}
	if len(resData.Info) == 0 {
		mongo.LogNo(funcName, os)
		util.HttpRequestCount.WithLabelValues(funcName + "noerr").Inc()
		return util.ResMsg{}
	}
	info := resData.Info[0]
	postData := util.ResMsg{
		Id:       "0",
		Weight:   0,
		State:    0,
		Title:    info.Title,
		Content:  info.Desc,
		ImageUrl: info.Img_src,
		Uri:      info.Curl,
	}
	postData.Displayreport = append(postData.Displayreport, "http://120.26.247.23/ad/report.php?method="+funcName+"&event=displayreport&os="+os+"&t="+util.GetRandom())
	postData.Clickreport = append(postData.Clickreport, "http://120.26.247.23/ad/report.php?method="+funcName+"&event=clickreport&os="+os+"&t="+util.GetRandom())

	return postData
}

func getMd5(key string, t int64, said string) string {
	md5Key := md5.Sum([]byte(key))
	strKey := hex.EncodeToString(md5Key[:])
	str := strKey + strconv.FormatInt(t, 10) + said
	md5Str := md5.Sum([]byte(str))
	return hex.EncodeToString(md5Str[:])
}
