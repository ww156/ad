package zhizun

type Info struct {
	Img_src string
	Title   string
	Desc    string
	Curl    string
}

type Adres struct {
	Code int    // 200、400
	Msg  string // 400
	Info []Info // 200
}
