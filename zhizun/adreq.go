package zhizun

type Adreq struct {
	Key         string `json:"key"`
	Said        string `json:"said"`
	Time        int64  `json:"time"`
	Encryption  string `json:"encryption"`
	Imei        string `json:"imei"` // 设备唯一标识符
	Scale       string `json:"scale"`
	Device_type int    `json:"device_type"`
	Os          int    `json:"os"`
	Screen_size string `json:"screen_size"` // "1080*1920"s
	Version     string `json:"version"`
	Phone_model string `json:"phone_model"`
	Network     string `json:"network"`
	Operator    string `json:"operator"`
	Ip          string `json:"ip"`
}
