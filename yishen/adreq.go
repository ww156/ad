package yishen

type adreq struct {
	Id     string  `json:"id"`
	Imp    []*imp  `json:"imp,omitempty"`
	Pid    string  `json:"pid"`
	Device *device `json:"device,omitempty"`
	User   *user   `json:"user,omitempty"`
	Time   int64   `json:"time"`
	Ssl    int     `json:"ssl"`
	Ext    *ext    `json:"ext,omitempty"`
}

type imp struct {
	Positionid string   `json:"positionid"`
	Adtype     int      `json:"adtype"`
	W          int      `json:"w"`
	H          int      `json:"h"`
	Ctype      []string `json:"ctype,omitempty"`
	Wmax       int      `json:"wmax"`
	Hmax       int      `json:"hmax"`
	Wmin       int      `json:"wmin"`
	Hmin       int      `json:"hmin"`
	Pos        int      `json:"pos"`
	Displaymgr string   `json:"displaymgr"`
	Ext        *ext     `json:"ext,omitempty"`
}

type device struct {
	Type     string `json:"type"`
	Ip       string `json:"ip"`
	Ua       string `json:"ua"`
	Geo      *geo   `json:"geo,omitempty"`
	Imei     string `json:"imei"`
	Imsi     string `json:"imsi"`
	Anid     string `json:"anid"`
	Udid     string `json:"udid"`
	Ouid     string `json:"ouid"`
	Mac      string `json:"mac"`
	Lang     string `json:"lang"`
	Brand    string `json:"brand"`
	Model    string `json:"model"`
	Dn       string `json:"dn"`
	Os       string `json:"os"`
	Osv      string `json:"osv"`
	Conntype string `json:"conntype"`
	Ifa      string `json:"ifa"`
	Oifa     string `json:"oifa"`
	Idv      string `json:"idv"`
	Sw       int    `json:"sw"`
	Sh       int    `json:"sh"`
	Den      int    `json:"den"`
	Ori      int    `json:"ori"`
	Jb       int    `json:"jb"`
	Kst      int64  `json:"kst"`
	Rm       string `json:"mac"`
	Rs       string `json:"rs"`
	Aaid     string `json:"aaid"`
	Ipv6     string `json:"ipv6"`
	Mnc      string `json:"mnc"`
	Mcc      string `json:"mcc"`
	Maker    string `json:"maker"`
	Js       int    `json:"js"`
	Sd       int    `json:"sd"`
	Mt       string `json:"mt"`
	Ext      *ext   `json:"ext,omitempty"`
}

type user struct {
	Id       string `json:"id"`
	Gender   string `json:"gender"`
	Age      string `json:"age"`
	Keywords string `json:"keywords"`
	Geo      *geo   `json:"geo,omitempty"`
	Ext      *ext   `json:"ext,omitempty"`
}

type geo struct {
	Lat    float64 `json:"lat"`
	Lon    float64 `json:"lon"`
	Cc     string  `json:"cc"`
	City   string  `json:"city"`
	Zip    string  `json:"zip"`
	Cellid string  `json:"cellid"`
	Ext    *ext    `json:"ext,omitempty"`
}

type ext struct{}
