package yishen

type adres struct {
	Result     bool
	Err        int
	Ver        string
	Sid        string
	Adnum      int
	Gimpmonurl []string
	Ads        []ad
}

type ad struct {
	Type        int
	Adid        string
	Curl        string
	Adpriority  string
	W           int
	H           int
	Ctype       int
	Clickeffect int
	Title       string
	Desc        string
	Stuffurl    string
	Stuffurls   []string
	Icon        string
	Targettype  string
	Impmonurl   string
	Clkmonurl   string
	Playmonurl  string
	Videouel    string
	Pkg         string
	Storeid     string
	Duration    string
	Ext         ext
}
