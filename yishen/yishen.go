package yishen

import (
	"bytes"
	// "encoding/json"
	// "fmt"
	"io/ioutil"
	// "log"
	"net/http"
	// "net/url"
	"math/rand"
	"strconv"
	"strings"
	"time"
	// "unsafe"
	// "regexp"

	"ad/mongo"
	"ad/util"
	"github.com/pquerna/ffjson/ffjson"
)

const (
	URL       = "http://36.110.80.158:8089/dsp-api-docking/core/esin.do"
	APPID     = "CT201708041636361501835796652"
	ADID      = "SKOYsDFkQA44g4hw"
	ADID_FLOW = "Rskcp0hFtDb95nHl"
	SECRET    = "b84f6ec5b218403f95786bba17586079"
)

func Yishen(getReq *util.ReqMsg, funcName string) util.ResMsg {
	return base(getReq, funcName, ADID, 600, 128, 0)
}

func YishenFlow(getReq *util.ReqMsg, funcName string) util.ResMsg {
	return base(getReq, funcName, ADID_FLOW, 680, 360, 4)
}
func YishenStartup(getReq *util.ReqMsg, funcName string) util.ResMsg {
	return base(getReq, funcName, ADID, 680, 960, 0)
}

func base(getReq *util.ReqMsg, funcName, adid string, w, h, adtype int) util.ResMsg {
	if getReq.Os == "1" {
		return util.ResMsg{}
	}
	width, err := strconv.Atoi(getReq.Screenwidth)
	if err != nil {
		width = 0
	}
	height, err := strconv.Atoi(getReq.Screenheight)
	if err != nil {
		height = 0
	}
	network := "6"
	switch getReq.Network {
	case "wifi":
		network = "1"
	case "2g":
		network = "2"
	case "3g":
		network = "3"
	case "4g":
		network = "4"
	default:
		network = "6"
	}
	imsi := ""
	switch getReq.Imsi {
	case "1":
		imsi = "46000" + strconv.Itoa(10000+rand.Intn(90000)) + strconv.Itoa(10000+rand.Intn(90000))
	case "2":
		imsi = "46001" + strconv.Itoa(10000+rand.Intn(90000)) + strconv.Itoa(10000+rand.Intn(90000))
	case "3":
		imsi = "46002" + strconv.Itoa(10000+rand.Intn(90000)) + strconv.Itoa(10000+rand.Intn(90000))
	default:
		imsi = ""
	}
	ts := time.Now().UnixNano() / 1e6
	lat, _ := strconv.ParseFloat(getReq.Lat, 64)
	lon, _ := strconv.ParseFloat(getReq.Lng, 64)
	sd, _ := strconv.Atoi(getReq.Sd)
	dpi := sd / 160
	postData := adreq{
		Id:   util.GetRandom(),
		Pid:  APPID,
		Ssl:  0,
		Time: ts,
		Imp: []*imp{
			&imp{
				Positionid: adid,
				W:          w,
				H:          h,
				Adtype:     adtype,
				Ctype:      []string{},
			},
		},
		Device: &device{
			Type: "4", //1
			Ip:   getReq.Ip,
			Ua:   getReq.Ua,
			Geo: &geo{
				Lat: lat,
				Lon: lon,
			},
			Imei:     getReq.Imei,
			Imsi:     imsi,
			Anid:     getReq.Androidid,
			Ouid:     getReq.Openudid,
			Mac:      getReq.Mac,
			Lang:     "zh",
			Brand:    getReq.Vendor,
			Model:    getReq.Model,
			Dn:       getReq.Vendor,
			Os:       "1",
			Osv:      getReq.Osversion,
			Conntype: network,
			Ifa:      strings.ToUpper(getReq.Idfa),
			Sw:       width,
			Sh:       height,
			Den:      dpi,
			Ori:      1,
		},
	}
	device := postData.Device
	if getReq.Os == "2" {
		device.Os = "0"
		device.Type = "1"
	}

	ma, err := ffjson.Marshal(&postData)
	if err != nil {
		return util.ResMsg{}
	}
	// fmt.Println(string(ma))
	// 请求广告
	req, err := http.NewRequest("POST", URL, bytes.NewReader(ma))
	req.Header.Set("Connection", "keep-alive")
	req.Header.Set("Content-Type", "application/json;charset=utf-8")
	req.Header.Set("X-CAD-S2S-VER", "1.0")
	req.Header.Set("X-FORWARDED-FOR", getReq.Ip)
	sign := util.Md5("pid=" + APPID + "&timestamp=" + strconv.Itoa(int(ts)) + "&os=" + device.Os + "&secret=" + SECRET)
	// fmt.Println(sign)
	req.Header.Set("X-CAD-S2S-SIGNITURE", strings.ToLower(sign))
	if err != nil {
		return util.ResMsg{}
	}
	resp, err := util.Client.Do(req)
	mongo.LogReq(funcName, getReq.Os)
	if err != nil {
		mongo.LogTimeout(funcName, getReq.Os)
		return util.ResMsg{}
	}
	data, err := ioutil.ReadAll(resp.Body)
	resp.Body.Close()
	if err != nil {
		mongo.LogNo(funcName, getReq.Os)
		return util.ResMsg{}
	}
	// fmt.Println(resp.StatusCode)
	if resp.StatusCode != 200 {
		mongo.LogTimeout(funcName, getReq.Os)
		return util.ResMsg{}
	}
	return FormateRes(data, getReq.Os, funcName)
}

// 根据广告平台的返回生成客户端返回
func FormateRes(res []byte, os, funcName string) util.ResMsg {
	resData := adres{}
	ffjson.Unmarshal(res, &resData)
	// fmt.Printf("%+v", resData)
	// fmt.Println(string(res))
	if !resData.Result {
		mongo.LogNo(funcName, os)
		util.HttpRequestCount.WithLabelValues(funcName + "noerr").Inc()
		return util.ResMsg{}
	}
	if len(resData.Ads) == 0 {
		// fmt.Println(2)
		mongo.LogNo(funcName, os)
		util.HttpRequestCount.WithLabelValues(funcName + "noerr").Inc()
		return util.ResMsg{}
	}

	ad := resData.Ads[0]

	postData := util.ResMsg{
		Id:       "0",
		Weight:   0,
		State:    1,
		Title:    "",
		Content:  "",
		ImageUrl: "",
		Uri:      ad.Curl,
	}
	if ad.Title != "" {
		postData.Title = ad.Title
	}
	if ad.Desc != "" {
		postData.Content = ad.Desc
	}
	if ad.Ctype == 1 {
		if ad.Stuffurl == "" {
			mongo.LogNo(funcName, os)
			return util.ResMsg{}
		}
		postData.ImageUrl = ad.Stuffurl
	} else if ad.Ctype == 21 {
		if len(ad.Stuffurls) == 0 {
			mongo.LogNo(funcName, os)
			return util.ResMsg{}
		} else {
			postData.ImageUrl = ad.Stuffurls[0]
		}
	}
	if ad.Impmonurl != "" {
		postData.Displayreport = append(postData.Displayreport, ad.Impmonurl)
	}
	postData.Displayreport = append(postData.Displayreport, "http://120.26.247.23/ad/report.php?method="+funcName+"&event=displayreport&os="+os+"&t="+util.GetRandom())
	if ad.Clkmonurl != "" {
		postData.Clickreport = append(postData.Clickreport, ad.Clkmonurl)
	}
	postData.Clickreport = append(postData.Clickreport, "http://120.26.247.23/ad/report.php?method="+funcName+"&event=clickreport&os="+os+"&t="+util.GetRandom())

	return postData
}
