package dianke

import (
	// "bytes"
	"encoding/json"
	// "fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	// "time"

	"ad/mongo"
	"ad/util"
)

// ios
const (
	URL = "http://adx.dk.cn/req/a08a92b86ef5fb439442b3713a220825"
	KEY = "a08a92b86ef5fb439442b3713a220825"
)

func Dianke(getReq *util.ReqMsg, funcName string) util.ResMsg {
	return base(getReq, funcName, "2")
}

func DiankeFlow(getReq *util.ReqMsg, funcName string) util.ResMsg {
	return base(getReq, funcName, "7") // 16:9
}

func base(getReq *util.ReqMsg, funcName, size string) util.ResMsg {
	v := url.Values{}
	v.Set("os", "1001")
	v.Set("osv", getReq.Osversion)
	v.Set("type", "1")
	v.Set("size", size)
	v.Set("ip", getReq.Ip)
	v.Set("imei", getReq.Imei)
	v.Set("mac", getReq.Mac)
	v.Set("idfa", getReq.Idfa)
	v.Set("brand", getReq.Vendor)
	v.Set("model", getReq.Model)
	v.Set("s_w", getReq.Screenwidth)
	v.Set("s_h", getReq.Screenheight)
	if getReq.Os == "2" {
		v.Set("os", "1002")
	}

	// client := &http.Client{Timeout: util.Timeout}
	req, err := http.NewRequest("GET", URL+"?"+v.Encode(), nil)
	if err != nil {
		return util.ResMsg{}
	}
	req.Header.Set("Connection", "keep-alive")
	resp, err := util.Client.Do(req)
	mongo.LogReq(funcName, getReq.Os)
	// 请求错误，日志记录为timeout
	if err != nil {
		mongo.LogTimeout(funcName, getReq.Os)
		util.HttpRequestCount.WithLabelValues(funcName + "timeouterr").Inc()
		return util.ResMsg{}
	}
	data, err := ioutil.ReadAll(resp.Body)
	resp.Body.Close()
	// 请求返回失败，日志记录为timeout
	if err != nil {
		mongo.LogNo(funcName, getReq.Os)
		util.HttpRequestCount.WithLabelValues(funcName + "readresperr").Inc()
		return util.ResMsg{}
	}
	if resp.StatusCode != 200 {
		mongo.LogTimeout(funcName, getReq.Os)
		util.HttpRequestCount.WithLabelValues(funcName + "not200err").Inc()
		return util.ResMsg{}
	}
	// fmt.Println(string(data))
	return FormateRes(data, getReq.Os, funcName)
}

func FormateRes(res []byte, os, funcName string) util.ResMsg {
	resData := adres{}
	json.Unmarshal(res, &resData)
	// fmt.Printf("%+v", resData)
	ad := resData.Res
	if ad.Source == "" {
		mongo.LogNo(funcName, os)
		util.HttpRequestCount.WithLabelValues(funcName + "noerr").Inc()
		return util.ResMsg{}
	}

	postData := util.ResMsg{
		Id:       "0",
		Weight:   0,
		State:    0,
		Title:    ad.Title,
		Content:  ad.Subtitle,
		ImageUrl: ad.Source,
		Uri:      ad.Click,
	}
	postData.Displayreport = append(postData.Displayreport, "http://120.26.247.23/ad/report.php?method="+funcName+"&event=displayreport&os="+os+"&t="+util.GetRandom())
	postData.Clickreport = append(postData.Clickreport, "http://120.26.247.23/ad/report.php?method="+funcName+"&event=clickreport&os="+os+"&t="+util.GetRandom())

	return postData
}
