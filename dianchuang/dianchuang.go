package dianchuang

import (
	"bytes"
	"encoding/json"
	// "fmt"
	"io/ioutil"
	// "log"
	"net/http"
	// "net/url"
	"strconv"
	"strings"
	"time"

	"ad/util"
)

const (
	URL          = "http://m.ssp.kanking.net"
	BANNER_APID  = "10201"
	STARTUP_APID = "10202"
	FLOW_APID    = "10203"
)

func Dianchaung(getReq *util.ReqMsg, funcName string) util.ResMsg {
	return base(getReq, funcName)
}

func DianchuangStartUp(getReq *util.ReqMsg, funcName string) util.ResMsg {
	return util.ResMsg{}
}

func DianchaungFlow(getReq *util.ReqMsg, funcName string) util.ResMsg {
	return util.ResMsg{}
}

func base(getReq *util.ReqMsg, funcName string) util.ResMsg {
	reqData := Adreq{
		Apid: BANNER_APID,
		Device: Device{
			Devicetype: 1, //手机
			Osv:        getReq.Osversion,
			Mac:        strings.ToUpper(getReq.Mac), // 设备 MAC 地址（大写，保留冒号分隔符）
			Ip:         getReq.Ip,
			Ua:         getReq.Ua,
			Make:       getReq.Vendor,     // 设备制造商
			Model:      getReq.Model,      // 设备型号
			Ts:         time.Now().Unix(), //发送请求时的本地 UNIX 时间戳，以秒为单位
		},
	}
	// os
	// android
	reqData.Device.Os = 2                       // android
	reqData.Device.Androidid = getReq.Androidid // 安卓设备广告标识符
	reqData.Device.Idfa = ""                    // 苹果设备广告标识符
	reqData.Device.Openudid = ""                // iOS 版本 6 以下的操作系统提供 OpenUDID
	reqData.Device.Imei = getReq.Imei           // 设备 IMEI 号
	reqData.App.Name = "cn.ecook"               // app包名
	// ios
	if getReq.Os == "2" {
		reqData.Device.Os = 1 // ios
		reqData.Device.Androidid = ""
		reqData.Device.Idfa = getReq.Idfa
		reqData.Device.Openudid = getReq.Openudid
		reqData.Device.Imei = ""
		reqData.App.Name = "cn.ecook.ecook" // app包名
	}
	// 屏宽度转化
	w, err := strconv.Atoi(getReq.Screenwidth)
	if err == nil {
		reqData.Device.W = w
	}
	// 屏高度转化
	h, err := strconv.Atoi(getReq.Screenheight)
	if err == nil {
		reqData.Device.H = h
	}
	// 运营商类型，1：中国移动，2：中国联通，3：中国电信
	// android
	imsi, err := strconv.Atoi(getReq.Imsi)
	if err == nil {
		reqData.Device.Carrier = imsi
	}
	reqData.Device.Carrier = 1 //默认中国移动
	// 联网类型，1：以太网，2：wifi，3：蜂窝网络-未识别，4：蜂窝网络-2G，5：蜂窝网络：3G，6：蜂窝网络：4G
	network, err := strconv.Atoi(getReq.Network)
	if err == nil {
		reqData.Device.Connectiontype = network
	}
	reqData.Device.Connectiontype = 2 //默认wifi

	ma, err := json.Marshal(reqData)
	if err != nil {
		return util.ResMsg{}
	}
	// 请求广告
	req, err := http.NewRequest("POST", URL, bytes.NewBuffer(ma))
	if err != nil {
		return util.ResMsg{}
	}
	// 请求头设置
	req.Header.Set("Connection", "keep-alive")
	req.Header.Set("Content-Type", "application/json")
	resp, err := util.Client.Do(req)
	if err != nil {
		return util.ResMsg{}
	}

	data, err := ioutil.ReadAll(resp.Body)
	resp.Body.Close()
	if err != nil {
		return util.ResMsg{}
	}
	if resp.StatusCode != 200 {
		return util.ResMsg{}
	}
	// 返回格式化的广告请求返回值
	return FormateRes(data, getReq.Os, funcName)
}

// 根据广告平台的返回生成客户端返回
func FormateRes(res []byte, os, funcName string) util.ResMsg {
	resData := Adres{}
	json.Unmarshal(res, &resData)
	postData := util.ResMsg{
		Id:       "0",
		Weight:   0,
		State:    0,
		Title:    resData.Title,
		Content:  resData.Desc,
		ImageUrl: resData.Sourceurl,
		Uri:      resData.Ldp,
	}
	postData.Displayreport = resData.Iurl
	postData.Displayreport = append(postData.Displayreport, "http://120.26.247.23/ad/report.php?method="+funcName+"&event=displayreport&os="+os+"&t="+util.GetRandom())
	postData.Clickreport = resData.Curl
	postData.Clickreport = append(postData.Clickreport, "http://120.26.247.23/ad/report.php?method="+funcName+"&event=clickreport&os="+os+"&t="+util.GetRandom())

	return postData
}
