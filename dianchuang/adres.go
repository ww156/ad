package dianchuang

type Adres struct {
	Apid      string   `json:"apid"`
	Sourceurl string   `json:"sourceurl"`
	Title     string   `json:"title"`
	Desc      string   `json:"desc"`
	Action    int      `json:"action"`
	Type      int      `json:"type"`
	Iurl      []string `json:"iurl"`
	Curl      []string `json:"curl"`
	Ldp       string   `json:"ldp"`
	Adm       string   `json:"adm"`
}
