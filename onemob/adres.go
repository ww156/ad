package onemob

type adres struct {
	Cnf cnf
}

type cnf struct {
	Dgfly dgfly
}
type dgfly struct {
	Adtype    string
	HrefType  int
	Url       string
	Href      string
	S_dur     int
	Ratio     string
	H_ratio   int
	AdID      string
	Dest_key  string
	Dplnk     string
	Yoff      string
	S_rpt     []string
	C_rpt     []string
	D_rpt     []string
	Dc_rpt    []string
	I_rpt     []string
	A_rpt     []string
	Si        bool
	Ia        string
	Ci        string
	Rtp       bool
	Vsb       bool
	In_broser bool
	Cl        int
	Rmsol     bool
	Rtpl      bool
}

type adres_1 struct {
	HrefType int
	Adtype   string
	Url      string
	Href     string
	Rtp      bool
	Rtpl     bool
	Ratio    string
	H_ratio  string
	Dplink   string
	S_rtp    []string
	C_rtp    []string
	D_rtp    []string
	Dc_rtp   []string
	I_rtp    []string
	A_rtp    []string
}

type adres_2 struct {
	Showtype string
	Adtype   string
	Icon_img string
	Ad_img   []string
	Rtp      bool
	Rtpl     bool
	W        string
	H        string
	Dplink   string
	Name     string
	Desc     string
	Down_url string
	S_rpt    []string
	C_rpt    []string
	D_rpt    []string
	Dc_rpt   []string
	I_rpt    []string
	A_rpt    []string
}
