package onemob

import (
	// "bytes"
	// "encoding/json"
	// "fmt"
	"io/ioutil"
	// "log"
	"math/rand"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	// "time"
	// "unsafe"

	"ad/mongo"
	"ad/util"

	"github.com/PuerkitoBio/goquery"
	"github.com/pquerna/ffjson/ffjson"
)

const (
	URL   = "http://adapi.yiticm.com:7702/adv/dgfly"
	APPID = "AD000"
)

func Onemob(getReq *util.ReqMsg, funcName string) util.ResMsg {
	return base(getReq, funcName, "us")
}
func OnemobFlow(getReq *util.ReqMsg, funcName string) util.ResMsg {
	return base(getReq, funcName, "us")
}
func OnemobStartup(getReq *util.ReqMsg, funcName string) util.ResMsg {
	return base(getReq, funcName, "app")
}

func base(getReq *util.ReqMsg, funcName, adtype string) util.ResMsg {
	nt := "Mobile"
	if getReq.Network == "wifi" {
		nt = "WIFI"
	}
	imsi := "0"
	switch getReq.Imsi {
	case "1":
		imsi = "46000" + strconv.Itoa(10000+rand.Intn(90000)) + strconv.Itoa(10000+rand.Intn(90000))
	case "2":
		imsi = "46001" + strconv.Itoa(10000+rand.Intn(90000)) + strconv.Itoa(10000+rand.Intn(90000))
	case "3":
		imsi = "46002" + strconv.Itoa(10000+rand.Intn(90000)) + strconv.Itoa(10000+rand.Intn(90000))
	default:
		imsi = "0"
	}
	v := url.Values{}
	v.Set("cp", APPID)
	v.Set("pan", "api74tang")
	v.Set("did", getReq.Imei)
	v.Set("imsi", imsi)
	v.Set("aid", getReq.Androidid)
	v.Set("mac", getReq.Mac)
	v.Set("l", "zh")
	v.Set("action", adtype)
	v.Set("rel", getReq.Osversion)
	v.Set("brand", getReq.Vendor)
	v.Set("mdl", getReq.Model)
	v.Set("dm", getReq.Screenwidth+"x"+getReq.Screenheight)
	v.Set("client_ip", getReq.Ip)
	v.Set("client_ua", getReq.Ua)
	v.Set("nt", getReq.Network)
	v.Set("no", imsi)
	v.Set("n", nt)
	v.Set("an", "cn.ecook")
	if getReq.Os == "2" {
		v.Set("did", getReq.Idfa)
		v.Set("an", "en.cook.cook")
	}
	req, err := http.NewRequest("GET", URL+"?"+v.Encode(), nil)
	if err != nil {
		return util.ResMsg{}
	}

	resp, err := util.Client.Do(req)
	mongo.LogReq(funcName, getReq.Os)
	if err != nil {
		// fmt.Println(err.Error())
		mongo.LogTimeout(funcName, getReq.Os)
		return util.ResMsg{}
	}
	data, err := ioutil.ReadAll(resp.Body)
	resp.Body.Close()
	if err != nil {
		mongo.LogNo(funcName, getReq.Os)
		return util.ResMsg{}
	}
	// fmt.Println(resp.StatusCode)
	if resp.StatusCode != 200 {
		mongo.LogTimeout(funcName, getReq.Os)
		return util.ResMsg{}
	}
	return FormateRes(data, getReq.Os, funcName)
}

func FormateRes(res []byte, os, funcName string) util.ResMsg {
	resData := &adres{}
	ffjson.Unmarshal(res, resData)
	// fmt.Println(string(res))

	ad := resData.Cnf.Dgfly
	if ad.HrefType != 1 && ad.HrefType != 2 {
		mongo.LogNo(funcName, os)
		return util.ResMsg{}
	}
	uri := ad.Url
	if uri == "" {
		mongo.LogNo(funcName, os)
		return util.ResMsg{}
	}
	uri, err := url.QueryUnescape(uri)
	if err != nil {
		// fmt.Println(err.Error())
		mongo.LogNo(funcName, os)
		return util.ResMsg{}
	}
	doc, err := goquery.NewDocumentFromReader(strings.NewReader(uri))
	if err != nil {
		// fmt.Println(err.Error())
		mongo.LogNo(funcName, os)
		return util.ResMsg{}
	}
	img := doc.Find("img").AttrOr("src", "")
	if img == "" {
		mongo.LogNo(funcName, os)
		return util.ResMsg{}
	}
	// fmt.Println(img)
	postData := util.ResMsg{
		Id:       "0",
		Weight:   0,
		State:    1,
		Title:    "",
		Content:  "",
		ImageUrl: img,
		Uri:      ad.Href,
	}
	if ad.Rtp {
		v := url.Values{}
		v.Set("sv", `{"down_x":"IT_CLrt[K_PNT_DOWN_X","down_y":"IT_CLK_PNT_DOWN_Y","up_x":"IT_CLK_PNT_UP_X","up_y":"IT_CLK_PNT_UP_Y"}`)
		postData.Uri = ad.Href + "&" + v.Encode()
	}

	postData.Displayreport = ad.S_rpt
	// fmt.Println(ad)
	postData.Displayreport = append(postData.Displayreport, "http://120.26.247.23/ad/report.php?method="+funcName+"&event=displayreport&os="+os+"&t="+util.GetRandom())
	postData.Clickreport = ad.C_rpt
	postData.Clickreport = append(postData.Clickreport, "http://120.26.247.23/ad/report.php?method="+funcName+"&event=clickreport&os="+os+"&t="+util.GetRandom())

	return postData
}
