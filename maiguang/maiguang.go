package maiguang

import (
	"bytes"
	// "encoding/json"
	// "fmt"
	"io/ioutil"
	// "log"
	"net/http"
	// "net/url"
	"strconv"
	"strings"
	// "math/rand"
	"time"
	// "unsafe"
	// "regexp"

	"ad/mongo"
	"ad/util"
	"github.com/pquerna/ffjson/ffjson"
)

const (
	DEV_URL = "http://vs.maimob.net/index.php/VSAPI/HB"
	// PRO_URL              = "http://sspapi.ilast.cc/v/ra"
	PRO_URL              = "http://sspapi.ilast.cc/v/tra"
	APPID_ANDROID        = "36914825a"
	ADID_ANDROID         = "8MRAelTw"
	ADID_ANDROID_FLOW    = "9GJoerUL"
	ADID_ANDROID_SYARTUP = "panspWyu"
	APPID_IOS            = "e5e9c8482"
	ADID_IOS             = "Dexid74L"
	ADID_IOS_FLOW        = "aILAEaCq"
	ADID_IOS_STARTUP     = "7i7iYghK"
)

func Maiguang(getReq *util.ReqMsg, funcName string) util.ResMsg {
	if getReq.Os == "1" {
		return base(getReq, funcName, ADID_ANDROID, 1)
	}
	return base(getReq, funcName, ADID_IOS, 1)
}

func MaiguangFlow(getReq *util.ReqMsg, funcName string) util.ResMsg {
	if getReq.Os == "1" {
		return base(getReq, funcName, ADID_ANDROID_FLOW, 4)
	}
	return base(getReq, funcName, ADID_IOS_FLOW, 4)
}

func MaiguangStartup(getReq *util.ReqMsg, funcName string) util.ResMsg {
	if getReq.Os == "1" {
		return base(getReq, funcName, ADID_ANDROID_SYARTUP, 2)
	}
	return base(getReq, funcName, ADID_IOS_STARTUP, 2)
}

func base(getReq *util.ReqMsg, funcName, adid string, adtype int) util.ResMsg {
	if getReq.Os == "2" {
		return util.ResMsg{}
	}
	// 请求参数
	switch getReq.Imsi {
	case "1":
		getReq.Imsi = "46000"
	case "2":
		getReq.Imsi = "46001"
	case "3":
		getReq.Imsi = "46002"
	default:
		getReq.Imsi = "46000"
	}
	network := 0
	switch getReq.Network {
	case "wifi":
		network = 1
	case "2g":
		network = 2
	case "3g":
		network = 3
	case "4g":
		network = 4
	default:
		network = 0
	}
	sd, err := strconv.Atoi(getReq.Sd)
	dip := strconv.Itoa(sd / 160)

	ts := strconv.Itoa(int(time.Now().Unix()))
	dt := 1
	dtstr := "1"
	appid := APPID_ANDROID
	dtv := getReq.Imei
	if getReq.Os == "2" {
		dt = 2
		dtstr = "2"
		dtv = getReq.Idfa
		appid = APPID_IOS
	}
	sign := util.Md5("ra" + appid + "2200" + ts + dtstr + dtv + getReq.Screenwidth + getReq.Screenheight)

	postData := adreq{
		Action:      "ra",
		Appid:       appid,
		Ver:         "2200",
		Tp:          ts,
		Is:          getReq.Imsi,
		Dt:          dt,
		Dtv:         dtv,
		W:           getReq.Screenwidth,
		H:           getReq.Screenheight,
		Brand:       getReq.Vendor,
		Mod:         getReq.Model,
		Ov:          getReq.Osversion,
		Nt:          network,
		Mac:         getReq.Mac,
		Lid:         adid,
		Pt:          adtype,
		Adrid:       getReq.Androidid,
		Ua:          getReq.Ua,
		Dip:         dip,
		Ip:          getReq.Ip,
		Lon:         getReq.Lng,
		Lat:         getReq.Lng,
		Density:     getReq.Sd,
		Orientation: "0",
		Dl:          1,
		Sign:        sign,
	}

	ma, err := ffjson.Marshal(postData)
	if err != nil {
		return util.ResMsg{}
	}
	req, err := http.NewRequest("POST", PRO_URL, bytes.NewReader(ma))
	if err != nil {
		return util.ResMsg{}
	}
	req.Header.Set("Connection", "keep-alive")
	req.Header.Set("Content-Type", "application/json;charset=utf-8")
	resp, err := util.Client.Do(req)
	mongo.LogReq(funcName, getReq.Os)
	if err != nil {
		mongo.LogTimeout(funcName, getReq.Os)
		return util.ResMsg{}
	}
	data, err := ioutil.ReadAll(resp.Body)
	resp.Body.Close()
	if err != nil {
		mongo.LogNo(funcName, getReq.Os)
		return util.ResMsg{}
	}
	if resp.StatusCode != 200 {
		mongo.LogTimeout(funcName, getReq.Os)
		return util.ResMsg{}
	}
	return FormateRes(data, getReq.Os, funcName)
}

func FormateRes(res []byte, os, funcName string) util.ResMsg {
	resData := &adres{}
	ffjson.Unmarshal(res, resData)
	// fmt.Println(string(res))
	if resData.ResultCode != 100 {
		mongo.LogNo(funcName, os)
		return util.ResMsg{}
	}
	if len(resData.Data) == 0 {
		mongo.LogNo(funcName, os)
		return util.ResMsg{}
	}
	ad := resData.Data[0]
	postData := util.ResMsg{
		Id:       "0",
		Weight:   0,
		State:    0,
		Title:    ad.Title,
		Content:  ad.Desc,
		ImageUrl: ad.Img,
		Uri:      ad.Url,
	}

	for _, value := range ad.Cb.Pv {
		value = strings.Replace(value, "%%DOWNX%%", "IT_CLK_PNT_DOWN_X", -1)
		value = strings.Replace(value, "%%DOWNY%%", "IT_CLK_PNT_DOWN_Y", -1)
		value = strings.Replace(value, "%%UPX%%", "IT_CLK_PNT_UP_X", -1)
		value = strings.Replace(value, "%%UPY%%", "IT_CLK_PNT_UP_Y", -1)
		postData.Displayreport = append(postData.Displayreport, value)
	}
	postData.Displayreport = append(postData.Displayreport, "http://120.26.247.23/ad/report.php?method="+funcName+"&event=displayreport&os="+os+"&t="+util.GetRandom())
	for _, value := range ad.Cb.C {
		value = strings.Replace(value, "%%DOWNX%%", "IT_CLK_PNT_DOWN_X", -1)
		value = strings.Replace(value, "%%DOWNY%%", "IT_CLK_PNT_DOWN_Y", -1)
		value = strings.Replace(value, "%%UPX%%", "IT_CLK_PNT_UP_X", -1)
		value = strings.Replace(value, "%%UPY%%", "IT_CLK_PNT_UP_Y", -1)
		postData.Clickreport = append(postData.Clickreport, value)
	}
	postData.Clickreport = append(postData.Clickreport, "http://120.26.247.23/ad/report.php?method="+funcName+"&event=clickreport&os="+os+"&t="+util.GetRandom())

	return postData
}
