package maiguang

type adres struct {
	ResultCode int
	Msg        string
	Data       []Ad
}

type Ad struct {
	Aid      int
	Name     string
	Title    string
	Desc     string
	Img      string
	Url      string
	Deeplink string
	Icon     string
	Category int
	Pk       string
	FileSize float64
	Type     int
	Pt       int
	Page     string
	Imgw     int
	Imgh     int
	Et       int
	Cb       cb
}

type cb struct {
	Pv []string
	C  []string
	Ds []string
	De []string
	Ie []string
	A  []string
}
