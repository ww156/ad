package maiguang

type adreq struct {
	Action      string `json:"action"`
	Appid       string `json:"appid"`
	Ver         string `json:"ver"`
	Tp          string `json:"tp"`
	Is          string `json:"is"` // imsi
	Dt          int    `json:"dt"`
	Dtv         string `json:"dtv"`
	Ic          string `json:"ic"`
	W           string `json:"w"`
	H           string `json:"h"`
	Brand       string `json:"brand"`
	Mod         string `json:"mod"`
	Os          string `json:"os"`
	Ov          string `json:"ov"`
	SdkVersion  string `json:"sdkVersion"`
	Mcc         string `json:"mcc"`
	Mnc         string `json:"mnc"`
	Lac         string `json:"lac"`
	Cid         string `json:"cid"`
	Nt          int    `json:"nt"`
	Mac         string `json:"mac"`
	Lid         string `json:"lid"`
	Pt          int    `json:"int"`
	Pl          string `json:"pl"`
	Adrid       string `json:"adrid"`
	Adnum       int    `json:"adnum"`
	Ua          string `json:"ua"`
	Dip         string `json:"dip"`
	Ip          string `json:"ip"`
	Aaid        string `json:"aaid"`
	Lon         string `json:"lon"`
	Lat         string `json:"lat"`
	Density     string `json:"density"`
	Orientation string `json:"orientation"`
	Brk         int    `json:"brk"`
	Dl          int    `json:"dl"`
	Bssid       string `json:"bssid"`
	Sign        string `json:"sign"`
}
