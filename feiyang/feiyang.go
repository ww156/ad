package feiyang

import (
	// "bytes"
	// "encoding/json"
	"fmt"
	"io/ioutil"
	// "log"
	"math/rand"
	"net/http"
	"net/url"
	"strconv"
	// "strings"
	// "time"
	// "unsafe"

	"ad/mongo"
	"ad/util"
	"github.com/pquerna/ffjson/ffjson"
)

// 网上厨房（andriod）
// ID：202092540523
// Appkey：0f63a352be927885265229ef6b3d81e7
// 广告位：
// 原生：1520209254052392461
// Banner：120209254052389296
// 开屏：520209254052320214
//
// 网上厨房（IOS）
// ID：202019340524
// Appkey：0c2575d84aae48923e187ca9a83fbca6
// 广告位：
// 原生：1520201934052473813
// Banner：120201934052490581
// 开屏：520201934052454537

const (
	URL      = "http://sspapi.adflying.com/v1/mview.php?"
	APPID_A  = "202092540523"
	APPKEY_A = "0f63a352be927885265229ef6b3d81e7"
	ADID_A   = "120209254052389296"
	ADID_A_F = "1520209254052392461"
	ADID_A_S = "520209254052320214"
	APPID_I  = "202019340524"
	APPKEY_I = "0c2575d84aae48923e187ca9a83fbca6"
	ADID_I   = "120201934052490581"
	ADID_I_F = "1520201934052473813"
	ADID_I_S = "520201934052454537"
)

func Feiyang(getReq *util.ReqMsg, funcName string) util.ResMsg {
	if getReq.Os == "1" {
		return base(getReq, funcName, ADID_A)
	}
	return base(getReq, funcName, ADID_I)
}

func FeiyangFlow(getReq *util.ReqMsg, funcName string) util.ResMsg {
	if getReq.Os == "1" {
		return base(getReq, funcName, ADID_A_F)
	}
	return base(getReq, funcName, ADID_I_F)
}

func FeiyangStartup(getReq *util.ReqMsg, funcName string) util.ResMsg {
	if getReq.Os == "1" {
		return base(getReq, funcName, ADID_A_S)
	}
	return base(getReq, funcName, ADID_I_S)
}

func base(getReq *util.ReqMsg, funcName, adid string) util.ResMsg {
	imsi := "0"
	operator := "0"
	switch getReq.Imsi {
	case "1":
		operator = "1"
		imsi = "46000" + strconv.Itoa(10000+rand.Intn(90000)) + strconv.Itoa(10000+rand.Intn(90000))
	case "2":
		operator = "2"
		imsi = "46001" + strconv.Itoa(10000+rand.Intn(90000)) + strconv.Itoa(10000+rand.Intn(90000))
	case "3":
		operator = "3"
		imsi = "46002" + strconv.Itoa(10000+rand.Intn(90000)) + strconv.Itoa(10000+rand.Intn(90000))
	default:
		operator = "0"
	}
	network := "0"
	switch getReq.Network {
	case "wifi":
		network = "1"
	case "2g":
		network = "3"
	case "3g":
		network = "4"
	case "4g":
		network = "5"
	default:
		network = "0"
	}
	appid := APPID_A
	sign := util.Md5(appid + APPKEY_A + adid)
	if getReq.Os == "2" {
		appid = APPID_I
		sign = util.Md5(appid + APPKEY_I + adid)
	}
	v := url.Values{}
	v.Set("appid", appid)
	v.Set("posid", adid)
	v.Set("sign", sign)
	v.Set("bid", "cn.ecook")
	v.Set("adcount", "1")
	v.Set("mac", getReq.Mac)
	v.Set("sw", getReq.Screenwidth)
	v.Set("sh", getReq.Screenheight)
	v.Set("remoteip", getReq.Ip)
	v.Set("brand", getReq.Vendor)
	v.Set("model", getReq.Model)
	v.Set("os", "android")
	v.Set("asver", getReq.Osversion)
	v.Set("imei", getReq.Imei)
	v.Set("aid", getReq.Androidid)
	v.Set("idfa", getReq.Idfa)
	v.Set("imsi", imsi)
	v.Set("dpi", getReq.Sd)
	v.Set("net", network)
	v.Set("isp", operator)
	v.Set("co", "CN")
	v.Set("lang", "zh")
	v.Set("or", "1")
	if getReq.Os == "2" {
		v.Set("bid", "cn.ecook.ecook")
		v.Set("os", "ios")
	}

	req, err := http.NewRequest("GET", URL+v.Encode(), nil)
	if err != nil {
		return util.ResMsg{}
	}

	resp, err := util.Client.Do(req)
	mongo.LogReq(funcName, getReq.Os)
	if err != nil {
		fmt.Println(err.Error())
		mongo.LogTimeout(funcName, getReq.Os)
		return util.ResMsg{}
	}
	data, err := ioutil.ReadAll(resp.Body)
	resp.Body.Close()
	if resp.StatusCode != 200 {
		mongo.LogTimeout(funcName, getReq.Os)
		return util.ResMsg{}
	}

	if err != nil {
		mongo.LogNo(funcName, getReq.Os)
		return util.ResMsg{}
	}
	return FormateRes(data, getReq.Os, funcName)
}

func FormateRes(res []byte, os, funcName string) util.ResMsg {
	resData := &adres{}
	ffjson.Unmarshal(res, resData)
	fmt.Println(string(res))
	if resData.IntError != 0 {
		mongo.LogNo(funcName, os)
		return util.ResMsg{}
	}
	if len(resData.ObjAds) == 0 {
		mongo.LogNo(funcName, os)
		return util.ResMsg{}
	}
	ad := resData.ObjAds[0]
	postData := util.ResMsg{
		Id:       "0",
		Weight:   0,
		State:    0,
		Title:    ad.StrTitle,
		Content:  ad.StrContent,
		ImageUrl: ad.StrImageUrl,
		Uri:      "",
	}
	v := url.Values{}
	v.Set("s", `{"down_x":"IT_CLK_PNT_DOWN_X","down_y":"IT_CLK_PNT_DOWN_Y","up_x":"IT_CLK_PNT_UP_X","up_y":"IT_CLK_PNT_UP_Y"}`)
	postData.Uri = ad.StrLinkUrl + "&" + v.Encode()

	postData.Displayreport = ad.ArrViewTrackUrl
	postData.Displayreport = append(postData.Displayreport, "http://120.26.247.23/ad/report.php?method="+funcName+"&event=displayreport&os="+os+"&t="+util.GetRandom())
	postData.Clickreport = ad.ArrClickTrackUrl
	postData.Clickreport = append(postData.Clickreport, "http://120.26.247.23/ad/report.php?method="+funcName+"&event=clickreport&os="+os+"&t="+util.GetRandom())

	return postData
}
