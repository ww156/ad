package feiyang

type adres struct {
	StrApiVersion string
	IntError      int
	StrMsg        string
	IntAdType     int
	IntAdCount    int
	ObjAds        []ad
}

type ad struct {
	StrAdId               string
	ArrViewTrackUrl       []string
	ArrClickTrackUrl      []string
	ArrDownloadTrackUrl   []string
	ArrDownloadedTrackUrl []string
	ArrInstalledTrackUrl  []string
	IntStuffType          int
	StrTitle              string
	StrContent            string
	StrIconImageUrl       string
	StrImageUrl           string
	StrLinkUrl            string
	StrStuffId            string
	StrStuffWidth         string
	StrStuffHeight        string
	IntFullScreen         int
	IntActionType         int
}
