package adhub

type adreq struct {
	Version   string       `json:"version"`
	SrcType   int          `json:"srcType"`
	ReqType   int          `json:"reqType"`
	TimeStamp int64        `json:"timeStamp"`
	Appid     string       `json:"appid"`
	DevInfo   *DevInfo     `json:"devInfo,omitempty"`
	EnvInfo   *EnvInfo     `json:"envInfo,omitempty"`
	AdReqInfo []*AdReqInfo `json:"adReqInfo,omitempty"`
}

type DevInfo struct {
	SdkUID     string   `json:"sdkUID"`
	Imei       string   `json:"imei"`
	Idfa       string   `json:"idfa"`
	Mac        string   `json:"mac"`
	Phone      []string `json:"phone"`
	Os         string   `json:"os"`
	Platform   int      `json:"platform"`
	DevType    int      `json:"devType"`
	Brand      string   `json:"brand"`
	Model      string   `json:"model"`
	Resolution string   `json:"resolution"`
	ScreenSize string   `json:"screenSize"`
	Language   string   `json:"language"`
	Density    string   `json:"density"`
	Idfv       string   `json:"idfv"`
	AndroidID  string   `json:"androidID"`
	OpenUDID   string   `json:"openUDID"`
}

type EnvInfo struct {
	Net       int    `json:"net"`
	Isp       int    `json:"isp"`
	Ip        string `json:"ip"`
	Geo       *Geo   `json:"geo"`
	UserAgent string `json:"userAgent"`
	Age       int    `json:"age"`
	Yob       int    `json:"yob"`
	Gender    int    `json:"gender"`
	Income    int    `json:"income"`
}

type Geo struct {
	Longitude string `json:"longitude"`
	Latitude  string `json:"latitude"`
}

type AdReqInfo struct {
	SpaceID      string   `json:"spaceID"`
	SpaceParam   string   `json:"spaceparam"`
	ScreenStatus int      `json:"screenStatus"`
	UserTags     []string `json:"userTags"`
}
