package adhub

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"
	"time"

	"ad/mongo"
	"ad/util"
)

const (
	URL                     = "http://api.htp.hubcloud.com.cn:45600/json/v1/sdk0"
	APPID_ANDROID           = "262"
	SPACEID_ANDROID         = "853"
	SPACEID_ANDROID_FLOW    = "855"
	SPACEID_ANDROID_STARTUP = "854"
	APPID_IOS               = "261"
	SPACEID_IOS             = "852"
	SPACEID_IOS_FLOW        = "857"
	SPACEID_IOS_STARTUP     = "856"
)

func Adhub(getReq *util.ReqMsg, funcName string) util.ResMsg {
	if getReq.Os == "1" {
		return base(getReq, funcName, SPACEID_ANDROID)
	}
	return base(getReq, funcName, SPACEID_IOS)
}

func AdhubFlow(getReq *util.ReqMsg, funcName string) util.ResMsg {
	if getReq.Os == "1" {
		return base(getReq, funcName, SPACEID_ANDROID_FLOW)
	}
	return base(getReq, funcName, SPACEID_IOS_FLOW)
}
func AdhubStartup(getReq *util.ReqMsg, funcName string) util.ResMsg {
	if getReq.Os == "1" {
		return base(getReq, funcName, SPACEID_ANDROID_STARTUP)
	}
	return base(getReq, funcName, SPACEID_IOS_STARTUP)
}

func base(getReq *util.ReqMsg, funcName, spaceid string) util.ResMsg {
	sd, err := strconv.Atoi(getReq.Sd)
	dpi := sd / 160
	network := 0
	switch getReq.Network {
	case "wifi":
		network = 4
	case "2g":
		network = 6
	case "3g":
		network = 1
	case "4g":
		network = 2
	case "5g":
		network = 3
	default:
		network = 0
	}
	carrier := 1
	switch getReq.Imsi {
	case "1":
		carrier = 1
	case "2":
		carrier = 2
	case "3":
		carrier = 3
	default:
		carrier = 0
	}
	postData := adreq{
		Version:   "1.2.4",
		SrcType:   1,
		ReqType:   1,
		TimeStamp: time.Now().Unix(),
		Appid:     APPID_ANDROID,
		DevInfo: &DevInfo{
			SdkUID:     util.Md5(getReq.Ip),
			Imei:       getReq.Imei,
			Idfa:       getReq.Idfa,
			Mac:        getReq.Mac,
			Os:         getReq.Osversion,
			Platform:   2,
			DevType:    1,
			Brand:      getReq.Vendor,
			Model:      getReq.Model,
			Resolution: getReq.Screenwidth + "_" + getReq.Screenheight,
			Language:   "zh-CN",
			Density:    strconv.Itoa(dpi),
			AndroidID:  getReq.Androidid,
		},
		EnvInfo: &EnvInfo{
			Net:       network,
			Isp:       carrier,
			Ip:        getReq.Ip,
			UserAgent: getReq.Ua,
		},
		AdReqInfo: []*AdReqInfo{
			{
				SpaceID: spaceid,
			},
		},
	}

	if getReq.Os == "2" {
		postData.Appid = APPID_IOS
		postData.DevInfo.Platform = 1
	}

	ma, err := json.Marshal(&postData)
	if err != nil {
		return util.ResMsg{}
	}
	header := http.Header{}
	header.Set("Connection", "keep-alive")
	header.Set("Content-Type", "application/json;charset=utf-8")
	resp, err := util.HttpPOST(URL, &header, ma)
	mongo.LogReq(funcName, getReq.Os)
	if err != nil {
		mongo.LogTimeout(funcName, getReq.Os)
		util.HttpRequestCount.WithLabelValues(funcName + "timeouterr").Inc()
		return util.ResMsg{}
	}
	data, err := ioutil.ReadAll(resp.Body)
	resp.Body.Close()
	if err != nil {
		mongo.LogNo(funcName, getReq.Os)
		util.HttpRequestCount.WithLabelValues(funcName + "readresperr").Inc()
		return util.ResMsg{}
	}
	if resp.StatusCode != 200 {
		mongo.LogTimeout(funcName, getReq.Os)
		util.HttpRequestCount.WithLabelValues(funcName + "not200err").Inc()
		return util.ResMsg{}
	}
	return FormateRes(data, getReq.Os, funcName, getReq.Screenwidth, getReq.Screenheight)
}

func FormateRes(res []byte, os, funcName, w, h string) util.ResMsg {
	resData := &adres{}
	json.Unmarshal(res, resData)
	// fmt.Printf("%+v", resData)
	if resData.Status != 0 {
		mongo.LogNo(funcName, os)
		util.HttpRequestCount.WithLabelValues(funcName + "noerr").Inc()
		return util.ResMsg{}
	}
	if len(resData.SpaceInfo) == 0 {
		mongo.LogNo(funcName, os)
		util.HttpRequestCount.WithLabelValues(funcName + "noerr").Inc()
		return util.ResMsg{}
	}

	spaceInfo := resData.SpaceInfo[0]
	if len(spaceInfo.AdResponse) == 0 {
		mongo.LogNo(funcName, os)
		util.HttpRequestCount.WithLabelValues(funcName + "noerr").Inc()
		return util.ResMsg{}
	}
	ads := spaceInfo.AdResponse
	if len(ads) == 0 {
		mongo.LogNo(funcName, os)
		util.HttpRequestCount.WithLabelValues(funcName + "noerr").Inc()
		return util.ResMsg{}
	}
	ad := ads[0]
	contentInfo := ad.ContentInfo
	if len(contentInfo) == 0 {
		mongo.LogNo(funcName, os)
		util.HttpRequestCount.WithLabelValues(funcName + "noerr").Inc()
		return util.ResMsg{}
	}

	interactInfo := ad.InteractInfo
	postData := util.ResMsg{
		Id:       "0",
		Weight:   0,
		State:    0,
		Title:    "",
		Content:  "",
		ImageUrl: "",
		Uri:      interactInfo.LandingPageUrl,
	}
	if contentInfo[0].RenderType == 4 {
		template := contentInfo[0].Template
		template = strings.Replace(template, "\n", "", -1)
		tmp := Template{}
		err := json.Unmarshal([]byte(template), &tmp)
		if err != nil {
			fmt.Println(err)
		}

		postData.Title = tmp.Headline
		desc := tmp.Texts
		if len(desc) == 0 {
			// fmt.Println(8)
			mongo.LogNo(funcName, os)
			util.HttpRequestCount.WithLabelValues(funcName + "noerr").Inc()
			return util.ResMsg{}
		}
		postData.Content = desc[0]
		images := tmp.Images
		if len(images) == 0 {
			// fmt.Println(6)
			mongo.LogNo(funcName, os)
			util.HttpRequestCount.WithLabelValues(funcName + "noerr").Inc()
			return util.ResMsg{}
		}
		if images[0] == "" {
			// fmt.Println(7)
			mongo.LogNo(funcName, os)
			util.HttpRequestCount.WithLabelValues(funcName + "noerr").Inc()
			return util.ResMsg{}
		}
		postData.ImageUrl = images[0]
	} else {
		adContent := contentInfo[0].AdcontentSlot

		postData.ImageUrl = adContent[0].Content
	}

	width, err := strconv.Atoi(w)
	if err != nil {
		width = 0
	}
	height, err := strconv.Atoi(h)
	if err != nil {
		height = 0
	}
	pos := util.CreatePos(width, height)
	hubdetec := interactInfo.HubDectInfo
	display := hubdetec.ViewUrl
	if display != "" {
		display = strings.Replace(display, ".AD_CLK_PT_DOWN_X.", strconv.Itoa(pos[0]), -1)
		display = strings.Replace(display, ".AD_CLK_PT_DOWN_Y.", strconv.Itoa(pos[1]), -1)
		display = strings.Replace(display, ".AD_CLK_PT_UP_X.", strconv.Itoa(pos[2]), -1)
		display = strings.Replace(display, ".AD_CLK_PT_UP_Y.", strconv.Itoa(pos[3]), -1)
		display = strings.Replace(display, ".SCRN_CLK_PT_DOWN_X.", strconv.Itoa(pos[0]), -1)
		display = strings.Replace(display, ".SCRN_CLK_PT_DOWN_Y.", strconv.Itoa(pos[1]), -1)
		display = strings.Replace(display, ".SCRN_CLK_PT_UP_X.", strconv.Itoa(pos[2]), -1)
		display = strings.Replace(display, ".SCRN_CLK_PT_UP_Y.", strconv.Itoa(pos[3]), -1)
		display = strings.Replace(display, ".UTC_TS.", strconv.Itoa(int(time.Now().Unix())), -1)
		postData.Displayreport = append(postData.Displayreport, display)

	}

	click := hubdetec.ClickUrl
	if click != "" {
		click = strings.Replace(click, ".AD_CLK_PT_DOWN_X.", strconv.Itoa(pos[0]), -1)
		click = strings.Replace(click, ".AD_CLK_PT_DOWN_Y.", strconv.Itoa(pos[1]), -1)
		click = strings.Replace(click, ".AD_CLK_PT_UP_X.", strconv.Itoa(pos[2]), -1)
		click = strings.Replace(click, ".AD_CLK_PT_UP_Y.", strconv.Itoa(pos[3]), -1)
		click = strings.Replace(click, ".SCRN_CLK_PT_DOWN_X.", strconv.Itoa(pos[0]), -1)
		click = strings.Replace(click, ".SCRN_CLK_PT_DOWN_Y.", strconv.Itoa(pos[1]), -1)
		click = strings.Replace(click, ".SCRN_CLK_PT_UP_X.", strconv.Itoa(pos[2]), -1)
		click = strings.Replace(click, ".SCRN_CLK_PT_UP_Y.", strconv.Itoa(pos[3]), -1)
		click = strings.Replace(click, ".UTC_TS.", strconv.Itoa(int(time.Now().Unix())), -1)
		postData.Clickreport = append(postData.Clickreport, click)
	}

	third := interactInfo.ThirdpartInfo
	for _, value := range third {
		display := value.ViewUrl
		display = strings.Replace(display, ".AD_CLK_PT_DOWN_X.", strconv.Itoa(pos[0]), -1)
		display = strings.Replace(display, ".AD_CLK_PT_DOWN_Y.", strconv.Itoa(pos[1]), -1)
		display = strings.Replace(display, ".AD_CLK_PT_UP_X.", strconv.Itoa(pos[2]), -1)
		display = strings.Replace(display, ".AD_CLK_PT_UP_Y.", strconv.Itoa(pos[3]), -1)
		display = strings.Replace(display, ".SCRN_CLK_PT_DOWN_X.", strconv.Itoa(pos[0]), -1)
		display = strings.Replace(display, ".SCRN_CLK_PT_DOWN_Y.", strconv.Itoa(pos[1]), -1)
		display = strings.Replace(display, ".SCRN_CLK_PT_UP_X.", strconv.Itoa(pos[2]), -1)
		display = strings.Replace(display, ".SCRN_CLK_PT_UP_Y.", strconv.Itoa(pos[3]), -1)
		display = strings.Replace(display, ".UTC_TS.", strconv.Itoa(int(time.Now().Unix())), -1)
		postData.Displayreport = append(postData.Displayreport, display)

		click := value.ClickUrl
		click = strings.Replace(click, ".AD_CLK_PT_DOWN_X.", strconv.Itoa(pos[0]), -1)
		click = strings.Replace(click, ".AD_CLK_PT_DOWN_Y.", strconv.Itoa(pos[1]), -1)
		click = strings.Replace(click, ".AD_CLK_PT_UP_X.", strconv.Itoa(pos[2]), -1)
		click = strings.Replace(click, ".AD_CLK_PT_UP_Y.", strconv.Itoa(pos[3]), -1)
		click = strings.Replace(click, ".SCRN_CLK_PT_DOWN_X.", strconv.Itoa(pos[0]), -1)
		click = strings.Replace(click, ".SCRN_CLK_PT_DOWN_Y.", strconv.Itoa(pos[1]), -1)
		click = strings.Replace(click, ".SCRN_CLK_PT_UP_X.", strconv.Itoa(pos[2]), -1)
		click = strings.Replace(click, ".SCRN_CLK_PT_UP_Y.", strconv.Itoa(pos[3]), -1)
		click = strings.Replace(click, ".UTC_TS.", strconv.Itoa(int(time.Now().Unix())), -1)
		postData.Clickreport = append(postData.Clickreport, click)
	}

	postData.Displayreport = append(postData.Displayreport, "http://api.htp.hubcloud.com.cn:45600/vw?extInfo="+ad.ExtInfo)
	postData.Displayreport = append(postData.Displayreport, "http://120.26.247.23/ad/report.php?method="+funcName+"&event=displayreport&os="+os+"&t="+util.GetRandom())
	postData.Clickreport = append(postData.Clickreport, "http://api.htp.hubcloud.com.cn:45600/ck?extInfo=XX"+ad.ExtInfo)
	postData.Clickreport = append(postData.Clickreport, "http://120.26.247.23/ad/report.php?method="+funcName+"&event=clickreport&os="+os+"&t="+util.GetRandom())

	return postData
}
