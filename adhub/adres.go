package adhub

type adres struct {
	Status    int
	Errcode   string
	Errmsg    string
	SpaceInfo []SpaceInfo
}

type SpaceInfo struct {
	SpaceID         string
	SpaceParam      string
	AdpType         int
	RefreshInterval int
	ScreenDirection int
	Width           string
	Height          string
	AdpPosiotion    AdpPosition
	AutoClose       bool
	MaxTime         int
	ManaulClose     bool
	MinTime         int
	WifiPreload     bool
	VideoWifiOnly   bool
	Mute            bool
	FullScreen      bool
	AutoPlay        bool
	AdResponse      []Ad
}

type AdpPosition struct {
	X string
	Y string
}

type Ad struct {
	ExtInfo      string
	AdnInfo      AdnInfo
	ContentInfo  []AdContentInfo
	InteractInfo AdInteractInfo
}

type AdnInfo struct {
	AndID    string
	SpaceID  string
	AdnParam string
}

type AdContentInfo struct {
	RenderType    int
	Size          int
	AdcontentSlot []AdContentSlot
	Template      string
	Md5           string
	Content       string
	PlayTime      int
}

type AdInteractInfo struct {
	HubDectInfo    DetectInfo
	ThirdpartInfo  []DetectInfo
	LandingPageUrl string
	PhoneNumber    string
}

type DetectInfo struct {
	ViewUrl    string
	ClickUrl   string
	ConvertUrl string
}

type AdContentSlot struct {
	Md5      string
	Content  string
	PlayTime int
}

type Template struct {
	Headline string
	Texts    []string
	Images   []string
	Action   string
	Videos   []string
}
