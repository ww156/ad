package mongo

import (
	"ad/util"
	"bytes"
	// "fmt"

	"github.com/bradfitz/gomemcache/memcache"
)

// 初始化memcache连接
var mc = memcache.New("10.28.143.99:11211")

func LogReq(name, os string) error {
	return logState(name, os, "request")
}
func LogYes(name, os string) error {
	return logState(name, os, "yes")
}
func LogNo(name, os string) error {
	return logState(name, os, "no")
}
func LogTimeout(name, os string) error {
	return logState(name, os, "timeout")
}
func logState(name, os, state string) error {
	if name == "" {
		return nil
	}
	time := util.FormateYMDH()
	// memcache的key
	key := name + "_" + os + "_" + time + "_" + state
	// memcache的标示key拼接的字符串
	keyStr := time + "_AdStr"
	// key是否存在
	if util.IsExist(key) {
		_, err := mc.Increment(key, 1)
		if err == memcache.ErrCacheMiss {
			item := memcache.Item{Key: key, Value: []byte("1"), Flags: 1, Expiration: 604800}
			err = mc.Set(&item)
			if err != nil {
				// delete(util.MemCache, key)
				util.DelCache(key)
			}
		}
	} else {
		//	获取存储key的字符串*_Adstr
		adstrItem, err := mc.Get(keyStr)
		if err == nil {
			adstr := adstrItem.Value
			str := append(adstr, []byte(",")...)
			if bytes.Contains(str, []byte(name+",")) {
				_, err := mc.Increment(key, 1)
				if err == memcache.ErrCacheMiss {
					item := memcache.Item{Key: key, Value: []byte("1"), Flags: 1, Expiration: 604800}
					err = mc.Set(&item)
					if err != nil {
						// delete(util.MemCache, key)
						util.DelCache(key)
					}
				}
			} else {
				adstrItem.Value = []byte(string(adstr) + "," + name)
				err := mc.CompareAndSwap(adstrItem)
				if err != nil {
					// delete(util.MemCache, key)
					util.DelCache(key)
				}

				item := memcache.Item{Key: key, Value: []byte("1"), Flags: 1, Expiration: 604800}
				err = mc.Set(&item)
				if err != nil {
					// delete(util.MemCache, key)
					util.DelCache(key)
				}
			}
		} else if err == memcache.ErrCacheMiss {
			adstrItem := memcache.Item{Key: keyStr, Value: []byte(name), Flags: 1, Expiration: 604800}
			err = mc.Set(&adstrItem)
			if err != nil {
				// delete(util.MemCache, key)
				util.DelCache(key)
			}

			item := memcache.Item{Key: key, Value: []byte("1"), Flags: 1, Expiration: 604800}
			err = mc.Set(&item)
			if err != nil {
				// delete(util.MemCache, key)
				util.DelCache(key)
			}
		}
	}
	return nil
}
