package mongo

// import (
// 	"fmt"
// 	// "log"
// 	// "net/url"
// 	"strconv"

// 	"gopkg.in/mgo.v2"
// 	"gopkg.in/mgo.v2/bson"

// 	"ad/util"
// )

// const (
// 	PRODUCTURL = "mongodb://supper:supWDxsf67%25H@10.30.195.62:27028"
// 	DEVELOPURL = "mongodb://127.0.0.1:27017"
// )

// var (
// 	globalMs *mgo.Session
// )

// type DbData struct {
// 	Os            int
// 	Request       int
// 	Yes           int
// 	No            int
// 	Timeout       int
// 	Displayreport int
// 	Clickreport   int
// 	Time          string
// 	Name          string
// }

// func init() {
// 	// info, err := mgo.ParseURL(PRODUCTURL)
// 	// // fmt.Println(info)
// 	// if err != nil {
// 	// 	fmt.Println(err.Error())
// 	// }
// 	// session, err := mgo.DialWithInfo(info)
// 	session, err := mgo.Dial(DEVELOPURL)
// 	if err != nil {
// 		fmt.Println(err.Error())
// 	}
// 	session.SetMode(mgo.Monotonic, true)
// 	session.SetPoolLimit(50)
// 	globalMs = session
// }

// func connect() (*mgo.Session, *mgo.Collection) {
// 	s := globalMs.Clone()
// 	c := s.DB("ecook").C("ecookAd")
// 	// fmt.Println(c.Name)
// 	return s, c
// }

// func LogReq(name, os string) error {
// 	data := DbData{
// 		Request: 1,
// 	}
// 	return logState(name, os, "request", &data)
// }
// func LogYes(name, os string) error {
// 	data := DbData{
// 		Yes: 1,
// 	}
// 	return logState(name, os, "yes", &data)
// }
// func LogNo(name, os string) error {
// 	data := DbData{
// 		No: 1,
// 	}
// 	return logState(name, os, "no", &data)
// }

// func LogTimeout(name, os string) error {
// 	data := DbData{
// 		Timeout: 1,
// 	}
// 	return logState(name, os, "timeout", &data)
// }

// func logState(name, os, state string, insert *DbData) error {
// 	o := intOs(os)
// 	time := util.FormateYMDH()
// 	s, c := connect()
// 	defer s.Close()
// 	// fmt.Println("-------------Mongodb:", s)
// 	key := util.Md5(name + os + time)
// 	// 如果本地存在或者服务器存在，执行更新操作
// 	// util.IsExist(key) ||
// 	// if isExist(c, o, name, time) {
// 	// 	err := c.Update(bson.M{"name": name, "os": o, "time": time}, bson.M{"$inc": bson.M{state: 1}})
// 	// 	// fmt.Println("+++++++++++++Mongodb:", err)
// 	// 	data := DbData{}
// 	// 	c.Find(bson.M{"name": name, "os": o, "time": time}).One(&data)
// 	// 	// fmt.Printf("%+v", data)
// 	// 	return err
// 	// }
// 	// n, err := c.Find(bson.M{"name": name, "os": o, "time": time}).Count()
// 	// if err == nil {
// 	//	if n > 0 {
// 	//		return c.Update(bson.M{"name": name, "os": o, "time": time}, bson.M{"$inc": bson.M{state: 1}})
// 	//	} else {
// 	//		// 服务器不存在，执行插入
// 	//		insert.Os = o
// 	//		insert.Time = time
// 	//		insert.Name = name
// 	//		return c.Insert(insert)
// 	//	}
// 	// }
// 	// // fmt.Println(err.Error())
// 	// return err
// 	// _, err := c.Upsert(bson.M{"name": name, "os": o, "time": time}, bson.M{"$inc": bson.M{state: 1}})
// 	// return err
// 	if util.IsExist(key) {
// 		return c.Update(bson.M{"name": name, "os": o, "time": time}, bson.M{"$inc": bson.M{state: 1}})
// 	} else {
// 		_, err := c.Upsert(bson.M{"name": name, "os": o, "time": time}, bson.M{"$inc": bson.M{state: 1}})
// 		return err
// 	}
// }

// func isExist(c *mgo.Collection, os int, name, time string) (result bool) {
// 	result = true
// 	query := c.Find(bson.M{"name": name, "os": os, "time": time})
// 	if err := query.One(&DbData{}); err != nil {
// 		result = false
// 	}
// 	return result
// }

// func Insert(data interface{}) error {
// 	s, c := connect()
// 	defer s.Close()
// 	return c.Insert(data)
// }

// func Update(selector, data interface{}) error {
// 	s, c := connect()
// 	defer s.Close()
// 	return c.Update(selector, data)
// }

// func UpdateAll(selector, data interface{}) (*mgo.ChangeInfo, error) {
// 	s, c := connect()
// 	defer s.Close()
// 	return c.UpdateAll(selector, data)
// }

// func FindOne(selector, result interface{}) error {
// 	s, c := connect()
// 	defer s.Close()
// 	return c.Find(selector).One(result)
// }

// func FindAll(selector, result interface{}) error {
// 	s, c := connect()
// 	defer s.Close()
// 	return c.Find(selector).All(result)
// }

// func intOs(os string) int {
// 	if os != "2" {
// 		os = "1"
// 	}
// 	result, _ := strconv.Atoi(os)
// 	return result
// }
