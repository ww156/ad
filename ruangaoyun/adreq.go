package ruangaoyun

type adreq struct {
	Id      string  `json:"id"`
	Version string  `json:"version"`
	Site    *Site   `json:"site"`
	Imp     *Imp    `json:"imp"`
	App     *App    `json:"app"`
	Device  *Device `json:"device"`
	User    *User   `json:user`
}

type Site struct {
	Id      string   `json:"id"`
	Name    string   `json:"name"`
	Page    string   `json:"page"`
	Ref     string   `json:"ref"`
	Content *Content `json:"content"`
}

type Content struct {
	Title    string   `json:"title"`
	Keywords []string `json:"keywords"`
}

type Imp struct {
	Id     string  `json:"id"`
	Tagid  string  `json:"tagid"`
	Banner *Banner `json:"banner"`
	Video  *Video  `json:"video"`
}

type Banner struct {
	W   int `json:"w"`
	H   int `json:"h"`
	Pos int `json:"pos"`
}

type Video struct {
	Mimes       []string `json:"mimes"`
	Linearity   int      `json:"linearity"`
	Minduration int      `json:"minduration"`
	Maxduration int      `json:"maxduration"`
	W           int      `json:"w"`
	H           int      `json:"h"`
	Startdelay  int      `json:"startdelay"`
}

type App struct {
	Id   string `json:"id"`
	Name string `json:"name"`
	Ver  string `json:"ver"`
}

type Device struct {
	Id             string `json:"id"`
	Dpid           string `json:"dpid"`
	Ua             string `json:"ua"`
	Ip             string `json:"ip"`
	Carrier        string `json:"carrier"`
	Model          string `json:"model"`
	Os             string `json:"os"`
	Osv            string `json:"osv"`
	Connectiontype int    `json:"connectiontype"`
	Devicetype     int    `json:"devicetype"`
	Mac            string `json:"mac"`
}

type User struct {
	ID string `json:"id"`
}
