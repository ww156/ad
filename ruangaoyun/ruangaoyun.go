package ruangaoyun

import (
	// "bytes"
	"encoding/json"
	// "fmt"
	"io/ioutil"
	// "log"
	"net/http"
	// "strconv"
	// "strings"
	// "time"
	// "unsafe"

	// "github.com/golang/protobuf/proto"

	"ad/mongo"
	"ad/util"
)

const (
	TAGID_STARTUP_ANDROID = "1000427"
	TAGID_STARTUP_IOS     = "1000428"
	TAGID_ANDROID         = "1000431"
	TAGID_IOS             = "1000432"
	TAGID_FLOW_ANDROID    = "1000429"
	TAGID_FLOW_IOS        = "1000430"
	TAGID_STORY_ANDROID   = "1000482"
	TAGID_STORY_IOS       = "1000483"
	SITEID                = "800034"
	URL                   = "http://uat1.bfsspadserver.8le8le.com/common/adrequest"
	// URL                   = "http://uattest1.bfsspadserver.8le8le.com/common/adrequest"
)

func Ruangaoyun(getReq *util.ReqMsg, funcName string) util.ResMsg {
	if getReq.Os == "1" {
		return base(getReq, funcName, TAGID_ANDROID, 640, 100, 5)
	}
	return base(getReq, funcName, TAGID_IOS, 640, 100, 5)
}

func RuangaoyunFlow(getReq *util.ReqMsg, funcName string) util.ResMsg {
	if getReq.Os == "1" {
		return base(getReq, funcName, TAGID_FLOW_ANDROID, 640, 360, 9)
	}
	return base(getReq, funcName, TAGID_FLOW_IOS, 640, 360, 9)
}

func RuangaoyunStoryFlow(getReq *util.ReqMsg, funcName string) util.ResMsg {
	if getReq.Os == "1" {
		return base(getReq, funcName, TAGID_STORY_ANDROID, 232, 132, 9)
	}
	return base(getReq, funcName, TAGID_STORY_IOS, 232, 132, 9)
}

func RuangaoyunStartUp(getReq *util.ReqMsg, funcName string) util.ResMsg {
	if getReq.Os == "1" {
		return base(getReq, funcName, TAGID_STARTUP_ANDROID, 640, 960, 7)
	}
	return base(getReq, funcName, TAGID_STARTUP_IOS, 640, 960, 7)
}

func base(getReq *util.ReqMsg, funcName, tagid string, w, h, pos int) util.ResMsg {
	postData := adreq{
		Id:      util.GetRandom(),
		Version: "1.1",
		Site: &Site{
			Id:   SITEID,
			Name: "网上厨房",
		},
		Imp: &Imp{
			Id:    util.GetRandom(),
			Tagid: tagid,
			Banner: &Banner{
				W:   w,
				H:   h,
				Pos: pos,
			},
		},
		App: &App{
			Id:   "cn.cook",
			Name: "网上厨房",
			Ver:  getReq.Appversion,
		},
		Device: &Device{
			Id:             getReq.Mac,
			Dpid:           getReq.Imei,
			Ua:             getReq.Ua,
			Ip:             getReq.Ip,
			Carrier:        getReq.Imsi,
			Model:          getReq.Model,
			Os:             "android",
			Osv:            getReq.Osversion,
			Connectiontype: 2,
			Devicetype:     3,
			Mac:            getReq.Mac,
		},
	}
	device := postData.Device
	app := postData.App
	switch getReq.Network {
	case "wifi":
		device.Connectiontype = 1
	case "2g":
		device.Connectiontype = 2
	case "3g":
		device.Connectiontype = 2
	case "4g":
		device.Connectiontype = 2
	default:
		device.Connectiontype = 0
	}
	// 运营商
	switch getReq.Imsi {
	case "1":
		device.Carrier = "46000"
	case "2":
		device.Carrier = "46001"
	case "3":
		device.Carrier = "46003"
	default:
		device.Carrier = "46004"
	}
	if getReq.Os == "2" {
		app.Id = "cn.ecook.ecook"
		device.Id = getReq.Idfa
		device.Dpid = getReq.Idfa
		device.Os = "ios"
		device.Devicetype = 1
	}
	ma, err := json.Marshal(&postData)
	if err != nil {
		mongo.LogTimeout(funcName, getReq.Os)
		util.HttpRequestCount.WithLabelValues(funcName + "jsonerr").Inc()
		return util.ResMsg{}
	}

	header := http.Header{}
	header.Set("Connection", "keep-alive")
	header.Set("Content-Type", "application/json;charset=utf-8")
	header.Set("Cache-Control", "no-cache")
	resp, err := util.HttpPOST(URL, &header, ma)
	mongo.LogReq(funcName, getReq.Os)
	if err != nil {
		mongo.LogTimeout(funcName, getReq.Os)
		util.HttpRequestCount.WithLabelValues(funcName + "timeouterr").Inc()
		return util.ResMsg{}
	}
	data, err := ioutil.ReadAll(resp.Body)
	resp.Body.Close()
	if err != nil {
		mongo.LogNo(funcName, getReq.Os)
		util.HttpRequestCount.WithLabelValues(funcName + "readresperr").Inc()
		return util.ResMsg{}
	}
	if resp.StatusCode != 200 {
		mongo.LogTimeout(funcName, getReq.Os)
		util.HttpRequestCount.WithLabelValues(funcName + "not200err").Inc()
		return util.ResMsg{}
	}
	return FormateRes(data, getReq.Os, funcName)
}

func FormateRes(res []byte, os, funcName string) util.ResMsg {
	resData := &adres{}
	json.Unmarshal(res, resData)
	// fmt.Printf("%+v", resData)
	if resData.Img == nil {
		mongo.LogNo(funcName, os)
		util.HttpRequestCount.WithLabelValues(funcName + "noerr").Inc()
		return util.ResMsg{}
	}
	postData := util.ResMsg{
		Id:       "0",
		Weight:   0,
		State:    0,
		Title:    resData.Title,
		Content:  resData.Desc,
		ImageUrl: resData.Img.Src,
		Uri:      resData.Target,
	}
	for _, attr := range resData.Pv {
		postData.Displayreport = append(postData.Displayreport, attr.Url)
	}
	for _, attr := range resData.Click {
		postData.Clickreport = append(postData.Clickreport, attr.Url)
	}
	postData.Displayreport = append(postData.Displayreport, "http://120.26.247.23/ad/report.php?method="+funcName+"&event=displayreport&os="+os+"&t="+util.GetRandom())
	postData.Clickreport = append(postData.Clickreport, "http://120.26.247.23/ad/report.php?method="+funcName+"&event=clickreport&os="+os+"&t="+util.GetRandom())

	return postData
}
