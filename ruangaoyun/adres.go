package ruangaoyun

type adres struct {
	Id           string
	Title        string
	Desc         string
	Pv           []Url
	Click        []*Url
	Img          *Img
	Target       string
	Appname      string
	Ad_type      int
	Package      string
	App_store_id string
}

type Url struct {
	Type_mma int
	Url      string
}

type Img struct {
	W   int
	H   int
	Src string
}
