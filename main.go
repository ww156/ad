package main

import (
	// "bytes"
	// "encoding/json"
	"fmt"
	// "io/ioutil"
	// "log"
	"net/http"
	// "net/url"
	// "os"
	"strconv"
	// "os/signal"
	// log "github.com/sirupsen/logrus"
	_ "net/http/pprof"
	"regexp"
	"runtime"
	"strings"
	"time"

	"ad/adhub"
	"ad/adxhi"
	"ad/alimm"
	"ad/appcoach"
	"ad/baicheng"
	"ad/beiai"
	"ad/chuwang"
	"ad/dashu"
	"ad/dianchuang"
	"ad/dianke"
	"ad/dianle"
	"ad/doumeng"
	"ad/eztmob"
	"ad/feiyang"
	"ad/haiyun"
	"ad/huayuan"
	"ad/huiniu"
	"ad/huixuan"
	"ad/jd"
	"ad/jinyi"
	"ad/jufeng"
	"ad/kuaiguo"
	"ad/maiguang"
	"ad/mdht"
	"ad/onemob"
	"ad/pingan"
	"ad/qichuang"
	"ad/ruangaoyun"
	"ad/ruien"
	"ad/ruishi"
	"ad/smaato"
	"ad/tianluo"
	"ad/tianluoa"
	"ad/tianluob"
	"ad/tianzhuo"
	"ad/wangmai"
	"ad/wangxiang"
	"ad/wosou"
	"ad/xingzhe"
	"ad/xumi"
	"ad/yezi"
	"ad/yidian"
	"ad/yishen"
	"ad/youmi"
	"ad/yousixing"
	"ad/youzi"
	"ad/yuemeng"
	"ad/zhihuishu"
	"ad/zhixiao"
	"ad/zhizun"
	"ad/zhongcheng"
	"ad/zhonghulian"
	"ad/zhongzhi"
	"ad/zibolan"

	"ad/mongo"
	"ad/util"
	"github.com/gin-contrib/gzip"
	"github.com/gin-gonic/gin"
	"github.com/pquerna/ffjson/ffjson"
	// "github.com/DeanThompson/ginpprof"
)

var globalRe, globalSubre *regexp.Regexp

func init() {
	globalRe, _ = regexp.Compile(`^([a-z]+?)\d+$`)
	globalSubre, _ = regexp.Compile(`^[a-z]+[^0-9]`)
}

func adData(c *gin.Context) {
	queryStrOrigin := c.Query("method")
	appid := c.Query("appid")
	// fmt.Println(queryStrOrigin)
	if queryStrOrigin == "" {
		c.String(http.StatusOK, "")
		return
	}
	// 是否随机生成数据标记
	multiple := false
	re := globalRe.Copy()
	subre := globalSubre.Copy()
	if re.MatchString(queryStrOrigin) {
		multiple = true
		// if strings.Contains(queryStrOrigin, "yezi") {
		//	multiple = false
		// }
	}
	queryStr := subre.FindString(queryStrOrigin)
	if strings.Contains(queryStrOrigin, "yeziflow") {
		queryStr = queryStrOrigin
	}
	// util.HttpRequestCount.WithLabelValues(queryStr).Inc() // 相应请求次数统计
	getReq := util.ReqMsg{}
	err := c.BindJSON(&getReq)
	if err != nil {
		c.String(http.StatusOK, "")
		return
	}
	// if getReq.Imei == "864167035958117" && queryStr == "yeziflow" {
	//	fmt.Println("yeziflow")
	// }
	getReq.Formate(multiple)
	// 路由分配
	data := util.ResMsg{}
	queryStrOrigin = appid + queryStrOrigin
	switch queryStr {
	//点创
	case "dianchuang":
		data = dianchuang.Dianchaung(&getReq, queryStrOrigin)
	// 至尊
	case "zhizun":
		data = zhizun.Zhizun(&getReq, queryStrOrigin)
	case "zhizunflow":
		data = zhizun.ZhizunFlow(&getReq, queryStrOrigin)
	// 豆盟
	case "doumeng":
		data = doumeng.Doumeng(&getReq, queryStrOrigin)
	case "doumengflow":
		data = doumeng.DoumengFlow(&getReq, queryStrOrigin)
	case "doumengstartup":
		data = doumeng.DoumengStartUp(&getReq, queryStrOrigin)
	// 阿里妈妈
	case "alimmflow":
		data = alimm.AlimmFlow(&getReq, queryStrOrigin)
	// 椰子
	case "yezi":
		data = yezi.Yezi(&getReq, queryStrOrigin)
	case "yeziflow":
		data = yezi.YeziFlow(&getReq, queryStrOrigin)
	// test
	case "yeziflow2":
		data = yezi.YeziFlow(&getReq, queryStrOrigin)
	case "yeziflow3":
		data = yezi.YeziFlow(&getReq, queryStrOrigin)
	case "yeziflow4":
		data = yezi.YeziFlow(&getReq, queryStrOrigin)
	case "yeziflow5":
		data = yezi.YeziFlow(&getReq, queryStrOrigin)
	case "yeziflow6":
		data = yezi.YeziFlow(&getReq, queryStrOrigin)
	case "yeziflow7":
		data = yezi.YeziFlow(&getReq, queryStrOrigin)
	case "yeziflow8":
		data = yezi.YeziFlow(&getReq, queryStrOrigin)
	case "yeziflow9":
		data = yezi.YeziFlow(&getReq, queryStrOrigin)
	case "yeziflow10":
		data = yezi.YeziFlow(&getReq, queryStrOrigin)
	// test
	case "yezistoryflow":
		data = yezi.YeziStoryFlow(&getReq, queryStrOrigin)
	case "yezistartup":
		data = yezi.YeziStartUp(&getReq, queryStrOrigin)
	// 京东(信息流)
	case "jd":
		data = jd.Jd(&getReq, queryStrOrigin)
	case "jdflow":
		data = jd.JdFlow(&getReq, queryStrOrigin)
	// smaato
	case "smaato":
		data = smaato.Smaato(&getReq, queryStrOrigin)
	case "smaatoflow":
		data = smaato.SmaatoFlow(&getReq, queryStrOrigin)
	// 一点资讯
	case "yidian":
		data = yidian.Yidian(&getReq, queryStrOrigin)
	case "yidianflow":
		data = yidian.YidianFlow(&getReq, queryStrOrigin)
	case "yidianstartup":
		data = yidian.YidainStartUp(&getReq, queryStrOrigin)
	// 华院
	case "huayuan":
		data = huayuan.Huayuan(&getReq, queryStrOrigin)
	case "huayuanflow":
		data = huayuan.HuayuanFlow(&getReq, queryStrOrigin)
	case "huayuanstartup":
		data = huayuan.HuayuanStartup(&getReq, queryStrOrigin)
	// 软告云
	case "ruangaoyun":
		data = ruangaoyun.Ruangaoyun(&getReq, queryStrOrigin)
	case "ruangaoyunstartup":
		data = ruangaoyun.RuangaoyunStartUp(&getReq, queryStrOrigin)
	case "ruangaoyunflow":
		data = ruangaoyun.RuangaoyunFlow(&getReq, queryStrOrigin)
	case "ruangaoyunstoryflow":
		data = ruangaoyun.RuangaoyunStoryFlow(&getReq, queryStrOrigin)
	// 旺翔
	case "wangxiang":
		data = wangxiang.Wangxiang(&getReq, queryStrOrigin)
	case "wangxiangflow":
		data = wangxiang.WangxiangFlow(&getReq, queryStrOrigin)
	case "wangxiangstartup":
		data = wangxiang.WangxiangStartup(&getReq, queryStrOrigin)
	// 天罗
	case "tianluo":
		data = tianluo.Tianluo(&getReq, queryStrOrigin)
	case "tianluoflow":
		data = tianluo.TianluoFlow(&getReq, queryStrOrigin)
	case "tianluostartup":
		data = tianluo.TianluoStartup(&getReq, queryStrOrigin)
	// 天罗b
	case "tianluob":
		data = tianluob.Tianluob(&getReq, queryStrOrigin)
	case "tianluobflow":
		data = tianluob.TianluobFlow(&getReq, queryStrOrigin)
	case "tianluobstartup":
		data = tianluob.TianluobStartup(&getReq, queryStrOrigin)
	// 百橙（银橙·无开屏）
	case "baicheng":
		data = baicheng.Baicheng(&getReq, queryStrOrigin)
	case "baichengflow":
		data = baicheng.BaichengFlow(&getReq, queryStrOrigin)
	case "baichengstartup":
		data = baicheng.BaichengStartup(&getReq, queryStrOrigin)
	// 我搜
	case "wosou":
		data = wosou.Wosou(&getReq, queryStrOrigin)
	case "wosouflow":
		data = wosou.WosouFlow(&getReq, queryStrOrigin)
	// 麦广
	case "maiguang":
		data = maiguang.Maiguang(&getReq, queryStrOrigin)
	case "maiguangflow":
		data = maiguang.MaiguangFlow(&getReq, queryStrOrigin)
	case "maiguangstartup":
		data = maiguang.MaiguangStartup(&getReq, queryStrOrigin)
	// 阅盟
	case "yuemeng":
		data = yuemeng.Yuemeng(&getReq, queryStrOrigin)
	case "yuemengflow":
		// fmt.Printf("%+v", getReq)
		data = yuemeng.YuemengFlow(&getReq, queryStrOrigin)
	case "yuemengstartup":
		data = yuemeng.YuemengStartup(&getReq, queryStrOrigin)
	// 触网
	case "chuwangflow":
		data = chuwang.ChuwangFlow(&getReq, queryStrOrigin)
	// 有米
	case "youmi":
		data = youmi.Youmi(&getReq, queryStrOrigin)
	case "youmiflow":
		data = youmi.YoumiFlow(&getReq, queryStrOrigin)
	case "youmistartup":
		data = youmi.YoumiStartup(&getReq, queryStrOrigin)
	// 众橙
	case "zhongcheng":
		data = zhongcheng.Zhongcheng(&getReq, queryStrOrigin)
	case "zhongchengflow":
		data = zhongcheng.ZhongchengFlow(&getReq, queryStrOrigin)
	// 大树
	case "dashu":
		data = dashu.Dashu(&getReq, queryStrOrigin)
	case "dashuflow":
		data = dashu.DashuFlow(&getReq, queryStrOrigin)
	case "dashustartup":
		data = dashu.DashuStartup(&getReq, queryStrOrigin)
	// 汇选
	case "huixuan":
		data = huixuan.Huixuan(&getReq, queryStrOrigin)
	case "huixuanflow":
		data = huixuan.HuixuanFlow(&getReq, queryStrOrigin)
	// eztmob
	case "eztmob":
		data = eztmob.Eztmob(&getReq, queryStrOrigin)
	case "eztmobstartup":
		data = eztmob.EztmobStartup(&getReq, queryStrOrigin)
	// 点乐
	case "dianle":
		data = dianle.Dianle(&getReq, queryStrOrigin)
	case "dianleflow":
		data = dianle.DianleFlow(&getReq, queryStrOrigin)
	// 海云
	case "haiyun":
		data = haiyun.Haiyun(&getReq, queryStrOrigin)
	case "haiyunflow":
		data = haiyun.HaiyunFlow(&getReq, queryStrOrigin)
	case "haiyunstartup":
		data = haiyun.HaiyunStartup(&getReq, queryStrOrigin)
	// 柚子
	case "youzi":
		data = youzi.Youzi(&getReq, queryStrOrigin)
	case "youzistartup":
		data = youzi.YouziStartup(&getReq, queryStrOrigin)
	case "youziflow":
		data = youzi.YouziFlow(&getReq, queryStrOrigin)
	// 企创
	case "qichuang":
		data = qichuang.Qichuang(&getReq, queryStrOrigin)
	case "qichuangflow":
		data = qichuang.QichuangFlow(&getReq, queryStrOrigin)
	case "qichuangstartup":
		data = qichuang.QichuangStartup(&getReq, queryStrOrigin)
	// 天罗（a）
	case "tianluoa":
		data = tianluoa.Tianluoa(&getReq, queryStrOrigin)
	case "tianluoaflow":
		data = tianluoa.TianluoaFlow(&getReq, queryStrOrigin)
	case "tianluoastartup":
		data = tianluoa.TianluoaStartup(&getReq, queryStrOrigin)
	// 平安
	case "pingan":
		data = pingan.Pingan(&getReq, queryStrOrigin)
	case "pinganflow":
		data = pingan.PinganFlow(&getReq, queryStrOrigin)
	case "pinganstartup":
		data = pingan.PinganStartup(&getReq, queryStrOrigin)
	// 汇牛
	case "huiniu":
		data = huiniu.Huiniu(&getReq, queryStrOrigin)
	case "huiniuflow":
		data = huiniu.HuiniuFlow(&getReq, queryStrOrigin)
	// 飓风
	case "jufeng":
		data = jufeng.Jufeng(&getReq, queryStrOrigin)
	case "jufengflow":
		data = jufeng.JufengFlow(&getReq, queryStrOrigin)
	case "jufengstartup":
		data = jufeng.JufengStartup(&getReq, queryStrOrigin)
	// 智效
	case "zhixiao":
		data = zhixiao.Zhixiao(&getReq, queryStrOrigin)
	case "zhixiaostartup":
		data = zhixiao.ZhixiaoStartup(&getReq, queryStrOrigin)
	// adhub
	case "adhub":
		data = adhub.Adhub(&getReq, queryStrOrigin)
	case "adhubflow":
		data = adhub.AdhubFlow(&getReq, queryStrOrigin)
	case "adhubstartup":
		data = adhub.AdhubStartup(&getReq, queryStrOrigin)
	// 点客
	case "dianke":
		data = dianke.Dianke(&getReq, queryStrOrigin)
	case "diankeflow":
		data = dianke.DiankeFlow(&getReq, queryStrOrigin)
	// 北艾
	case "beiai":
		data = beiai.Beiai(&getReq, queryStrOrigin)
	case "beiaiflow":
		data = beiai.BeiaiFlow(&getReq, queryStrOrigin)
	case "beiaistartup":
		data = beiai.BeiaiStartup(&getReq, queryStrOrigin)
	// 智慧树
	case "zhihuishu":
		data = zhihuishu.Zhihuishu(&getReq, queryStrOrigin)
	case "zhihuishuflow":
		data = zhihuishu.ZhihuishuFlow(&getReq, queryStrOrigin)
	case "zhihuishustartup":
		data = zhihuishu.ZhihuishuStartup(&getReq, queryStrOrigin)
	// 瑞恩
	case "ruien":
		data = ruien.Ruien(&getReq, queryStrOrigin)
	case "ruienflow":
		data = ruien.RuienFlow(&getReq, queryStrOrigin)
	case "ruienstartup":
		data = ruien.RuienStartup(&getReq, queryStrOrigin)
	// 金易
	case "jinyi":
		data = jinyi.Jinyi(&getReq, queryStrOrigin)
	case "jinyiflow":
		data = jinyi.JinyiFlow(&getReq, queryStrOrigin)
	case "jinyidtartup":
		data = jinyi.JinyiStartup(&getReq, queryStrOrigin)
	// 翼神
	case "yishen":
		data = yishen.Yishen(&getReq, queryStrOrigin)
	case "yishenflow":
		data = yishen.YishenFlow(&getReq, queryStrOrigin)
	case "yishenstartup":
		data = yishen.YishenStartup(&getReq, queryStrOrigin)
	// appcoach
	case "appcoach":
		data = appcoach.Appcoach(&getReq, queryStrOrigin)
	case "appcoachflow":
		data = appcoach.AppcoachFlow(&getReq, queryStrOrigin)
	case "appcoachstartup":
		data = appcoach.AppcoachStartup(&getReq, queryStrOrigin)
	case "appcoachstoryflow":
		data = appcoach.AppcoachStoryFlow(&getReq, queryStrOrigin)
	// 忧思行
	case "yousixing":
		data = yousixing.Yousixing(&getReq, queryStrOrigin)
	case "yousixingflow":
		data = yousixing.YousixingFlow(&getReq, queryStrOrigin)
	case "yousixingstartup":
		data = yousixing.YousixingStartup(&getReq, queryStrOrigin)
	case "yousixingstoryflow":
		data = yousixing.YousixingStoryFlow(&getReq, queryStrOrigin)
	// 天卓
	case "tianzhuo":
		data = tianzhuo.Tianzhuo(&getReq, queryStrOrigin)
	case "tianzhuoflow":
		data = tianzhuo.TianzhuoFlow(&getReq, queryStrOrigin)
	case "tianzhuostartup":
		data = tianzhuo.TianzhuoStartup(&getReq, queryStrOrigin)
	case "tianzhuostoryflow":
		data = tianzhuo.TianzhuoStoryFlow(&getReq, queryStrOrigin)
	// 奥菲
	case "zhonghulian":
		data = zhonghulian.Zhonghulian(&getReq, queryStrOrigin)
	case "zhonghulianflow":
		data = zhonghulian.ZhonghulianFlow(&getReq, queryStrOrigin)
	case "zhonghulianstartup":
		data = zhonghulian.ZhonghulianStartup(&getReq, queryStrOrigin)
	// 旭米
	case "xumi":
		data = xumi.Xumi(&getReq, queryStrOrigin)
	case "xumiflow":
		data = xumi.XumiFlow(&getReq, queryStrOrigin)
	case "xumistartup":
		data = xumi.XumiStartup(&getReq, queryStrOrigin)
	// 中至
	case "zhongzhi":
		data = zhongzhi.Zhongzhi(&getReq, queryStrOrigin)
	case "zhongzhiflow":
		data = zhongzhi.ZhongzhiFlow(&getReq, queryStrOrigin)
	case "zhongzhistoryflow":
		data = zhongzhi.ZhongzhiStoryFlow(&getReq, queryStrOrigin)
	case "zhongzhistartup":
		data = zhongzhi.ZhongzhiStartup(&getReq, queryStrOrigin)
	// 快果
	case "kuaiguo":
		data = kuaiguo.Kuaiguo(&getReq, queryStrOrigin)
	case "kuaiguoflow":
		data = kuaiguo.KuaiguoFlow(&getReq, queryStrOrigin)
	case "kuaiguostartup":
		data = kuaiguo.KuaiguoStartup(&getReq, queryStrOrigin)
	// 瑞狮
	case "ruishi":
		data = ruishi.Ruishi(&getReq, queryStrOrigin)
	case "ruishiflow":
		data = ruishi.RuishiFlow(&getReq, queryStrOrigin)
	case "ruishistartup":
		data = ruishi.RuishiStartup(&getReq, queryStrOrigin)
	// 旺脉
	case "wangmai":
		data = wangmai.Wangmai(&getReq, queryStrOrigin)
	case "wangmaiflow":
		data = wangmai.WangmaiFlow(&getReq, queryStrOrigin)
	case "wangmaistartup":
		data = wangmai.WangmaiStartup(&getReq, queryStrOrigin)
	// 行者天下
	case "xingzhe":
		data = xingzhe.Xingzhe(&getReq, queryStrOrigin)
	case "xingzheflow":
		data = xingzhe.XingzheFlow(&getReq, queryStrOrigin)
	case "xingzhestartup":
		data = xingzhe.XingzheStartup(&getReq, queryStrOrigin)
	// 飞扬
	case "feiyang":
		data = feiyang.Feiyang(&getReq, queryStrOrigin)
	case "feiyangflow":
		data = feiyang.FeiyangFlow(&getReq, queryStrOrigin)
	case "feiyangstartup":
		data = feiyang.FeiyangStartup(&getReq, queryStrOrigin)
	// 紫博蓝
	case "zibolan":
		data = zibolan.Zibolan(&getReq, queryStrOrigin)
	case "zibolanflow":
		data = zibolan.ZibolanFlow(&getReq, queryStrOrigin)
	case "zibolanstartup":
		data = zibolan.ZibolanStartup(&getReq, queryStrOrigin)
	// adxhi
	case "adxhi":
		data = adxhi.Adxhi(&getReq, queryStrOrigin)
	case "adxhiflow":
		data = adxhi.AdxhiFlow(&getReq, queryStrOrigin)
	case "adxhistartup":
		data = adxhi.AdxhiStartup(&getReq, queryStrOrigin)
	// onemob
	case "onemob":
		data = onemob.Onemob(&getReq, queryStrOrigin)
	case "onemobflow":
		data = onemob.OnemobFlow(&getReq, queryStrOrigin)
	case "onemobstartup":
		data = onemob.OnemobStartup(&getReq, queryStrOrigin)
	// mdht
	case "mdht":
		data = mdht.Mdht(&getReq, queryStrOrigin)
	case "mdhtflow":
		data = mdht.MdhtFlow(&getReq, queryStrOrigin)
	case "mdhtstartup":
		data = mdht.MdhtStartup(&getReq, queryStrOrigin)
	default:
		fmt.Println("参数无法匹配:", queryStr, queryStrOrigin)
	}

	// elapsed := (float64)(time.Since(start) / time.Millisecond)
	// util.HttpRequestDuration.WithLabelValues(queryStr).Observe(elapsed)
	// fmt.Println("Accept-Encoding: ", c.GetHeader("Accept-Encoding"))

	if data.ImageUrl == "" || data.Displayreport == nil {
		// c.JSON(http.StatusOK, nil)
		// c.Header("Connection", "keep-alive")
		c.String(http.StatusOK, "")
	} else {
		// if getReq.Imei == "864167035958117" && queryStr == "yeziflow" {
		// 	fmt.Println(data)
		// }
		// util.HttpRequestCount.WithLabelValues(queryStr + "yes").Inc()
		data.State = 1
		mongo.LogYes(queryStrOrigin, getReq.Os)
		fmt.Println("writen begin.")
		// fmt.Println(queryStrOrigin)
		// c.Header("Connection", "keep-alive")

		maData, err := ffjson.Marshal(&data)
		if err != nil {
			c.String(http.StatusOK, "")
			return
		}
		c.Header("Content-Type", "application/json; charset=utf-8")
		if !strings.Contains(c.GetHeader("Accept-Encoding"), "gzip") {
			c.Header("Content-Length", strconv.Itoa(len(maData)))
		}
		c.Writer.WriteHeader(http.StatusOK)
		_, err = c.Writer.Write(maData)
		fmt.Println("writen end.")
		if err != nil {
			fmt.Println(err.Error())
		}
		// c.JSON(http.StatusOK, &data)
	}
}

func status(c *gin.Context) {
	// c.Header("Connection", "keep-alive")
	// fmt.Println(c.GetHeader("Accept-Encoding"))
	c.JSON(http.StatusOK, gin.H{
		"msg":  "running",
		"code": 0,
	})
}

func main() {
	runtime.GOMAXPROCS(runtime.NumCPU())
	// log.WithFields(log.Fields{
	// 	"omg":    true,
	// 	"number": 122,
	// }).Warn("The group's number increased tremendously!")
	// fmt.Println("hello world.")
	// // 信号监听
	// c := make(chan os.Signal, 1)
	// signal.Notify(c, os.Interrupt)

	// // Block until a signal is received.
	// go func() {
	// 	select {
	// 	case <-c:
	// 		fmt.Printf("%+v", util.MemCache)
	// 	}
	// }()
	// http处理
	// http.HandleFunc("/", handler)
	// http.HandleFunc("/status", status)
	// // http.Handle("/metrics", promhttp.Handler())
	// fmt.Println("server is listening on 8088...")
	// err := http.ListenAndServe(":8088", nil)
	// if err != nil {
	// 	fmt.Println("Listening error:", err)
	// }
	go func() {
		http.ListenAndServe("localhost:6060", nil)
	}()

	gin.SetMode(gin.ReleaseMode)
	router := gin.Default()
	router.Use(gzip.Gzip(gzip.DefaultCompression))
	// router.Use(gin.Logger())
	router.GET("/", func(c *gin.Context) {
		// c.Header("Connection", "keep-alive")
		c.String(http.StatusOK, "ok")
	})
	router.HEAD("/", func(c *gin.Context) {
		// c.Header("Connection", "keep-alive")
		c.String(http.StatusOK, "ok")
	})
	router.POST("/ad", adData)
	router.Any("/status", status)

	// ginpprof.Wrap(router)
	// router.Run(":8088")
	s := &http.Server{
		Addr:           ":8088",
		Handler:        router,
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1 << 20,
	}
	s.SetKeepAlivesEnabled(false)
	s.ListenAndServe()
}
