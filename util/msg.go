package util

import (
	"net/url"
	"strings"
)

// android客户端请求数据
type AndroidMsg struct {
	Screenwidth  string
	Os           string
	Ip           string
	Appversion   string
	Imsi         string
	Terminal     string
	Ua           string
	Version      string
	Mac          string
	Network      string
	Sd           string
	Machine      string
	Vendor       string
	Imei         string
	Model        string
	Id           string
	Osversion    string
	Screenheight string
	Device       string
	Androidid    string
}

// 格式化andriod端请求数据
func (android *AndroidMsg) Formate() {
	// 网络类型转化小写
	android.Network = strings.ToLower(android.Network)
}

// ios客户端请求数据
type IosMsg struct {
	Screenwidth  string
	Os           string
	Idfa         string
	Ip           string
	Appversion   string
	Imsi         string
	Terminal     string
	Ua           string
	Version      string
	Openudid     string
	Network      string
	Sd           string
	Vendor       string
	Width        string
	Model        string
	Osversion    string
	Screenheight string
	Device       string
	Height       string
}

// 格式化ios端请求数据
func (ios *IosMsg) Formate() {

}

// 客户端请求数据，android和ios
type ReqMsg struct {
	Screenwidth  string // 屏幕宽度
	Screenheight string // 屏幕高度
	Os           string // 设备操作系统，1: iOS，2：Android，3：Windows，4：其它
	Osversion    string // 系统版本号
	Imsi         string // 网络型号1：中国移动、2：中国联通、3：中国电信
	Ip           string // ip地址
	Mac          string // 设备 MAC 地址（大写，保留冒号分隔符）
	Network      string // 网络类型
	Device       string // 设备型号
	Androidid    string // 安卓设备广告标识符
	Idfa         string // 苹果设备广告标识符
	Openudid     string // iOS 版本 6 以下的操作系统提供 OpenUDID
	Appversion   string // app版本
	Terminal     string // 忽略
	Ua           string // 用户代理
	Version      string // app版本
	Sd           string // 分辨率或密度比（/160）
	Machine      string // 机器编码
	Vendor       string // 设备生产商
	Imei         string // 设备 IMEI 号
	Model        string // 设备型号
	Id           string
	Width        string
	Height       string
	Lat          string
	Lng          string
}

// 格式化客户端请求数据
func (req *ReqMsg) Formate(multiple bool) {
	// 兼容老版本
	if req.Screenwidth == "" {
		req.Screenwidth = req.Width
	}
	if req.Screenheight == "" {
		req.Screenheight = req.Height
	}
	// 网络类型转化小写
	req.Network = strings.ToLower(req.Network)
	// 联网类型，1：以太网，2：wifi，3：蜂窝网络-未识别，4：蜂窝网络-2G，5：蜂窝网络：3G，6：蜂窝网络：4G
	// switch req.Network {
	// case "以太网":
	// 	req.Network = "1"
	// case "wifi":
	// 	req.Network = "2"
	// case "unknow":
	// 	req.Network = "3"
	// case "2g":
	// 	req.Network = "4"
	// case "3g":
	// 	req.Network = "5"
	// case "4g":
	// 	req.Network = "6"
	// }
	// android
	if req.Os == "1" {
		// 运营商类型，1：中国移动，2：中国联通，3：中国电信
		switch strings.ToLower(req.Imsi) {
		case "46000":
			req.Imsi = "1"
		case "46001":
			req.Imsi = "2"
		case "46002":
			req.Imsi = "3"
		default:
			req.Imsi = "unknown"
		}
		// imei
		if req.Imei == "" || req.Imei == "000000000000000" {
			req.Imei = CreateImei("")
		}
	}
	// ios
	if req.Os == "2" {
		// 运营商类型，1：中国移动，2：中国联通，3：中国电信
		u, err := url.QueryUnescape(req.Imsi)
		if err == nil {
			switch u {
			case "中国移动":
				req.Imsi = "1"
			case "中国联通":
				req.Imsi = "2"
			case "中国电信":
				req.Imsi = "3"
			default:
				req.Imsi = "unknown"
			}
		}
		// 生成idfa
		if req.Idfa == "00000000-0000-0000-0000-000000000000" {
			req.Idfa = CreateIdfa()
		}
		if req.Openudid == "" {
			req.Openudid = CreateOpenudid()
		}
	}
	// mac地址为空，则随机生成mac
	if req.Mac == "" {
		req.Mac = CreateMac()
	}
	if multiple {
		if req.Os == "1" {
			req.Androidid = CreateAndroidid()
			req.Imei = CreateImei(req.Imei)
		} else {
			req.Idfa = CreateIdfa()
			req.Openudid = CreateOpenudid()
		}
	}
}

// 向客户端响应的数据
type ResMsg struct {
	Id            string   `json:"id"`     // "0"
	Weight        int      `json:"weight"` // 0
	State         int      `json:"state"`  // 1
	Title         string   `json:"title"`
	Content       string   `json:"content"`
	ImageUrl      string   `json:"imageUrl"`
	Uri           string   `json:"uri"` // 落地页
	Displayreport []string `json:"displayreport"`
	Clickreport   []string `json:"clickreport"`
}

// 格式化向客户端响应的数据
func (res *ResMsg) Formate() {

}
