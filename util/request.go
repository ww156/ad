package util

import (
	"bytes"
	"crypto/tls"
	"net"
	"net/http"
	"time"
)

var Timeout = 800 * time.Millisecond
var roundtrip = &http.Transport{
	TLSClientConfig: &tls.Config{
		InsecureSkipVerify: true,
	},
	// DisableKeepAlives: true,
	Proxy: http.ProxyFromEnvironment,
	DialContext: (&net.Dialer{
		Timeout:   30 * time.Second,
		KeepAlive: 30 * time.Second,
		DualStack: true,
	}).DialContext,
	MaxIdleConns:          200,
	MaxIdleConnsPerHost:   10,
	IdleConnTimeout:       90 * time.Second,
	TLSHandshakeTimeout:   10 * time.Second,
	ExpectContinueTimeout: 1 * time.Second,
}
var Client = &http.Client{Timeout: Timeout, Transport: roundtrip}

func HttpPOST(url string, header *http.Header, body []byte) (*http.Response, error) {
	req, err := http.NewRequest("POST", url, bytes.NewReader(body))
	if err != nil {
		return nil, err
	}
	req.Header = *header
	return Client.Do(req)
}
