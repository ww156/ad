package util

import (
	"math/rand"
	// "time"
)

// 生成信息流点击坐标
func CreateFPos(w, h int) [4]int {
	pos := [4]int{}
	width := createN(w)
	pos[0] = width
	pos[2] = width
	height := createN(h)
	pos[1] = height
	pos[3] = height
	return pos
}

// 生成开屏点击坐标
func CreateSPos(w, h int) [4]int {
	pos := [4]int{}
	width := createN(w)
	pos[0] = width
	pos[2] = width
	height := createN(h)
	pos[1] = height
	pos[3] = height
	return pos
}

// 生成banner点击坐标
func CreateBPos(w, h int) [4]int {
	pos := [4]int{}
	width := createN(w)
	pos[0] = width
	pos[2] = width
	height := createN(h)
	pos[1] = height
	pos[3] = height
	return pos
}

func createN(n int) int {
	maxN := n * 60 / 100
	rN := rand.Intn(maxN)
	return n*20/100 + rN
}
