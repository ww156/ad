package util

import (
	"crypto/md5"
	"encoding/hex"
	"math/rand"
	"strconv"
	"time"
)

func GetRandom() string {
	unixStr := strconv.FormatInt(time.Now().Unix(), 10)
	str := strconv.FormatInt(rand.New(rand.NewSource(99)).Int63(), 10)
	md5Ctx := md5.New()
	md5Ctx.Write([]byte(unixStr + str))
	data := md5Ctx.Sum(nil)
	return hex.EncodeToString(data[:])
}
