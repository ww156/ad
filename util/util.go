package util

import (
	"crypto/md5"
	"crypto/sha1"
	"encoding/hex"
	"errors"
	// "fmt"
	"math"
	"math/rand"
	"regexp"
	"strconv"
	"strings"
	"sync"
	"time"

	"github.com/prometheus/client_golang/prometheus"
)

var HttpRequestCount = prometheus.NewCounterVec(
	prometheus.CounterOpts{
		Name: "http_request_count",
		Help: "http request count",
	},
	[]string{"endpoint"},
)

var HttpRequestDuration = prometheus.NewSummaryVec(
	prometheus.SummaryOpts{
		Name: "http_request_duration",
		Help: "http request duration",
	},
	[]string{"endpoint"},
)

// 数据库数据状态缓存
var MemCache = struct {
	sync.RWMutex
	m map[string]int
}{m: make(map[string]int)}

func init() {
	prometheus.MustRegister(HttpRequestCount)
	prometheus.MustRegister(HttpRequestDuration)
	// MemCache = make(map[string]int)
}

func Md5(str string) string {
	md5Ctx := md5.New()
	md5Ctx.Write([]byte(str))
	data := md5Ctx.Sum(nil)
	return hex.EncodeToString(data[:])
}

func Sha1(str string) string {
	sha1Ctx := sha1.New()
	sha1Ctx.Write([]byte(str))
	data := sha1Ctx.Sum(nil)
	return hex.EncodeToString(data[:])
}

// 判断数据库中是否存在某些数据
func IsExist(key string) bool {
	MemCache.RLock()
	_, ok := MemCache.m[key]
	MemCache.RUnlock()
	if ok {
		return true
	} else {
		MemCache.Lock()
		MemCache.m[key] = 1
		MemCache.Unlock()
		return false
	}
}

// 删除缓存中的key
func DelCache(key string) {
	MemCache.Lock()
	delete(MemCache.m, key)
	MemCache.Unlock()
}

// 截取n位字符串
// 从start位置开始，向后截取str中的n位，
// 如果start<0，则从倒数start位向后开始截取
// 如果n<0，则向前截取，最多截取到字符串开始
func SubStrN(str string, start, n int) (string, error) {
	arr := []rune(str)
	length := len(arr)
	if math.Abs(float64(start)) > float64(length-1) {
		return "", errors.New("起始位置超出数组长度")
	}
	// 如果start<0，则从最后面截取
	if start < 0 {
		start = length + start
	}
	if n < 0 {
		if float64(start) < math.Abs(float64(n)) {
			return string(arr[:start+1]), nil
		} else {
			return string(arr[start-n+1 : start+1]), nil
		}
	}
	if start == length-1 {
		return string(arr[start]), nil
	}
	if start+n > length-1 {
		return string(arr[start:]), nil
	}
	return string(arr[start : start+n]), nil
}

// 生成mac地址
// 算法：随机十六进制数字
func CreateMac() string {
	mac := ""
	str := GetRandom()
	arr := strings.Split(str, "")
	for k, v := range arr {
		if k < 12 {
			mac = mac + v
			if k%2 == 1 && k != 11 {
				mac = mac + ":"
			}
		}
	}
	return mac
}

// 生成idfa
// 算法：
func CreateIdfa() string {
	idfa := ""
	str := Md5(GetRandom())
	subStr, _ := SubStrN(str, 0, 8)
	idfa += subStr + "-"
	subStr, _ = SubStrN(str, 8, 4)
	idfa += subStr + "-4"
	subStr, _ = SubStrN(str, 13, 3)
	idfa += subStr + "-" + [4]string{"8", "9", "A", "b"}[rand.Intn(4)]
	subStr, _ = SubStrN(str, 17, 3)
	idfa += subStr + "-"
	subStr, _ = SubStrN(str, 20, 12)
	idfa += subStr
	idfa = strings.ToUpper(idfa)
	// fmt.Println(idfa)
	return idfa
}

// 生成openudid
// 算法：
func CreateOpenudid() string {
	str, _ := SubStrN(GetRandom(), 0, 8)
	// fmt.Println(GetRandom() + str)
	return GetRandom() + str
}

// 生成androidid
// 算法：
func CreateAndroidid() string {
	str := Md5(GetRandom())
	id, _ := SubStrN(str, 10, 16)
	return id
}

// 根据imei生成新的imei
// 算法：
func CreateImei(imei string) string {
	re, _ := regexp.Compile(`^\d{15}$`)
	if re.MatchString(imei) {
		str, _ := SubStrN(imei, 0, 12)
		str += strconv.Itoa(rand.Intn(10)) + strconv.Itoa(rand.Intn(10))
		sum := 0
		length := 14
		mul := 2
		for i := 0; i < length; i++ {
			digist, _ := SubStrN(str, i, 1)
			tp, _ := strconv.Atoi(digist)
			if i%2 != 0 {
				tp *= mul
			}
			if tp >= 10 {
				sum += tp%10 + 1
			} else {
				sum += tp
			}
		}
		chk := (10 - sum%10) % 10
		// fmt.Println(sum, chk)
		return str + strconv.Itoa(chk)
	} else {
		str, _ := SubStrN(GetRandom(), 10, 8)
		return "A10000" + str
	}
}

// 12 34 56 78 90 12 34
// 86 07 34 03 93 06 71
// 12 14  8  6  6 12  2
// 8+1+2+0+1+4+3+8+0+6+9+6+0+1+2+7+2=
// imsi:imsi+strconv.Itoa(10000+rand.Intn(90000))+strconv.Itoa(10000+rand.Intn(90000))

// 生成点击坐标
func CreatePos(w, h int) [4]int {
	pos := [4]int{}
	x := rand.New(rand.NewSource(time.Now().UnixNano())).Intn(99)
	pos[0] = w * x / 100
	pos[2] = w * x / 100
	y := rand.New(rand.NewSource(time.Now().UnixNano() + 10000)).Intn(99)
	pos[1] = h * y / 100
	pos[3] = h * y / 100
	return pos
}
