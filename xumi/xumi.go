package xumi

import (
	"bytes"
	// "encoding/json"
	// "fmt"
	"io/ioutil"
	// "log"
	"net/http"
	// "net/url"
	"math/rand"
	"strconv"
	// "strings"
	// "time"
	// "unsafe"
	// "regexp"

	"ad/mongo"
	"ad/util"
	"github.com/pquerna/ffjson/ffjson"
)

// 网上厨房横幅	o8fn86lj
// 网上厨房插屏	379v47br
// 网上厨房开屏	2ocrffev
// 网上厨房信息流	mtxopjgz

const (
	URL           = "http://yi.iyisou.cn:8080/api/v1"
	APPID         = "o8fn86lj"
	APPID_FLOW    = "mtxopjgz"
	APPID_STARTUP = "2ocrffev"
)

func Xumi(getReq *util.ReqMsg, funcName string) util.ResMsg {
	return base(getReq, funcName, APPID, 1)
}
func XumiFlow(getReq *util.ReqMsg, funcName string) util.ResMsg {
	return base(getReq, funcName, APPID_FLOW, 4)
}
func XumiStartup(getReq *util.ReqMsg, funcName string) util.ResMsg {
	return base(getReq, funcName, APPID_STARTUP, 3)
}

func base(getReq *util.ReqMsg, funcName, appid string, adtype int) util.ResMsg {
	width, err := strconv.Atoi(getReq.Screenwidth)
	if err != nil {
		width = 0
	}
	height, err := strconv.Atoi(getReq.Screenheight)
	if err != nil {
		height = 0
	}
	carrier := "46000"
	switch getReq.Imsi {
	case "1":
		carrier = "46000"
	case "2":
		carrier = "46001"
	case "3":
		carrier = "46002"
	default:
		carrier = "0"
	}
	sd, _ := strconv.Atoi(getReq.Sd)
	dpi := sd / 160
	imsi := ""
	switch getReq.Imsi {
	case "1":
		imsi = "46000" + strconv.Itoa(10000+rand.Intn(90000)) + strconv.Itoa(10000+rand.Intn(90000))
	case "2":
		imsi = "46001" + strconv.Itoa(10000+rand.Intn(90000)) + strconv.Itoa(10000+rand.Intn(90000))
	case "3":
		imsi = "46002" + strconv.Itoa(10000+rand.Intn(90000)) + strconv.Itoa(10000+rand.Intn(90000))
	default:
		imsi = ""
	}
	postData := adreq{
		Appid:              appid,
		Adslottype:         adtype,
		Apiver:             "4",
		Appversion:         getReq.Appversion,
		Connecttype:        getReq.Network,
		Countrycode:        "CN",
		Devicename:         getReq.Model,
		Dlanguage:          "zh",
		Dmanufacturer:      getReq.Vendor,
		Dversion:           getReq.Osversion,
		Height:             height,
		Width:              width,
		Imei:               getReq.Imei,
		Ips:                getReq.Ip,
		Macadd:             getReq.Mac,
		Pkgname:            "cn.ecook",
		Screendensity:      dpi,
		Screentype:         2,
		SimNetworkOperator: carrier,
		Simcountryiso:      "cn",
		Simname:            imsi,
		Ua:                 getReq.Ua,
		Androidid:          getReq.Androidid,
	}

	if getReq.Os == "2" {
		postData.Pkgname = "cn.ecook.ecook"
	}

	ma, err := ffjson.Marshal(&postData)
	if err != nil {
		return util.ResMsg{}
	}
	// fmt.Println(string(ma))
	// 请求广告
	req, err := http.NewRequest("POST", URL, bytes.NewReader(ma))
	req.Header.Set("Connection", "keep-alive")
	req.Header.Set("Content-Type", "application/json;charset=utf-8")
	if err != nil {
		return util.ResMsg{}
	}
	resp, err := util.Client.Do(req)
	mongo.LogReq(funcName, getReq.Os)
	if err != nil {
		mongo.LogTimeout(funcName, getReq.Os)
		return util.ResMsg{}
	}
	data, err := ioutil.ReadAll(resp.Body)
	resp.Body.Close()
	if err != nil {
		mongo.LogNo(funcName, getReq.Os)
		return util.ResMsg{}
	}
	// fmt.Println(resp.StatusCode)
	if resp.StatusCode != 200 {
		mongo.LogTimeout(funcName, getReq.Os)
		return util.ResMsg{}
	}
	return FormateRes(data, getReq.Os, funcName)
}

// 根据广告平台的返回生成客户端返回
func FormateRes(res []byte, os, funcName string) util.ResMsg {
	resData := adres{}
	ffjson.Unmarshal(res, &resData)
	// fmt.Printf("%+v", resData)
	// fmt.Println(string(res))
	if len(resData.Ads) == 0 {
		// fmt.Println(2)
		mongo.LogNo(funcName, os)
		return util.ResMsg{}
	}

	ad := resData.Ads[0]
	if ad.Material_type != 1 {
		mongo.LogNo(funcName, os)
		return util.ResMsg{}
	}
	native := ad.Native_material
	if native.Image_url == "" {
		mongo.LogNo(funcName, os)
		return util.ResMsg{}
	}

	postData := util.ResMsg{
		Id:       "0",
		Weight:   0,
		State:    1,
		Title:    native.Title,
		Content:  native.Description1,
		ImageUrl: native.Image_url,
		Uri:      native.Click_url,
	}

	postData.Displayreport = native.Impression_log_url
	postData.Displayreport = append(postData.Displayreport, "http://120.26.247.23/ad/report.php?method="+funcName+"&event=displayreport&os="+os+"&t="+util.GetRandom())
	postData.Clickreport = native.Click_monitor_url
	postData.Clickreport = append(postData.Clickreport, "http://120.26.247.23/ad/report.php?method="+funcName+"&event=clickreport&os="+os+"&t="+util.GetRandom())

	return postData
}
