package xumi

type adreq struct {
	Appid              string `json:"appid"`
	Adslottype         int    `json:"adslottype"`
	Apiver             string `json:"apiver"`
	Appversion         string `json:"appversion"`
	Connecttype        string `json:"connecttype"`
	Countrycode        string `json:"countrycode"`
	Devicename         string `json:"devicename"`
	Dlanguage          string `json:"dlanguage"`
	Dmanufacturer      string `json:"dmanufacturer"`
	Dversion           string `json:"dversion"`
	Height             int    `json:"height"`
	Imei               string `json:"imei"`
	Ips                string `json:"ips"`
	Macadd             string `json:"macadd"`
	Pkgname            string `json:"pkgname"`
	Screendensity      int    `json:"screendensity"`
	Screentype         int    `json:"screentype"`
	SimNetworkOperator string `json:"SimNetworkOperator"`
	Simcountryiso      string `json:"simcountryiso"`
	Simname            string `json:"simname"`
	Uuid               string `json:"uuid"`
	Width              int    `json:"width"`
	Wifibssid          string `json:"wifidssid"`
	Wifissid           string `json:"wifissid"`
	Ua                 string `json:"ua"`
	Androidid          string `json:"androidid"`
}
