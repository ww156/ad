package xumi

type adres struct {
	Success bool
	Ads     []ad
}
type ad struct {
	Adslot_id       string
	Material_type   int
	Html_snippet    string
	Native_material native
}

type native struct {
	Id                 string
	Type               int
	Interaction_type   int
	Title              string
	Description1       string
	Description2       string
	Image_url          string
	Logo_url           string
	Click_url          string
	Impression_log_url []string
	Click_monitor_url  []string
	AppName            string
	Package            string
	AppDownload        []string
	AppInstall         []string
	AppActive          []string
	Image_size         size
	Timestamp          string
}

type size struct {
	Width  int
	Height int
}
