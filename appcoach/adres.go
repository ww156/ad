package appcoach

type adres struct {
	Ads ads
}

type ads struct {
	Id    string
	Count int
	Ad    []ad
}

type ad struct {
	Slotid       string
	Image_url    string
	Bid_type     string
	Price        float64
	Width        int
	Height       int
	Pkgname      string
	Name         string
	Description  string
	Platform     string
	ActionType   int
	Click_url    string
	Tracking_url string
	Icon_url     string
}
