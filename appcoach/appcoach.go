package appcoach

import (
	"bytes"
	"encoding/json"
	// "fmt"
	"io/ioutil"
	// "log"
	"net/http"
	// "net/url"
	"strconv"
	// "strings"
	// "time"
	// "unsafe"
	// "regexp"

	"ad/mongo"
	"ad/util"
)

const (
	URL            = "http://api.appcoachs.net/v1/m/image"
	APPID_ANDROID  = "kyDE8Cwf"
	APPID_IOS      = "h4Kgkyd4"
	ADID           = 2
	ADID_FLOW      = 3
	ADID_STARTUP   = 1
	ADID_STORYFLOW = 4
)

func Appcoach(getReq *util.ReqMsg, funcName string) util.ResMsg {
	return base(getReq, funcName, ADID)
}

func AppcoachFlow(getReq *util.ReqMsg, funcName string) util.ResMsg {
	return base(getReq, funcName, ADID_FLOW)
}

func AppcoachStartup(getReq *util.ReqMsg, funcName string) util.ResMsg {
	return base(getReq, funcName, ADID_STARTUP)
}

func AppcoachStoryFlow(getReq *util.ReqMsg, funcName string) util.ResMsg {
	return base(getReq, funcName, ADID_STORYFLOW)
}

func base(getReq *util.ReqMsg, funcName string, adid int) util.ResMsg {
	lat, _ := strconv.ParseFloat(getReq.Lat, 64)
	lon, _ := strconv.ParseFloat(getReq.Lng, 64)
	siteid := APPID_ANDROID
	osname := "Android"
	if getReq.Os == "2" {
		siteid = APPID_IOS
		osname = "IOS"
	}
	postData := adreq{
		Site: &site{
			Id: siteid,
		},
		Impression: []*impression{
			{
				Slotid: adid,
			},
		},
		Device: &device{
			Ip:    getReq.Ip,
			Ua:    getReq.Ua,
			Local: "zh",
			Ctype: getReq.Network,
			Os: &os{
				Name:    osname,
				Version: getReq.Osversion,
			},
			Android: &android{
				Id:   getReq.Androidid,
				Imei: getReq.Imei,
			},
			Ios: &ios{
				Idfa: getReq.Idfa,
			},
			Geo: &geo{
				Lat: lat,
				Lng: lon,
			},
		},
	}

	ma, err := json.Marshal(&postData)
	if err != nil {
		return util.ResMsg{}
	}
	// fmt.Println(string(ma))
	// 请求广告
	req, err := http.NewRequest("POST", URL, bytes.NewReader(ma))
	if err != nil {
		return util.ResMsg{}
	}
	req.Header.Set("Connection", "keep-alive")
	req.Header.Set("Content-Type", "application/json;charset=utf-8")
	req.Header.Set("X-AppcoachS-Site", siteid)

	resp, err := util.Client.Do(req)
	mongo.LogReq(funcName, getReq.Os)
	if err != nil {
		mongo.LogTimeout(funcName, getReq.Os)
		return util.ResMsg{}
	}
	data, err := ioutil.ReadAll(resp.Body)
	resp.Body.Close()
	if err != nil {
		mongo.LogNo(funcName, getReq.Os)
		return util.ResMsg{}
	}
	// fmt.Println(resp.StatusCode)
	if resp.StatusCode != 200 {
		mongo.LogTimeout(funcName, getReq.Os)
		return util.ResMsg{}
	}
	// fmt.Println(string(data))
	return FormateRes(data, getReq.Os, funcName)
}

// 根据广告平台的返回生成客户端返回
func FormateRes(res []byte, os, funcName string) util.ResMsg {
	resData := adres{}
	json.Unmarshal(res, &resData)
	// fmt.Printf("%+v", resData)
	// fmt.Println(string(res))
	if len(resData.Ads.Ad) == 0 {
		mongo.LogNo(funcName, os)
		return util.ResMsg{}
	}

	ad := resData.Ads.Ad[0]
	if ad.Image_url == "" {
		mongo.LogNo(funcName, os)
		return util.ResMsg{}
	}
	postData := util.ResMsg{
		Id:       "0",
		Weight:   0,
		State:    1,
		Title:    ad.Name,
		Content:  ad.Description,
		ImageUrl: ad.Image_url,
		Uri:      ad.Click_url,
	}
	if ad.Tracking_url != "" {
		postData.Displayreport = append(postData.Displayreport, ad.Tracking_url)
	}
	postData.Displayreport = append(postData.Displayreport, "http://120.26.247.23/ad/report.php?method="+funcName+"&event=displayreport&os="+os+"&t="+util.GetRandom())
	postData.Clickreport = append(postData.Clickreport, "http://120.26.247.23/ad/report.php?method="+funcName+"&event=clickreport&os="+os+"&t="+util.GetRandom())

	return postData
}
