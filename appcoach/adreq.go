package appcoach

type adreq struct {
	Site       *site         `json:"site"`
	Aff_id     string        `json:"aff_id"`
	Impression []*impression `json:"impression,omitempty"`
	Device     *device       `json:"device,omitempty"`
	User       *user         `json:"user,omitempty"`
}
type site struct {
	Id string `json:"id"`
}
type impression struct {
	Slotid int `json:"slotid"`
}
type device struct {
	Ip      string   `json:"ip"`
	Ua      string   `json:"ua"`
	Local   string   `json:"local"`
	Ctype   string   `json:"ctype"`
	Os      *os      `json:"os,omitempty"`
	Android *android `json:"android,omitempty"`
	Ios     *ios     `json:"ios,omitempty"`
	Geo     *geo     `json:"geo,omitempty"`
}

type os struct {
	Name    string `json:"name"`
	Version string `json:"version"`
}

type android struct {
	Gald      string `json:"gald"`
	Gald_md5  string `json:"gald_md5"`
	Gald_sha1 string `json:"gald_sha1"`
	Id        string `json:"id"`
	Id_md5    string `json:"id_md5"`
	Id_sha1   string `json:"Id_sha1"`
	Imei      string `json:"imei"`
	Imei_md5  string `json:"imei"`
	Imei_sha1 string `json:"imei_sha1"`
}

type ios struct {
	Idfa      string `json:"idfa"`
	Idfa_md5  string `json:"idfa_md5"`
	Idfa_sha1 string `json:"idfa_sha1"`
}

type geo struct {
	Lat float64 `json:"lat"`
	Lng float64 `json:"lng"`
}

type user struct {
	Tags   string `json:"tags"`
	Gender string `json:"gender"`
	Job    int    `json:"job"`
	Ref    string `json:"ref"`
}
