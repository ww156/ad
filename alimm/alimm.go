package alimm

import (
	"bytes"
	// "encoding/json"
	// "fmt"
	"io/ioutil"
	// "log"
	"net/http"
	"strconv"
	// "strings"
	// "time"

	"github.com/golang/protobuf/proto"

	"ad/mongo"
	"ad/util"
)

const (
	// URL              = "http://140.205.241.3/api"
	URL              = "http://ope.tanx.com/api"
	ANDROID_FLOW_PID = "mm_26632556_23974014_80540294"
	IOS_FLOW_PID     = "mm_26632556_23974014_80532473"
)

func Alimm(getReq *util.ReqMsg, funcName string) util.ResMsg {
	if getReq.Os == "1" {
		return base(getReq, funcName, ANDROID_FLOW_PID)
	}
	return base(getReq, funcName, IOS_FLOW_PID)
}

func AlimmStartUp(getReq *util.ReqMsg, funcName string) util.ResMsg {
	if getReq.Os == "1" {
		return base(getReq, funcName, ANDROID_FLOW_PID)
	}
	return base(getReq, funcName, IOS_FLOW_PID)
}

func AlimmFlow(getReq *util.ReqMsg, funcName string) util.ResMsg {
	if getReq.Os == "1" {
		return base(getReq, funcName, ANDROID_FLOW_PID)
	}
	return base(getReq, funcName, IOS_FLOW_PID)
}

func base(getReq *util.ReqMsg, funcName string, pid string) util.ResMsg {
	reqData := Request{
		Version: proto.Int(2),
		Id:      proto.String(util.GetRandom()),
		Imp: []*Request_Impression{
			{
				Id:  proto.Int(0),
				Pid: proto.String(pid),
			},
		},
		Device: &Request_Device{
			Ip:          proto.String(getReq.Ip),
			UserAgent:   proto.String(getReq.Ua),
			Idfa:        proto.String(getReq.Idfa),
			Imei:        proto.String(getReq.Imei),
			Mac:         proto.String(getReq.Mac),
			AndroidId:   proto.String(getReq.Androidid),
			DeviceType:  proto.Int(0),
			Brand:       proto.String(getReq.Vendor),
			Model:       proto.String(getReq.Model),
			Os:          proto.String("Android"),
			Osv:         proto.String(getReq.Osversion),
			Orientation: proto.Int(0), // 屏幕方向未知： 0
		},
		App: &Request_App{
			PackageName: proto.String("cn.ecook"),
			AppName:     proto.String("网上厨房"),
		},
	}
	device := reqData.Device
	app := reqData.App

	// Operator:    proto.Int(getReq.Imsi),
	operator, err := strconv.Atoi(getReq.Imsi)
	if err == nil {
		device.Operator = proto.Int(operator)
	}
	// Width:       proto.Int(getReq.Screenwidth),
	width, err := strconv.Atoi(getReq.Screenwidth)
	if err == nil {
		device.Operator = proto.Int(width)
	}
	// Height:      proto.Int(getReq.Screenheight),
	height, err := strconv.Atoi(getReq.Screenheight)
	if err == nil {
		device.Height = proto.Int(height)
	}
	// 网络
	switch getReq.Network {
	case "以太网":
		device.Network = proto.Int(0)
	case "wifi":
		device.Network = proto.Int(1)
	case "unknow":
		device.Network = proto.Int(0)
	case "2g":
		device.Network = proto.Int(2)
	case "3g":
		device.Network = proto.Int(3)
	case "4g":
		device.Network = proto.Int(4)
	}
	sd, err := strconv.Atoi(getReq.Sd)
	if err == nil {
		device.PixelRatio = proto.Int(sd / 160)
	}
	if getReq.Os == "2" {
		device.Os = proto.String("Ios")
		app.AppName = proto.String("cn.ecook.ecook")
	}
	// 编码请求数据
	ma, err := proto.Marshal(&reqData)
	if err != nil {
		return util.ResMsg{}
	}
	// 请求广告
	req, err := http.NewRequest("POST", URL, bytes.NewBuffer(ma))
	if err != nil {
		return util.ResMsg{}
	}
	// 请求头设置
	req.Header.Set("Connection", "keep-alive")
	req.Header.Set("Content-Type", "application/octet-stream")
	resp, err := util.Client.Do(req)
	mongo.LogReq(funcName, getReq.Os)
	// 请求错误，日志记录为timeout
	if err != nil {
		mongo.LogTimeout(funcName, getReq.Os)
		util.HttpRequestCount.WithLabelValues(funcName + "timeouterr").Inc()
		return util.ResMsg{}
	}
	data, err := ioutil.ReadAll(resp.Body)
	resp.Body.Close()
	if err != nil {
		mongo.LogNo(funcName, getReq.Os)
		util.HttpRequestCount.WithLabelValues(funcName + "readresperr").Inc()
		return util.ResMsg{}
	}
	// 请求返回失败，日志记录为timeout
	if resp.StatusCode != 200 {
		mongo.LogTimeout(funcName, getReq.Os)
		util.HttpRequestCount.WithLabelValues(funcName + "not200err").Inc()
		return util.ResMsg{}
	}
	return FormateRes(data, getReq.Os, funcName)
}

// 根据广告平台的返回生成客户端返回
func FormateRes(res []byte, os, funcName string) util.ResMsg {
	resData := Response{}
	proto.Unmarshal(res, &resData)
	// fmt.Printf("doumeng:%+v\n", resData)
	if *resData.Status != 0 {
		mongo.LogNo(funcName, os)
		util.HttpRequestCount.WithLabelValues(funcName + "noerr").Inc()
		return util.ResMsg{}
	}
	if len(resData.Seat) == 0 {
		mongo.LogNo(funcName, os)
		util.HttpRequestCount.WithLabelValues(funcName + "noerr").Inc()
		return util.ResMsg{}
	}
	if len(resData.Seat[0].Ad) == 0 {
		mongo.LogNo(funcName, os)
		util.HttpRequestCount.WithLabelValues(funcName + "noerr").Inc()
		return util.ResMsg{}
	}
	ad := resData.Seat[0].Ad[0]
	attr := ad.NativeAd.Attr
	var img_url, title, content string
	for _, value := range attr {
		if *value.Name == "img_url" {
			img_url = *value.Value
		}
		if *value.Name == "title" {
			title = *value.Value
		}
		if *value.Name == "description" {
			content = *value.Value
		}
	}
	postData := util.ResMsg{
		Id:       "0",
		Weight:   0,
		State:    0,
		Title:    title,
		Content:  content,
		ImageUrl: img_url,
		Uri:      *ad.ClickThroughUrl,
	}
	postData.Displayreport = ad.ImpressionTrackingUrl
	postData.Displayreport = append(postData.Displayreport, "http://120.26.247.23/ad/report.php?method="+funcName+"&event=displayreport&os="+os+"&t="+util.GetRandom())
	postData.Clickreport = ad.ClickTrackingUrl
	postData.Clickreport = append(postData.Clickreport, "http://120.26.247.23/ad/report.php?method="+funcName+"&event=clickreport&os="+os+"&t="+util.GetRandom())

	return postData
}
