package zibolan

import (
	"bytes"
	// "encoding/json"
	// "fmt"
	"io/ioutil"
	// "log"
	"net/http"
	// "net/url"
	"strconv"
	"strings"
	// "time"
	// "unsafe"
	// "regexp"
	"compress/gzip"

	"ad/mongo"
	"ad/util"
	"github.com/pquerna/ffjson/ffjson"
)

const (
	URL      = "http://ssp.zibolan.com/api2/ad.do"
	APPID_A  = "M710872"
	ADID_A   = "A96202818"
	ADID_A_F = "A30601059"
	ADID_A_S = "A44751998"
	APPID_I  = "M536128"
	ADID_I   = "A43728080"
	ADID_I_F = "A73462928"
	ADID_I_S = "A23036541"
)

func Zibolan(getReq *util.ReqMsg, funcName string) util.ResMsg {
	if getReq.Os == "1" {
		return base(getReq, funcName, APPID_A, ADID_A, 640, 100, 1)
	}
	return base(getReq, funcName, APPID_I, ADID_I, 640, 100, 1)
}

func ZibolanFlow(getReq *util.ReqMsg, funcName string) util.ResMsg {
	if getReq.Os == "1" {
		return base(getReq, funcName, APPID_A, ADID_A_F, 640, 360, 2)
	}
	return base(getReq, funcName, APPID_I, ADID_I_F, 640, 360, 2)
}

func ZibolanStartup(getReq *util.ReqMsg, funcName string) util.ResMsg {
	if getReq.Os == "1" {
		return base(getReq, funcName, APPID_A, ADID_A_S, 640, 960, 3)
	}
	return base(getReq, funcName, APPID_I, ADID_I_S, 640, 960, 3)
}

func base(getReq *util.ReqMsg, funcName, appid, adid string, w, h, adtype int) util.ResMsg {
	imsi := ""
	switch getReq.Imsi {
	case "1":
		imsi = "46000"
	case "2":
		imsi = "46001"
	case "3":
		imsi = "46002"
	default:
		imsi = ""
	}
	network := 0
	switch getReq.Network {
	case "以太网":
		network = 1
	case "wifi":
		network = 2
	case "2g":
		network = 4
	case "3g":
		network = 5
	case "4g":
		network = 6
	default:
		network = 0
	}
	width, err := strconv.Atoi(getReq.Screenwidth)
	if err != nil {
		width = 0
	}
	height, err := strconv.Atoi(getReq.Screenheight)
	if err != nil {
		height = 0
	}
	sd, _ := strconv.Atoi(getReq.Sd)
	lat, err := strconv.ParseFloat(getReq.Lat, 64)
	if err != nil {
		lat = 0
	}
	lon, err := strconv.ParseFloat(getReq.Lng, 64)
	if err != nil {
		lon = 0
	}
	postData := adreq{
		Id: util.GetRandom(),
		App: &app{
			Id:     appid,
			Name:   "网上厨房",
			Ver:    getReq.Appversion,
			Bundle: "cn.ecook",
		},
		Device: &device{
			Os:             "android",
			Osv:            getReq.Osversion,
			Make:           getReq.Vendor,
			Model:          getReq.Model,
			Ip:             getReq.Ip,
			Ua:             getReq.Ua,
			W:              width,
			H:              height,
			Ppi:            sd,
			Dpidsha1:       util.Sha1(getReq.Androidid),
			Connectiontype: network,
			Devicetype:     4,
			Geo: &geo{
				Lat: lat,
				Lon: lon,
			},
			Ext: &dext{
				Plmn:        imsi,
				Imei:        getReq.Imei,
				Mac:         getReq.Mac,
				Android_id:  getReq.Androidid,
				Adid:        getReq.Idfa,
				Orientation: 1,
			},
		},
		Imp: []*imp{
			&imp{
				Id: util.GetRandom(),
				Banner: &banner{
					W: w,
					H: h,
				},
				Tagid: adid,
				Ext: &iext{
					Inventory_types: []int{1, 2},
				},
			},
		},
		Test: true,
	}
	app := postData.App
	imp := postData.Imp[0]
	baext := postData.Imp[0].Ext
	if getReq.Os == "2" {
		app.Bundle = "cn.ecook.ecook"
	}
	switch adtype {
	case 1:
		imp.Instl = true
	case 2:
		baext.Is_native = true
	case 3:
		baext.Is_splash_screen = true
	}

	ma, err := ffjson.Marshal(&postData)
	if err != nil {
		return util.ResMsg{}
	}

	// 请求广告
	req, err := http.NewRequest("POST", URL, bytes.NewBuffer(ma))
	if err != nil {
		return util.ResMsg{}
	}
	req.Header.Set("Connection", "keep-alive")
	req.Header.Set("Content-Type", "application/json;charset=UTF-8")
	req.Header.Set("Accept-Encoding", "gzip")

	resp, err := util.Client.Do(req)
	mongo.LogReq(funcName, getReq.Os)
	if err != nil {
		// fmt.Println(err.Error())
		mongo.LogTimeout(funcName, getReq.Os)
		return util.ResMsg{}
	}
	data := []byte{}
	if strings.Contains(resp.Header.Get("Content-Encoding"), "gzip") {
		r, err := gzip.NewReader(resp.Body)
		resp.Body.Close()
		if err != nil {
			mongo.LogNo(funcName, getReq.Os)
			return util.ResMsg{}
		}
		data, err = ioutil.ReadAll(r)
		r.Close()
		if err != nil {
			mongo.LogNo(funcName, getReq.Os)
			return util.ResMsg{}
		}
	} else {
		data, err = ioutil.ReadAll(resp.Body)
		resp.Body.Close()
	}

	if err != nil {
		mongo.LogNo(funcName, getReq.Os)
		return util.ResMsg{}
	}
	// fmt.Println(resp.StatusCode)
	if resp.StatusCode != 200 {
		mongo.LogTimeout(funcName, getReq.Os)
		return util.ResMsg{}
	}

	return FormateRes(data, getReq.Os, funcName)
}

// 根据广告平台的返回生成客户端返回
func FormateRes(res []byte, os, funcName string) util.ResMsg {
	resData := adres{}
	ffjson.Unmarshal(res, &resData)
	// fmt.Println(string(res))
	if len(resData.Seatbid) == 0 {
		mongo.LogNo(funcName, os)
		return util.ResMsg{}
	}
	if len(resData.Seatbid[0].Bid) == 0 {
		mongo.LogNo(funcName, os)
		return util.ResMsg{}
	}

	ad := resData.Seatbid[0].Bid[0]
	rext := ad.Ext
	uri := rext.Clkurl
	if rext.Action == 7 {
		uri = strings.Replace(uri, "${ACCT_TYPE}", "0", -1)
	}

	postData := util.ResMsg{
		Id:       "0",
		Weight:   0,
		State:    0,
		Title:    rext.Title,
		Content:  rext.Desc,
		ImageUrl: ad.Iurl,
		Uri:      replace(uri),
	}
	// fmt.Println(ad.Curl)
	if len(ad.Ext.Native_pics) > 0 && ad.Ext.Native_pics[0] != "" {
		postData.ImageUrl = ad.Ext.Native_pics[0]
	}

	postData.Displayreport = rext.Imptrackers
	postData.Displayreport = append(postData.Displayreport, "http://120.26.247.23/ad/report.php?method="+funcName+"&event=displayreport&os="+os+"&t="+util.GetRandom())
	for _, v := range rext.Clktrackers {
		if v != "" {
			postData.Clickreport = append(postData.Clickreport, replace(v))
		}
	}
	postData.Clickreport = append(postData.Clickreport, "http://120.26.247.23/ad/report.php?method="+funcName+"&event=clickreport&os="+os+"&t="+util.GetRandom())

	return postData
}

func replace(str string) string {
	str = strings.Replace(str, "${DOWN_X}", "IT_CLK_PNT_DOWN_X", -1)
	str = strings.Replace(str, "${DOWN_Y}", "IT_CLK_PNT_DOWN_Y", -1)
	str = strings.Replace(str, "${UP_X}", "IT_CLK_PNT_UP_X", -1)
	str = strings.Replace(str, "${UP_Y}", "IT_CLK_PNT_UP_Y", -1)
	return str
}
