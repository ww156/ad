package zibolan

type adres struct {
	Id      string
	Seatbid []seatbid
	Nbr     string
}
type seatbid struct {
	Bid []bid
}
type bid struct {
	Id     string
	Impid  string
	Bundle string
	Iurl   string
	W      int
	H      int
	Cat    []string
	Ext    resext
	Adm    string
}
type resext struct {
	App_ver            string
	Clkurl             string
	Imptrackers        []string
	Clktrackers        []string
	StartDownload      []string
	FinishDownload     []string
	Installed          []string
	Html_snippet       string
	Inventory_type     int
	Title              string
	Desc               string
	Action             int
	Download_file_name string
	Native_logo        string
	Native_pics        []string
	Deeplink           string
	Video              resvideo
}
type resvideo struct {
	Adtype           string
	Package_name     string
	Url              string
	Duration         string
	Start_ocover     string
	Over_cover       string
	Landing_url      string
	Deep_link        string
	Start_url        []string
	Over_url         []string
	Click_url        []string
	StartDownload    []string
	FinishedDownload []string
	InstallStart     []string
	Installed        []string
	Ad_source_mark   string
}
