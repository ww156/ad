package zibolan

type adreq struct {
	Id     string  `json:"id"`
	App    *app    `json:"app"`
	Device *device `json:"device"`
	Imp    []*imp  `json:"imp"`
	Bcat   []*bcat `json:"bcat"`
	User   *user   `json:"user"`
	Test   bool    `json:"test"`
	Ext    *ext    `json:"ext"`
}

type app struct {
	Id     string   `json:"id"`
	Name   string   `json:"name"`
	Ver    string   `json:"ver"`
	Bundle string   `json:"bundle"`
	Cat    []string `json:"cat"`
}
type device struct {
	Os             string `json:"os"`
	Dnt            bool   `json:"dnt"`
	Osv            string `json:"osv"`
	Make           string `json:"make"`
	Model          string `json:"model"`
	Ip             string `json:"ip"`
	Ua             string `json:"ua"`
	Hwv            string `json:"hwv"`
	W              int    `json:"w"`
	H              int    `json:"h"`
	Ppi            int    `json:"ppi"`
	Macsha1        string `json:"macsha1"`
	Didsha1        string `json:"didsha1"`
	Dpidsha1       string `json:"dpidsha1"`
	Connectiontype int    `json:"connectiontype"`
	Devicetype     int    `json:"devicetype"`
	Geo            *geo   `json:"geo"`
	Ext            *dext  `json:"ext"`
}
type dext struct {
	Plmn        string `json:"plmn"`
	Imei        string `json:"imei"`
	Mac         string `json:"mac"`
	Android_id  string `json:"android_id"`
	Adid        string `json:"adid"`
	Orientation int    `json:"orientation"`
	Tel         string `json:"tel"`
	Sex         string `json:"sex"`
	Name        string `json:"name"`
}

type geo struct {
	Lat        float64 `json:"lat"`
	Lon        float64 `json:"lon"`
	Country    string  `json:"country"`
	Region     string  `json:"region"`
	City       string  `json:"city"`
	LocationTy int     `json:"locationTy"`
	Pe         string  `json:"pe"`
	Ext        *gext   `json:"ext"`
}
type gext struct {
	Accu   int    `json:"accu"`
	Street string `json:"street"`
}

type imp struct {
	Id     string  `json:"id"`
	Instl  bool    `json:"instl"`
	Banner *banner `json:"banner"`
	Video  *video  `json:"video"`
	Tagid  string  `json:"tagid"`
	Ext    *iext   `json:"ext"`
}
type iext struct {
	Is_splash_screen bool  `json:"is_splash_screen"`
	Is_native        bool  `json:"is_native"`
	Inventory_types  []int `json:"inventory_types"`
}
type banner struct {
	W    int `json:"w"`
	H    int `json:"h"`
	Wmax int `json:"wmax"`
	Hmax int `json:"Hmax"`
	Wmin int `json:"wmin"`
	Hmin int `json:"hmin"`
	Pos  int `json:"pos"`
}
type video struct {
	Mimes       []string `json:"mimes"`
	Protocols   []int    `json:"protocols"`
	Minduration int      `json:"minduration"`
	Maxduration int      `json:"maxduration"`
	W           int      `json:"w"`
	H           int      `json:"h"`
	Pos         int      `json:"pos"`
}
type bcat struct {
}
type user struct {
	Id     string  `json:"id"`
	Yob    int     `json:"yob"`
	Gender string  `json:"gender"`
	Geo    *geo    `json:"geo"`
	Data   []*uext `json:"data"`
}
type uext struct {
	Segment []*segment
}

type segment struct {
	Id    string `json:"id"`
	Value string `json:"value"`
}
type ext struct {
	Html     bool `json:"html"`
	Deeplink bool `json:"deeplink"`
}
