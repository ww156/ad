package ruishi

type adres struct {
	Status       int
	Cid          string
	W            int
	H            int
	Adm          string
	Nativead     native
	Vtype        int
	Vast         string
	Video        video
	Imp_tracking []string
	Clk_tracking []string
}

type native struct {
	Title            string
	Desc             string
	Icon             []img
	Img              []img
	Btnname          string
	Rating           int
	Down_count       int
	Ldp              string
	App_Download_url string
}

type img struct {
	Url string
	W   int
	H   int
}

type video struct {
	Vurl       string
	Duration   int
	Vm_d_start []string
	Vm_d_fail  []string
	Vm_d_succ  []string
	Vm_p_start []string
	Vm_p_fail  []string
	Vm_p_succ  []string
}
