package ruishi

import (
	// "bytes"
	// "encoding/json"
	// "fmt"
	"io/ioutil"
	// "log"
	// "math/rand"
	"net/http"
	"net/url"
	// "strconv"
	"strings"
	// "time"
	// "unsafe"
	// "regexp"

	"ad/mongo"
	"ad/util"
	"github.com/PuerkitoBio/goquery"
	"github.com/pquerna/ffjson/ffjson"
)

const (
	URL                  = "http://a.vlion.cn/ssp"
	APPID_ANDROID        = "85"
	ADID_ANDROID         = "179"
	ADID_ANDROID_FLOW    = "181"
	ADID_ANDROID_STARTUP = "182"
	APPID_IOS            = "86"
	ADID_IOS             = "180"
	ADID_IOS_FLOW        = "183"
	ADID_IOS_STARTUP     = "184"
)

func Ruishi(getReq *util.ReqMsg, funcName string) util.ResMsg {
	if getReq.Os == "1" {
		return base(getReq, funcName, "0", APPID_ANDROID, ADID_ANDROID)
		// return base(getReq, funcName, "0", "60", "187")
	}
	return base(getReq, funcName, "0", APPID_IOS, ADID_IOS)
}
func RuishiFlow(getReq *util.ReqMsg, funcName string) util.ResMsg {
	if getReq.Os == "1" {
		return base(getReq, funcName, "3", APPID_ANDROID, ADID_ANDROID_FLOW)
	}
	return base(getReq, funcName, "3", APPID_IOS, ADID_IOS_FLOW)
}
func RuishiStartup(getReq *util.ReqMsg, funcName string) util.ResMsg {
	if getReq.Os == "1" {
		return base(getReq, funcName, "2", APPID_ANDROID, ADID_ANDROID_STARTUP)
	}
	return base(getReq, funcName, "2", APPID_IOS, ADID_IOS_STARTUP)
}
func base(getReq *util.ReqMsg, funcName, adtype, appid, adid string) util.ResMsg {
	network := "0"
	switch getReq.Network {
	case "wifi":
		network = "1"
	case "2g":
		network = "2"
	case "3g":
		network = "3"
	case "4g":
		network = "4"
	default:
		network = "0"
	}
	v := url.Values{}
	// appid = "60"
	// adid = "187"
	v.Set("tagid", adid)
	v.Set("appid", appid)
	v.Set("appname", "网上厨房")
	v.Set("pkgname", "cn.ecook")
	v.Set("appversion", getReq.Appversion)
	v.Set("os", "1")
	v.Set("osv", getReq.Osversion)
	v.Set("carrier", getReq.Imsi)
	v.Set("conn", network)
	v.Set("ip", getReq.Ip)
	v.Set("make", getReq.Vendor)
	v.Set("imei", getReq.Imei)
	v.Set("idfa", getReq.Idfa)
	v.Set("anid", getReq.Androidid)
	v.Set("mac", getReq.Mac)
	v.Set("lon", getReq.Lng)
	v.Set("lat", getReq.Lat)
	v.Set("sw", getReq.Screenwidth)
	v.Set("sh", getReq.Screenheight)
	v.Set("devicetype", "1")
	v.Set("ua", getReq.Ua)
	v.Set("adt", adtype)
	if getReq.Os == "2" {
		v.Set("pkgname", "cn.ecook.ecook")
		v.Set("os", "2")
	}

	// 请求广告
	req, err := http.NewRequest("GET", URL+"?"+v.Encode(), nil)
	req.Header.Set("Connection", "keep-alive")
	req.Header.Set("Content-Type", "application/json;charset=utf-8")

	if err != nil {
		return util.ResMsg{}
	}
	resp, err := util.Client.Do(req)
	mongo.LogReq(funcName, getReq.Os)
	if err != nil {
		mongo.LogTimeout(funcName, getReq.Os)
		return util.ResMsg{}
	}
	data, err := ioutil.ReadAll(resp.Body)
	resp.Body.Close()
	if err != nil {
		mongo.LogNo(funcName, getReq.Os)
		return util.ResMsg{}
	}
	// fmt.Println(resp.StatusCode)
	if resp.StatusCode != 200 {
		mongo.LogTimeout(funcName, getReq.Os)
		return util.ResMsg{}
	}
	return FormateRes(data, getReq.Os, funcName, adtype)
}

// 根据广告平台的返回生成客户端返回
func FormateRes(res []byte, os, funcName, adtype string) util.ResMsg {
	resData := adres{}
	ffjson.Unmarshal(res, &resData)
	// fmt.Printf("%+v", resData)
	// fmt.Println(string(res))
	if resData.Status != 0 && resData.Status != 101 {
		mongo.LogNo(funcName, os)
		return util.ResMsg{}
	}

	ad := resData.Nativead

	postData := util.ResMsg{
		Id:       "0",
		Weight:   0,
		State:    1,
		Title:    ad.Title,
		Content:  ad.Desc,
		ImageUrl: "",
		Uri:      ad.Ldp,
	}

	if adtype == "3" {
		if len(ad.Img) == 0 || ad.Img[0].Url == "" {
			mongo.LogNo(funcName, os)
			return util.ResMsg{}
		}
		postData.ImageUrl = ad.Img[0].Url
	} else {
		htmlStr := resData.Adm
		doc, err := goquery.NewDocumentFromReader(strings.NewReader(htmlStr))
		if err != nil {
			mongo.LogNo(funcName, os)
			return util.ResMsg{}
		}
		postData.ImageUrl = doc.Find("img").AttrOr("src", "")
		postData.Uri = doc.Find("a").AttrOr("href", "")
	}

	postData.Displayreport = resData.Imp_tracking
	postData.Displayreport = append(postData.Displayreport, "http://120.26.247.23/ad/report.php?method="+funcName+"&event=displayreport&os="+os+"&t="+util.GetRandom())
	postData.Clickreport = resData.Clk_tracking
	postData.Clickreport = append(postData.Clickreport, "http://120.26.247.23/ad/report.php?method="+funcName+"&event=clickreport&os="+os+"&t="+util.GetRandom())

	return postData
}
