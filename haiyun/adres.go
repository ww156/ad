package haiyun

type adres struct {
	Reqid string
	Pt    float64
	V     string
	Ad    Ad
}

type Ad struct {
	Action   int
	Aid      string
	Width    int
	Height   int
	Pid      string
	Adm      string
	Url      string
	Ext_adms []string
	Title    string
	Desc     string
	Imp      []string
	Clk      []string
	Dp_url   string
	Dp_clk   string
	App      App
}

type App struct {
	Icon_url string
	Name     string
	Pname    string
}
