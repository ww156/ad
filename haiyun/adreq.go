package haiyun

type Device struct {
	Openudid  string `json:"openudid,omitempty"`
	Androidid string `json:"androidid,omitempty"`
	Imei      string `json:"imei,omitempty"`
	Idfa      string `json:"idfa,omitempty"`
	Mac       string `json:"mac,omitempty"`
	Os        string `json:"os,omitempty"`
	Os_v      string `json:"os_v,omitempty"`
	Dev       string `json:"dev,omitempty"`
	Md        string `json:"md,omitempty"`
	Network   string `json:"network,omitempty"`
	Operator  string `json:"operator,omitempty"`
	Width     string `json:"width,omitempty"`
	Height    string `json:"height,omitempty"`
}
