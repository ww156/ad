package haiyun

import (
	// "bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	// "log"
	"net/http"
	"net/url"
	// "strconv"
	// "strings"
	// "time"
	// "unsafe"
	// "regexp"

	"ad/mongo"
	"ad/util"
)

// IOS：
// 开屏：  21b188f7
// 信息流：febdaba4
// banner：dad93d09
// 安卓：
// 开屏：  1754d494
// 信息流：a017a460
// banner：1b1cfccd

const (
	PRO_URL             = "http://hyssp.haiyunx.com/spush/s2s.gif"
	DEV_URL             = "http://fupinglife.6655.la:8888/spush/s2s.gif"
	PID_ANDROID         = "1b1cfccd"
	PID_ANDROID_FLOW    = "a017a460"
	PID_ANDROID_STARTUP = "1754d494"
	PID_IOS             = "dad93d09"
	PID_IOS_FLOW        = "febdaba4"
	PID_IOS_STARTUP     = "21b188f7"
)

func Haiyun(getReq *util.ReqMsg, funcName string) util.ResMsg {
	if getReq.Os == "1" {
		return base(getReq, funcName, PID_ANDROID)
	}
	return base(getReq, funcName, PID_IOS)
}

func HaiyunFlow(getReq *util.ReqMsg, funcName string) util.ResMsg {
	if getReq.Os == "1" {
		return base(getReq, funcName, PID_ANDROID_FLOW)
	}
	return base(getReq, funcName, PID_IOS_FLOW)
}

func HaiyunStartup(getReq *util.ReqMsg, funcName string) util.ResMsg {
	if getReq.Os == "1" {
		return base(getReq, funcName, PID_ANDROID_STARTUP)
	}
	return base(getReq, funcName, PID_IOS_STARTUP)
}

func base(getReq *util.ReqMsg, funcName, pid string) util.ResMsg {
	value := url.Values{}
	value.Set("pid", pid)
	value.Set("ptype", "1")
	value.Set("ip", getReq.Ip)
	value.Set("ua", getReq.Ua)
	value.Set("reqid", util.GetRandom())
	value.Set("app_version", getReq.Appversion)
	value.Set("v", "2.0.0")
	// device对象
	if getReq.Network == "以太网" {
		getReq.Network = "0"
	}
	switch getReq.Network {
	case "以太网":
		getReq.Network = "0"
	case "wifi":
		getReq.Network = "1"
	case "2g":
		getReq.Network = "2"
	case "3g":
		getReq.Network = "3"
	case "4g":
		getReq.Network = "4"
	}
	if getReq.Imsi == "unknown" {
		getReq.Imsi = "0"
	}
	device := Device{
		Openudid:  getReq.Openudid,
		Androidid: getReq.Androidid,
		Imei:      getReq.Imei,
		Idfa:      getReq.Idfa,
		Mac:       getReq.Mac,
		Os:        getReq.Os,
		Os_v:      getReq.Osversion,
		Dev:       getReq.Model,
		Md:        getReq.Vendor,
		Network:   getReq.Network,
		Operator:  getReq.Imsi,
		Width:     getReq.Screenwidth,
		Height:    getReq.Screenheight,
	}
	mdevice, err := json.Marshal(device)
	if err != nil {
		fmt.Println("[haiyun.go-89]", err.Error())
	}
	value.Set("device", string(mdevice))

	// client := &http.Client{Timeout: util.Timeout}
	req, err := http.NewRequest("GET", PRO_URL+"?"+value.Encode(), nil)
	if err != nil {
		return util.ResMsg{}
	}
	req.Header.Set("Connection", "keep-alive")
	req.Header.Set("Content-Type", "application/json;charset=utf-8")
	resp, err := util.Client.Do(req)
	mongo.LogReq(funcName, getReq.Os)
	if err != nil {
		fmt.Println("[haiyun.go-75]", err.Error())
		mongo.LogTimeout(funcName, getReq.Os)
		util.HttpRequestCount.WithLabelValues(funcName + "timeouterr").Inc()
		return util.ResMsg{}
	}
	data, err := ioutil.ReadAll(resp.Body)
	resp.Body.Close()
	if err != nil {
		mongo.LogNo(funcName, getReq.Os)
		util.HttpRequestCount.WithLabelValues(funcName + "readresperr").Inc()
		return util.ResMsg{}
	}
	if resp.StatusCode != 200 {
		mongo.LogTimeout(funcName, getReq.Os)
		util.HttpRequestCount.WithLabelValues(funcName + "not200err").Inc()
		return util.ResMsg{}
	}
	// fmt.Println("response:", string(data))
	return FormateRes(data, getReq.Os, funcName)
}

func FormateRes(res []byte, os, funcName string) util.ResMsg {
	resData := &adres{}
	json.Unmarshal(res, resData)
	// fmt.Printf("%+v", resData)
	ad := resData.Ad
	if ad.Adm == "" {
		mongo.LogNo(funcName, os)
		util.HttpRequestCount.WithLabelValues(funcName + "noerr").Inc()
		return util.ResMsg{}
	}
	title, _ := url.QueryUnescape(ad.Title)
	desc, _ := url.QueryUnescape(ad.Desc)
	adm, _ := url.QueryUnescape(ad.Adm)
	if adm == "" {
		mongo.LogNo(funcName, os)
		util.HttpRequestCount.WithLabelValues(funcName + "noerr").Inc()
		return util.ResMsg{}
	}
	uri, _ := url.QueryUnescape(ad.Url)
	postData := util.ResMsg{
		Id:       "0",
		Weight:   0,
		State:    0,
		Title:    title,
		Content:  desc,
		ImageUrl: adm,
		Uri:      uri,
	}

	for _, value := range ad.Imp {
		unescape, err := url.PathUnescape(value)
		if err == nil {
			postData.Displayreport = append(postData.Displayreport, unescape)
		}
	}
	postData.Displayreport = append(postData.Displayreport, "http://120.26.247.23/ad/report.php?method="+funcName+"&event=displayreport&os="+os+"&t="+util.GetRandom())
	for _, value := range ad.Clk {
		unescape, err := url.PathUnescape(value)
		if err == nil {
			postData.Clickreport = append(postData.Clickreport, unescape)
		}
	}
	postData.Clickreport = append(postData.Clickreport, "http://120.26.247.23/ad/report.php?method="+funcName+"&event=clickreport&os="+os+"&t="+util.GetRandom())

	return postData
}
