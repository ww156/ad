package pingan

type adres struct {
	Id            string
	Position_type int
	Code          int
	Ads           []Ad
}

type Ad struct {
	Id                string
	Title             string
	Desc              string
	File_urls         []string
	Deeplink_url      string
	Target_url        string
	Impression_urls   []string
	Click_notice_urls []string
}
