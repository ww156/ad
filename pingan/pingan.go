package pingan

import (
	"bytes"
	"encoding/json"
	// "encoding/pem"
	"encoding/base64"
	// "encoding/hex"
	"fmt"
	"io/ioutil"
	// "log"
	// "crypto/rand"
	// "errors"
	"net/http"
	"strconv"
	"strings"
	"time"
	// "unsafe"
	// "crypto"
	// "crypto/aes"
	// "crypto/cipher"
	// "crypto/rsa"
	// "crypto/x509"
	"sort"

	// "github.com/golang/protobuf/proto"

	"ad/mongo"
	"ad/util"
)

const (
	PRO_URL = "https://adx-sdk.pa18.com/api/getad"
	DEV_URL = "http://103.28.214.80:41887/api/getad"
	// price 3 6 12
	ID_ANDROID_STARTUP = "10001"
	ID_ANDROID_FLOW    = "10002"
	ID_ANDROID         = "10003"
	ID_IOS_STARTUP     = "10004"
	ID_IOS_FLOW        = "10005"
	ID_IOS             = "10006"
	APPID              = "20001"
	APP_KEY            = "b9b82d11fe9710c7f34b17dcd263d42b"
	APP_SECRET         = "1933303d2083734d8d50ddd89d06794bf658b168"
	RSA_PUBKEY         = `
-----BEGIN PUBLIC KEY-----
MFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBAKtzezzADAlVbTr1ap85xp8S+YGf6n8X
VRL8ajv2dx2vxT3bhQMsMX1NgI8KzGqgDdbEInkenxPdK+ADHAaDK0UCAwEAAQ==
-----END PUBLIC KEY-----
`
)

func Pingan(getReq *util.ReqMsg, funcName string) util.ResMsg {
	if getReq.Os == "1" {
		return base(getReq, funcName, 4, ID_ANDROID)
	}
	return base(getReq, funcName, 4, ID_IOS)
}

func PinganFlow(getReq *util.ReqMsg, funcName string) util.ResMsg {
	if getReq.Os == "1" {
		return base(getReq, funcName, 3, ID_ANDROID_FLOW)
	}
	return base(getReq, funcName, 3, ID_IOS_FLOW)
}

func PinganStartup(getReq *util.ReqMsg, funcName string) util.ResMsg {
	if getReq.Os == "1" {
		return base(getReq, funcName, 4, ID_ANDROID_STARTUP)
	}
	return base(getReq, funcName, 4, ID_IOS_STARTUP)
}

func base(getReq *util.ReqMsg, funcName string, adtype int, adid string) util.ResMsg {
	// return nil
	w, err := strconv.Atoi(getReq.Screenwidth)
	if err != nil {
		w = 0
	}
	h, err := strconv.Atoi(getReq.Screenheight)
	if err != nil {
		h = 0
	}
	carrier := 0
	switch getReq.Imsi {
	case "1":
		carrier = 1
	case "2":
		carrier = 2
	case "3":
		carrier = 3
	default:
		carrier = 0
	}
	network := 0
	switch getReq.Network {
	case "wifi":
		network = 1
	case "2g":
		network = 2
	case "3g":
		carrier = 3
	case "4g":
		carrier = 4
	default:
		carrier = 0
	}
	sd, err := strconv.Atoi(getReq.Sd)
	dpi := sd / 160
	postData := adreq{
		Id:              adid,
		Num:             1,
		Appid:           APPID,
		App_key:         APP_KEY,
		Timestamp:       time.Now().Unix(),
		Is_test:         false,
		Device_type:     2,
		Os:              2,
		Os_version:      getReq.Osversion,
		Screen_width:    w,
		Screen_height:   h,
		Dpi:             dpi,
		Carrier:         carrier,
		Connection_type: network,
		Brand_and_model: getReq.Model,
		Idfa:            getReq.Idfa,
		Imei:            getReq.Imei,
		Manufacturer:    getReq.Vendor,
		Android_id:      getReq.Androidid,
		Ip:              getReq.Ip,
	}
	if getReq.Os == "2" {
		postData.Os = 1
	}
	test := "0"
	if postData.Is_test {
		test = "1"
	} else {
		test = "0"
	}
	// aa, _ := json.Marshal(&postData)
	// fmt.Printf("postData1:%+v\n", string(aa))
	m := map[string]string{
		"id":              postData.Id,
		"num":             strconv.Itoa(postData.Num),
		"appid":           postData.Appid,
		"app_key":         postData.App_key,
		"timestamp":       strconv.Itoa(int(postData.Timestamp)),
		"is_test":         test,
		"device_type":     strconv.Itoa(postData.Device_type),
		"os":              strconv.Itoa(postData.Os),
		"os_version":      postData.Os_version,
		"screen_width":    strconv.Itoa(postData.Screen_width),
		"screen_height":   strconv.Itoa(postData.Screen_height),
		"dpi":             strconv.Itoa(postData.Dpi),
		"carrier":         strconv.Itoa(postData.Carrier),
		"connection_type": strconv.Itoa(postData.Connection_type),
		"brand_and_model": postData.Brand_and_model,
		"idfa":            postData.Idfa,
		"imei":            postData.Imei,
		"manufacturer":    postData.Manufacturer,
		"android_id":      postData.Android_id,
		"ip":              postData.Ip,
	}
	// fmt.Printf("%+v", m)
	key := make([]string, 0)
	for k, _ := range m {
		key = append(key, k)
	}
	sort.Strings(key)
	str := strings.Join(key, "")
	for _, value := range key {
		str = str + m[value]
	}
	str = str + APP_SECRET
	// fmt.Println(str)
	postData.Sign = util.Md5(str)
	ma, err := json.Marshal(&postData)
	if err != nil {
		return util.ResMsg{}
	}

	header := http.Header{}
	header.Set("Connection", "keep-alive")
	header.Set("Content-Type", "application/json;charset=utf-8")
	header.Set("X-PAADXAPI-Version", "0.0.0")
	privatekey := createAeskey()
	dekey, err := rsaEncrypt(privatekey)
	if err != nil {
		return util.ResMsg{}
	}
	// fmt.Println(string(privatekey))
	dema, err := aesEncrypt(ma, privatekey)
	if err != nil {
		return util.ResMsg{}
	}
	// fmt.Println(base64.StdEncoding.EncodeToString(dekey) + "|" + base64.StdEncoding.EncodeToString(dema))
	resp, err := util.HttpPOST(PRO_URL, &header, []byte(base64.StdEncoding.EncodeToString(dekey)+"|"+base64.StdEncoding.EncodeToString(dema)))
	// resp, err := util.HttpPOST(DEV_URL, &header, ma)
	mongo.LogReq(funcName, getReq.Os)
	if err != nil {
		fmt.Println("[pingan.go-103]", err.Error())
		mongo.LogTimeout(funcName, getReq.Os)
		util.HttpRequestCount.WithLabelValues(funcName + "timeouterr").Inc()
		return util.ResMsg{}
	}
	data, err := ioutil.ReadAll(resp.Body)
	resp.Body.Close()
	if err != nil {
		mongo.LogNo(funcName, getReq.Os)
		util.HttpRequestCount.WithLabelValues(funcName + "readresperr").Inc()
		return util.ResMsg{}
	}
	if resp.StatusCode != 200 {
		mongo.LogTimeout(funcName, getReq.Os)
		util.HttpRequestCount.WithLabelValues(funcName + "not200err").Inc()
		return util.ResMsg{}
	}
	return FormateRes(data, getReq.Os, funcName, getReq)
}

func FormateRes(res []byte, os, funcName string, getReq *util.ReqMsg) util.ResMsg {
	resData := adres{}
	str := string(res)
	// fmt.Println(str)
	arr := strings.Split(str, "|")

	rsaEncrypt([]byte(arr[0]))
	// 公钥解密出私钥
	aesprivatekey, err := rsaPublicKeyDecryptBase64(RSA_PUBKEY, []byte(arr[0]))
	if err != nil {
		return util.ResMsg{}
	}
	// fmt.Println("aesprivatekey:", string(aesprivatekey))
	aesStr, err := base64.StdEncoding.DecodeString(arr[1])
	if err != nil {
		return util.ResMsg{}
	}
	// 私钥解密数据
	dedata, err := aesDecrypt(aesprivatekey, aesStr)
	if err != nil {
		return util.ResMsg{}
	}
	// fmt.Println(dedata)
	// fmt.Println(string(dedata))
	dedata = bytes.TrimRight(dedata, string([]byte{0}))
	// fmt.Println(dedata)
	json.Unmarshal(dedata, &resData)
	// fmt.Printf("%+v", resData)
	if resData.Code != 10000 {
		mongo.LogNo(funcName, os)
		util.HttpRequestCount.WithLabelValues(funcName + "noerr").Inc()
		return util.ResMsg{}
	}
	if len(resData.Ads) == 0 {
		mongo.LogNo(funcName, os)
		util.HttpRequestCount.WithLabelValues(funcName + "noerr").Inc()
		return util.ResMsg{}
	}
	ad := resData.Ads[0]
	for _, value := range resData.Ads {
		if value.Id != "" {
			ad = value
		}
	}
	if ad.Id == "" {
		mongo.LogNo(funcName, os)
		util.HttpRequestCount.WithLabelValues(funcName + "noerr").Inc()
		return util.ResMsg{}
	}
	postData := util.ResMsg{
		Id:       "0",
		Weight:   0,
		State:    0,
		Title:    ad.Title,
		Content:  ad.Desc,
		ImageUrl: ad.File_urls[0],
		Uri:      ad.Target_url,
	}
	postData.Displayreport = ad.Impression_urls
	postData.Displayreport = append(postData.Displayreport, "http://120.26.247.23/ad/report.php?method="+funcName+"&event=displayreport&os="+os+"&t="+util.GetRandom())
	postData.Clickreport = ad.Click_notice_urls
	postData.Clickreport = append(postData.Clickreport, "http://120.26.247.23/ad/report.php?method="+funcName+"&event=clickreport&os="+os+"&t="+util.GetRandom())

	return postData
}
