package pingan

type adreq struct {
	Id              string   `json:"id"`
	Num             int      `json:"num"`
	Appid           string   `json:"appid"`
	App_key         string   `json:"app_key"`
	Timestamp       int64    `json:"timestamp"`
	Is_test         bool     `json:"is_test"`
	Device_type     int      `json:"device_type"`
	Os              int      `json:"os"`
	Os_version      string   `json:"os_version"`
	Screen_width    int      `json:"screen_width"`
	Screen_height   int      `json:"screen_height"`
	Dpi             int      `json:"dpi"`
	Carrier         int      `json:"carrier"`
	Connection_type int      `json:"connection_type"`
	Brand_and_model string   `json:"brand_and_model"`
	Language        string   `json:"language,omitempty"`
	Idfa            string   `json:"idfa"`
	Imei            string   `json:"imei"`
	Manufacturer    string   `json:"manufacturer"`
	Android_id      string   `json:"android_id"`
	Lat             int      `json:"lat,omitempty"`
	Lon             int      `json:"lon,omitempty"`
	Accuracy        int64    `json:"accuracy,omitempty"`
	Ext             []string `json:"ext,omitempty"`
	Sign            string   `json:"sign,omitempty"`
	Ip              string   `json:"ip"`
}
