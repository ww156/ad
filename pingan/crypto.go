package pingan

import (
	"bytes"
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"encoding/base64"
	"encoding/pem"
	"errors"
	// "fmt"
	"math/big"
	// mrand "math/rand"
	// "strconv"
	// "time"
	"ad/util"
)

var (
	ErrIllegalParameter = errors.New("illegal parameter(s)")
	ErrDataToLarge      = errors.New("data is too large (len > 128) ")
	ErrDataLen          = errors.New("data length error")
	ErrDataBroken       = errors.New("data broken, first byte is not zero")
	ErrKeyPairDismatch  = errors.New("data is not encrypted by the private key")
)

const (
	//MaxDataLen max data single decrypt data length.
	maxDataLen = 64
)

// leftPad returns a new slice of length size. The contents of input are right
// aligned in the new slice.
// copy from crypto/rsa/rsa.go.
func leftPad(input []byte, size int) (out []byte) {
	n := len(input)
	if n > size {
		n = size
	}
	out = make([]byte, size)
	copy(out[len(out)-n:], input)
	return
}

func rsaPublicKeyDecryptHelper(pemKey string, data []byte) ([]byte, error) {

	if len(data) > maxDataLen {
		return nil, ErrDataToLarge
	}

	block, _ := pem.Decode([]byte(pemKey))
	pub, err := x509.ParsePKIXPublicKey(block.Bytes)

	if err != nil {
		return nil, err
	}

	pubKey := pub.(*rsa.PublicKey)

	k := (pubKey.N.BitLen() + 7) / 8

	if k != len(data) {
		return nil, ErrDataLen
	}
	m := new(big.Int).SetBytes(data)

	if m.Cmp(pubKey.N) > 0 {
		return nil, ErrDataToLarge
	}

	m.Exp(m, big.NewInt(int64(pubKey.E)), pubKey.N)

	d := leftPad(m.Bytes(), k)

	if d[0] != 0 {
		return nil, ErrDataBroken
	}

	if d[1] != 0 && d[1] != 1 {
		return nil, ErrKeyPairDismatch
	}

	var i = 2
	for ; i < len(d); i++ {
		if d[i] == 0 {
			break
		}
	}
	i++
	if i == len(d) {
		return nil, nil
	}
	return d[i:], nil

}

// rsa公钥解密
//RSAPublicKeyDecryptBase64 implement decrypt with base64 first and then RSA Public Key.
func rsaPublicKeyDecryptBase64(pem string, data []byte) ([]byte, error) {

	if pem == "" || data == nil {
		return nil, ErrIllegalParameter
	}

	dst := make([]byte, base64.StdEncoding.DecodedLen(len(data)))
	_, err := base64.StdEncoding.Decode(dst, data)
	if err != nil {
		return nil, err
	}

	return rsaPublicKeyDecrypt(pem, dst)
}

// rsa公钥解密
//RSAPublicKeyDecrypt implement decrypt with RSA Public Key.
func rsaPublicKeyDecrypt(pem string, data []byte) ([]byte, error) {

	if pem == "" || data == nil {
		return nil, ErrIllegalParameter
	}

	var (
		err       error
		plainText []byte
	)
	buf := &bytes.Buffer{}

	for len(data) >= maxDataLen {

		dataStub := data[:maxDataLen]
		data = data[maxDataLen:]

		plainText, err = rsaPublicKeyDecryptHelper(pem, dataStub)
		if err != nil {
			break
		}
		buf.Write(plainText)
	}
	return buf.Bytes(), nil
}

// rsa公钥加密
func rsaEncrypt(origData []byte) ([]byte, error) {
	block, _ := pem.Decode([]byte(RSA_PUBKEY)) //将密钥解析成公钥实例
	if block == nil {
		return nil, errors.New("public key error")
	}
	pubInterface, err := x509.ParsePKIXPublicKey(block.Bytes) //解析pem.Decode（）返回的Block指针实例
	if err != nil {
		return nil, err
	}
	pub := pubInterface.(*rsa.PublicKey)
	return rsa.EncryptPKCS1v15(rand.Reader, pub, origData) //RSA算法加密
}

//aes解密字符串
func aesDecrypt(key, src []byte) ([]byte, error) {
	var iv = []byte(key)[:aes.BlockSize]
	decrypted := src[:]
	var aesBlockDecrypter cipher.Block
	aesBlockDecrypter, err := aes.NewCipher([]byte(key))
	if err != nil {
		return nil, err
	}
	aesDecrypter := cipher.NewCBCDecrypter(aesBlockDecrypter, iv)
	aesDecrypter.CryptBlocks(decrypted, decrypted)
	return decrypted, nil
}

//aes加密字符串
func aesEncrypt(origData, key []byte) ([]byte, error) {
	l := len(origData)
	padding := l % aes.BlockSize
	src := make([]byte, l+aes.BlockSize-padding)
	for i := range origData {
		src[i] = origData[i]
	}
	block, err := aes.NewCipher(key)
	if err != nil {
		return nil, err
	}
	// fmt.Println(l, l+aes.BlockSize-padding)
	blockMode := cipher.NewCBCEncrypter(block, key)
	crypted := make([]byte, l+aes.BlockSize-padding)
	blockMode.CryptBlocks(crypted, src)
	return crypted, nil
}

// 生成aes密钥
func createAeskey() []byte {
	return []byte(util.GetRandom())[15:31]
}
