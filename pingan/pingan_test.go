package pingan

import (
	"encoding/hex"
	"fmt"
	"math/rand"
	"testing"
)

func Test_createAeskey(t *testing.T) {
	for i := 0; i < 10; i++ {
		fmt.Println(string(createAeskey()))
		fmt.Println(rand.Intn(2))
	}
}

func Test_aesEncrypt(t *testing.T) {
	str, _ := aesEncrypt([]byte("hello world!"), []byte("1234567890123456"))
	fmt.Println(hex.EncodeToString(str))
}

func Test_aesDecrypt(t *testing.T) {
	str, _ := aesEncrypt([]byte("hello world!"), []byte("1234567890123456"))
	destr, _ := aesDecrypt([]byte("1234567890123456"), str)
	fmt.Println(destr)
}
