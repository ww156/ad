package zhongcheng

import (
	// "bytes"
	"encoding/json"
	// "fmt"
	"io/ioutil"
	// "log"
	"net/http"
	"net/url"
	// "strconv"
	// "strings"
	// "time"
	// "unsafe"
	// "regexp"

	"ad/mongo"
	"ad/util"
)

const (
	URL          = "https://oa-panther.data.aliyun.com/get_creative"
	MID_ANDROID  = "309"
	PID_ANDROID  = "400"
	MID_IOS_FLOW = "312"
	PID_IOS_FLOW = "404"
)

func Zhongcheng(getReq *util.ReqMsg, funcName string) util.ResMsg {
	if getReq.Os == "1" {
		return base(getReq, funcName, "103", "640*100", MID_ANDROID, PID_ANDROID)
	}
	return util.ResMsg{}
}
func ZhongchengFlow(getReq *util.ReqMsg, funcName string) util.ResMsg {
	if getReq.Os == "2" {
		return base(getReq, funcName, "102", "640*360", MID_IOS_FLOW, PID_IOS_FLOW)
	}
	return util.ResMsg{}
}

func base(getReq *util.ReqMsg, funcName, adtype, as, mid, pid string) util.ResMsg {
	value := url.Values{}
	value.Set("ct", "1")
	value.Set("at", "2")
	value.Set("n", "1")
	value.Set("s", "0")
	value.Set("vt", adtype)
	value.Set("c", "43005")
	value.Set("mid", mid)
	value.Set("pid", pid)
	value.Set("v", "1")
	value.Set("did", "206525952")
	value.Set("as", as) // 创意广告的大小，宽*高
	// value.Set("cb", util.GetRandom())
	value.Set("nf", "0")
	value.Set("mo", "android")
	value.Set("imei", getReq.Imei)
	if getReq.Os == "2" {
		value.Set("mo", "iphone")
		value.Set("idfa", getReq.Idfa)
		value.Del("imei")
	}
	value.Set("ua", getReq.Ua)
	value.Set("mac", getReq.Mac)
	value.Set("i", getReq.Ip)

	// fmt.Println(URL + "?" + value.Encode())
	// client := &http.Client{Timeout: util.Timeout}
	req, err := http.NewRequest("GET", URL+"?"+value.Encode(), nil)
	if err != nil {
		return util.ResMsg{}
	}
	req.Header.Set("Connection", "keep-alive")
	req.Header.Set("Content-Type", "application/json;charset=utf-8")
	resp, err := util.Client.Do(req)
	mongo.LogReq(funcName, getReq.Os)
	if err != nil {
		mongo.LogTimeout(funcName, getReq.Os)
		util.HttpRequestCount.WithLabelValues(funcName + "timeouterr").Inc()
		return util.ResMsg{}
	}
	data, err := ioutil.ReadAll(resp.Body)
	resp.Body.Close()
	if err != nil {
		mongo.LogNo(funcName, getReq.Os)
		util.HttpRequestCount.WithLabelValues(funcName + "readresperr").Inc()
		return util.ResMsg{}
	}
	if resp.StatusCode != 200 {
		mongo.LogTimeout(funcName, getReq.Os)
		util.HttpRequestCount.WithLabelValues(funcName + "not200err").Inc()
		return util.ResMsg{}
	}
	return FormateRes(data, getReq.Os, funcName)
}

func FormateRes(res []byte, os, funcName string) util.ResMsg {
	resData := &adres{}
	json.Unmarshal(res, resData)
	// fmt.Printf("%+v", resData)
	if len(resData.Results) == 0 {
		mongo.LogNo(funcName, os)
		util.HttpRequestCount.WithLabelValues(funcName + "noerr").Inc()
		return util.ResMsg{}
	}
	ad := resData.Results[0]
	postData := util.ResMsg{
		Id:       "0",
		Weight:   0,
		State:    0,
		Title:    ad.Title,
		Content:  ad.Creative_desc,
		ImageUrl: ad.Img_path,
		Uri:      ad.Dest_url,
	}
	postData.Displayreport = append(postData.Displayreport, "http://120.26.247.23/ad/report.php?method="+funcName+"&event=displayreport&os="+os+"&t="+util.GetRandom())
	postData.Clickreport = append(postData.Clickreport, "http://120.26.247.23/ad/report.php?method="+funcName+"&event=clickreport&os="+os+"&t="+util.GetRandom())

	return postData
}
