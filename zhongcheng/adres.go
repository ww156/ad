package zhongcheng

type adres struct {
	C          int
	Cb         string
	Status     string
	Message    string
	Total_num  int
	Return_num int
	Open_id    string
	Results    []Ad
}

type Ad struct {
	Img_path      string
	Title         string
	Creative_desc string
	Dest_url      string
	Callback_url  string
	Monitor_url   string
	Download_url  string
	Creative_id   string
	Creative_name string
	Img_size      string
	Category      string
	Fields        string
}
