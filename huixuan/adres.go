package huixuan

type adres struct {
	Id      string
	Bidid   string
	Seatbid []Seatbid
	Nbr     int
}

type Seatbid struct {
	Bid  []Bid
	Seat string
}

type Bid struct {
	Id          string
	Impid       int
	Iurl        string
	Crid        int
	W           int
	H           int
	Adm         string
	Clkurl      string
	Clkurl302   string
	Imptrackers []string
	Clktrackers []string
	Action_type int
	Duration    int
	Bundle      string
	Nbr         int
	Native      Native_res
}

type Native_res struct {
	Assets      []*Asset_res
	Link        *Link_res
	Imptrackers []string
}

type Link_res struct {
	Clkurl      string
	Clkurl302   string
	Clktrackers []string
	Fallback    string
	Intro_url   string
	Action_type int
}

type Asset_res struct {
	Id    int
	Title *Title_res
	Img   *Img_res
	Video *Video_res
	Data  *Data_res
	Link  *Link_res
}

type Title_res struct {
	Text string
}
type Img_res struct {
	Urls []string
	W    int
	H    int
}

type Video_res struct {
	Url      string
	W        int
	H        int
	Duration int
}

type Data_res struct {
	Label string
	Value string
}
