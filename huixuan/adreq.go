package huixuan

type adreq struct {
	Id        string  `json:"id"`
	Device    *Device `json:"device,omitempty"`
	App       *App    `json:"app,omitempty"`
	Site      *Site   `json:"site,omitempty"`
	User      *User   `json:"user,omitempty"`
	Imp       []*Imp  `json:"imp,omitempty"`
	Media_id  int     `json:"media_id"`
	Ts        int     `json:"ts"`
	Token_md5 string  `json:"token_md5"`
}

type Device struct {
	Dnt            bool   `json:"dnt,omitempty"`
	Ua             string `json:"ua,omitempty"`
	Ip             string `json:"ip,omitempty"`
	Geo            *Geo   `json:"geo,omitempty"`
	Didsha1        string `json:"didsha1,omitempty"`
	Didmd5         string `json:"didmd5,omitempty"`
	Dpidsha1       string `json:"dpidsha1,omitempty"`
	Dpidmd5        string `json:"dpidmd5,omitempty"`
	Carrier        string `json:"carrier,omitempty"`
	Language       string `json:"language,omitempty"`
	Make           string `json:"make,omitempty"`
	Model          string `json:"model,omitempty"`
	Os             string `json:"os,omitempty"`
	Osv            string `json:"osv,omitempty"`
	Hwv            string `json:"hwv,omitempty"`
	W              int    `json:"w"`
	H              int    `json:"h"`
	Ppi            int    `json:"ppi,omitempty"`
	Ssid           string `json:"ssid,omitempty"`
	Js             string `json:"js,omitempty"`
	Connectiontype int    `json:"Connectiontype,omitempty"`
	Devicetype     int    `json:"devicetype,omitempty"`
	Mac            string `json:"mac,omitempty"`
	Macmd5         string `json:"macmd5,omitempty"`
	Macsha1        string `json:"macsha1,omitempty"`
	Did            string `json:"did,omitempty"`
	Dpid           string `json:"dpid,omitempty"`
	Imei           string `json:"imei"`
	Idfa           string `json:"Idfa"`
	Androidid      string `json:"androidid,omitempty"`
	Aaid           string `json:"aaid,omitempty"`
	Duid           string `json:"duid,omitempty"`
	Brk            bool   `json:"brk,omitempty"`
}

type App struct {
	Id       string   `json:"id"`
	Name     string   `json:"name"`
	Domain   string   `json:"domain"`
	Ver      string   `json:"ver"`
	Bundle   string   `json:"bundle"`
	Paid     int      `json:"paid"`
	Keywords []string `json:"keywords,omitempty"`
	Storeurl string   `json:"storeurl"`
	Mkt_id   int      `json:"mkt_id"`
	Mkt_sn   string   `json:"mkt_sn"`
}

type Site struct {
	Name     string   `json:"name"`
	Domain   string   `json:"domain"`
	Page     string   `json:"page"`
	Ref      string   `json:"ref"`
	Keywords []string `json:"keywords,omitempty"`
}

type User struct {
	Id     string `json:"id"`
	Yob    int    `json:"yob"`
	Gender string `json:"gender"`
	Geo    *Geo   `json:"geo,omitempty"`
	Marry  bool   `json:"marry"`
	Bree   int    `json:"bree"`
}

type Imp struct {
	Id             string  `json:"id"`
	Tagid          string  `json:"tagid"`
	Instl          int     `json:"instl"`
	Bidfloor       int     `json:"bidfloor"`
	Banner         *Banner `json:"banner,omitempty"`
	Video          *Video  `json:"video,omitempty"`
	Native         *Native `json:"native,omitempty"`
	Screenlocation int     `json:"screenlocation"`
	Allow_adm      bool    `json:"allow_adm,omitempty"`
	Action_type    []int   `json:"action_type,omitempty"`
}

type Banner struct {
	W     int   `json:"w"`
	H     int   `json:"h"`
	Mimes []int `json:"mimes,omitempty"`
}

type Video struct {
	W           int   `json:"w"`
	H           int   `json:"h"`
	Pos         int   `json:"pos"`
	Mimes       []int `json:"mimes,omitempty"`
	Linearity   int   `json:"linearity"`
	Minduration int   `json:"minduration"`
	Maxduration int   `json:"maxduration"`
}

type Native struct {
	Layout int      `json:"layout"`
	Assets []*Asset `json:"assets,omitempty"`
}

type Asset struct {
	Id       int          `json:"id"`
	Required bool         `json:"required"`
	Title    *Title       `json:"title,omitempty"`
	Img      *Img         `json:"img,omitempty"`
	Video    *Asset_video `json:"video,omitempty"`
	Data     *Data        `json:"data,omitempty"`
}

type Title struct {
	Len int `json:"len"`
}
type Img struct {
	Type  int   `json:"type"`
	W     int   `json:"w"`
	H     int   `json:"h"`
	Mimes []int `json:"mimes,omitempty"`
}

type Asset_video struct {
	W         int   `json:"w"`
	H         int   `json:"h"`
	Pos       int   `json:"pos"`
	Mimes     []int `json:"mimes,omitempty"`
	Linearity int   `json:"linearity"`
}

type Data struct {
	Len  int `json:"len"`
	Type int `json:"type"`
}

type Geo struct {
	Lat     float64 `json:"lat"`
	Lon     float64 `json:"lon"`
	Country string  `json:"country"`
	Region  string  `json:"region"`
	City    string  `json:"city"`
	Zip     string  `json:"zip"`
	Govcode string  `json:"govcode"`
	Type    int     `json:"type"`
}
