package huixuan

import (
	"bytes"
	"encoding/json"
	// "fmt"
	"io/ioutil"
	// "log"
	"net/http"
	// "net/url"
	"strconv"
	// "strings"
	"time"
	// "unsafe"
	// "regexp"

	"ad/mongo"
	"ad/util"
)

const (
	DEV_URL            = "http://ad.test.rtbs.cn/a/api"
	PRO_URL            = "http://ad.rtbs.cn/a/api"
	MEDIAID            = 243
	TOKEN              = "PWaRr3TihHHJDiF8KTkNRcDHjh2wti2j"
	TAGID_ANDROID      = "100548"
	TAGID_ANDROID_FLOW = "100549"
	TAGID_IOS          = "100546"
	TAGID_IOS_FLOW     = "100547"
)

func Huixuan(getReq *util.ReqMsg, funcName string) util.ResMsg {
	if getReq.Os == "1" {
		return base(getReq, funcName, TAGID_ANDROID, 1, 200, 600, 128)
	}
	return base(getReq, funcName, TAGID_IOS, 1, 200, 600, 128)
}

func HuixuanFlow(getReq *util.ReqMsg, funcName string) util.ResMsg {
	if getReq.Os == "1" {
		return base(getReq, funcName, TAGID_ANDROID_FLOW, 10, 300, 680, 360)
	}
	return base(getReq, funcName, TAGID_IOS_FLOW, 10, 300, 680, 360)
}

func base(getReq *util.ReqMsg, funcName, tagid string, adtype, price, w, h int) util.ResMsg {
	width, err := strconv.Atoi(getReq.Screenwidth)
	if err != nil {
		width = 0
	}
	height, err := strconv.Atoi(getReq.Screenheight)
	if err != nil {
		height = 0
	}
	network := 2
	switch getReq.Network {
	case "以太网":
		network = 1
	case "wifi":
		network = 2
	case "2g":
		network = 4
	case "3g":
		network = 5
	case "4g":
		network = 6
	}
	postData := adreq{
		Id: util.GetRandom(),
		Device: &Device{
			Dnt:            false,
			Ua:             getReq.Ua,
			Ip:             getReq.Ip,
			Make:           getReq.Vendor,
			Model:          getReq.Model,
			Os:             "android",
			Osv:            getReq.Osversion,
			Hwv:            getReq.Vendor,
			W:              width,
			H:              height,
			Connectiontype: network,
			Devicetype:     4,
			Mac:            getReq.Mac,
			Imei:           getReq.Imei,
			Idfa:           getReq.Idfa,
			Androidid:      getReq.Androidid,
		},
		App: &App{
			Id:     TOKEN,
			Name:   "网上厨房",
			Ver:    getReq.Appversion,
			Bundle: "cn.ecook",
		},
		Imp: []*Imp{
			{
				Id:       util.GetRandom(),
				Tagid:    tagid,
				Instl:    adtype,
				Bidfloor: price,
				Native: &Native{
					Layout: 6,
					Assets: []*Asset{
						{
							Id:       1,
							Required: true,
							Title: &Title{
								Len: 0,
							},
						},
						{
							Id:       2,
							Required: true,
							Data: &Data{
								Len:  0,
								Type: 2,
							},
						},
						{
							Id:       3,
							Required: true,
							Img: &Img{
								Type: 3,
								W:    w,
								H:    h,
							},
						},
					},
				},
			},
		},
		Media_id:  MEDIAID,
		Ts:        int(time.Now().Unix()),
		Token_md5: util.Md5(TOKEN),
	}
	device := postData.Device
	app := postData.App
	if getReq.Os == "2" {
		device.Os = "ios"
		app.Bundle = "cn.ecook.ecook"
	}
	imp := postData.Imp[0]
	if adtype == 1 {
		imp.Native = nil
		imp.Banner = &Banner{
			W:     w,
			H:     h,
			Mimes: []int{1, 2},
		}
	}

	ma, err := json.Marshal(&postData)
	if err != nil {
		return util.ResMsg{}
	}
	// fmt.Println(string(ma))
	// 请求广告
	// ma = []byte(`{"id":"2f8916a38ffc2a424886234d697892c7","app":{"id":"PWaRr3TihHHJDiF8KTkNRcDHjh2wti2j","name":"网上厨房","domain":"","ver":"13.6.6","bundle":"cn.ecook.ecook","storeurl":""},"device":{ "ua":"Mozilla/5.0 (iPhone; CPU iPhone OS 10_0_2 like Mac OS X) AppleWebKit/602.1.50 (KHTML, like Gecko) Mobile/14A456 MagicKitchen/13.6.6 NetType/4G Tag/82d601d32e6c09efb9d9ee84c5a1c573.1501400903000DEVICE/O7288e1a07d15e6bcbd0f57ecc86380f86dde9f41","ip":"183.251.17.74 ","make":"Apple","model":"iPhone 6","os":"ios","osv":"10.0.2","hwv":"Apple","w":750,"h":1334,"idfa":"DCBB304A-AE8C-4F62-B9E6-D910905EC8B4","mac":"2f:89:16:a3:8f:fc","imei":"","connectiontype":2,"devicetype":4},"imp":[{"id":"2f8916a38ffc2a424886234d697892c7","tagid":"100546","banner":{"w":600,"h":128,"mimes":[1,2]},"instl":1, "bidfloor":200,"impid":0}],"ts":1501689982}`)
	// client := &http.Client{Timeout: util.Timeout}
	req, err := http.NewRequest("POST", PRO_URL, bytes.NewReader(ma))
	req.Header.Set("Connection", "keep-alive")
	req.Header.Set("Content-Type", "application/json;charset=utf-8")
	if err != nil {
		return util.ResMsg{}
	}
	resp, err := util.Client.Do(req)
	mongo.LogReq(funcName, getReq.Os)
	if err != nil {
		mongo.LogTimeout(funcName, getReq.Os)
		util.HttpRequestCount.WithLabelValues(funcName + "timeouterr").Inc()
		return util.ResMsg{}
	}
	data, err := ioutil.ReadAll(resp.Body)
	resp.Body.Close()
	if err != nil {
		mongo.LogNo(funcName, getReq.Os)
		return util.ResMsg{}
	}
	// fmt.Println(resp.StatusCode)
	if resp.StatusCode != 200 {
		mongo.LogTimeout(funcName, getReq.Os)
		return util.ResMsg{}
	}
	// fmt.Println(string(data))
	return FormateRes(data, getReq.Os, funcName)
}

// 根据广告平台的返回生成客户端返回
func FormateRes(res []byte, os, funcName string) util.ResMsg {
	resData := adres{}
	json.Unmarshal(res, &resData)
	// fmt.Printf("%+v", resData)
	// fmt.Println(string(res))
	if len(resData.Seatbid) == 0 {
		// fmt.Println(2)
		mongo.LogNo(funcName, os)
		util.HttpRequestCount.WithLabelValues(funcName + "noerr").Inc()
		return util.ResMsg{}
	}
	if len(resData.Seatbid[0].Bid) == 0 {
		// fmt.Println(3)
		mongo.LogNo(funcName, os)
		util.HttpRequestCount.WithLabelValues(funcName + "noerr").Inc()
		return util.ResMsg{}
	}

	ad := resData.Seatbid[0].Bid[0]
	// if len(ad.Assets) == 0 {
	// 	// fmt.Println(4)
	// 	mongo.LogNo(funcName, os)
	// 	util.HttpRequestCount.WithLabelValues(funcName + "noerr").Inc()
	// 	return util.ResMsg{}
	// }
	postData := util.ResMsg{
		Id:       "0",
		Weight:   0,
		State:    0,
		Title:    "",
		Content:  "",
		ImageUrl: ad.Iurl,
		// Uri:      ad.Link.Clkurl,
		Uri: ad.Clkurl,
	}
	// for _, v := range ad.Assets {
	// 	if v.Id == 1 {
	// 		postData.Title = v.Title.Text
	// 	}
	// 	if v.Id == 2 {
	// 		postData.Content = v.Data.Value
	// 	}
	// 	if v.Id == 3 {
	// 		urls := v.Img.Urls
	// 		if len(v.Img.Urls) == 0 || urls[0] == "" {
	// 			fmt.Println(5)
	// 			mongo.LogNo(funcName, os)
	// 			util.HttpRequestCount.WithLabelValues(funcName + "noerr").Inc()
	// 			return util.ResMsg{}
	// 		}
	// 		postData.ImageUrl = urls[0]
	// 	}
	// }
	// postData.Displayreport = ad.Imptrackers
	postData.Displayreport = ad.Imptrackers
	postData.Displayreport = append(postData.Displayreport, "http://120.26.247.23/ad/report.php?method="+funcName+"&event=displayreport&os="+os+"&t="+util.GetRandom())
	// postData.Clickreport = ad.Link.Clktrackers
	postData.Clickreport = ad.Clktrackers
	postData.Clickreport = append(postData.Clickreport, "http://120.26.247.23/ad/report.php?method="+funcName+"&event=clickreport&os="+os+"&t="+util.GetRandom())

	return postData
}
