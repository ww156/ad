package dianle

type adres struct {
	Id      string
	Bidid   string
	Seatbid []Seatbid
	Nbr     int
	Errmsg  string
}

type Seatbid struct {
	Bid []Bid
}

type Bid struct {
	Id         string
	Impid      string
	Price      float64
	Adid       string
	Cid        string
	Crid       string
	Bundle     string
	Bundle_md5 string
	Nurl       string
	Adm        string
	Iurl       string
	W          int
	H          int
	Cat        []string
	Ext        Bidext
}

type Bidext struct {
	App_ver           string
	Action            int
	Clkurl            string
	Vstrackers        []string
	Vftrackers        []string
	Imptrackers       []string
	Clktrackers       []string
	Inltrackers       []string
	Ntftrackers       []string
	Inventory_type    int
	Img_url_landscape string
	Img_url_portrait  string
	Video_url         string
	Video_md5         string
	Title             string
	Desc              string
	Html_snippet      string
	Html_url          string
}
