package dianle

type adreq struct {
	Id     string   `json:"id"`
	App    *App     `json:"app"`
	Device *Device  `json:"device"`
	Imp    []*Imp   `json:"imp,omitempty"`
	Bcat   []string `json:"bcat,omitempty"`
	User   *User    `json:"user,omitempty"`
	Test   bool     `json:"test"`
	Ext    *Ext     `json:"ext,omitempty"`
}

type Ext struct {
	Version    int  `json:"version"`
	Need_https bool `json:"need_https"`
	Timestamp  int  `json:"timestamp"`
}

type App struct {
	Id        string   `json:"id"`
	Name      string   `json:"name"`
	Bundle    string   `json:"bundle"`
	Channelid string   `json:"channelid"`
	Ver       string   `json:"ver"`
	Cat       []string `json:"cat,omitempty"`
}

type Device struct {
	Deviceid       string     `json:"deviceid"`
	Os             string     `json:"os"`
	Osv            string     `json:"osv"`
	Ip             string     `json:"ip"`
	Dnt            bool       `json:"dnt"`
	Make           string     `json:"make"`
	Model          string     `json:"model"`
	Ua             string     `json:"ua"`
	Hwv            string     `json:"hwv"`
	W              int        `json:"w"`
	H              int        `json:"h"`
	Ppi            int        `json:"ppi"`
	Macsha1        string     `json:"macsha1"`
	Didsha1        string     `json:"didsha1"`
	Dpidsha1       string     `json:"dpidsha1"`
	Language       string     `json:"language"`
	Connectiontype int        `json:"connectiontype"`
	Devicetype     int        `json:"devicetype"`
	Geo            *Geo       `json:"geo,omitempty"`
	Ext            *Deviceext `json:"ext,omitempty"`
}

type Deviceext struct {
	Plmn        string `json:"plmn"`
	Imei        string `json:"imei"`
	Imsi        string `json:"imsi"`
	Idfv        string `json:"idfv"`
	Mac         string `json:"mac"`
	Android_id  string `json:"android_id"`
	Adid        string `json:"adid"`
	Orientation string `json:"orientation"`
}

type Geo struct {
	Lat     float64 `json:"lat"`
	Lon     float64 `json:"lon"`
	Country string  `json:"country"`
	Region  string  `json:"region"`
	City    string  `json:"city"`
	Type    int     `json:"type"`
	Ext     *Geoext `json:"ext,omitempty"`
}

type Geoext struct {
	Accu   int    `json:"accu"`
	Street string `json:"street"`
}

type Imp struct {
	Id              string  `json:"id"`
	Bidfloor        float64 `json:"bidfloor"`
	Bidfloorcur     string  `json:"bidfloorcur"`
	Ad_type         int     `json:"ad_type"`
	Inventory_types []int   `json:"inventory_types,omitempty"`
	Banner          *Banner `json:"banner"`
	Video           *Video  `json:"video,omitempty"`
	Tagid           string  `json:"tagid"`
	Ext             *Impext `json:"ext,omitempty"`
}

type Banner struct {
	W   int `json:"w"`
	H   int `json:"h"`
	Pos int `json:"pos"`
}

type Video struct {
	Mimes       []string `josn:"mimes,omitempty"`
	Protocols   []string `json:"protocols,omitempty"`
	Minduration int      `json:"minduration"`
	Maxduration int      `json:"maxduration"`
	W           int      `json:"w"`
	H           int      `josn:"h"`
	Pos         int      `json:"pos"`
}

type Impext struct {
}

type User struct {
	Id     string `json:"id"`
	Yob    int    `json:"yob"`
	Gender string `josn:"genger"`
	Geo    *Geo   `json:"geo,omitempty"`
	Data   *Data  `josn:"data,omitempty"`
}

type Data struct {
	Segment []*Segment `json:"segment,omitempty"`
}

type Segment struct {
	Id    string `json:"id"`
	Value string `json:"value"`
}
