package dianle

import (
	"encoding/json"
	// "fmt"
	"io/ioutil"
	"net/http"
	"strconv"

	"ad/mongo"
	"ad/util"
)

const (
	URL       = "http://a-api.dianview.com/dsp/pullad.php"
	CHANNELID = "169"
)

func Dianle(getReq *util.ReqMsg, funcName string) util.ResMsg {
	return base(getReq, funcName, 640, 100, 1)
}

func DianleFlow(getReq *util.ReqMsg, funcName string) util.ResMsg {
	return base(getReq, funcName, 640, 360, 1)
}

func base(getReq *util.ReqMsg, funcName string, w, h, adtype int) util.ResMsg {

	width, err := strconv.Atoi(getReq.Screenwidth)
	if err != nil {
		w = 0
	}
	height, err := strconv.Atoi(getReq.Screenheight)
	if err != nil {
		h = 0
	}
	network := 0
	switch getReq.Network {
	case "wifi":
		network = 1
	case "2g":
		network = 2
	case "3g":
		network = 3
	case "4g":
		network = 4
	default:
		network = 0
	}
	sd, err := strconv.Atoi(getReq.Sd)
	ppi := sd / 160
	postData := &adreq{
		Id: util.GetRandom(),
		App: &App{
			Id:        util.Md5("cn.ecook" + CHANNELID),
			Name:      "网上厨房",
			Bundle:    "cn.ecook",
			Channelid: CHANNELID,
			Ver:       getReq.Appversion,
		},
		Device: &Device{
			Deviceid:       getReq.Imei,
			Os:             "android",
			Osv:            getReq.Osversion,
			Ip:             getReq.Ip,
			Dnt:            true,
			Make:           getReq.Vendor,
			Model:          getReq.Model,
			Ua:             getReq.Ua,
			W:              width,
			H:              height,
			Ppi:            ppi,
			Connectiontype: network,
			Devicetype:     4,
			Language:       "zh_CN",
			Ext: &Deviceext{
				Imei:       getReq.Imei,
				Mac:        getReq.Mac,
				Android_id: getReq.Androidid,
			},
		},
		Imp: []*Imp{
			{
				Id:              "1",
				Bidfloor:        0.3,
				Ad_type:         adtype,
				Inventory_types: []int{1, 2, 6},
				Banner: &Banner{
					W: w,
					H: h,
				},
			},
		},
		Test: true,
	}
	if getReq.Os == "2" {
		postData.App.Id = util.Md5("cn.ecook.ecook" + CHANNELID)
		postData.Device.Deviceid = getReq.Idfa
		postData.Device.Os = "ios"
	}

	ma, err := json.Marshal(&postData)
	if err != nil {
		return util.ResMsg{}
	}

	header := http.Header{}
	header.Set("Connection", "keep-alive")
	header.Set("Content-Type", "application/json;charset=utf-8")
	resp, err := util.HttpPOST(URL, &header, ma)
	mongo.LogReq(funcName, getReq.Os)
	if err != nil {
		mongo.LogTimeout(funcName, getReq.Os)
		util.HttpRequestCount.WithLabelValues(funcName + "timeouterr").Inc()
		return util.ResMsg{}
	}
	data, err := ioutil.ReadAll(resp.Body)
	resp.Body.Close()
	if err != nil {
		mongo.LogNo(funcName, getReq.Os)
		util.HttpRequestCount.WithLabelValues(funcName + "readresperr").Inc()
		return util.ResMsg{}
	}
	if resp.StatusCode != 200 {
		mongo.LogTimeout(funcName, getReq.Os)
		util.HttpRequestCount.WithLabelValues(funcName + "not200err").Inc()
		return util.ResMsg{}
	}
	return FormateRes(data, getReq.Os, funcName)
}

func FormateRes(res []byte, os, funcName string) util.ResMsg {
	resData := &adres{}
	json.Unmarshal(res, resData)
	// fmt.Printf("%+v", resData)
	if resData.Nbr != -1 {
		// fmt.Println(1)
		mongo.LogNo(funcName, os)
		util.HttpRequestCount.WithLabelValues(funcName + "noerr").Inc()
		return util.ResMsg{}
	}
	if len(resData.Seatbid) == 0 {
		// fmt.Println(2)
		mongo.LogNo(funcName, os)
		util.HttpRequestCount.WithLabelValues(funcName + "noerr").Inc()
		return util.ResMsg{}
	}
	if len(resData.Seatbid[0].Bid) == 0 {
		// fmt.Println(3)
		mongo.LogNo(funcName, os)
		util.HttpRequestCount.WithLabelValues(funcName + "noerr").Inc()
		return util.ResMsg{}
	}

	ad := resData.Seatbid[0].Bid[0].Ext
	if ad.Img_url_portrait == "" {
		// fmt.Println(4)
		mongo.LogNo(funcName, os)
		util.HttpRequestCount.WithLabelValues(funcName + "noerr").Inc()
		return util.ResMsg{}
	}
	postData := util.ResMsg{
		Id:       "0",
		Weight:   0,
		State:    0,
		Title:    ad.Title,
		Content:  ad.Desc,
		ImageUrl: ad.Img_url_portrait,
		Uri:      ad.Clkurl,
	}
	postData.Displayreport = ad.Imptrackers
	postData.Displayreport = append(postData.Displayreport, "http://120.26.247.23/ad/report.php?method="+funcName+"&event=displayreport&os="+os+"&t="+util.GetRandom())
	postData.Clickreport = ad.Clktrackers
	postData.Clickreport = append(postData.Clickreport, "http://120.26.247.23/ad/report.php?method="+funcName+"&event=clickreport&os="+os+"&t="+util.GetRandom())

	return postData
}
