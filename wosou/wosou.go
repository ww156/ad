package wosou

import (
	// "bytes"
	"encoding/json"
	// "fmt"
	"io/ioutil"
	// "log"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	// "time"
	// "unsafe"
	// "regexp"
	// "math/rand"

	"ad/mongo"
	"ad/util"
)

const (
	URL          = "http://api.mssp.woso.cn/v4/"
	WSID         = "1495784296" //"1495449761"
	WSID_FLOW    = "1495784149" //"1495449708"
	WSID_STARTUP = "1495449627"
)

func Wosou(getReq *util.ReqMsg, funcName string) util.ResMsg {
	return base(getReq, funcName, "640", "100", "1", WSID)
}

func WosouFlow(getReq *util.ReqMsg, funcName string) util.ResMsg {
	return base(getReq, funcName, "640", "360", "8", WSID_FLOW)
}

func WosouStartup(getReq *util.ReqMsg, funcName string) util.ResMsg {
	return base(getReq, funcName, "640", "960", "4", WSID_STARTUP)
}

func base(getReq *util.ReqMsg, funcName, w, h, adtype, wsid string) util.ResMsg {
	if getReq.Os == "2" {
		return util.ResMsg{}
	}
	value := url.Values{}
	value.Set("wsid", wsid)
	value.Set("os", getReq.Os)
	value.Set("mouid", getReq.Imei)
	value.Set("ifa", getReq.Idfa)
	value.Set("dve", getReq.Vendor)
	value.Set("dmo", getReq.Model)
	dop := "0"
	switch getReq.Imsi {
	case "1":
		dop = "1"
	case "2":
		dop = "2"
	case "3":
		dop = "3"
	default:
		dop = "0"
	}
	value.Set("dop", dop)
	net := "0"
	switch getReq.Network {
	case "wifi":
		net = "1"
	case "2g":
		net = "2"
	case "3g":
		net = "3"
	case "4g":
		net = "4"
	default:
		net = "0"
	}
	value.Set("net_type", net)
	value.Set("aw", w)
	value.Set("ah", h)
	value.Set("ai", getReq.Androidid)
	value.Set("detype", "1")
	value.Set("apv", getReq.Appversion)
	value.Set("dev", getReq.Osversion)
	value.Set("pckname", "cn.ecook")
	if getReq.Os == "2" {
		value.Set("pckname", "cn.ecook.ecook")
	}
	value.Set("adtype", adtype)
	value.Set("ip", getReq.Ip)
	value.Set("secure", "0")
	value.Set("mac", getReq.Mac)
	value.Set("dw", getReq.Screenwidth)
	value.Set("dh", getReq.Screenheight)
	value.Set("ua", getReq.Ua)
	// client := &http.Client{Timeout: util.Timeout}
	req, err := http.NewRequest("GET", URL+"?"+value.Encode(), nil)
	if err != nil {
		return util.ResMsg{}
	}
	req.Header.Set("Connection", "keep-alive")
	resp, err := util.Client.Do(req)
	mongo.LogReq(funcName, getReq.Os)
	if err != nil {
		mongo.LogTimeout(funcName, getReq.Os)
		util.HttpRequestCount.WithLabelValues(funcName + "timeouterr").Inc()
		return util.ResMsg{}
	}
	data, err := ioutil.ReadAll(resp.Body)
	resp.Body.Close()
	if err != nil {
		mongo.LogNo(funcName, getReq.Os)
		util.HttpRequestCount.WithLabelValues(funcName + "readresperr").Inc()
		return util.ResMsg{}
	}
	if resp.StatusCode != 200 {
		mongo.LogTimeout(funcName, getReq.Os)
		util.HttpRequestCount.WithLabelValues(funcName + "not200err").Inc()
		return util.ResMsg{}
	}
	rh := 0
	intW, err := strconv.Atoi(getReq.Screenwidth)
	if err != nil {
		intW = 0
	}
	switch adtype {
	case "1":
		rh = intW * 100 / 640
	case "8":
		rh = intW * 360 / 640
	case "4":
		rh = intW * 960 / 640
	}
	return FormateRes(data, getReq.Os, funcName, w, h, intW, rh)
}

func FormateRes(res []byte, os, funcName, w, h string, width, height int) util.ResMsg {
	resData := &adres{}
	json.Unmarshal(res, resData)
	// fmt.Printf("%+v", resData)
	// fmt.Println(string(res))
	if resData.Code != 0 {
		mongo.LogNo(funcName, os)
		util.HttpRequestCount.WithLabelValues(funcName + "noerr").Inc()
		return util.ResMsg{}
	}
	if len(resData.Ads) == 0 {
		mongo.LogNo(funcName, os)
		util.HttpRequestCount.WithLabelValues(funcName + "noerr").Inc()
		return util.ResMsg{}
	}
	ad := resData.Ads[0]
	if len(ad.Image_src) == 0 {
		mongo.LogNo(funcName, os)
		util.HttpRequestCount.WithLabelValues(funcName + "noerr").Inc()
		return util.ResMsg{}
	}
	postData := util.ResMsg{
		Id:       "0",
		Weight:   0,
		State:    0,
		Title:    ad.Text,
		Content:  ad.Desc,
		ImageUrl: ad.Image_src[0],
		Uri:      "",
	}
	if ad.Deeplink != "" {
		postData.Uri = ad.Deeplink
	} else {
		uri := ad.Click_url
		if ad.AdType == 0 || ad.AdType == 3 {
			pos := util.CreateFPos(width, height)
			// fmt.Println(width, height, pos)
			uri = strings.Replace(uri, "__REQ_WIDTH__", w, -1)
			uri = strings.Replace(uri, "__REQ_HEIGHT__", h, -1)
			uri = strings.Replace(uri, "__WIDTH__", strconv.Itoa(width), -1)
			uri = strings.Replace(uri, "__HEIGHT__", strconv.Itoa(height), -1)
			uri = strings.Replace(uri, "__DOWN_X__", strconv.Itoa(pos[0]), -1)
			uri = strings.Replace(uri, "__DOWN_Y__", strconv.Itoa(pos[1]), -1)
			uri = strings.Replace(uri, "__UP_X__", strconv.Itoa(pos[2]), -1)
			uri = strings.Replace(uri, "__UP_Y__", strconv.Itoa(pos[3]), -1)
		}
		postData.Uri = uri
	}

	postData.Displayreport = ad.Expose_url
	postData.Displayreport = append(postData.Displayreport, "http://120.26.247.23/ad/report.php?method="+funcName+"&event=displayreport&os="+os+"&t="+util.GetRandom())
	postData.Clickreport = ad.Clicker_url
	postData.Clickreport = append(postData.Clickreport, "http://120.26.247.23/ad/report.php?method="+funcName+"&event=clickreport&os="+os+"&t="+util.GetRandom())

	return postData
}
