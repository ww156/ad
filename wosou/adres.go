package wosou

type adres struct {
	Message string
	Code    int
	Ads     []Ad
}

type Ad struct {
	AdType             int
	Text               string
	Desc               string
	Icon_src           string
	Image_src          []string
	Click_url          string
	Deeplink           string
	Clicker_url        []string
	Expose_url         []string
	Targetid           string
	Api_name           int
	StartDownloadUrls  []string
	FinishDownloadUrls []string
	StartInstallUrls   []string
	FinishInstallUrls  []string
}
