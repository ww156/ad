package wosou

type adreq struct {
	Wsid           string `json:"wsid"`
	Os             int    `json:"os"`
	Mouid          string `json:"mouid"`
	Ifa            string `json:"ifa"`
	Dve            string `json:"dve"`
	Dmo            string `json:"dmo"`
	Dop            int    `json:"dop"`
	Net_type       int    `json:"net_type"`
	Aw             int    `json:"aw"`
	Ah             int    `json:"ah"`
	Ai             string `json:"ai"`
	Detype         int    `json:"detype"`
	Apv            string `json:"apv"`
	Dev            string `json:"dev"`
	Pckname        string `json:"pckname"`
	Adtype         int    `json:"adtype"`
	Ip             string `json:"ip"`
	Secure         int    `json:"secure"`
	Mac            string `json:"mac"`
	AAid           string `json:"aaid"`
	Lat            int    `json:"lat"`
	Lng            int    `json:"lng"`
	Coordtime      int    `json:"coordtime"`
	Dw             int    `json:"dw"`
	Dh             int    `json:"dh"`
	UA             string `json:"ua"`
	Screen_density string `json:"screen_density"`
	Full_screen    bool   `json:"full_screen"`
}
