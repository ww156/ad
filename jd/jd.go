package jd

import (
	// "bytes"
	// "encoding/json"
	// "fmt"
	"io/ioutil"
	// "log"
	"net/http"
	"strconv"
	// "strings"
	// "time"
	// "unsafe"

	"github.com/golang/protobuf/proto"

	"ad/mongo"
	"ad/util"
)

const (
	URL                 = "http://adx.jd.com/ssp/bidding"
	BANNER              = 201 // 横幅
	STARTUP             = 202 // 开屏
	FLOW                = 206 // 信息流（Native）
	SSP_TOKEN           = "12a2000008845843"
	MEDIA_ID_ANDROID    = "cd1c000008845844"
	ADID_ANDROID_BANNER = "478d000008845848"
	ADID_ANDROID_FLOW   = "15bc000008845849"
	MEDIA_ID_IOS        = "c304000008845845"
	ADID_IOS_BANNER     = "db20000008845846"
	ADID_IOS_FLOW       = "2789000008845847" // "2789000008845840"
)

func Jd(getReq *util.ReqMsg, funcName string) util.ResMsg {
	if getReq.Os == "1" {
		return base(getReq, funcName, BANNER, 640, 100, MEDIA_ID_ANDROID, ADID_ANDROID_BANNER, `640x100`)
	}
	return base(getReq, funcName, BANNER, 640, 100, MEDIA_ID_IOS, ADID_IOS_BANNER, `640x100`)
}

func JdFlow(getReq *util.ReqMsg, funcName string) util.ResMsg {
	if getReq.Os == "1" {
		return base(getReq, funcName, FLOW, 640, 360, MEDIA_ID_ANDROID, ADID_ANDROID_FLOW, `640x360`)
	}
	return base(getReq, funcName, FLOW, 640, 360, MEDIA_ID_IOS, ADID_IOS_FLOW, `640x360`)
}

func base(getReq *util.ReqMsg, funcName string, viewType, w, h uint32, media_id, ad_id, size string) util.ResMsg {
	postData := BidRequest{
		Version:   proto.Uint32(1),
		RequestId: proto.String(util.GetRandom()),
		Test:      proto.Bool(false),
		SspToken:  proto.String(SSP_TOKEN),
		Ip:        proto.String(getReq.Ip),
		UserAgent: proto.String(getReq.Ua),
		Imp: []*BidRequest_Imp{
			{
				MediaId: proto.String(media_id),
				AdId:    proto.String(ad_id),
				Size:    proto.String(size),
				// Size:     proto.String("640x360"),
				FloorPrice: proto.Uint32(300),
				ViewType:   proto.Uint32(viewType),
				AdDevice:   proto.Uint32(3),
				Native: &BidRequest_Imp_Native{
					ImageWidth:  proto.Uint32(w),
					ImageHeight: proto.Uint32(h),
					ImageNums:   proto.Uint32(1),
					Fields: []NativeField{
						NativeField_TITLE,
						NativeField_DESC,
						NativeField_IMGS,
						NativeField_CLICK_URL,
					},
				},
			},
		},
		App: &BidRequest_App{
			AppName:     proto.String("网上厨房"),
			PackageName: proto.String("cn.ecook"),
			Device: &BidRequest_App_Device{
				Os:          proto.String("android"),
				Osv:         proto.String(getReq.Osversion),
				DeviceType:  proto.String(getReq.Device),
				Orientation: proto.Uint32(1),
				Brand:       proto.String(getReq.Model),
				Operator:    proto.Uint32(1),
				Network:     proto.Uint32(1),
				// H:           getReq.Screenheight,
				// W:           getReq.Screenwidth,
				DeviceSize: proto.String(getReq.Screenwidth + "x" + getReq.Screenheight),
				DeviceId:   proto.String(getReq.Imei),
				Mac:        proto.String(getReq.Mac),
				// AndroidId:  proto.String(getReq.Androidid),
			},
		},
	}
	// fmt.Println(*postData.RequestId)
	if h == 100 {
		postData.Imp[0].Native = nil
	}
	device := postData.App.Device
	// screensize
	width, err := strconv.Atoi(getReq.Screenwidth)
	if err != nil {
		width = 0
	}
	device.W = proto.Uint32(uint32(width))
	height, err := strconv.Atoi(getReq.Screenheight)
	if err != nil {
		height = 0
	}
	device.H = proto.Uint32(uint32(height))
	// 运营商
	switch getReq.Imsi {
	case "1":
		device.Operator = proto.Uint32(1)
	case "2":
		device.Operator = proto.Uint32(2)
	case "3":
		device.Operator = proto.Uint32(3)
	default:
		device.Operator = proto.Uint32(0)
	}
	// 网络类型
	switch getReq.Network {
	case "wifi":
		device.Network = proto.Uint32(1)
	case "2g":
		device.Network = proto.Uint32(2)
	case "3g":
		device.Network = proto.Uint32(3)
	case "4g":
		device.Network = proto.Uint32(4)
	default:
		device.Network = proto.Uint32(0)
	}
	if getReq.Os == "2" {
		postData.App.PackageName = proto.String("cn.ecook.ecook")
		device.Os = proto.String("ios")
		device.DeviceId = proto.String(getReq.Idfa)
	}
	// fmt.Println("jdnet:", funcName, "-", postData.App.Device.GetNetwork())
	ma, err := proto.Marshal(&postData)
	if err != nil {
		mongo.LogTimeout(funcName, getReq.Os)
		util.HttpRequestCount.WithLabelValues(funcName + "jsonerr").Inc()
		return util.ResMsg{}
	}

	header := http.Header{}
	header.Set("Connection", "keep-alive")
	header.Set("Content-Type", "application/octet-stream;charset=utf-8")
	resp, err := util.HttpPOST(URL, &header, ma)
	mongo.LogReq(funcName, getReq.Os)
	if err != nil {
		mongo.LogTimeout(funcName, getReq.Os)
		util.HttpRequestCount.WithLabelValues(funcName + "timeouterr").Inc()
		return util.ResMsg{}
	}
	data, err := ioutil.ReadAll(resp.Body)
	resp.Body.Close()
	if err != nil {
		// log.Fatal("ReadResponse error:", err)
		mongo.LogNo(funcName, getReq.Os)
		util.HttpRequestCount.WithLabelValues(funcName + "readresperr").Inc()
		return util.ResMsg{}
	}
	if resp.StatusCode != 200 {
		mongo.LogTimeout(funcName, getReq.Os)
		util.HttpRequestCount.WithLabelValues(funcName + "not200err").Inc()
		return util.ResMsg{}
	}

	return FormateRes(data, getReq.Os, funcName)
}

func FormateRes(res []byte, os, funcName string) util.ResMsg {
	resData := BidResponse{}
	proto.Unmarshal(res, &resData)
	if len(resData.Ads) == 0 {
		mongo.LogNo(funcName, os)
		util.HttpRequestCount.WithLabelValues(funcName + "noerr").Inc()
		return util.ResMsg{}
	}
	ad := resData.Ads[0]
	postData := util.ResMsg{
		Id:       "0",
		Weight:   0,
		State:    0,
		Title:    "jd",
		Content:  "jd",
		ImageUrl: *ad.Imgurl,
		Uri:      *ad.Ldp,
	}
	if ad.Attr != nil {
		for _, attr := range ad.Attr {
			if *attr.Name == "title" {
				postData.Title = *attr.Value
			}
			if *attr.Name == "nativeDesc" {
				postData.Content = *attr.Value
			}
			if *attr.Name == "imgs1" {
				postData.ImageUrl = *attr.Value
			}
			if *attr.Name == "clickUrl" {
				postData.Uri = *attr.Value
			}
		}
	}
	postData.Displayreport = ad.ShowUrl
	postData.Displayreport = append(postData.Displayreport, "http://120.26.247.23/ad/report.php?method="+funcName+"&event=displayreport&os="+os+"&t="+util.GetRandom())
	postData.Clickreport = ad.ClickUrl
	postData.Clickreport = append(postData.Clickreport, "http://120.26.247.23/ad/report.php?method="+funcName+"&event=clickreport&os="+os+"&t="+util.GetRandom())

	return postData
}
