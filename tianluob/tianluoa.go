package tianluob

import (
	// "bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	// "log"
	// "crypto/tls"
	"net/http"
	"net/url"
	"strconv"
	// "strings"
	"time"
	// "unsafe"
	// "regexp"

	"ad/mongo"
	"ad/util"
	"github.com/pquerna/ffjson/ffjson"
)

const (
	URL = "http://hyssp.haiyunx.com/spush/s2s.gif?"

	ADID_ANDROID         = "c73e963b"
	ADID_ANDROID_FLOW    = "d2a0769a"
	ADID_ANDROID_STARTUP = "fa7d0fc9"
	ADID_IOS             = "652af064"
	ADID_IOS_FLOW        = "6b592ada"
	ADID_IOS_STARTUP     = "2f62672f"
)

func Tianluob(getReq *util.ReqMsg, funcName string) util.ResMsg {
	if getReq.Os == "1" {
		return base(getReq, funcName, ADID_ANDROID)
	}
	return base(getReq, funcName, ADID_IOS)
}

func TianluobFlow(getReq *util.ReqMsg, funcName string) util.ResMsg {
	if getReq.Os == "1" {
		return base(getReq, funcName, ADID_ANDROID_FLOW)

	} else {
		return base(getReq, funcName, ADID_IOS_FLOW)
	}
}

func TianluobStartup(getReq *util.ReqMsg, funcName string) util.ResMsg {
	if getReq.Os == "1" {
		return base(getReq, funcName, ADID_ANDROID_STARTUP)

	} else {
		return base(getReq, funcName, ADID_IOS_STARTUP)
	}
}

func base(getReq *util.ReqMsg, funcName, adid string) util.ResMsg {
	carrier := "0"
	switch getReq.Imsi {
	case "1":
		carrier = "1"
	case "2":
		carrier = "2"
	case "3":
		carrier = "3"
	default:
		carrier = "0"
	}
	network := "0"
	switch getReq.Network {
	case "wifi":
		network = "1"
	case "2g":
		network = "2"
	case "3g":
		network = "3"
	case "4g":
		network = "4"
	default:
		network = "0"
	}
	time := strconv.Itoa(int(time.Now().UnixNano() / 1e6))

	v := url.Values{}
	v.Set("pid", adid)
	v.Set("ptype", "1")
	v.Set("ip", getReq.Ip)
	v.Set("ua", getReq.Ua)
	v.Set("uid", getReq.Imei)
	v.Set("reqid", util.GetRandom())
	v.Set("app_version", getReq.Appversion)
	v.Set("v", "2.0.0")
	device := device{
		Openudid:  getReq.Openudid,
		Androidid: getReq.Androidid,
		Imei:      getReq.Imei,
		Idfa:      getReq.Idfa,
		Mac:       getReq.Mac,
		Os:        getReq.Os,
		Os_v:      getReq.Osversion,
		Dev:       getReq.Vendor,
		Md:        getReq.Model,
		Network:   network,
		Operator:  carrier,
		Width:     getReq.Screenwidth,
		Height:    getReq.Screenheight,
	}
	if getReq.Os == "2" {
		v.Set("uid", getReq.Idfa)
	}
	gps := gps{
		Lat: getReq.Lat,
		Lng: getReq.Lng,
		T:   time,
	}
	d, err := ffjson.Marshal(device)
	if err != nil {
		d = []byte("{}")
	}
	g, err := ffjson.Marshal(gps)
	if err != nil {
		g = []byte("{}")
	}

	header := http.Header{}
	header.Set("Connection", "keep-alive")
	header.Set("Content-Type", "application/json;charset=utf-8")
	req, err := http.NewRequest("GET", URL+v.Encode()+"&device="+string(d)+"&gps="+string(g), nil)
	if err != nil {
		return util.ResMsg{}
	}
	resp, err := util.Client.Do(req)
	mongo.LogReq(funcName, getReq.Os)
	if err != nil {
		mongo.LogTimeout(funcName, getReq.Os)
		return util.ResMsg{}
	}
	data, err := ioutil.ReadAll(resp.Body)
	resp.Body.Close()
	if err != nil {
		mongo.LogNo(funcName, getReq.Os)
		return util.ResMsg{}
	}
	fmt.Println(resp.StatusCode)
	if resp.StatusCode != 200 {
		mongo.LogTimeout(funcName, getReq.Os)
		return util.ResMsg{}
	}
	return FormateRes(data, getReq.Os, funcName)
}

func FormateRes(res []byte, os, funcName string) util.ResMsg {
	resData := &adres{}
	json.Unmarshal(res, resData)
	fmt.Println(string(res))
	if len(resData.Ad) == 0 {
		mongo.LogNo(funcName, os)
		util.HttpRequestCount.WithLabelValues(funcName + "noerr").Inc()
		return util.ResMsg{}
	}
	ad := resData.Ad[0]
	postData := util.ResMsg{
		Id:       "0",
		Weight:   0,
		State:    1,
		Title:    ad.Title,
		Content:  ad.Desc,
		ImageUrl: ad.Adm,
		Uri:      ad.Url,
	}

	postData.Displayreport = ad.Imp
	postData.Displayreport = append(postData.Displayreport, "http://120.26.247.23/ad/report.php?method="+funcName+"&event=displayreport&os="+os+"&t="+util.GetRandom())
	postData.Clickreport = ad.Clk
	postData.Clickreport = append(postData.Clickreport, "http://120.26.247.23/ad/report.php?method="+funcName+"&event=clickreport&os="+os+"&t="+util.GetRandom())

	return postData
}
