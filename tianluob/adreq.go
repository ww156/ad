package tianluob

type device struct {
	Openudid  string `json:"openuidid"`
	Androidid string `json:"androidid"`
	Imei      string `json:"imei"`
	Idfa      string `json:"idfa"`
	Mac       string `json:"mac"`
	Os        string `json:"os"`
	Os_v      string `json:"os_v"`
	Dev       string `json:"dev"`
	Md        string `json:"md"`
	Network   string `json:"network"`
	Operator  string `json:"operator"`
	Width     string `json:"width"`
	Height    string `json:"height"`
}

type gps struct {
	Lat string `json:"lat"`
	Lng string `json:"lng"`
	T   string `json:"t"`
}
