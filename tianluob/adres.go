package tianluob

type adres struct {
	Ad    []ad
	V     string
	Pt    int
	Reqid string
}
type ad struct {
	Action   int
	Aid      string
	Width    int
	Height   int
	Pid      string
	Adm      string
	Url      string
	Ext_adms []string
	Title    string
	Desc     string
	Imp      []string
	Clk      []string
	Dp_url   string
	Dp_clk   string
	App      app
}

type app struct {
	Icon_url string
	Name     string
	Pname    string
}
