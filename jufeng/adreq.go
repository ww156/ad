package jufeng

type adreq struct {
	Appvn     string `json:"appvn"`
	Ei        string `json:"ei"`
	Si        string `json:"si"`
	AndroidId string `json:"androidId"`
	Osv       string `json:"osv"`
	Pm        string `json:"pm"`
	Mac       string `json:"mac"`
	HttpUA    string `json:"httpUa"`
	Wh        string `json:"wh"`
	Cnet      string `json:"cnet"`
	Ct        string `json:"ct"`
	Pkg       string `json:"pkg"`
	Ch        string `json:"ch"`
	Addr      *Addr  `json:"addr"`
}

type Addr struct {
	Ip  string  `json:"ip"`
	Lat float64 `json:"lat,omitempty"`
	Lng float64 `json:"lng,omitempty"`
}
