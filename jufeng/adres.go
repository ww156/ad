package jufeng

type adres struct {
	Code int
	Msg  string
	Data []Ad
}

type Ad struct {
	Aid            int
	AdType         string
	SourceType     string
	Url            string
	Img            Img
	Icon           string
	Des            string
	Imgtracking    []string
	Thclkurl       []string
	Closeur        []string
	ExpirationTime string
	Extra          Extra
}

type Img struct {
	Url string
}

type Extra struct {
	DownUrl    []string
	DowSuccUrl []string
	InsUrl     []string
	InsSuccUrl []string
	OpenUrl    []string
	Pkg        string
	Size       float64
	Deeplink   string
}
