package jufeng

import (
	// "bytes"
	"encoding/json"
	// "fmt"
	"io/ioutil"
	// "log"
	"net/http"
	"strconv"
	"strings"
	"time"
	// "unsafe"
	"math/rand"

	// "github.com/golang/protobuf/proto"

	"ad/mongo"
	"ad/util"
)

const (
	URL = "http://api.mincache.com/v1/ssp/"
	CH  = "ssptest"
)

func Jufeng(getReq *util.ReqMsg, funcName string) util.ResMsg {
	return base(getReq, funcName, "3Banner")
}

func JufengFlow(getReq *util.ReqMsg, funcName string) util.ResMsg {
	return base(getReq, funcName, "3Feed")
}

func JufengStartup(getReq *util.ReqMsg, funcName string) util.ResMsg {
	return base(getReq, funcName, "3MC")
}

func base(getReq *util.ReqMsg, funcName, adtype string) util.ResMsg {
	carrier := ""
	switch getReq.Imsi {
	case "1":
		carrier = "46000"
	case "2":
		carrier = "46001"
	case "3":
		carrier = "46002"
	default:
		carrier = ""
	}
	if carrier != "" {
		carrier = carrier + strconv.Itoa(10000+rand.Intn(90000)) + strconv.Itoa(10000+rand.Intn(90000))
	}
	postData := adreq{
		Appvn:     "3.5",
		Ei:        getReq.Imei,
		Si:        carrier,
		AndroidId: getReq.Androidid,
		Osv:       getReq.Osversion,
		Pm:        getReq.Model,
		Mac:       getReq.Mac,
		HttpUA:    getReq.Ua,
		Wh:        getReq.Screenwidth + ":" + getReq.Screenheight,
		Cnet:      getReq.Network,
		Ct:        "android",
		Pkg:       "网上厨房",
		Ch:        CH,
		Addr: &Addr{
			Ip: getReq.Ip,
		},
	}
	if getReq.Os == "2" {
		postData.Ct = "ios"
	}

	ma, err := json.Marshal(&postData)
	if err != nil {
		return util.ResMsg{}
	}
	// fmt.Printf("%+v", string(ma))
	header := http.Header{}
	header.Set("Connection", "keep-alive")
	header.Set("Content-Type", "application/x-www-form-urlencoded")
	header.Set("Accept", "encrypt/0")
	resp, err := util.HttpPOST(URL+adtype, &header, []byte("data="+string(ma)))
	mongo.LogReq(funcName, getReq.Os)
	if err != nil {
		mongo.LogTimeout(funcName, getReq.Os)
		util.HttpRequestCount.WithLabelValues(funcName + "timeouterr").Inc()
		return util.ResMsg{}
	}
	data, err := ioutil.ReadAll(resp.Body)
	resp.Body.Close()
	if err != nil {
		mongo.LogNo(funcName, getReq.Os)
		util.HttpRequestCount.WithLabelValues(funcName + "readresperr").Inc()
		return util.ResMsg{}
	}
	if resp.StatusCode != 200 {
		mongo.LogTimeout(funcName, getReq.Os)
		util.HttpRequestCount.WithLabelValues(funcName + "not200err").Inc()
		return util.ResMsg{}
	}
	return Formate(data, getReq.Os, funcName, getReq.Screenwidth, getReq.Screenheight)
}

func Formate(res []byte, os, funcName, w, h string) util.ResMsg {
	resData := &adres{}
	json.Unmarshal(res, resData)
	// fmt.Println(string(res))
	// fmt.Printf("%+v", resData)
	if resData.Code != 200 {
		mongo.LogNo(funcName, os)
		util.HttpRequestCount.WithLabelValues(funcName + "noerr").Inc()
		return util.ResMsg{}
	}
	if len(resData.Data) == 0 {
		mongo.LogNo(funcName, os)
		util.HttpRequestCount.WithLabelValues(funcName + "noerr").Inc()
		return util.ResMsg{}
	}
	ad := resData.Data[0]

	if ad.Img.Url == "" {
		mongo.LogNo(funcName, os)
		util.HttpRequestCount.WithLabelValues(funcName + "noerr").Inc()
		return util.ResMsg{}
	}
	width, err := strconv.Atoi(w)
	if err != nil {
		width = 0
	}
	height, err := strconv.Atoi(h)
	if err != nil {
		height = 0
	}
	pos := createPos(width, height)
	ad.Url = strings.Replace(ad.Url, "${DOWN_X}", strconv.Itoa(pos[0]), -1)
	ad.Url = strings.Replace(ad.Url, "${DOWN_Y}", strconv.Itoa(pos[1]), -1)
	ad.Url = strings.Replace(ad.Url, "${UP_X}", strconv.Itoa(pos[2]), -1)
	ad.Url = strings.Replace(ad.Url, "${UP_Y}", strconv.Itoa(pos[3]), -1)
	postData := util.ResMsg{
		Id:       "0",
		Weight:   0,
		State:    0,
		Title:    ad.Des,
		Content:  "",
		ImageUrl: ad.Img.Url,
		Uri:      ad.Url,
	}
	postData.Displayreport = ad.Imgtracking
	postData.Displayreport = append(postData.Displayreport, "http://120.26.247.23/ad/report.php?method="+funcName+"&event=displayreport&os="+os+"&t="+util.GetRandom())
	clickUrl := make([]string, len(ad.Thclkurl))
	for i, value := range ad.Thclkurl {
		value = strings.Replace(value, "${DOWN_X}", strconv.Itoa(pos[0]), -1)
		value = strings.Replace(value, "${DOWN_Y}", strconv.Itoa(pos[1]), -1)
		value = strings.Replace(value, "${UP_X}", strconv.Itoa(pos[2]), -1)
		value = strings.Replace(value, "${UP_Y}", strconv.Itoa(pos[3]), -1)
		clickUrl[i] = value
	}
	postData.Clickreport = clickUrl
	postData.Clickreport = append(postData.Clickreport, "http://120.26.247.23/ad/report.php?method="+funcName+"&event=clickreport&os="+os+"&t="+util.GetRandom())

	return postData
}

func createPos(w, h int) [4]int {
	pos := [4]int{}
	x := rand.New(rand.NewSource(time.Now().UnixNano())).Intn(99)
	pos[0] = w * x / 100
	pos[2] = w * x / 100
	y := rand.New(rand.NewSource(time.Now().UnixNano() + 10000)).Intn(99)
	pos[1] = h * y / 100
	pos[3] = h * y / 100
	return pos
}
