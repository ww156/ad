package jinyi

type adres struct {
	Status int
	Msg    string
	Data   data
}

type data struct {
	List []list
}

type list struct {
	Status int
	Msg    string
	Posid  string
	Ads    []ad
}

type ad struct {
	Adid        string
	Showmode    string
	Crt_type    int
	Title       string
	Content     string
	Desc        string
	Brand       string
	Iurl        string
	Images      []img
	Video       video
	Target_type int
	Targeturl   string
	Showurl     string
	Sr          []report
	Cr          []report
	Clr         []report
	Ext         ext
	Cfg         cfg
}

type img struct {
	Imgurl string
	W      int
	H      int
}

type video struct {
	Vurl     string
	Duration int
}
type report struct {
	Url string
}
type ext struct {
}

type cfg struct {
	Logo    string
	Logourl string
}
