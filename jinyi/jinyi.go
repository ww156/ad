package jinyi

import (
	// "bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	// "log"
	"net/http"
	"net/url"
	"strconv"
	// "strings"
	// "time"
	// "unsafe"
	// "regexp"

	"ad/mongo"
	"ad/util"
	// "github.com/pquerna/ffjson/ffjson"
)

const (
	URL               = "https://api.golddsp.com/ad.do"
	APPID             = "6050305154328807"
	ADID_IOS          = "1104241292"
	ADID_IOS_FLOW     = "1104241294"
	ADID_ANDROID_FLOW = "1104241293"
)

func Jinyi(getReq *util.ReqMsg, funcName string) util.ResMsg {
	if getReq.Os == "1" {
		return util.ResMsg{}
	}
	return base(getReq, funcName, "640", "100", ADID_IOS)
}

func JinyiFlow(getReq *util.ReqMsg, funcName string) util.ResMsg {
	if getReq.Os == "1" {
		return base(getReq, funcName, "600", "500", ADID_ANDROID_FLOW)
	}
	return base(getReq, funcName, "600", "500", ADID_IOS_FLOW)
}

func JinyiStartup(getReq *util.ReqMsg, funcName string) util.ResMsg {
	// return base(getReq, funcName, "640", "960")
	return util.ResMsg{}
}

func base(getReq *util.ReqMsg, funcName, w, h, adid string) util.ResMsg {
	v := url.Values{}
	v.Set("count", "1")
	v.Set("posid", adid)
	v.Set("appid", APPID)
	v.Set("posw", w)
	v.Set("posh", h)
	v.Set("ver", "1.0")
	v.Set("net", getReq.Network)
	v.Set("carrier", getReq.Imsi)
	v.Set("os", "android")
	v.Set("osver", getReq.Osversion)
	v.Set("local", "zh_Cn")
	v.Set("remoteip", getReq.Ip)
	v.Set("lat", getReq.Lat)
	v.Set("lng", getReq.Lng)
	v.Set("useragent", getReq.Ua)
	sd, _ := strconv.Atoi(getReq.Sd)
	dpi := strconv.Itoa(sd / 160)
	v.Set("screen_density", dpi)
	v.Set("c_w", getReq.Screenwidth)
	v.Set("c_h", getReq.Screenheight)
	v.Set("c_ori", "0")
	v.Set("pkg", "cn.ecook")
	v.Set("mac", getReq.Mac)
	v.Set("aid", getReq.Androidid)
	v.Set("increment", getReq.Vendor)
	v.Set("dn", getReq.Vendor)
	v.Set("dt", getReq.Model)
	v.Set("idfa", getReq.Idfa)
	if getReq.Os == "2" {
		v.Set("pkg", "cn.ecook.ecook")
	}

	req, err := http.NewRequest("GET", URL+"?pas="+v.Encode(), nil)
	if err != nil {
		fmt.Println(err.Error())
		return util.ResMsg{}
	}
	resp, err := util.Client.Do(req)
	mongo.LogReq(funcName, getReq.Os)
	if err != nil {
		fmt.Println(err.Error())
		mongo.LogTimeout(funcName, getReq.Os)
		return util.ResMsg{}
	}
	data, err := ioutil.ReadAll(resp.Body)
	resp.Body.Close()
	if err != nil {
		fmt.Println(err.Error())
		mongo.LogNo(funcName, getReq.Os)
		return util.ResMsg{}
	}
	fmt.Println(resp.StatusCode)
	if resp.StatusCode != 200 {
		mongo.LogTimeout(funcName, getReq.Os)
		return util.ResMsg{}
	}
	// fmt.Println(string(data))
	return FormateRes(data, getReq.Os, funcName)
}

// 根据广告平台的返回生成客户端返回
func FormateRes(res []byte, os, funcName string) util.ResMsg {
	resData := adres{}
	json.Unmarshal(res, &resData)
	// fmt.Printf("%+v", resData)
	fmt.Println(string(res))
	if resData.Status != 1 {
		// fmt.Println(2)
		mongo.LogNo(funcName, os)
		return util.ResMsg{}
	}
	if len(resData.Data.List) == 0 {
		// fmt.Println(2)
		mongo.LogNo(funcName, os)
		return util.ResMsg{}
	}
	if len(resData.Data.List[0].Ads) == 0 {
		// fmt.Println(2)
		mongo.LogNo(funcName, os)
		return util.ResMsg{}
	}

	ad := resData.Data.List[0].Ads[0]

	postData := util.ResMsg{
		Id:       "0",
		Weight:   0,
		State:    1,
		Title:    ad.Title,
		Content:  ad.Desc,
		ImageUrl: "",
		Uri:      ad.Targeturl,
	}
	if len(ad.Images) == 0 {
		mongo.LogNo(funcName, os)
		return util.ResMsg{}
	}
	postData.ImageUrl = ad.Images[0].Imgurl

	for _, v := range ad.Sr {
		u := v.Url
		if u != "" {
			postData.Displayreport = append(postData.Displayreport, u)
		}
	}
	postData.Displayreport = append(postData.Displayreport, "http://120.26.247.23/ad/report.php?method="+funcName+"&event=displayreport&os="+os+"&t="+util.GetRandom())
	for _, v := range ad.Cr {
		u := v.Url
		if u != "" {
			postData.Clickreport = append(postData.Clickreport, u)
		}
	}
	postData.Clickreport = append(postData.Clickreport, "http://120.26.247.23/ad/report.php?method="+funcName+"&event=clickreport&os="+os+"&t="+util.GetRandom())

	return postData
}
