package yidian

import (
	// "bytes"
	"crypto/hmac"
	"crypto/sha1"
	"encoding/base64"
	"encoding/hex"
	// "encoding/json"
	// "fmt"
	"io"
	"io/ioutil"
	// "log"
	"net/http"
	// "net/url"
	"strconv"
	"strings"
	"time"

	"github.com/golang/protobuf/proto"

	"ad/mongo"
	"ad/util"
)

const (
	URL     = "http://dsp.yidianzixun.com/bid"
	DEV_URL = "http://test.dsp.ad.yidianzixun.com/bid"
	RTB     = "ecook"
	KEY     = "vQpXdEv)J@4PgfnLBvDGAfY3iis~oOPt"
	BS64    = "OlVgiNyt5CaI6Ws4GFeLdj7BrSY_09nkvRTmhbHq2puXc8Dx3P-ZwKMAJQzE1fUo"
)

func Yidian(getReq *util.ReqMsg, funcName string) util.ResMsg {
	return base(getReq, funcName, 640, 100)
}

func YidainStartUp(getReq *util.ReqMsg, funcName string) util.ResMsg {
	return base(getReq, funcName, 640, 960)
}

func YidianFlow(getReq *util.ReqMsg, funcName string) util.ResMsg {
	return base(getReq, funcName, 640, 360)
}

func base(getReq *util.ReqMsg, funcName string, w, h int32) util.ResMsg {
	return util.ResMsg{}
	postData := BidRequest{
		Id: proto.String(util.GetRandom()),
		Imp: []*BidRequest_Imp{
			{
				Id: proto.String(util.GetRandom()),
				Banner: &BidRequest_Imp_Banner{
					Template: []string{"SINGLE_IMAGE"},
					Ctype:    proto.Int32(0),
					W:        proto.Int32(w),
					H:        proto.Int32(h),
				},
			},
		},
		Device: &BidRequest_Device{
			Ua:         proto.String(getReq.Ua),
			Ip:         proto.String(getReq.Ip),
			Devicetype: BidRequest_Device_DT_PHONE.Enum(),
			Os:         proto.String("android"),
			Osv:        proto.String(getReq.Osversion),
			// W: 0,
			// H: 0,
			Make:  proto.String(getReq.Vendor),
			Model: proto.String(getReq.Model),
			// Pxrate: 0,
			// Connectiontype: 0,
			Didmd5:  proto.String(util.Md5(getReq.Imei)),
			Dpidmd5: proto.String(util.Md5(getReq.Androidid)),
			Macmd5:  proto.String(util.Md5(getReq.Mac)),
		},
		App: &BidRequest_App{
			Ver:  proto.String(getReq.Appversion),
			Name: proto.String("网上厨房"),
		},
		At:   proto.Int32(1),
		Test: proto.Bool(true),
	}

	device := postData.Device
	// screensize
	width, err := strconv.Atoi(getReq.Screenwidth)
	if err != nil {
		width = 0
	}
	device.W = proto.Int32(int32(width))
	height, err := strconv.Atoi(getReq.Screenheight)
	if err != nil {
		height = 0
	}
	device.H = proto.Int32(int32(height))

	// 网络类型
	switch getReq.Network {
	case "wifi":
		device.Connectiontype = BidRequest_Device_CT_WIFI.Enum()
	case "2g":
		device.Connectiontype = BidRequest_Device_CT_CELLULAR_2G.Enum()
	case "3g":
		device.Connectiontype = BidRequest_Device_CT_CELLULAR_3G.Enum()
	case "4g":
		device.Connectiontype = BidRequest_Device_CT_CELLULAR_4G.Enum()
	default:
		device.Connectiontype = BidRequest_Device_CT_UNKNOWN.Enum()
	}
	if getReq.Os == "2" {
		device.Os = proto.String("ios")
		device.Didmd5 = proto.String(util.Md5(getReq.Idfa))
	}

	ma, err := proto.Marshal(&postData)
	if err != nil {
		return util.ResMsg{}
	}
	header := http.Header{}
	header.Set("Connection", "keep-alive")
	header.Set("Content-Type", "application/x-protobuf")
	header.Set("x-caifeng-rtb-version", "1.0")
	header.Set("x-caifeng-rtb-from", RTB)
	resp, err := util.HttpPOST(URL, &header, ma)
	mongo.LogReq(funcName, getReq.Os)
	if err != nil {
		mongo.LogTimeout(funcName, getReq.Os)
		util.HttpRequestCount.WithLabelValues(funcName + "timeouterr").Inc()
		return util.ResMsg{}
	}
	data, err := ioutil.ReadAll(resp.Body)
	resp.Body.Close()
	if err != nil {
		mongo.LogNo(funcName, getReq.Os)
		util.HttpRequestCount.WithLabelValues(funcName + "readresperr").Inc()
		return util.ResMsg{}
	}
	if resp.StatusCode != 200 {
		mongo.LogNo(funcName, getReq.Os)
		util.HttpRequestCount.WithLabelValues(funcName + "not200err").Inc()
		return util.ResMsg{}
	}
	return FormateRes(data, getReq.Os, funcName)
}

func FormateRes(res []byte, os, funcName string) util.ResMsg {
	resData := BidResponse{}
	proto.Unmarshal(res, &resData)
	// fmt.Println(string(res))
	if len(resData.SeatBid) == 0 {
		mongo.LogNo(funcName, os)
		util.HttpRequestCount.WithLabelValues(funcName + "noerr").Inc()
		return util.ResMsg{}
	}
	if len(resData.SeatBid[0].Bid) == 0 {
		mongo.LogNo(funcName, os)
		util.HttpRequestCount.WithLabelValues(funcName + "noerr").Inc()
		return util.ResMsg{}
	}
	bid := resData.SeatBid[0].Bid[0]
	postData := util.ResMsg{
		Id:       "0",
		Weight:   0,
		State:    0,
		Title:    "",
		Content:  "",
		ImageUrl: bid.Aurl[0],
		Uri:      "",
	}
	if bid.Title != nil {
		postData.Title = *bid.Title
	}
	if bid.Summary != nil {
		postData.Content = *bid.Summary
	}
	if bid.Curl != nil {
		postData.Uri = *bid.Curl
	}
	t := strconv.Itoa(int(time.Now().UnixNano() / 1e6))
	price := strconv.Itoa(int(*bid.Price))
	str := "id=" + *bid.Id + "&impid=" + *bid.Impid + "&adid=" + *bid.Adid + "&crid=" + *bid.Crid + "&price=" + price + "&t=" + t
	// fmt.Println(str)
	mac := hmac.New(sha1.New, []byte(KEY))
	io.WriteString(mac, str)
	hmacsha1 := mac.Sum(nil)
	// fmt.Println(hmacsha1)
	str = str + "&ck=" + hex.EncodeToString(hmacsha1[:])
	// fmt.Println(str)
	bs64 := base64.NewEncoding(BS64)
	nurl := strings.Replace(*bid.Nurl, "${PRICE}", bs64.EncodeToString([]byte(str)), -1)
	// fmt.Println(bs64.EncodeToString(util.ResMsg(str)))
	// de, _ := bs64.DecodeString(bs64.EncodeToString(util.ResMsg(str)))
	// fmt.Println(string(de))
	postData.Displayreport = bid.Murl
	postData.Displayreport = append(postData.Displayreport, nurl, "http://120.26.247.23/ad/report.php?method="+funcName+"&event=displayreport&os="+os+"&t="+util.GetRandom())
	postData.Clickreport = bid.Cmurl
	postData.Clickreport = append(postData.Clickreport, "http://120.26.247.23/ad/report.php?method="+funcName+"&event=clickreport&os="+os+"&t="+util.GetRandom())

	return postData
}
