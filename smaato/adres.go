package smaato

type Adres struct {
	Sessionid string
	Success   string `json:"success"`
	Status    string `json:"status"`
	Type      string
	Link      string
	Target    string
	Width     int
	Height    int
	Beacons   []string
	Ads       Ads `json:"ads"`
}

type Ads struct {
	Ad Ad `json:"ad"`
}

type Ad struct {
	Beacons Beacons `json:"beacons"`
	Snast   Snast   `json:"SNAST"`
}
type Beacons struct {
	Beacon []string `json:"beacon"`
}
type Snast struct {
	Adtitle   string `json:"adtitle"`
	Adtext    string `json:"adtext"`
	Mainimage Image  `json:"mainimage"`
	Clickurl  string `json:"clickurl"`
}

type Image struct {
	Image string `json:"image"`
}
