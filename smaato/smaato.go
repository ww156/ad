package smaato

import (
	// "bytes"
	// "encoding/json"
	// "encoding/xml"
	// "fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	// "time"

	"ad/mongo"
	"ad/util"
	"github.com/pquerna/ffjson/ffjson"
)

// ios
const (
	URL          = "http://soma.smaato.net/oapi/reqAd.jsp"
	ADSPACE_FLOW = "130270323" // 信息流：130270323
	ADSPACE      = "130299034"
	PUB          = "1100032654" // 开发者id
)

func Smaato(getReq *util.ReqMsg, funcName string) util.ResMsg {
	return base(getReq, funcName, ADSPACE, "320", "50", "all")
}

func SmaatoFlow(getReq *util.ReqMsg, funcName string) util.ResMsg {
	return base(getReq, funcName, ADSPACE_FLOW, "640", "360", "native")
}

func base(getReq *util.ReqMsg, funcName, adspace string, w, h, adtype string) util.ResMsg {
	if getReq.Os == "1" {
		return util.ResMsg{}
	}
	v := url.Values{}
	v.Set("apiver", "502")
	v.Set("adspace", adspace)
	v.Set("pub", PUB)
	v.Set("devip", getReq.Ip)
	v.Set("device", getReq.Ua)
	// v.Set("nver", "1") // 原生广告
	v.Set("mraidver", "0")
	v.Set("format", adtype)
	v.Set("iosadid", getReq.Idfa)
	v.Set("iosadtracking", "true")
	v.Set("androidid", getReq.Androidid)
	v.Set("ownid", getReq.Imei)
	v.Set("response", "JSON")
	v.Set("width", w)
	v.Set("height", h)
	v.Set("session", util.GetRandom())
	v.Set("devicemodel", getReq.Model)
	v.Set("devicemake", getReq.Vendor)
	v.Set("areacode", "")
	v.Set("age", "35")

	req, err := http.NewRequest("GET", URL+"?"+v.Encode(), nil)
	if err != nil {
		return util.ResMsg{}
	}
	req.Header.Set("Connection", "keep-alive")
	req.Header.Set("x-forwarded-for", getReq.Ip)
	resp, err := util.Client.Do(req)
	mongo.LogReq(funcName, getReq.Os)
	// 请求错误，日志记录为timeout
	if err != nil {
		mongo.LogTimeout(funcName, getReq.Os)
		return util.ResMsg{}
	}
	data, err := ioutil.ReadAll(resp.Body)
	resp.Body.Close()
	if err != nil {
		mongo.LogNo(funcName, getReq.Os)
		return util.ResMsg{}
	}
	// 请求返回失败，日志记录为timeout
	// fmt.Println(resp.StatusCode)
	if resp.StatusCode != 200 {
		mongo.LogTimeout(funcName, getReq.Os)
		return util.ResMsg{}
	}
	return FormateRes(data, getReq.Os, funcName)
}

func FormateRes(res []byte, os, funcName string) util.ResMsg {
	resData := Adres{}
	ffjson.Unmarshal(res, &resData)
	// fmt.Println(string(res))
	if resData.Link == "" {
		mongo.LogNo(funcName, os)
		return util.ResMsg{}
	}

	postData := util.ResMsg{
		Id:       "0",
		Weight:   0,
		State:    1,
		Title:    "",
		Content:  "",
		ImageUrl: resData.Link,
		Uri:      resData.Target,
	}

	postData.Displayreport = resData.Beacons
	postData.Displayreport = append(postData.Displayreport, "http://120.26.247.23/ad/report.php?method="+funcName+"&event=displayreport&os="+os+"&t="+util.GetRandom())
	postData.Clickreport = append(postData.Clickreport, "http://120.26.247.23/ad/report.php?method="+funcName+"&event=clickreport&os="+os+"&t="+util.GetRandom())

	return postData
}
