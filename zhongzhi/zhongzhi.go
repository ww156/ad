package zhongzhi

import (
	"bytes"
	// "encoding/json"
	// "fmt"
	"io/ioutil"
	// "log"
	"net/http"
	// "net/url"
	"math/rand"
	"strconv"
	"strings"
	"time"
	// "unsafe"
	// "regexp"

	"ad/mongo"
	"ad/util"
	"github.com/pquerna/ffjson/ffjson"
)

// "skey": "50002",
// "zaduid": "510001",
// "n": 4,
// "pt": 1,
// "w": 640,
// "h": 150,
// "ot": 1

const (
	URL                       = "http://api.ssp.zonst.com/ad/get"
	SKEY_ANDROID              = "50002"
	ZADUID_ANDROID            = ""
	ZADUID_ANDROID_FLOW       = "510002"
	ZADUID_ANDROID_STARTUP    = ""
	ZADUID_ANDROID_STORY_FLOW = "510003"
	SKEY_IOS                  = "50003"
	ZADUID_IOS                = ""
	ZADUID_IOS_FLOW           = "510004"
	ZADUID_IOS_STARTUP        = ""
	ZADUID_IOS_STORY_FLOW     = "510005"
)

func Zhongzhi(getReq *util.ReqMsg, funcName string) util.ResMsg {
	if getReq.Os == "1" {
		return base(getReq, funcName, 1, 640, 100, SKEY_ANDROID, ZADUID_ANDROID)
	}
	return base(getReq, funcName, 1, 640, 100, SKEY_IOS, ZADUID_IOS)
}
func ZhongzhiFlow(getReq *util.ReqMsg, funcName string) util.ResMsg {
	if getReq.Os == "1" {
		return base(getReq, funcName, 4, 640, 360, SKEY_ANDROID, ZADUID_ANDROID_FLOW)
	}
	return base(getReq, funcName, 4, 640, 360, SKEY_IOS, ZADUID_IOS_FLOW)
}
func ZhongzhiStartup(getReq *util.ReqMsg, funcName string) util.ResMsg {
	if getReq.Os == "1" {
		return base(getReq, funcName, 3, 640, 960, SKEY_ANDROID, ZADUID_ANDROID_STARTUP)
	}
	return base(getReq, funcName, 3, 640, 960, SKEY_IOS, ZADUID_IOS_STARTUP)
}
func ZhongzhiStoryFlow(getReq *util.ReqMsg, funcName string) util.ResMsg {
	if getReq.Os == "1" {
		return base(getReq, funcName, 4, 232, 132, SKEY_ANDROID, ZADUID_ANDROID_STORY_FLOW)
	}
	return base(getReq, funcName, 4, 232, 132, SKEY_IOS, ZADUID_IOS_STORY_FLOW)
}
func base(getReq *util.ReqMsg, funcName string, adtype, w, h int, appid, adid string) util.ResMsg {
	width, err := strconv.Atoi(getReq.Screenwidth)
	if err != nil {
		width = 0
	}
	height, err := strconv.Atoi(getReq.Screenheight)
	if err != nil {
		height = 0
	}
	sd, _ := strconv.Atoi(getReq.Sd)
	dpi := sd / 160
	operator := ""
	imsi := ""
	switch getReq.Imsi {
	case "1":
		operator = "46000"
		imsi = "46000" + strconv.Itoa(10000+rand.Intn(90000)) + strconv.Itoa(10000+rand.Intn(90000))
	case "2":
		operator = "46001"
		imsi = "46001" + strconv.Itoa(10000+rand.Intn(90000)) + strconv.Itoa(10000+rand.Intn(90000))
	case "3":
		operator = "46002"
		imsi = "46002" + strconv.Itoa(10000+rand.Intn(90000)) + strconv.Itoa(10000+rand.Intn(90000))
	default:
		imsi = ""
	}
	ts := time.Now().Unix()
	postData := adreq{
		Zad: &zad{
			Skey:   appid,
			Zaduid: adid,
			N:      1,
			Pt:     adtype,
			W:      w,
			H:      h,
			Ot:     1,
		},
		App: &app{
			Appname: "网上厨房",
			Pkgname: "cn.ecook",
		},
		Device: &device{
			Sn:       getReq.Imei,
			Time:     ts,
			Ip:       getReq.Ip,
			Dt:       0,
			Os:       1,
			Imsi:     imsi,
			Osv:      getReq.Osversion,
			Openudid: getReq.Openudid,
			Andid:    getReq.Androidid,
			Mac:      getReq.Mac,
			Tp:       getReq.Vendor,
			Brd:      getReq.Model,
			Sw:       width,
			Sh:       height,
			Deny:     float64(dpi),
			Nt:       getReq.Network,
			Nop:      operator,
			Ua:       getReq.Ua,
		},
	}
	zad := postData.Zad
	app := postData.App
	device := postData.Device
	if getReq.Os == "2" {
		zad.Zaduid = adid
		zad.Skey = appid
		app.Pkgname = "cn.ecook.ecook"
		device.Sn = getReq.Idfa
		device.Os = 2
		device.Idfv = "0"
	}
	token := util.Md5("SDK" + zad.Zaduid + device.Sn + getReq.Os + operator + app.Pkgname + strconv.Itoa(int(ts)) + zad.Skey)
	postData.Token = token

	ma, err := ffjson.Marshal(&postData)
	if err != nil {
		return util.ResMsg{}
	}
	// fmt.Println(string(ma))
	// 请求广告
	req, err := http.NewRequest("POST", URL, bytes.NewReader(ma))
	req.Header.Set("Connection", "keep-alive")
	req.Header.Set("Content-Type", "application/json;charset=utf-8")
	req.Header.Set("Accept", "text/json;version=0.1")
	id := util.GetRandom()
	bid := []byte(id)
	req.Header.Set("id", string(bid[0:8])+"-"+string(bid[8:12])+"-"+string(bid[12:16])+"-"+string(bid[16:20])+"-"+string(bid[20:]))
	if err != nil {
		return util.ResMsg{}
	}
	resp, err := util.Client.Do(req)
	mongo.LogReq(funcName, getReq.Os)
	if err != nil {
		mongo.LogTimeout(funcName, getReq.Os)
		return util.ResMsg{}
	}
	data, err := ioutil.ReadAll(resp.Body)
	resp.Body.Close()
	if err != nil {
		mongo.LogNo(funcName, getReq.Os)
		return util.ResMsg{}
	}
	// fmt.Println(resp.StatusCode)
	if resp.StatusCode != 200 {
		mongo.LogTimeout(funcName, getReq.Os)
		return util.ResMsg{}
	}
	return FormateRes(data, getReq.Os, funcName)
}

// 根据广告平台的返回生成客户端返回
func FormateRes(res []byte, os, funcName string) util.ResMsg {
	resData := adres{}
	ffjson.Unmarshal(res, &resData)
	// fmt.Printf("%+v", resData)
	// fmt.Println(string(res))
	if resData.Rt != 0 {
		mongo.LogNo(funcName, os)
		return util.ResMsg{}
	}
	if len(resData.Adlst) == 0 {
		mongo.LogNo(funcName, os)
		return util.ResMsg{}
	}
	ad := resData.Adlst[0]

	if ad.Img == "" {
		mongo.LogNo(funcName, os)
		return util.ResMsg{}
	}

	postData := util.ResMsg{
		Id:       "0",
		Weight:   0,
		State:    1,
		Title:    ad.Tt,
		Content:  ad.Stt,
		ImageUrl: ad.Img,
		Uri:      ad.Lurl,
	}
	ct := ad.Ct_t
	postData.Displayreport = ct.Imp_url
	postData.Displayreport = append(postData.Displayreport, "http://120.26.247.23/ad/report.php?method="+funcName+"&event=displayreport&os="+os+"&t="+util.GetRandom())
	for _, v := range ct.Ck_url {
		clk := v
		if clk != "" {
			clk = strings.Replace(clk, "CK_D_X", "IT_CLK_PNT_DOWN_X", -1)
			clk = strings.Replace(clk, "CK_D_Y", "IT_CLK_PNT_DOWN_X", -1)
			clk = strings.Replace(clk, "CK_P_X", "IT_CLK_PNT_DOWN_X", -1)
			clk = strings.Replace(clk, "CK_P_Y", "IT_CLK_PNT_DOWN_X", -1)
			postData.Clickreport = append(postData.Clickreport, clk)
		}
	}
	postData.Clickreport = append(postData.Clickreport, "http://120.26.247.23/ad/report.php?method="+funcName+"&event=clickreport&os="+os+"&t="+util.GetRandom())

	return postData
}
