package zhongzhi

type adreq struct {
	Token  string  `json:"token"`
	Zad    *zad    `json:"zad"`
	App    *app    `json:"app"`
	Device *device `json:"device"`
}
type zad struct {
	Skey   string `json:"skey"`
	Zaduid string `json:"zaduid"`
	N      int    `json:"n"`
	Pt     int    `json:"pt"`
	W      int    `json:"w"`
	H      int    `json:"h"`
	Ot     int    `json:"ot"`
}
type app struct {
	Appname string `json:"appname"`
	Pkgname string `json:"pkgname"`
	Mkt     int    `json:"mkt,omitempty"`
	Mkt_sn  int    `json:"mkt_sn,omitempty"`
	Mkt_cat int    `json:"mkt_cat,omitempty"`
	Mkt_tag int    `json:"mkt_tag,omitempty"`
}

type device struct {
	Sn       string  `json:"sn"`
	Time     int64   `json:"time"`
	Ip       string  `json:"ip"`
	Dt       int     `json:"dt"`
	Os       int     `json:"os"`
	Imsi     string  `json:"imsi"`
	Osv      string  `json:"osv"`
	Idfv     string  `json:"idfv,omitempty"`
	Openudid string  `json:"openudid,omitempty"`
	Andid    string  `json:"andid,omitempty"`
	Mac      string  `json:"mac,omitempty"`
	Adid     string  `json:"adid,omitempty"`
	Duid     string  `json:"duid,omitempty"`
	Tp       string  `json:"tp"`
	Brd      string  `json:"brd"`
	Sw       int     `json:"sw"`
	Sh       int     `json:"sh"`
	Deny     float64 `json:"deny"`
	Nt       string  `json:"nt"`
	Nop      string  `json:"nop,omitempty"`
	Bssid    string  `json:"bssid,omitempty"`
	Ssid     string  `json:"ssid,omitempty"`
	Iccid    string  `json:"iccid,omitempty"`
	Gd       string  `json:"gd,omitempty"`
	Ua       string  `json:"ua"`
	Brk      int     `json:"brk"`
}
