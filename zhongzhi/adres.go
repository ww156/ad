package zhongzhi

type adres struct {
	Rt    int
	Mg    string
	Cnt   int
	Expd  int
	Adlst []ad
}

type ad struct {
	At   int
	Pn   string
	Tt   string
	Stt  string
	Img  string
	Ic   string
	Lurl string
	Ct_t ct_t
}
type ct_t struct {
	Img     []string
	Imp_url []string
	Ck_url  []string
	Ds_url  []string
	De_url  []string
	Iss_url []string
	Ise_url []string
}
