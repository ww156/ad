package dashu

type adres struct {
	Code        int
	AdInfos     []Ad
	CacheAssets []Asset
}

type Ad struct {
	Id                        int
	Title                     string
	Summary                   string
	Brand                     string
	Assets                    []Asset
	AdMark                    string
	AdType                    int
	TargetType                int
	Deeplink                  string
	LandingPageUrl            string
	ActionUrl                 string
	IconUrl                   string
	PackageName               string
	TotalDownloadNum          int
	AdControl                 Adcontrol
	ViewMonitorUrls           []string
	ClickMonitorUrls          []string
	SkipMonitorUrls           []string
	StartDownloadMonitorUrls  []string
	FinishDownloadMonitorUrls []string
	StartInstallMonitorUrls   []string
	FinishInstallMonitorUrls  []string
}

type Asset struct {
	Url          string
	Digest       string
	MaterialType int
}

type Adcontrol struct {
	Duration       int
	StartTimeMills int
	EndTimeMills   int
}
