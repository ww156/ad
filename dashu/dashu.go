package dashu

import (
	// "bytes"
	"encoding/json"
	// "fmt"
	"io/ioutil"
	// "log"
	"net/http"
	"net/url"
	// "strconv"
	// "strings"
	// "time"
	// "unsafe"
	// "regexp"

	"ad/mongo"
	"ad/util"
)

// 7db708d12ef73294 信息流大图
// 3a5c8591dfcb00b6 信息流小图
// 55e15a219d44bbd2 信息流组图
// 9c985da9fd211c9b 横幅
// c5c75f568976d3b8 插屏
// 638cb2bff337652a 开屏
const (
	URL         = "http://api.ssp.bigtree.mobi/request"
	SID         = "9c985da9fd211c9b"
	SID_FLOW    = "7db708d12ef73294"
	SID_STARTUP = "638cb2bff337652a"
)

func Dashu(getReq *util.ReqMsg, funcName string) util.ResMsg {
	return base(getReq, funcName, SID, "640", "100")
}

func DashuFlow(getReq *util.ReqMsg, funcName string) util.ResMsg {
	return base(getReq, funcName, SID_FLOW, "640", "360")
}

func DashuStartup(getReq *util.ReqMsg, funcName string) util.ResMsg {
	return base(getReq, funcName, SID_STARTUP, "640", "960")
}

func base(getReq *util.ReqMsg, funcName, sid, w, h string) util.ResMsg {
	value := url.Values{}
	value.Set("adid", util.GetRandom())
	value.Set("sid", sid)
	value.Set("w", w)
	value.Set("h", h)
	value.Set("appv", getReq.Appversion)
	if getReq.Os == "1" {
		value.Set("androidid", getReq.Androidid)
		value.Set("imei", getReq.Imei)
	}
	if getReq.Os == "2" {
		value.Set("idfa", getReq.Idfa)
	}
	value.Set("osv", getReq.Osversion)
	value.Set("devtype", "phone")
	value.Set("vendor", getReq.Vendor)
	value.Set("model", getReq.Model)
	value.Set("ip", getReq.Ip)
	if getReq.Network != "wifi" {
		getReq.Network = "cell"
	}
	value.Set("connntype", getReq.Network)
	switch getReq.Imsi {
	case "1":
		getReq.Imsi = "46001"
	case "2":
		getReq.Imsi = "46002"
	case "3":
		getReq.Imsi = "46003"
	default:
		getReq.Imsi = "46001"
	}
	value.Set("carrier", getReq.Imsi)
	value.Set("v", "2.2")

	// client := &http.Client{Timeout: util.Timeout}
	req, err := http.NewRequest("GET", URL+"?"+value.Encode(), nil)
	if err != nil {
		return util.ResMsg{}
	}
	req.Header.Set("Connection", "keep-alive")
	req.Header.Set("Content-Type", "application/json;charset=utf-8")
	resp, err := util.Client.Do(req)
	mongo.LogReq(funcName, getReq.Os)
	if err != nil {
		mongo.LogTimeout(funcName, getReq.Os)
		util.HttpRequestCount.WithLabelValues(funcName + "timeouterr").Inc()
		return util.ResMsg{}
	}
	data, err := ioutil.ReadAll(resp.Body)
	resp.Body.Close()
	if err != nil {
		mongo.LogNo(funcName, getReq.Os)
		util.HttpRequestCount.WithLabelValues(funcName + "readresperr").Inc()
		return util.ResMsg{}
	}
	if resp.StatusCode != 200 {
		mongo.LogTimeout(funcName, getReq.Os)
		util.HttpRequestCount.WithLabelValues(funcName + "not200err").Inc()
		return util.ResMsg{}
	}
	return FormateRes(data, getReq.Os, funcName)
}

func FormateRes(res []byte, os, funcName string) util.ResMsg {
	resData := &adres{}
	json.Unmarshal(res, resData)
	// fmt.Printf("%+v", resData)
	if len(resData.AdInfos) == 0 {
		mongo.LogNo(funcName, os)
		util.HttpRequestCount.WithLabelValues(funcName + "noerr").Inc()
		return util.ResMsg{}
	}
	ad := resData.AdInfos[0]
	if len(ad.Assets) == 0 {
		mongo.LogNo(funcName, os)
		util.HttpRequestCount.WithLabelValues(funcName + "noerr").Inc()
		return util.ResMsg{}
	}
	postData := util.ResMsg{
		Id:       "0",
		Weight:   0,
		State:    0,
		Title:    ad.Title,
		Content:  ad.Summary,
		ImageUrl: ad.Assets[0].Url,
		Uri:      ad.LandingPageUrl,
	}
	postData.Displayreport = ad.ViewMonitorUrls
	postData.Displayreport = append(postData.Displayreport, "http://120.26.247.23/ad/report.php?method="+funcName+"&event=displayreport&os="+os+"&t="+util.GetRandom())
	postData.Clickreport = ad.ClickMonitorUrls
	postData.Clickreport = append(postData.Clickreport, "http://120.26.247.23/ad/report.php?method="+funcName+"&event=clickreport&os="+os+"&t="+util.GetRandom())

	return postData
}
