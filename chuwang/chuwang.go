package chuwang

import (
	// "bytes"
	"encoding/json"
	// "fmt"
	"io/ioutil"
	// "log"
	"net/http"
	"net/url"
	"strconv"
	// "strings"
	// "time"
	// "unsafe"
	// "regexp"

	"ad/mongo"
	"ad/util"
)

const (
	DEV_URL      = "http://api-test.cwmobi.com/api.html"
	PEO_URL      = "http://api.cwmobi.com/api.html"
	CWID_ANDROID = "38a8aa5f78a348b29a20137abbbb0945"
	CWID_IOS     = "dafdc5b256554936ae2582e7c63b8ad9"
)

func ChuwangFlow(getReq *util.ReqMsg, funcName string) util.ResMsg {
	return base(getReq, funcName, "3")
}

func base(getReq *util.ReqMsg, funcName, adtype string) util.ResMsg {
	carrier := getReq.Imsi
	if carrier == "unknown" {
		carrier = "5"
	}
	network := "wifi"
	switch getReq.Network {
	case "2g":
		network = "2g"
	case "3g":
		network = "3g"
	case "4g":
		network = "4g"
	case "wifi":
		network = "wifi"
	default:
		network = "其他"
	}
	sd, err := strconv.Atoi(getReq.Sd)
	dpid := sd / 160
	value := url.Values{}
	value.Set("cwid", CWID_ANDROID)
	value.Set("adtype", adtype)
	value.Set("os", "0")
	value.Set("osversion", getReq.Osversion)
	value.Set("brand", getReq.Vendor)
	value.Set("product", getReq.Model)
	value.Set("uuid", getReq.Imei)
	value.Set("androidid", getReq.Androidid)
	value.Set("gateway", carrier)
	value.Set("network", network)
	value.Set("ua", getReq.Ua)
	value.Set("ip", getReq.Ip)
	value.Set("mac", getReq.Mac)
	value.Set("density", strconv.Itoa(dpid))
	value.Set("screenwidth", getReq.Screenwidth)
	value.Set("screenheight", getReq.Screenheight)
	value.Set("packagename", "cn.ecook")
	value.Set("appname", "网上厨房")
	if getReq.Os == "2" {
		value.Set("cwid", CWID_IOS)
		value.Set("os", "1")
		value.Set("uuid", getReq.Idfa)
		value.Set("packagename", "cn.ecook.ecook")
	}
	// client := &http.Client{Timeout: util.Timeout}
	req, err := http.NewRequest("GET", PEO_URL+"?"+value.Encode(), nil)
	if err != nil {
		return util.ResMsg{}
	}
	req.Header.Set("Connection", "keep-alive")
	req.Header.Set("Content-Type", "application/json;charset=utf-8")
	resp, err := util.Client.Do(req)
	mongo.LogReq(funcName, getReq.Os)
	if err != nil {
		mongo.LogTimeout(funcName, getReq.Os)
		util.HttpRequestCount.WithLabelValues(funcName + "timeouterr").Inc()
		return util.ResMsg{}
	}
	data, err := ioutil.ReadAll(resp.Body)
	resp.Body.Close()
	if err != nil {
		mongo.LogNo(funcName, getReq.Os)
		util.HttpRequestCount.WithLabelValues(funcName + "readresperr").Inc()
		return util.ResMsg{}
	}
	if resp.StatusCode != 200 {
		mongo.LogTimeout(funcName, getReq.Os)
		util.HttpRequestCount.WithLabelValues(funcName + "not200err").Inc()
		return util.ResMsg{}
	}
	// fmt.Printf("%+v", string(data))
	return FormateRes(data, getReq.Os, funcName)
}

func FormateRes(res []byte, os, funcName string) util.ResMsg {
	resData := &adres{}
	json.Unmarshal(res, resData)
	// fmt.Printf("%+v", resData)
	if resData.Returncode != "200" {
		mongo.LogNo(funcName, os)
		util.HttpRequestCount.WithLabelValues(funcName + "noerr").Inc()
		return util.ResMsg{}
	}
	if len(resData.Ads) == 0 {
		mongo.LogNo(funcName, os)
		util.HttpRequestCount.WithLabelValues(funcName + "noerr").Inc()
		return util.ResMsg{}
	}
	ad := resData.Ads[0]
	if ad.Imgurl == "" {
		mongo.LogNo(funcName, os)
		util.HttpRequestCount.WithLabelValues(funcName + "noerr").Inc()
		return util.ResMsg{}
	}
	postData := util.ResMsg{
		Id:       "0",
		Weight:   0,
		State:    0,
		Title:    ad.Title,
		Content:  ad.Desc,
		ImageUrl: ad.Imgurl,
		Uri:      ad.Clickurl,
	}
	postData.Displayreport = ad.Showtracking
	postData.Displayreport = append(postData.Displayreport, "http://120.26.247.23/ad/report.php?method="+funcName+"&event=displayreport&os="+os+"&t="+util.GetRandom())
	postData.Clickreport = ad.Clicktracking
	postData.Clickreport = append(postData.Clickreport, "http://120.26.247.23/ad/report.php?method="+funcName+"&event=clickreport&os="+os+"&t="+util.GetRandom())

	return postData
}
