package chuwang

type adres struct {
	Returncode string
	Num        string
	Ads        []Ad
}

type Ad struct {
	Imgurl        string
	Clickurl      string
	Title         string
	Desc          string
	Showtracking  []string
	Clicktracking []string
	Bodytype      int
	Html          string
}
