package beiai

type adres struct {
	Id      string
	Seatbid []Seatbid
}

type Seatbid struct {
	Bid []Ad
}

type Ad struct {
	Id    string
	Impid string
	Price int
	Nurl  string
	Iurl  string
	Adm   string
	H     int
	W     int
}

type ResNative struct {
	Ver         string
	Assets      []Resasset
	Link        Link
	Imptrackers []string
}

type Resasset struct {
	Id       int
	Required int
	Title    Restitle
	Img      Resimg
	Data     Resdata
}

type Restitle struct {
	Text string
}
type Resimg struct {
	Url string
	W   int
	H   int
}

type Resdata struct {
	Value string
}

type Link struct {
	Url           string
	Clicktrackers []string
	Fallbaxk      string
}
