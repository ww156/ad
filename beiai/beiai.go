package beiai

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	// "net/url"
	"strconv"
	"strings"
	// "time"

	"ad/mongo"
	"ad/util"
)

const (
	URL           = "http://101.37.151.26:3003/adyun"
	TAGID         = "500102"
	TAGID_FLOW    = "500103"
	TAGID_STARTUP = "500101"
)

func Beiai(getReq *util.ReqMsg, funcName string) util.ResMsg {
	return base(getReq, funcName, TAGID, 640, 360)
}

func BeiaiFlow(getReq *util.ReqMsg, funcName string) util.ResMsg {
	return base(getReq, funcName, TAGID_FLOW, 640, 360)
}

func BeiaiStartup(getReq *util.ReqMsg, funcName string) util.ResMsg {
	return base(getReq, funcName, TAGID_STARTUP, 640, 360)
}

func base(getReq *util.ReqMsg, funcName, tagid string, w, h int) util.ResMsg {
	request := Request{
		Ver: "1.1",
		Assets: []*Asset{
			{
				Id:       "1",
				Required: 1,
				Title: &Title{
					Len: 200,
				},
			},
			{
				Id:       "2",
				Required: 1,
				Img: &Img{
					Type: 3,
					W:    w,
					H:    h,
				},
			},
			{
				Id:       "3",
				Required: 1,
				Data: &Data{
					Type: 2,
					Len:  100,
				},
			},
		},
	}
	r, _ := json.Marshal(request)
	width, err := strconv.Atoi(getReq.Screenwidth)
	if err != nil {
		width = 0
	}
	height, err := strconv.Atoi(getReq.Screenheight)
	if err != nil {
		height = 0
	}
	carrier := "CMCC"
	switch getReq.Imsi {
	case "1":
		carrier = "CMCC"
	case "2":
		carrier = "CUCC"
	case "3":
		carrier = "CTCC"
	default:
		carrier = "CMCC"
	}
	network := 0
	switch getReq.Network {
	case "wifi":
		network = 2
	case "2g":
		network = 4
	case "3g":
		network = 5
	case "4g":
		network = 6
	default:
		network = 0
	}
	sd, err := strconv.Atoi(getReq.Sd)
	postData := adreq{
		Id: util.GetRandom(),
		Imp: []*Imp{
			{
				Id: util.GetRandom(),
				Native: &Native{
					Ver:     "1.1",
					Request: string(r),
				},
				Tagid: tagid,
			},
		},
		App: &App{
			Name:   "网上厨房",
			Bundle: "cn.ecook",
			Ver:    getReq.Appversion,
		},
		Device: &Device{
			Ua:             getReq.Ua,
			Ip:             getReq.Ip,
			Devicetype:     4,
			Make:           getReq.Vendor,
			Model:          getReq.Model,
			Os:             "android",
			Osv:            getReq.Osversion,
			H:              height,
			W:              width,
			Ppi:            sd,
			Laguage:        "zh-CN",
			Carrier:        carrier,
			Connectiontype: network,
			Ifa:            getReq.Imei,
			Macidmd5:       util.Md5(getReq.Mac),
		},
	}
	if getReq.Os == "2" {
		postData.App.Bundle = "cn.ecook.ecook"
		postData.Device.Os = "ios"
		postData.Device.Ifa = getReq.Idfa
	}
	ma, _ := json.Marshal(&postData)
	header := http.Header{}
	header.Set("Connection", "keep-alive")
	header.Set("Content-Type", "application/json")
	resp, err := util.HttpPOST(URL, &header, ma)
	mongo.LogReq(funcName, getReq.Os)
	if err != nil {
		fmt.Println("[beiai.go-137]", err.Error())
		mongo.LogTimeout(funcName, getReq.Os)
		util.HttpRequestCount.WithLabelValues(funcName + "timeouterr").Inc()
		return util.ResMsg{}
	}
	data, err := ioutil.ReadAll(resp.Body)
	resp.Body.Close()
	if resp.StatusCode != 200 {
		fmt.Println(resp.StatusCode)
		mongo.LogTimeout(funcName, getReq.Os)
		util.HttpRequestCount.WithLabelValues(funcName + "not200err").Inc()
		return util.ResMsg{}
	}
	if err != nil {
		mongo.LogNo(funcName, getReq.Os)
		util.HttpRequestCount.WithLabelValues(funcName + "readresperr").Inc()
		return util.ResMsg{}
	}

	return FormateRes(data, getReq.Os, funcName)
}

func FormateRes(res []byte, os, funcName string) util.ResMsg {
	resData := &adres{}
	json.Unmarshal(res, resData)
	// fmt.Println(string(res))
	// fmt.Printf("%+v", resData)
	if len(resData.Seatbid) == 0 {
		mongo.LogNo(funcName, os)
		util.HttpRequestCount.WithLabelValues(funcName + "noerr").Inc()
		return util.ResMsg{}
	}
	if len(resData.Seatbid[0].Bid) == 0 {
		mongo.LogNo(funcName, os)
		util.HttpRequestCount.WithLabelValues(funcName + "noerr").Inc()
		return util.ResMsg{}
	}
	bid := resData.Seatbid[0].Bid[0]

	postData := util.ResMsg{
		Id:       "0",
		Weight:   0,
		State:    0,
		Title:    "",
		Content:  "",
		ImageUrl: "",
		Uri:      "",
	}
	adm := bid.Adm
	admInfo := ResNative{}
	// fmt.Println(adm)
	json.Unmarshal([]byte(adm), &admInfo)
	postData.Uri = admInfo.Link.Url
	// fmt.Printf("%+v", admInfo)
	for _, value := range admInfo.Assets {
		if value.Title.Text != "" {
			postData.Title = value.Title.Text
		}
		if value.Data.Value != "" {
			postData.Content = value.Data.Value
		}
		if value.Img.Url != "" {
			postData.ImageUrl = value.Img.Url
		}
	}
	if postData.ImageUrl == "" {
		mongo.LogNo(funcName, os)
		util.HttpRequestCount.WithLabelValues(funcName + "noerr").Inc()
		return util.ResMsg{}
	}
	if len(admInfo.Imptrackers) != 0 {
		postData.Displayreport = admInfo.Imptrackers
	}
	bid.Nurl = strings.Replace(bid.Nurl, "${AUCTION_PRICE}", strconv.Itoa(bid.Price), -1)
	postData.Displayreport = append(postData.Displayreport, bid.Nurl)
	postData.Displayreport = append(postData.Displayreport, bid.Iurl)
	postData.Displayreport = append(postData.Displayreport, "http://120.26.247.23/ad/report.php?method="+funcName+"&event=displayreport&os="+os+"&t="+util.GetRandom())
	if len(admInfo.Link.Clicktrackers) != 0 {
		postData.Clickreport = admInfo.Link.Clicktrackers
	}
	postData.Clickreport = admInfo.Link.Clicktrackers
	postData.Clickreport = append(postData.Clickreport, "http://120.26.247.23/ad/report.php?method="+funcName+"&event=clickreport&os="+os+"&t="+util.GetRandom())

	return postData
}
