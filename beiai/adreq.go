package beiai

type adreq struct {
	Id     string  `json:"id"`
	Imp    []*Imp  `json:"imp"`
	Site   *Site   `json:"site"`
	App    *App    `json:"app"`
	Device *Device `json:"device"`
}

type Imp struct {
	Id     string  `json:"id"`
	Banner *Banner `json:"banner,omitempty"`
	Native *Native `json:"native,omitempty"`
	Tagid  string  `json:"tagid"`
}

type Banner struct {
	W     int      `json:"w"`
	H     int      `json:"h"`
	Pos   int      `json:"pos"`
	Mimes []string `json:"mimes"`
}

type Native struct {
	Request string `json:"request"`
	Ver     string `json:"ver"`
}

type Site struct {
	Name string `json:"name"`
	Page string `json:"page"`
	Ref  string `json:"ref"`
}
type App struct {
	Name   string `json:"name"`
	Bundle string `json:"bundle"`
	Ver    string `json:"ver"`
}
type Device struct {
	Ua             string `json:"ua"`
	Geo            *Geo   `json:"geo,omitempty"`
	Ip             string `json:"ip"`
	Ipv6           string `json:"ipv6"`
	Devicetype     int    `json:"devicetype"`
	Make           string `json:"make"`
	Model          string `json:"model"`
	Os             string `json:"os"`
	Osv            string `json:"osv"`
	Hwv            string `json:"hwv"`
	H              int    `json:"h"`
	W              int    `json:"w"`
	Ppi            int    `json:"ppi"`
	Laguage        string `json:"language"`
	Carrier        string `json:"carrier"`
	Connectiontype int    `json:"connectiontype"`
	Ifa            string `json:"ifa"`
	Didmd5         string `json:"didmd5"`
	Dpidmd5        string `json:"dpidmd5"`
	Macidmd5       string `json:"macidmd5"`
}

type Geo struct {
	Lat     float64 `json:"lat"`
	Lon     float64 `json:"lon"`
	Lastfix int     `json:"lastfix"`
}

type Request struct {
	Ver    string   `json:"ver"`
	Assets []*Asset `json:"asset"`
}

type Asset struct {
	Id       string `json:"id"`
	Required int    `json:"required"`
	Title    *Title `json:"title"`
	Img      *Img   `json:"img"`
	Data     *Data  `json:"data"`
}

type Title struct {
	Len int `json:"len"`
}
type Img struct {
	Type int `json:"type"`
	W    int `json:"w"`
	H    int `json:"h"`
}
type Data struct {
	Type int `json:"type"`
	Len  int `json:"len"`
}
