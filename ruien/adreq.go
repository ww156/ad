package ruien

type adreq struct {
	Id     string  `json:"id"`
	Imp    []*imp  `json:"imp,omitempty"`
	App    *app    `json:"app,omitempty"`
	Device *device `json:"device,omitempty"`
	User   *user   `json:"user,omitempty"`
	Ext    *ext    `json:"ext,omitempty"`
}

type imp struct {
	Id                  string  `json:"id"`
	Tagid               string  `json:"tagid"`
	Banner              *banner `json:"banner,omitempty"`
	Native              *native `json:"native,omitempty"`
	Video               *video  `json:"video,omitempty"`
	Instl               int     `json:"instl"`
	Bidfloor            float64 `json:"bidfloor"`
	Cur                 string  `json:"cur"`
	Pmp                 *pmp    `json:"pmp,omitempty"`
	Is_support_deeplink int     `json:"is_support_deeplink"`
	Ext                 *ext    `json:"ext,omitempty"`
}

type banner struct {
	Type int  `json:"type"`
	W    int  `json:"w"`
	H    int  `json:"h"`
	Ext  *ext `json:"ext,omitempty"`
}

type video struct {
	Protocol    int  `json:"protocol"`
	W           int  `json:"w"`
	H           int  `josn:"h"`
	Minduration int  `json:"minduration"`
	Maxduration int  `json:"Maxduration"`
	Linearity   int  `json:"linearity"`
	Ext         *ext `josn:"ext,omitempty"`
}

type native struct {
	Title *title `json:"title"`
	Desc  *title `json:"desc"`
	Icon  *icon  `json:"icon"`
	Img   *icon  `json:"img"`
	Ext   *ext   `json:"ext,omitempty"`
}

type title struct {
	Len int  `json:"len"`
	Ext *ext `josn:"ext,omitempty"`
}

type icon struct {
	W   int  `json:"w"`
	H   int  `json:"h"`
	Ext *ext `json:"ext,omitempty"`
}
type pmp struct {
	Deals []*deal `json:"deals"`
	Ext   *ext    `json:"ext,omitempty"`
}

type deal struct {
	Id       string `json:"id"`
	Bidfloor int    `json:"bidfloor"`
	Ext      *ext   `json:"ext,omitempty"`
}

type app struct {
	Id      string   `json:"id"`
	Name    string   `json:"name"`
	Bundle  string   `json:"bundle"`
	Cat     []string `json:"cat"`
	Context []string `json:"context"`
	Ext     *ext     `json:"ext,omitempty"`
}

type device struct {
	W              int    `json:"w"`
	H              int    `json:"h"`
	Ua             string `json:"ua"`
	Ip             string `json:"ip"`
	Geo            *geo   `json:"geo,omitempty"`
	Did            string `json:"did"`
	Didsha1        string `json:"didsha1"`
	Didmd5         string `json:"didmd5"`
	Dpid           string `json:"dpid"`
	Dpidsha1       string `json:"dpidsha1"`
	Dpidmd5        string `json:"dpidmd5"`
	Mac            string `json:"mac"`
	Macsha1        string `json:"macsha1"`
	Macmd5         string `json:"macmd5"`
	Ifa            string `json:"ifa"`
	Make           string `json:"make"`
	Model          string `json:"model"`
	Os             string `json:"os"`
	Osv            string `json:"osv"`
	Carrier        string `json:"carrier"`
	Language       string `json:"language"`
	Js             int    `json:"js"`
	Connectiontype int    `json:"connectiontype"`
	Devicetype     int    `json:"devicetype"`
	Ext            *ext   `json:"ext,omitempty"`
}

type geo struct {
	Lat int  `json:"lat"`
	Lon int  `json:"lon"`
	Ext *ext `json:"ext,omitempty"`
}

type ext struct {
}

type user struct {
	Tags []string `json:"tags"`
	Ext  ext      `json:"ext"`
}
