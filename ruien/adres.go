package ruien

type adres struct {
	Id      string
	Bidid   string
	Seatbid []*seatbid
	Cur     string
	Ext     ext
}

type seatbid struct {
	Bid []*bid
	Ext ext
}

type bid struct {
	Impid     string
	Price     string
	Nurl      string
	Banner_ad banner_ad
	Native_ad native_ad
	Video_ad  video_ad
	Lattr     int
	Dealid    string
	Ext       ext
}

type banner_ad struct {
	Mtype          int
	Title          string
	Desc           string
	Image_url      string
	Html           string
	Landing        string
	Deep_link      string
	W              int
	H              int
	Impress        []string
	Click          []string
	Package_name   string
	App_name       string
	Is_marked      int
	Ad_source_mark string
	Ext            ext
}

type video_ad struct {
	Src              string
	Duration         int
	Landing          string
	Deep_link        string
	Starttrackers    []string
	Completetrackers []string
	Clicktrackers    []string
	Package_name     string
	App_name         string
	Is_marked        int
	Ad_source_mark   string
	Ext              ext
}

type native_ad struct {
	Title          string
	Desc           string
	Icon           string
	Img            string
	Landing        string
	Deep_link      string
	Imptrackers    []string
	Clicktrackers  []string
	Package_name   string
	App_name       string
	Img_urls       []string
	Is_marked      int
	Ad_source_mark string
	Ext            ext
}
