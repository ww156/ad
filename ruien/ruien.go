package ruien

import (
	"bytes"
	"encoding/json"
	// "fmt"
	"io/ioutil"
	// "log"
	"net/http"
	// "net/url"
	"strconv"
	"strings"
	// "time"
	// "unsafe"
	// "regexp"

	"ad/mongo"
	"ad/util"
)

const (
	URL   = "http://video.rnhx.com.cn/ecook"
	TAGID = "1"
)

// banner1，信息流8，开屏2

func Ruien(getReq *util.ReqMsg, funcName string) util.ResMsg {
	return base(getReq, funcName, TAGID, 1, 300)
}

func RuienFlow(getReq *util.ReqMsg, funcName string) util.ResMsg {
	return base(getReq, funcName, TAGID, 8, 300)
}

func RuienStartup(getReq *util.ReqMsg, funcName string) util.ResMsg {
	return base(getReq, funcName, TAGID, 2, 300)
}
func base(getReq *util.ReqMsg, funcName, tagid string, adtype int, price float64) util.ResMsg {
	operator := "0"
	switch getReq.Imsi {
	case "1":
		operator = "46000"
	case "2":
		operator = "46001"
	case "3":
		operator = "46003"
	default:
		operator = "0"
	}
	network := 0
	switch getReq.Network {
	case "wifi":
		network = 2
	case "以太网":
		network = 1
	case "2g":
		network = 3
	case "3g":
		network = 4
	case "4g":
		network = 5
	default:
		network = 0
	}
	width, err := strconv.Atoi(getReq.Screenwidth)
	if err != nil {
		width = 0
	}
	height, err := strconv.Atoi(getReq.Screenheight)
	if err != nil {
		height = 0
	}
	lat, err := strconv.Atoi(getReq.Lat)
	if err != nil {
		lat = 0
	}
	lon, err := strconv.Atoi(getReq.Lng)
	if err != nil {
		lon = 0
	}
	postData := adreq{
		Id: util.GetRandom(),
		Imp: []*imp{
			{
				Id:                  util.GetRandom(),
				Tagid:               tagid,
				Instl:               adtype,
				Bidfloor:            price,
				Is_support_deeplink: 1,
			},
		},
		App: &app{
			Id:     util.GetRandom(),
			Name:   "网上厨房",
			Bundle: "cn.ecook",
		},
		Device: &device{
			W:  width,
			H:  height,
			Ua: getReq.Ua,
			Ip: getReq.Ip,
			Geo: &geo{
				Lat: lat,
				Lon: lon,
			},
			Did:            getReq.Imei,
			Didmd5:         util.Md5(getReq.Imei),
			Dpid:           getReq.Androidid,
			Dpidmd5:        util.Md5(getReq.Androidid),
			Mac:            getReq.Mac,
			Ifa:            getReq.Idfa,
			Make:           getReq.Vendor,
			Model:          getReq.Model,
			Os:             "Android",
			Osv:            getReq.Osversion,
			Carrier:        operator,
			Language:       "zh-CN",
			Connectiontype: network,
			Devicetype:     0,
		},
	}
	device := postData.Device
	if getReq.Os == "2" {
		postData.App.Bundle = "cn.ecook.ecook"
		device.Dpid = getReq.Openudid
		device.Dpidmd5 = util.Md5(getReq.Openudid)
	}
	imp := postData.Imp[0]
	if adtype == 8 {
		imp.Native = &native{
			Title: &title{Len: 12},
			Desc:  &title{Len: 12},
			Img:   &icon{W: 640, H: 360},
		}
	} else {
		imp.Banner = &banner{
			Type: 1,
			W:    640,
			H:    100,
		}
		if adtype == 2 {
			imp.Banner = &banner{
				Type: 1,
				W:    640,
				H:    960,
			}
		}
	}

	ma, _ := json.Marshal(&postData)

	req, _ := http.NewRequest("POST", URL, bytes.NewReader(ma))
	req.Header.Set("Connection", "keep-alive")
	resp, err := util.Client.Do(req)
	mongo.LogReq(funcName, getReq.Os)
	if err != nil {
		mongo.LogTimeout(funcName, getReq.Os)
		util.HttpRequestCount.WithLabelValues(funcName + "timeouterr").Inc()
		return util.ResMsg{}
	}
	data, err := ioutil.ReadAll(resp.Body)
	resp.Body.Close()
	if err != nil {
		mongo.LogNo(funcName, getReq.Os)
		util.HttpRequestCount.WithLabelValues(funcName + "readresperr").Inc()
		return util.ResMsg{}
	}
	if resp.StatusCode != 200 {
		mongo.LogTimeout(funcName, getReq.Os)
		util.HttpRequestCount.WithLabelValues(funcName + "not200err").Inc()
		return util.ResMsg{}
	}
	return FormateRes(data, getReq.Os, funcName, adtype)
}

func FormateRes(res []byte, os, funcName string, adtype int) util.ResMsg {
	resData := &adres{}
	json.Unmarshal(res, resData)
	// fmt.Println(string(res))
	seatbid := resData.Seatbid
	if len(seatbid) == 0 {
		mongo.LogNo(funcName, os)
		util.HttpRequestCount.WithLabelValues(funcName + "noerr").Inc()
		return util.ResMsg{}
	}
	bids := seatbid[0]
	if len(bids.Bid) == 0 {
		mongo.LogNo(funcName, os)
		util.HttpRequestCount.WithLabelValues(funcName + "noerr").Inc()
		return util.ResMsg{}
	}
	bid := bids.Bid[0]
	if adtype == 8 {
		ad := bid.Native_ad
		postData := util.ResMsg{
			Id:       "0",
			Weight:   0,
			State:    0,
			Title:    ad.Title,
			Content:  ad.Desc,
			ImageUrl: ad.Img,
			Uri:      ad.Landing,
		}
		nurl := bid.Nurl
		nurl = strings.Replace(nurl, "${AUCTION_PRICE}", bid.Price, -1)
		postData.Displayreport = ad.Imptrackers
		postData.Displayreport = append(postData.Displayreport, nurl)
		postData.Displayreport = append(postData.Displayreport, "http://120.26.247.23/ad/report.php?method="+funcName+"&event=displayreport&os="+os+"&t="+util.GetRandom())
		postData.Clickreport = ad.Clicktrackers
		postData.Clickreport = append(postData.Clickreport, "http://120.26.247.23/ad/report.php?method="+funcName+"&event=clickreport&os="+os+"&t="+util.GetRandom())

		return postData
	} else {
		ad := bid.Banner_ad
		postData := util.ResMsg{
			Id:       "0",
			Weight:   0,
			State:    0,
			Title:    ad.Title,
			Content:  ad.Desc,
			ImageUrl: ad.Image_url,
			Uri:      ad.Landing,
		}
		nurl := bid.Nurl
		nurl = strings.Replace(nurl, "${AUCTION_PRICE}", bid.Price, -1)
		postData.Displayreport = ad.Impress
		postData.Displayreport = append(postData.Displayreport, nurl)
		postData.Displayreport = append(postData.Displayreport, "http://120.26.247.23/ad/report.php?method="+funcName+"&event=displayreport&os="+os+"&t="+util.GetRandom())
		postData.Clickreport = ad.Click
		postData.Clickreport = append(postData.Clickreport, "http://120.26.247.23/ad/report.php?method="+funcName+"&event=clickreport&os="+os+"&t="+util.GetRandom())

		return postData
	}
}
