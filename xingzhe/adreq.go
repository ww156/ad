package xingzhe

type adreq struct {
	Version       string `json:"version"`
	C_type        int    `json:"c_type"`
	Mid           string `json:"mid"`
	Uid           string `json:"uid"`
	Os            string `json:"os"`
	Mac           string `json:"mac"`
	Osv           string `json:"osv"`
	Networktype   int    `json:"networktype"`
	Remoteip      string `json:"remoteip"`
	Make          string `json:"make"`
	Brand         string `json:"brand"`
	Model         string `json:"model"`
	Devicetype    string `json:"devicetype"`
	Imei          string `json:"imei"`
	Imsi          string `json:"imsi"`
	Androidid     string `json:"androidid"`
	Idfa          string `json:"idfa"`
	Width         int    `json:"width"`
	Height        int    `json:"height"`
	Orientation   int    `json:"orientation"`
	Appid         string `json:"appid"`
	Appname       string `json:"appname"`
	Position      int    `json:"position"`
	Operator      int    `json:"operator"`
	Geo_type      int    `json:"geo_type"`
	Geo_latitude  int    `json:"geo_latitude"`
	Geo_longitude int    `json:"geo_longitude"`
	Geo_time      int    `json:"Geo_time"`
	Wifi_aps      string `json:"wifi_aps"`
}
