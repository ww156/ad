package xingzhe

import (
	"bytes"
	// "encoding/json"
	"fmt"
	"io/ioutil"
	// "log"
	"net/http"
	"net/url"
	"strconv"
	// "strings"
	"math/rand"
	"time"
	// "unsafe"
	// "regexp"

	"ad/mongo"
	"ad/util"
	"github.com/pquerna/ffjson/ffjson"
)

// 京东
// 安卓
// 信息流：20170523
// banner：201705240001

// 广点通
// 安卓
// banner：201705250001
// 原生：testgjdsy0010
const (
	URL               = "http://zxf-api.mobsmart.cn/yd3/mApiView"
	ADID_ANDROID      = "201705240001"
	ADID_ANDROID_FLOW = "20170523"
)

func Xingzhe(getReq *util.ReqMsg, funcName string) util.ResMsg {
	return base(getReq, funcName, ADID_ANDROID)
}

func XingzheFlow(getReq *util.ReqMsg, funcName string) util.ResMsg {
	return base(getReq, funcName, ADID_ANDROID_FLOW)
}

func XingzheStartup(getReq *util.ReqMsg, funcName string) util.ResMsg {
	return base(getReq, funcName, "")
}

func base(getReq *util.ReqMsg, funcName, adid string) util.ResMsg {
	if getReq.Os == "2" {
		return util.ResMsg{}
	}
	// 请求参数
	imsi := "0"
	operator := 0
	switch getReq.Imsi {
	case "1":
		operator = 1
		imsi = "46000" + strconv.Itoa(10000+rand.Intn(90000)) + strconv.Itoa(10000+rand.Intn(90000))
	case "2":
		operator = 2
		imsi = "46001" + strconv.Itoa(10000+rand.Intn(90000)) + strconv.Itoa(10000+rand.Intn(90000))
	case "3":
		operator = 3
		imsi = "46002" + strconv.Itoa(10000+rand.Intn(90000)) + strconv.Itoa(10000+rand.Intn(90000))
	default:
		operator = 0
	}
	width, err := strconv.Atoi(getReq.Screenwidth)
	if err != nil {
		width = 0
	}
	height, err := strconv.Atoi(getReq.Screenheight)
	if err != nil {
		height = 0
	}
	network := 0
	switch getReq.Network {
	case "wifi":
		network = 1
	case "以太网":
		network = 2
	case "2g":
		network = 3
	case "3g":
		network = 4
	case "4g":
		network = 5
	default:
		network = 0
	}
	lat, err := strconv.Atoi(getReq.Lat)
	if err != nil {
		lat = 0
	}
	lon, err := strconv.Atoi(getReq.Lng)
	if err != nil {
		lon = 0
	}
	postData := adreq{
		Version:       "1.0",
		C_type:        1,
		Mid:           adid,
		Uid:           util.GetRandom(),
		Os:            "ANDROID",
		Mac:           getReq.Mac,
		Osv:           getReq.Osversion,
		Networktype:   network,
		Remoteip:      getReq.Ip,
		Make:          getReq.Vendor,
		Brand:         getReq.Vendor,
		Model:         getReq.Model,
		Devicetype:    "1",
		Imei:          getReq.Imei,
		Imsi:          imsi,
		Androidid:     getReq.Androidid,
		Idfa:          getReq.Idfa,
		Width:         width,
		Height:        height,
		Orientation:   1,
		Appid:         "cn.ecook",
		Operator:      operator,
		Geo_type:      2,
		Geo_latitude:  lat,
		Geo_longitude: lon,
		Geo_time:      time.Now().Second(),
	}
	if getReq.Os == "2" {
		postData.Os = "IOS"
		postData.Appid = "cn.ecook.ecook"
	}

	ma, err := ffjson.Marshal(postData)
	if err != nil {
		return util.ResMsg{}
	}
	req, err := http.NewRequest("POST", URL, bytes.NewReader(ma))
	if err != nil {
		return util.ResMsg{}
	}
	req.Header.Set("Content-Type", "application/json;charset=utf-8")
	req.Header.Set("Connection", "keep-alive")
	resp, err := util.Client.Do(req)
	mongo.LogReq(funcName, getReq.Os)
	if err != nil {
		mongo.LogTimeout(funcName, getReq.Os)
		return util.ResMsg{}
	}
	data, err := ioutil.ReadAll(resp.Body)
	resp.Body.Close()
	if err != nil {
		mongo.LogNo(funcName, getReq.Os)
		return util.ResMsg{}
	}
	if resp.StatusCode != 200 {
		mongo.LogTimeout(funcName, getReq.Os)
		return util.ResMsg{}
	}
	return FormateRes(data, getReq.Os, funcName)
}

func FormateRes(res []byte, os, funcName string) util.ResMsg {
	resData := &adres{}
	ffjson.Unmarshal(res, resData)
	fmt.Println(string(res))
	if resData.Ret != "0" {
		mongo.LogNo(funcName, os)
		return util.ResMsg{}
	}
	ad := resData.Adinfo
	if ad.Ac_type == 4 {
		// 无法处理上报响应内容，返回空
		return util.ResMsg{}
	}
	postData := util.ResMsg{
		Id:       "0",
		Weight:   0,
		State:    1,
		Title:    ad.Title,
		Content:  ad.Description,
		ImageUrl: ad.Img_url,
		Uri:      ad.Click_url,
	}
	fmt.Println(ad.Creative_type)
	if ad.Creative_type == 4 {
		fmt.Println(ad.HtmlStr)
		postData.Uri = ad.HtmlStr
	}

	v := url.Values{}
	v.Set("s", `{"down_x":"IT_CLK_PNT_DOWN_X","down_y":"IT_CLK_PNT_DOWN_Y","up_x":"IT_CLK_PNT_UP_X","up_y":"IT_CLK_PNT_UP_Y"}`)
	if ad.Click_position == 1 && ad.Ac_type != 4 {
		postData.Uri = postData.Uri + v.Encode()
	}
	if ad.Click_position == 1 && ad.Ac_type == 4 {
		ad.Click_notice_urld[0] = ad.Click_notice_urld[0] + v.Encode()
	}
	postData.Displayreport = ad.Impress_notice_urls
	postData.Displayreport = append(postData.Displayreport, "http://120.26.247.23/ad/report.php?method="+funcName+"&event=displayreport&os="+os+"&t="+util.GetRandom())
	postData.Clickreport = ad.Click_notice_urld
	postData.Clickreport = append(postData.Clickreport, "http://120.26.247.23/ad/report.php?method="+funcName+"&event=clickreport&os="+os+"&t="+util.GetRandom())

	return postData
}
