package xingzhe

type adres struct {
	Version string
	Ret     string
	Adinfo  ad
}
type ad struct {
	Ad_type                    int
	Width                      int
	Height                     int
	Creative_type              int
	HtmlStr                    string
	Img_url                    string
	Click_url                  string
	Ac_type                    int
	Click_position             int
	App_package                string
	App_size                   string
	Title                      string
	Description                string
	Impress_notice_urls        []string
	Click_notice_urld          []string
	Before_impress_notice_urls []string
	Install_notice_urls        []string
	Img_sm                     string
	Img_url2                   string
	Img_url3                   string
	Img_url4                   string
	Download_start_notice_urls []string
	Download_notice_urls       []string
}
