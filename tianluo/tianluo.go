package tianluo

import (
	// "bytes"
	"encoding/json"
	// "fmt"
	"io/ioutil"
	// "log"
	// "net/http"
	"net/url"
	"strconv"
	// "strings"
	// "crypto/tls"
	"time"
	// "unsafe"
	// "regexp"

	"ad/mongo"
	"ad/util"
)

// TL_网上厨房_iOS

// AppId： D796C54181EA451DAF6146205EED4BC7
// APIKey： E0C2BA6DE32EEEF7B787DE0B7197B6C6

// 网上厨房_iOS_banner_TL
// Lid： D11CD9B185BD44F69E2AEEA473F4EA8A

// 网上厨房_iOS_信息流_TL
// lid： 2E2227E39D7947178490E0EB5551C453

// 网上厨房_iOS_开屏_TL
// Lid： 9C72BD9D96FD4EBD93C22C1B012F086A

// TL_网上厨房_安卓

// AppId F96A3463853442CBAC4E26EB42A632A7
// APIKey 795ABE021B0391CE23B2B7C694FF8BEE

// 网上厨房_安卓_信息流_TL
// Lid：F7E4C658970C4362ACCDDECD3CB03558

// 网上厨房_安卓_开屏_TL
// Lid：37EE38DC52B04CDE94731C76B3324EC9

// 网上厨房_安卓_banner_TL
// Lid：FA4D3B16DE114D7FAB14ADAB4E123047
const (
	URL                 = "https://api.tianrow.com/v10/getad"
	KEY                 = "5AB18A812D9C05112B1676C5212350FC"
	APPID_IOS           = "D796C54181EA451DAF6146205EED4BC7"
	APIKEY_IOS          = "E0C2BA6DE32EEEF7B787DE0B7197B6C6"
	LID_IOS_BANNER      = "D11CD9B185BD44F69E2AEEA473F4EA8A"
	LID_IOS_FLOW        = "2E2227E39D7947178490E0EB5551C453"
	LID_IOS_STARTUP     = "9C72BD9D96FD4EBD93C22C1B012F086A"
	APPID_ANDROID       = "F96A3463853442CBAC4E26EB42A632A7"
	APIKEY_ANDROID      = "795ABE021B0391CE23B2B7C694FF8BEE"
	LID_ANDROID_BANNER  = "FA4D3B16DE114D7FAB14ADAB4E123047"
	LID_ANDROID_FLOW    = "F7E4C658970C4362ACCDDECD3CB03558"
	LID_ANDROID_STARTUP = "37EE38DC52B04CDE94731C76B3324EC9"
)

func Tianluo(getReq *util.ReqMsg, funcName string) util.ResMsg {
	if getReq.Os == "1" {
		return base(getReq, funcName, LID_ANDROID_BANNER, "640", "100")
	}
	return base(getReq, funcName, LID_IOS_BANNER, "640", "100")
}

func TianluoFlow(getReq *util.ReqMsg, funcName string) util.ResMsg {
	if getReq.Os == "1" {
		return base(getReq, funcName, LID_ANDROID_FLOW, "640", "360")
	}
	return base(getReq, funcName, LID_IOS_FLOW, "640", "360")
}

func TianluoStartup(getReq *util.ReqMsg, funcName string) util.ResMsg {
	if getReq.Os == "1" {
		return base(getReq, funcName, LID_ANDROID_STARTUP, "640", "960")
	}
	return base(getReq, funcName, LID_IOS_STARTUP, "640", "960")
}

func base(getReq *util.ReqMsg, funcName, lid string, w, h string) util.ResMsg {
	time := strconv.Itoa(int(time.Now().UnixNano() / 1e6))
	postData := adreq{
		Ver:            "1.7.4",
		Appid:          APPID_ANDROID,
		Lid:            lid,
		Os:             getReq.Os,
		Osversion:      getReq.Osversion,
		Appversion:     getReq.Appversion,
		Androidid:      getReq.Androidid,
		Imei:           getReq.Imei,
		Mac:            getReq.Mac,
		Idfa:           getReq.Idfa,
		Udid:           getReq.Openudid,
		Appname:        "网上厨房",
		Apppackagename: "cn.cook",
		Imsi:           getReq.Imsi,
		Ip:             getReq.Ip,
		Ua:             getReq.Ua,
		Network:        getReq.Network,
		Time:           time,
		Screenwidth:    getReq.Screenwidth,
		Screenheight:   getReq.Screenheight,
		Width:          w,
		Height:         h,
		Manufacturer:   getReq.Vendor,
		Brand:          getReq.Vendor,
		Model:          getReq.Model,
		Language:       "zh",
		Token:          util.Md5(APPID_ANDROID + APIKEY_ANDROID + lid + time), // md5(appid+key+lid+time)
		IsGp:           "0",
	}
	switch getReq.Imsi {
	case "1":
		postData.Imsi = "46000"
	case "2":
		postData.Imsi = "46001"
	case "3":
		postData.Imsi = "46003"
	default:
		postData.Imsi = "unknown"
	}
	if getReq.Os == "2" {
		postData.Appid = APPID_IOS
		postData.Token = util.Md5(APPID_IOS + APIKEY_IOS + lid + time)
	}

	ma, err := json.Marshal(&postData)
	if err != nil {
		return util.ResMsg{}
	}
	// req, err := http.NewRequest("POST", URL, strings.NewReader(url.Values{"data": {string(ma)}}.Encode()))
	// if err != nil {
	// 	return util.ResMsg{}
	// }
	resp, err := util.Client.PostForm(URL, url.Values{"data": {string(ma)}})
	// resp, err := util.Client.Do(req)
	mongo.LogReq(funcName, getReq.Os)
	if err != nil {
		mongo.LogTimeout(funcName, getReq.Os)
		util.HttpRequestCount.WithLabelValues(funcName + "timeouterr").Inc()
		return util.ResMsg{}
	}
	data, err := ioutil.ReadAll(resp.Body)
	resp.Body.Close()
	if err != nil {
		mongo.LogNo(funcName, getReq.Os)
		util.HttpRequestCount.WithLabelValues(funcName + "readresperr").Inc()
		return util.ResMsg{}
	}
	if resp.StatusCode != 200 {
		mongo.LogTimeout(funcName, getReq.Os)
		util.HttpRequestCount.WithLabelValues(funcName + "not200err").Inc()
		return util.ResMsg{}
	}
	return FormateRes(data, getReq.Os, funcName, getReq.Imei)
}

func FormateRes(res []byte, os, funcName, imei string) util.ResMsg {
	resData := &adres{}
	json.Unmarshal(res, resData)
	if resData.Code != 0 {
		mongo.LogNo(funcName, os)
		util.HttpRequestCount.WithLabelValues(funcName + "noerr").Inc()
		return util.ResMsg{}
	}
	if len(resData.Ads) == 0 {
		mongo.LogNo(funcName, os)
		util.HttpRequestCount.WithLabelValues(funcName + "noerr").Inc()
		return util.ResMsg{}
	}
	ad := resData.Ads[0]
	postData := util.ResMsg{
		Id:       "0",
		Weight:   0,
		State:    0,
		Title:    ad.Title,
		Content:  ad.Desc,
		ImageUrl: ad.Src,
		Uri:      ad.Link,
	}
	if ad.Src == "" {
		if ad.Icon == "" {
			mongo.LogNo(funcName, os)
			util.HttpRequestCount.WithLabelValues(funcName + "noerr").Inc()
			return util.ResMsg{}
		}
		postData.ImageUrl = ad.Icon
	}
	for _, value := range ad.Displayreport {
		postData.Displayreport = append(postData.Displayreport, value.Reporturl)
	}
	postData.Displayreport = append(postData.Displayreport, "http://120.26.247.23/ad/report.php?method="+funcName+"&event=displayreport&os="+os+"&t="+util.GetRandom())
	postData.Clickreport = ad.Clickreport
	postData.Clickreport = append(postData.Clickreport, "http://120.26.247.23/ad/report.php?method="+funcName+"&event=clickreport&os="+os+"&t="+util.GetRandom())

	return postData
}
