package tianluo

type adres struct {
	Code   int
	Count  int
	Msg    string
	Pvid   string
	Adcode string
	Ads    []Ad
}

type Ad struct {
	Title         string
	Desc          string
	Src           string
	Icon          string
	Link          string
	Clickreport   []string
	Displayreport []Report
	Action        int
	Adtype        int
	Aps           int
	Em            int
	Height        int
	Width         int
	Page          string
	Smarth        string
}

type Report struct {
	Repoettime string
	Reporturl  string
}
