package yousixing

import (
	// "bytes"
	// "encoding/json"
	// "fmt"
	"io/ioutil"
	// "log"
	"net/http"
	"net/url"
	"strconv"
	// "strings"
	// "time"
	// "unsafe"
	// "regexp"

	"ad/mongo"
	"ad/util"
	"github.com/pquerna/ffjson/ffjson"
)

const (
	URL                     = "http://123.59.48.113/req_ad"
	APPID                   = "100238"
	ADID_ANDROID            = "1000939"
	ADID_IOS                = "1000940"
	ADID_ANDROID_FLOW       = "1000941"
	ADID_IOS_FLOW           = "1000942"
	ADID_ANDROID_STARTUP    = "1000937"
	ADID_IOS_STARTUP        = "1000938"
	ADID_ANDROID_STORY_FLOW = "1001023"
	ADID_IOS_STORY_FLOW     = "1001024"
)

func Yousixing(getReq *util.ReqMsg, funcName string) util.ResMsg {
	if getReq.Os == "1" {
		return base(getReq, funcName, "1", ADID_ANDROID, "640", "100")
	}
	return base(getReq, funcName, "1", ADID_IOS, "640", "100")
}

func YousixingFlow(getReq *util.ReqMsg, funcName string) util.ResMsg {
	if getReq.Os == "1" {
		return base(getReq, funcName, "1", ADID_ANDROID_FLOW, "640", "360")
	}
	return base(getReq, funcName, "1", ADID_IOS_FLOW, "640", "360")
}

func YousixingStartup(getReq *util.ReqMsg, funcName string) util.ResMsg {
	if getReq.Os == "1" {
		return base(getReq, funcName, "1", ADID_ANDROID_STARTUP, "640", "960")
	}
	return base(getReq, funcName, "1", ADID_IOS_STARTUP, "640", "960")
}

func YousixingStoryFlow(getReq *util.ReqMsg, funcName string) util.ResMsg {
	if getReq.Os == "1" {
		return base(getReq, funcName, "1", ADID_ANDROID_STORY_FLOW, "232", "132")
	}
	return base(getReq, funcName, "1", ADID_IOS_STORY_FLOW, "232", "232")
}

func base(getReq *util.ReqMsg, funcName, adtype, adid, w, h string) util.ResMsg {
	network := "5"
	switch getReq.Network {
	case "wifi":
		network = "1"
	case "2g":
		network = "4"
	case "3g":
		network = "3"
	case "4g":
		network = "2"
	default:
		network = "5"
	}
	imsi := ""
	switch getReq.Imsi {
	case "1":
		imsi = "46000"
	case "2":
		imsi = "46001"
	case "3":
		imsi = "46003"
	default:
		imsi = ""
	}
	sd, _ := strconv.Atoi(getReq.Sd)
	dpi := strconv.Itoa(sd / 160)
	v := url.Values{
		"pid":                {adid},
		"type":               {"api"},
		"ad_type":            {adtype},
		"ad_w":               {w},
		"ad_h":               {h},
		"app_package":        {"cn.ecook"},
		"app_id":             {APPID},
		"app_name":           {"网上厨房"},
		"device_geo_lat":     {getReq.Lat},
		"device_geo_lon":     {getReq.Lng},
		"device_imei":        {getReq.Imei},
		"device_adid":        {getReq.Androidid},
		"device_mac":         {getReq.Mac},
		"device_type_os":     {getReq.Osversion},
		"device_brand":       {getReq.Vendor},
		"device_model":       {getReq.Model},
		"device_width":       {getReq.Screenwidth},
		"device_height":      {getReq.Screenheight},
		"device_imsi":        {imsi},
		"device_network":     {network},
		"device_os":          {"Android"},
		"device_density":     {dpi},
		"device_ip":          {getReq.Ip},
		"device_ua":          {getReq.Ua},
		"device_orientation": {"0"},
		"device_lan":         {"zh-CN"},
		"device_type":        {"1"},
		"is_mobile":          {"1"},
	}
	if getReq.Os == "2" {
		v["App_package"] = []string{"cn.ecook.ecook"}
		v["Device_adid"] = []string{getReq.Idfa}
		v["Device_os"] = []string{"iOS"}
	}

	// fmt.Println(string(ma))
	// 请求广告
	req, err := http.NewRequest("POST", URL+"?"+v.Encode(), nil)
	if err != nil {
		return util.ResMsg{}
	}
	req.Header.Set("Connection", "keep-alive")
	req.Header.Set("Content-Type", "application/json;charset=utf-8")

	resp, err := util.Client.Do(req)
	mongo.LogReq(funcName, getReq.Os)
	if err != nil {
		// fmt.Println(err.Error())
		mongo.LogTimeout(funcName, getReq.Os)
		return util.ResMsg{}
	}
	data, err := ioutil.ReadAll(resp.Body)
	resp.Body.Close()
	if err != nil {
		// fmt.Println(err.Error())
		mongo.LogNo(funcName, getReq.Os)
		return util.ResMsg{}
	}
	// fmt.Println(resp.StatusCode)
	if resp.StatusCode != 200 {
		mongo.LogTimeout(funcName, getReq.Os)
		return util.ResMsg{}
	}
	// fmt.Println(string(data))
	return FormateRes(data, getReq.Os, funcName)
}

// 根据广告平台的返回生成客户端返回
func FormateRes(res []byte, os, funcName string) util.ResMsg {
	resData := adres{}
	ffjson.Unmarshal(res, &resData)
	// fmt.Printf("%+v", resData)
	// fmt.Println(string(res))
	if len(resData.SrcUrls) == 0 || resData.SrcUrls[0] == "" {
		// fmt.Println(2)
		mongo.LogNo(funcName, os)
		return util.ResMsg{}
	}

	if len(resData.DUrl) == 0 {
		mongo.LogNo(funcName, os)
		return util.ResMsg{}
	}
	uri := resData.DUrl[0]
	if resData.Target_type == 1 {
		uri = resData.Down_url
	}

	postData := util.ResMsg{
		Id:       "0",
		Weight:   0,
		State:    1,
		Title:    resData.Title,
		Content:  resData.Content,
		ImageUrl: resData.SrcUrls[0],
		Uri:      uri,
	}

	postData.Displayreport = resData.MonitorUrl
	postData.Displayreport = append(postData.Displayreport, "http://120.26.247.23/ad/report.php?method="+funcName+"&event=displayreport&os="+os+"&t="+util.GetRandom())
	postData.Clickreport = resData.Clickurl
	postData.Clickreport = append(postData.Clickreport, "http://120.26.247.23/ad/report.php?method="+funcName+"&event=clickreport&os="+os+"&t="+util.GetRandom())

	return postData
}
