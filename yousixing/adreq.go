package yousixing

type adreq struct {
	Pid                string `json:"pid"`
	Type               string `json:"type"`
	Ad_type            string `json:"ad_type"`
	Protocols          string `json:"protocols"`
	Ad_w               string `json:"ad_w"`
	Ad_h               string `json:"ad_h"`
	App_package        string `json:"app_package"`
	App_id             string `json:"app_id"`
	App_name           string `json:"app_name"`
	Device_geo_lat     string `json:"device_geo_lat"`
	Device_geo_lon     string `json:"device_geo_lon"`
	Device_imei        string `json:"device_imei"`
	Device_adid        string `json:"device_adid"`
	Device_mac         string `json:"device_mac"`
	Device_type_os     string `json:"device_os_type"`
	Device_brand       string `json:"device_brand"`
	Device_model       string `json:"device_model"`
	Device_width       string `json:"device_width"`
	Device_height      string `json:"device_height"`
	Device_imsi        string `json:"device_imsi"`
	Device_network     string `json:"device_network"`
	Device_os          string `json:"device_os"`
	Device_density     string `json:"device_density"`
	Device_ip          string `json:"device_ip"`
	Device_ua          string `json:"device_ua"`
	Device_orientation string `json:"device_orientation"`
	Device_lan         string `json:"device_lan"`
	Device_isroot      string `json:"device_isroot"`
	Device_type        string `json:"device_type"`
	Is_mobile          string `josn:"is_mobile"`
}
