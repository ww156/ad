package adxhi

type adreq struct {
	Requestid   string       `json:"requestid"`
	ApiVersion  string       `json:"apiVersion"`
	ChannelId   string       `json:"channelId"`
	PositionId  string       `json:"positionId"`
	DeciceInfo  *deviceInfo  `json:"deviceInfo"`
	Network     *network     `json:"network"`
	Userprofile *userProfile `json:"userProfile"`
	Location    *location    `json:"location"`
	AdCount     int          `json:"adCount"`
}

type deviceInfo struct {
	Os           string `json:"os"`
	OsVersion    string `json:"osVersion"`
	Imei         string `json:"imei"`
	Mac          string `json:"mac"`
	Idfa         string `json:"idfa"`
	AndroidId    string `json:"androidId"`
	IdfaMd5      string `json:"idfaMd5"`
	ImeiMd5      string `json:"imeiMd5"`
	AndroidIdMd5 string `json:"androidIdMd5"`
	UserAgent    string `json:"userAgent"`
	DeviceType   int    `json:"deviceType"`
	Brand        string `json:"brand"`
	Model        string `json:"model"`
	ScrennnWidth int    `json:"screenWidth"`
	ScreenHeight int    `json:"scrennHeight"`
	Extra        string `json:"extra"`
}
type network struct {
	Ip             string `json:"ip"`
	ConnectionType int    `json:"connectionType"`
	OperatorType   int    `json:"operatorType"`
	CellularId     string `json:"cellularId"`
}

type userProfile struct {
	Age      string `json:"age"`
	Gender   string `json:"gender"`
	Keywords string `json:"keywords"`
	Extra    string `json:"extra"`
}

type location struct {
	LogLongitude float64 `json:"logLongitude"`
	LogLatitude  float64 `json:"logLatitude"`
	Province     string  `json:"province"`
	City         string  `json:"city"`
	Address      string  `json:"address"`
}
