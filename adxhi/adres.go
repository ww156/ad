package adxhi

type adres struct {
	StatusCode int
	Msg        string
	RequestId  string
	PositionId string
	Ads        []*adInfo
}

type adInfo struct {
	TrackingId     string
	ClickUrl       string
	ClickAction    int
	AdCreative     ad
	EventTracking  map[string][]string
	Price          float64
	ExpirationTime int
}

type ad struct {
	TemplateType   int
	Title          string
	SubTitle       string
	Description    string
	AppName        string
	PackageName    string
	Category       string
	VideoUrl       string
	VideoDuration  int
	Icon           string
	ImageUrls      string
	MaterialWidth  int
	MaterialHeight int
}
