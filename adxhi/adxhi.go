package adxhi

import (
	"bytes"
	// "encoding/json"
	// "fmt"
	"io/ioutil"
	// "log"
	"net/http"
	// "net/url"
	"strconv"
	"strings"
	"time"
	// "unsafe"
	// "regexp"

	"ad/mongo"
	"ad/util"
	"github.com/pquerna/ffjson/ffjson"
)

const (
	DEV_URL = "http://211.95.56.10:9980/frontend/api/ad/pull"
	PRO_URL = "http://api.adxhi.com/frontend/api/ad/pull"
)

func Adxhi(getReq *util.ReqMsg, funcName string) util.ResMsg {
	return base(getReq, funcName)
}

func AdxhiFlow(getReq *util.ReqMsg, funcName string) util.ResMsg {
	return base(getReq, funcName)
}

func AdxhiStartup(getReq *util.ReqMsg, funcName string) util.ResMsg {
	return base(getReq, funcName)
}

func base(getReq *util.ReqMsg, funcName string) util.ResMsg {
	time.Sleep(500 * time.Millisecond)
	appid := "2103818027"
	adid := "2203268831"
	imsi := 0
	switch getReq.Imsi {
	case "1":
		imsi = 1
	case "2":
		imsi = 2
	case "3":
		imsi = 3
	default:
		imsi = 0
	}
	conType := 0
	switch getReq.Network {
	case "wifi":
		conType = 1
	case "2g":
		conType = 2
	case "3g":
		conType = 3
	case "4g":
		conType = 4
	default:
		conType = 0
	}
	width, err := strconv.Atoi(getReq.Screenwidth)
	if err != nil {
		width = 0
	}
	height, err := strconv.Atoi(getReq.Screenheight)
	if err != nil {
		height = 0
	}
	lat, err := strconv.ParseFloat(getReq.Lat, 64)
	if err != nil {
		lat = 0
	}
	lon, err := strconv.ParseFloat(getReq.Lng, 64)
	if err != nil {
		lon = 0
	}
	postData := adreq{
		Requestid:  util.GetRandom(),
		ApiVersion: "1.0",
		ChannelId:  appid,
		PositionId: adid,
		DeciceInfo: &deviceInfo{
			Os:           "Android",
			OsVersion:    getReq.Osversion,
			Imei:         getReq.Imei,
			Mac:          getReq.Mac,
			Idfa:         getReq.Idfa,
			AndroidId:    getReq.Androidid,
			IdfaMd5:      util.Md5(getReq.Idfa),
			ImeiMd5:      util.Md5(getReq.Imei),
			AndroidIdMd5: util.Md5(getReq.Androidid),
			UserAgent:    getReq.Ua,
			DeviceType:   1,
			Brand:        getReq.Vendor,
			Model:        getReq.Model,
			ScrennnWidth: width,
			ScreenHeight: height,
		},
		Network: &network{
			Ip:             getReq.Ip,
			ConnectionType: conType,
			OperatorType:   imsi,
		},
		Location: &location{
			LogLatitude:  lat,
			LogLongitude: lon,
		},
		AdCount: 1,
	}

	ma, err := ffjson.Marshal(&postData)
	if err != nil {
		return util.ResMsg{}
	}

	// 请求广告
	req, err := http.NewRequest("POST", PRO_URL, bytes.NewBuffer(ma))
	if err != nil {
		return util.ResMsg{}
	}
	req.Header.Set("Connectio", "keep-alive")
	req.Header.Set("Content-Type", "application/json;charset=UTF-8")

	resp, err := util.Client.Do(req)
	mongo.LogReq(funcName, getReq.Os)
	if err != nil {
		// fmt.Println(err.Error())
		mongo.LogTimeout(funcName, getReq.Os)
		return util.ResMsg{}
	}
	data, err := ioutil.ReadAll(resp.Body)
	resp.Body.Close()
	if err != nil {
		mongo.LogNo(funcName, getReq.Os)
		return util.ResMsg{}
	}
	// fmt.Println(resp.StatusCode)
	if resp.StatusCode != 200 {
		mongo.LogTimeout(funcName, getReq.Os)
		return util.ResMsg{}
	}

	return FormateRes(data, getReq.Os, funcName)
}

// 根据广告平台的返回生成客户端返回
func FormateRes(res []byte, os, funcName string) util.ResMsg {
	resData := adres{}
	ffjson.Unmarshal(res, &resData)
	// fmt.Println(string(res))
	if resData.StatusCode != 200 {
		mongo.LogNo(funcName, os)
		return util.ResMsg{}
	}
	if len(resData.Ads) == 0 {
		mongo.LogNo(funcName, os)
		return util.ResMsg{}
	}
	info := resData.Ads[0]
	ad := info.AdCreative
	if ad.ImageUrls == "" {
		mongo.LogNo(funcName, os)
		return util.ResMsg{}
	}

	postData := util.ResMsg{
		Id:       "0",
		Weight:   0,
		State:    0,
		Title:    ad.Title,
		Content:  ad.Description,
		ImageUrl: strings.Split(ad.ImageUrls, ",")[0],
		Uri:      info.ClickUrl,
	}
	// fmt.Println(ad.Curl)

	postData.Displayreport = info.EventTracking["exp"]
	postData.Displayreport = append(postData.Displayreport, "http://120.26.247.23/ad/report.php?method="+funcName+"&event=displayreport&os="+os+"&t="+util.GetRandom())
	postData.Clickreport = info.EventTracking["clc"]
	postData.Clickreport = append(postData.Clickreport, "http://120.26.247.23/ad/report.php?method="+funcName+"&event=clickreport&os="+os+"&t="+util.GetRandom())

	return postData
}
