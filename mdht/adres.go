package mdht

type adres struct {
	Request_id string
	Error_code int
	Message    string
	Data       []data
}
type data struct {
	Ads        ads
	Impression []string
	Click      []string
	Event      []event
	Tracking   []tracking
	Ad_native  []native
}
type ads struct {
	Media   string
	Ad_type int
	Height  int
	Width   int
}
type event struct {
	Event_key   int
	Event_value string
}
type tracking struct {
	Tracking_key   int
	Tracking_value []string
}
type native struct {
	Icon        string
	Image       string
	Title       string
	Description string
}
