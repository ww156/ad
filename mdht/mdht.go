package mdht

import (
	// "bytes"
	// "encoding/json"
	"fmt"
	"io/ioutil"
	// "log"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	// "time"
	// "unsafe"
	// "regexp"
	"compress/gzip"
	"math/rand"

	"ad/mongo"
	"ad/util"
	"github.com/pquerna/ffjson/ffjson"
)

const (
	URL = "http://114.55.226.22/full/config/init?"
)

func Mdht(getReq *util.ReqMsg, funcName string) util.ResMsg {
	return base(getReq, funcName, "640", "100")
}

func MdhtFlow(getReq *util.ReqMsg, funcName string) util.ResMsg {
	return base(getReq, funcName, "640", "360")
}

func MdhtStartup(getReq *util.ReqMsg, funcName string) util.ResMsg {
	return base(getReq, funcName, "640", "960")
}

func base(getReq *util.ReqMsg, funcName, w, h string) util.ResMsg {
	operator := "0"
	imsi := ""
	switch getReq.Imsi {
	case "1":
		operator = "1"
		imsi = "46000" + strconv.Itoa(10000+rand.Intn(90000)) + strconv.Itoa(10000+rand.Intn(90000))
	case "2":
		operator = "2"
		imsi = "46001" + strconv.Itoa(10000+rand.Intn(90000)) + strconv.Itoa(10000+rand.Intn(90000))
	case "3":
		operator = "3"
		imsi = "46002" + strconv.Itoa(10000+rand.Intn(90000)) + strconv.Itoa(10000+rand.Intn(90000))
	default:
		operator = "0"
		imsi = ""
	}
	network := "0"
	switch getReq.Network {
	case "wifi":
		network = "1"
	case "2g":
		network = "2"
	case "3g":
		network = "3"
	case "4g":
		network = "4"
	default:
		network = "0"
	}
	v := url.Values{}
	v.Set("pid", "5ba6dcc0d2f4c2e194d03db43f99cfae")
	v.Set("api_ver", "2.10.4")
	v.Set("ter_type", getReq.Vendor)
	v.Set("user_agt", getReq.Model)
	v.Set("os", getReq.Osversion)
	v.Set("tid", strings.ToUpper(strings.Replace(getReq.Mac, ":", "", -1)))
	v.Set("net_oper", operator)
	v.Set("acc_point", network)
	v.Set("longitude", getReq.Lng)
	v.Set("latitude", getReq.Lat)
	v.Set("sdk_type", getReq.Os)
	v.Set("density", "1")
	v.Set("serialno", getReq.Androidid)
	v.Set("uuid2", getReq.Imei)
	v.Set("uuid3", imsi)
	v.Set("ip", getReq.Ip)
	v.Set("client_agt", getReq.Ua)
	v.Set("scrn_w", getReq.Screenwidth)
	v.Set("scrn_h", getReq.Screenheight)
	v.Set("ad_w", w)
	v.Set("ad_h", h)
	v.Set("app_name", "网上厨房")
	v.Set("bndl_id", "cn.ecook")
	if getReq.Os == "2" {
		v.Set("serialno", getReq.Idfa)
		v.Set("bndl_id", "cn.ecook.ecook")
	}

	// 请求广告
	req, err := http.NewRequest("GET", URL+v.Encode(), nil)
	if err != nil {
		return util.ResMsg{}
	}
	req.Header.Set("Connection", "keep-alive")
	req.Header.Set("Content-Type", "application/json;charset=UTF-8")
	req.Header.Set("Accept-Encoding", "gzip")

	resp, err := util.Client.Do(req)
	mongo.LogReq(funcName, getReq.Os)
	if err != nil {
		fmt.Println(err.Error())
		mongo.LogTimeout(funcName, getReq.Os)
		return util.ResMsg{}
	}
	data := []byte{}
	if strings.Contains(resp.Header.Get("Content-Encoding"), "gzip") {
		r, err := gzip.NewReader(resp.Body)
		resp.Body.Close()
		if err != nil {
			mongo.LogNo(funcName, getReq.Os)
			return util.ResMsg{}
		}
		data, err = ioutil.ReadAll(r)
		r.Close()
		if err != nil {
			mongo.LogNo(funcName, getReq.Os)
			return util.ResMsg{}
		}
	} else {
		data, err = ioutil.ReadAll(resp.Body)
		resp.Body.Close()
	}

	if err != nil {
		mongo.LogNo(funcName, getReq.Os)
		return util.ResMsg{}
	}
	fmt.Println(resp.StatusCode)
	if resp.StatusCode != 200 {
		mongo.LogTimeout(funcName, getReq.Os)
		return util.ResMsg{}
	}

	return FormateRes(data, getReq.Os, funcName)
}

// 根据广告平台的返回生成客户端返回
func FormateRes(res []byte, os, funcName string) util.ResMsg {
	resData := adres{}
	ffjson.Unmarshal(res, &resData)
	fmt.Println(string(res))
	if resData.Error_code != 0 {
		mongo.LogNo(funcName, os)
		return util.ResMsg{}
	}
	if len(resData.Data) == 0 {
		mongo.LogNo(funcName, os)
		return util.ResMsg{}
	}

	ad := resData.Data[0]

	postData := util.ResMsg{
		Id:       "0",
		Weight:   0,
		State:    0,
		Title:    "",
		Content:  "",
		ImageUrl: "",
		Uri:      "",
	}

	if len(ad.Ad_native) > 0 {
		na := ad.Ad_native[0]
		postData.Title = na.Title
		postData.Content = na.Description
		postData.ImageUrl = ad.Ads.Media
	}

	postData.Displayreport = ad.Impression
	postData.Displayreport = append(postData.Displayreport, "http://120.26.247.23/ad/report.php?method="+funcName+"&event=displayreport&os="+os+"&t="+util.GetRandom())
	postData.Clickreport = ad.Click
	postData.Clickreport = append(postData.Clickreport, "http://120.26.247.23/ad/report.php?method="+funcName+"&event=clickreport&os="+os+"&t="+util.GetRandom())

	return postData
}

func replace(str string) string {
	str = strings.Replace(str, "${DOWN_X}", "IT_CLK_PNT_DOWN_X", -1)
	str = strings.Replace(str, "${DOWN_Y}", "IT_CLK_PNT_DOWN_Y", -1)
	str = strings.Replace(str, "${UP_X}", "IT_CLK_PNT_UP_X", -1)
	str = strings.Replace(str, "${UP_Y}", "IT_CLK_PNT_UP_Y", -1)
	return str
}
