package qichuang

type adreq struct {
	Version        int    `json:"version"`
	Dnt            int    `json:"dnt"`
	Adid           string `json:"adid"`
	Ver            string `json:"ver"`
	Storeurl       string `json:"storeurl"`
	Pos            int    `json:"pos"`
	Adtype         int    `json:"adtype"`
	Width          int    `json:"width"`
	Height         int    `json:"height"`
	Bundle         string `json:"bundle"`
	Appname        string `json:"appname"`
	Ua             string `json:"ua"`
	Devicetype     int    `json:"devicetype"`
	Os             string `json:"os"`
	Osv            string `json:"osv"`
	Carrier        int    `json:"carrier"`
	Connectiontype int    `json:"connectiontype"`
	Ip             string `json:"ip"`
	Density        int    `json:"density"`
	Make           string `json:"make"`
	Model          string `json:"model"`
	Language       string `json:"language"`
	Js             int    `json:"js"`
	Imei           string `json:"imei,omitempty"`
	Didsha1        string `json:"didsha1,omitempty"`
	Didmd5         string `json:"didmd5,omitempty"`
	Idfa           string `json:"idfa,omitempty"`
	Androidid      string `json:"androidid,omitempty"`
	Dpidsha1       string `json:"dpidsha1,omitempty"`
	Dpidmd5        string `json:"dpidmd5,omitempty"`
	Mac            string `json:"mac,omitempty"`
	Macsha1        string `json:"macsha1,omitempty"`
	Macmd5         string `json:"macmd5,omitempty"`
	Lat            string `json:"lat,omitempty"`
	Lon            string `json:"lon,omitempty"`
	Orientation    int    `json:"orientation"`
	Sw             int    `json:"sw"`
	Sh             int    `json:"sh"`
	User           *User  `json:"user,omitempty"`
}

type User struct {
	Yob      int    `json:"yob,omitempty"`
	Gender   string `json:"gender,omitempty"`
	Keywords string `json:"keywords,omitempty"`
}
