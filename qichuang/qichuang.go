package qichuang

import (
	// "bytes"
	"encoding/json"
	// "fmt"
	"io/ioutil"
	// "log"
	"net/http"
	"strconv"
	// "strings"
	// "time"
	// "unsafe"

	// "github.com/golang/protobuf/proto"

	"ad/mongo"
	"ad/util"
)

const (
	DEV_URL              = "http://adx.corpize.com/ssp/corpize"
	ADID_ANDROID         = "584c68c5b943aee87a8ed62962247165"
	ADID_IOS             = "59dc631d27d5f1315f437fa8c9ae7ab4"
	ADID_ANDROID_FLOW    = "d4ae20ffdb81c6dee033f4a8f5649b4b"
	ADID_IOS_FLOW        = "79192cd16bce5edd65e5448429e917ca"
	ADID_ANDROID_STARTUP = "0d4b1042ea374830fe2a756d5e1741a4"
	ADID_IOS_STARTUP     = "dac1aa38de8eb49c81ba43463cac5aec"
)

func Qichuang(getReq *util.ReqMsg, funcName string) util.ResMsg {
	if getReq.Os == "1" {
		return base(getReq, funcName, ADID_ANDROID, 640, 100, 4, 201)
	}
	return base(getReq, funcName, ADID_IOS, 640, 100, 4, 201)
}

func QichuangFlow(getReq *util.ReqMsg, funcName string) util.ResMsg {
	if getReq.Os == "1" {
		return base(getReq, funcName, ADID_ANDROID_FLOW, 640, 360, 3, 207)
	}
	return base(getReq, funcName, ADID_IOS_FLOW, 640, 360, 3, 207)
}

func QichuangStartup(getReq *util.ReqMsg, funcName string) util.ResMsg {
	if getReq.Os == "1" {
		return base(getReq, funcName, ADID_ANDROID_STARTUP, 640, 960, 1, 202)
	}
	return base(getReq, funcName, ADID_IOS_STARTUP, 640, 960, 1, 202)
}

func base(getReq *util.ReqMsg, funcName, adid string, w, h, pos, adtype int) util.ResMsg {
	width, err := strconv.Atoi(getReq.Screenwidth)
	if err != nil {
		width = 0
	}
	height, err := strconv.Atoi(getReq.Screenheight)
	if err != nil {
		height = 0
	}
	carrier := 0
	switch getReq.Imsi {
	case "1":
		carrier = 46000
	case "2":
		carrier = 46001
	case "3":
		carrier = 46002
	default:
		carrier = 0
	}
	network := 0
	switch getReq.Network {
	case "以太网":
		network = 1
	case "wifi":
		network = 2
	case "2g":
		network = 4
	case "3g":
		network = 5
	case "4g":
		network = 6
	default:
		network = 0
	}
	sd, err := strconv.Atoi(getReq.Sd)
	// dpi := sd / 160
	postData := adreq{
		Version:        20170518,
		Dnt:            0,
		Adid:           adid,
		Ver:            getReq.Appversion,
		Storeurl:       "http://a.app.qq.com/o/simple.jsp?pkgname=cn.ecook",
		Pos:            pos,
		Adtype:         adtype,
		Width:          w,
		Height:         h,
		Bundle:         "cn.ecook",
		Appname:        "网上厨房",
		Ua:             getReq.Ua,
		Devicetype:     4,
		Os:             "Android",
		Osv:            getReq.Osversion,
		Carrier:        carrier,
		Connectiontype: network,
		Ip:             getReq.Ip,
		Density:        sd,
		Make:           getReq.Vendor,
		Model:          getReq.Model,
		Language:       "zh-CN",
		Js:             1,
		Imei:           getReq.Imei,
		Idfa:           getReq.Idfa,
		Androidid:      getReq.Androidid,
		Mac:            getReq.Mac,
		Orientation:    0,
		Sw:             width,
		Sh:             height,
	}
	if getReq.Os == "2" {
		postData.Os = "IOS"
		postData.Storeurl = "http://itunes.apple.com/cn/app/id455221304?mt=8#"
		postData.Bundle = "cn.ecook.ecook"
		postData.Mac = ""
	}

	ma, err := json.Marshal(&postData)
	if err != nil {
		return util.ResMsg{}
	}
	// fmt.Printf("%+v", string(ma))
	header := http.Header{}
	header.Set("Connection", "keep-alive")
	header.Set("Content-Type", "application/json")
	resp, err := util.HttpPOST(DEV_URL, &header, ma)
	mongo.LogReq(funcName, getReq.Os)
	if err != nil {
		mongo.LogTimeout(funcName, getReq.Os)
		util.HttpRequestCount.WithLabelValues(funcName + "timeouterr").Inc()
		return util.ResMsg{}
	}
	data, err := ioutil.ReadAll(resp.Body)
	resp.Body.Close()
	if err != nil {
		mongo.LogNo(funcName, getReq.Os)
		util.HttpRequestCount.WithLabelValues(funcName + "readresperr").Inc()
		return util.ResMsg{}
	}
	if resp.StatusCode != 200 {
		// fmt.Println(resp.StatusCode)
		mongo.LogTimeout(funcName, getReq.Os)
		util.HttpRequestCount.WithLabelValues(funcName + "not200err").Inc()
		return util.ResMsg{}
	}
	if adtype == 207 {
		return FormateFlowRes(data, getReq.Os, funcName)
	}
	return FormateRes(data, getReq.Os, funcName)
}

func FormateFlowRes(res []byte, os, funcName string) util.ResMsg {
	resData := &adres{}
	json.Unmarshal(res, resData)
	// fmt.Println(string(res))
	// fmt.Printf("%+v", resData)
	if resData.Status != 200 {
		mongo.LogNo(funcName, os)
		util.HttpRequestCount.WithLabelValues(funcName + "noerr").Inc()
		return util.ResMsg{}
	}
	native := resData.Native
	ad := native.Link

	postData := util.ResMsg{
		Id:       "0",
		Weight:   0,
		State:    0,
		Title:    "",
		Content:  "",
		ImageUrl: "",
		Uri:      ad.Url,
	}

	for _, value := range native.Assets {
		title := value.Title
		img := value.Img
		data := value.Data
		if postData.Title == "" {
			postData.Title = title.Text
		}
		if postData.Content == "" {
			if data.Label == "desc" {
				postData.Content = data.Value
			}
		}
		if postData.ImageUrl == "" {
			if len(img.Url) > 0 {
				postData.ImageUrl = img.Url[0]
			}
		}
	}
	if postData.ImageUrl == "" {
		mongo.LogNo(funcName, os)
		util.HttpRequestCount.WithLabelValues(funcName + "noerr").Inc()
		return util.ResMsg{}
	}
	postData.Displayreport = native.Imptrackers
	postData.Displayreport = append(postData.Displayreport, "http://120.26.247.23/ad/report.php?method="+funcName+"&event=displayreport&os="+os+"&t="+util.GetRandom())
	postData.Clickreport = ad.Clicktrackers
	postData.Clickreport = append(postData.Clickreport, "http://120.26.247.23/ad/report.php?method="+funcName+"&event=clickreport&os="+os+"&t="+util.GetRandom())

	return postData
}

func FormateRes(res []byte, os, funcName string) util.ResMsg {
	// fmt.Println(string(res))
	resData := &adres{}
	json.Unmarshal(res, resData)
	// fmt.Println(string(res))
	// fmt.Printf("%+v", resData)
	if resData.Status != 200 {
		mongo.LogNo(funcName, os)
		util.HttpRequestCount.WithLabelValues(funcName + "noerr").Inc()
		return util.ResMsg{}
	}
	ad := resData.Ext
	if ad.Iurl == "" {
		mongo.LogNo(funcName, os)
		util.HttpRequestCount.WithLabelValues(funcName + "noerr").Inc()
		return util.ResMsg{}
	}

	postData := util.ResMsg{
		Id:       "0",
		Weight:   0,
		State:    0,
		Title:    "",
		Content:  "",
		ImageUrl: ad.Iurl,
		Uri:      ad.Clickurl,
	}

	postData.Displayreport = ad.Imptrackers
	postData.Displayreport = append(postData.Displayreport, "http://120.26.247.23/ad/report.php?method="+funcName+"&event=displayreport&os="+os+"&t="+util.GetRandom())
	postData.Clickreport = ad.Clicktrackers
	postData.Clickreport = append(postData.Clickreport, "http://120.26.247.23/ad/report.php?method="+funcName+"&event=clickreport&os="+os+"&t="+util.GetRandom())

	return postData
}
