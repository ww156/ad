package qichuang

type adres struct {
	Version int
	Status  int
	Counts  int
	Adm     string
	Native  Native
	Ext     Ext
}

type Native struct {
	Assets      []Asset
	Link        Link
	Imptrackers []string
}

// type Asset map[string]interface{}

type Asset struct {
	Id    int
	Title Title
	Img   Img
	Data  Data
}

type Title struct {
	Text string
}

type Img struct {
	Type int
	Url  []string
	W    int
	H    int
}

type Data struct {
	Label string
	Value string
}

type Link struct {
	Url           string
	Clicktrackers []string
	Fallback      string
	Action        int
	Dfn           string
}

type Ext struct {
	Iurl          string
	Clickurl      string
	Imptrackers   []string
	Clicktrackers []string
	Action        int
	Dfn           string
	Title         string
	Desc          string
}
