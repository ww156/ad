package tianluoa

type adreq struct {
	Version    string  `json:"version"`
	Time       string  `json:"time"`
	Token      string  `json:"token"`
	Reqid      string  `json:"reqid"`
	Appid      string  `json:"appid"`
	Appver     string  `json:"appver"`
	Adspotid   string  `json:"adspotid"`
	Impsize    int     `json:"impsize"`
	Ip         string  `json:"ip"`
	Ua         string  `json:"ua"`
	Lat        float64 `json:"float64"`
	Lon        float64 `json:"float64"`
	Sw         int     `json:"sw"`
	Sh         int     `json:"sh"`
	Ppi        int     `json:"ppi"`
	Make       string  `json:"make"`
	Model      string  `json:"model"`
	Os         int     `json:"os"`
	Osv        string  `json:"osv"`
	Imei       string  `json:"imei"`
	Mac        string  `json:"mac"`
	Androidid  string  `json:"androidid,omitempty"`
	Idfa       string  `json:"idfa,omitempty"`
	Carrier    string  `json:"carrier"`
	Network    int     `json:"network"`
	Devicetype int     `json:"devicetype"`
	Res_type   int     `json:"res_type"`
}
