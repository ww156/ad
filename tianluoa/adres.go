package tianluoa

type adres struct {
	Code int
	Msg  string
	Imp  []Ad
}

type Ad struct {
	Impid      string
	Price      float64
	Compid     int
	Crid       string
	Adsource   string
	Image      []Image
	Word       []Word
	Video      []Video
	Htmlstring string
	Link       string
	Deeplink   string
	Action     int
	Imptk      []string
	Clicktk    []string
	Playtk     []string
	Ext        interface{}
}

type Image struct {
	Type int
	Iurl string
}

type Word struct {
	Type int
	Text string
}

type Video struct {
	Type     int
	Duration int
	Bitrate  int
	Vurl     string
}
