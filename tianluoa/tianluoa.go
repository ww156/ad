package tianluoa

import (
	// "bytes"
	"encoding/json"
	// "fmt"
	"io/ioutil"
	// "log"
	// "crypto/tls"
	"net/http"
	// "net/url"
	"strconv"
	"strings"
	"time"
	// "unsafe"
	// "regexp"

	"ad/mongo"
	"ad/util"
)

const (
	URL            = "http://links.adthetop.cn/shuttle"
	APPKEY_IOS     = "51a6586439e898abb0958fbce7b3f794"
	APPID_IOS      = "100181"
	APPKEY_ANDROID = "25a27687d7c72a68a631a21f5637ec62"
	APPID_ANDROID  = "100182"

	ADSPOTID_IOS             = "10000420"
	ADSPOTID_IOS_FLOW        = "10000424"
	ADSPOTID_IOS_STARTUP     = "10000426"
	ADSPOTID_ANDROID         = "10000421"
	ADSPOTID_ANDROID_FLOW    = "10000425"
	ADSPOTID_ANDROID_STARTUP = "10000427"

	ADTYPE         = "254"
	ADTYPE_FLOW    = "1116"
	ADTYPE_STARTUP = "20"
)

func Tianluoa(getReq *util.ReqMsg, funcName string) util.ResMsg {
	if getReq.Os == "1" {
		return base(getReq, funcName, ADSPOTID_ANDROID, ADTYPE)
	}
	return base(getReq, funcName, ADSPOTID_IOS, ADTYPE)
}

func TianluoaFlow(getReq *util.ReqMsg, funcName string) util.ResMsg {
	if getReq.Os == "1" {
		return base(getReq, funcName, ADSPOTID_ANDROID_FLOW, ADTYPE_FLOW)

	} else {
		return base(getReq, funcName, ADSPOTID_IOS_FLOW, ADTYPE_FLOW)
	}
}

func TianluoaStartup(getReq *util.ReqMsg, funcName string) util.ResMsg {
	if getReq.Os == "1" {
		return base(getReq, funcName, ADSPOTID_ANDROID_STARTUP, ADTYPE_STARTUP)

	} else {
		return base(getReq, funcName, ADSPOTID_IOS_STARTUP, ADTYPE_STARTUP)
	}
}

func base(getReq *util.ReqMsg, funcName, adid, adtype string) util.ResMsg {
	time := strconv.Itoa(int(time.Now().UnixNano() / 1e6))
	width, err := strconv.Atoi(getReq.Screenwidth)
	if err != nil {
		width = 0
	}
	height, err := strconv.Atoi(getReq.Screenheight)
	if err != nil {
		height = 0
	}
	sd, err := strconv.Atoi(getReq.Sd)
	dpi := sd / 160

	carrier := "46000"
	switch getReq.Imsi {
	case "1":
		carrier = "46000"
	case "2":
		carrier = "46001"
	case "3":
		carrier = "46002"
	default:
		carrier = "unknown"
	}
	network := 0
	switch getReq.Network {
	case "wifi":
		network = 1
	case "2g":
		network = 2
	case "3g":
		network = 3
	case "4g":
		network = 4
	default:
		network = 0
	}
	postData := adreq{
		Version:    "2.0",
		Time:       time,
		Token:      strings.ToLower(util.Md5(APPID_ANDROID + APPKEY_ANDROID + time)),
		Reqid:      util.GetRandom(),
		Appid:      APPID_ANDROID,
		Appver:     getReq.Appversion,
		Adspotid:   adid,
		Impsize:    1,
		Ip:         getReq.Ip,
		Ua:         getReq.Ua,
		Sw:         width,
		Sh:         height,
		Ppi:        dpi,
		Make:       getReq.Vendor,
		Model:      getReq.Model,
		Os:         2,
		Osv:        getReq.Osversion,
		Imei:       getReq.Imei,
		Mac:        getReq.Mac,
		Androidid:  getReq.Androidid,
		Idfa:       getReq.Idfa,
		Carrier:    carrier,
		Network:    network,
		Devicetype: 1,
	}
	if getReq.Os == "2" {
		postData.Os = 1
		postData.Appid = APPID_IOS
		postData.Token = strings.ToLower(util.Md5(APPID_IOS + APPKEY_IOS + time))
	}

	ma, err := json.Marshal(&postData)
	if err != nil {
		return util.ResMsg{}
	}
	header := http.Header{}
	header.Set("Connection", "keep-alive")
	header.Set("Content-Type", "application/json;charset=utf-8")
	resp, err := util.HttpPOST(URL, &header, ma)
	mongo.LogReq(funcName, getReq.Os)
	if err != nil {
		mongo.LogTimeout(funcName, getReq.Os)
		util.HttpRequestCount.WithLabelValues(funcName + "timeouterr").Inc()
		return util.ResMsg{}
	}
	data, err := ioutil.ReadAll(resp.Body)
	resp.Body.Close()
	if err != nil {
		mongo.LogNo(funcName, getReq.Os)
		util.HttpRequestCount.WithLabelValues(funcName + "readresperr").Inc()
		return util.ResMsg{}
	}
	if resp.StatusCode != 200 {
		mongo.LogTimeout(funcName, getReq.Os)
		util.HttpRequestCount.WithLabelValues(funcName + "not200err").Inc()
		return util.ResMsg{}
	}
	// fmt.Println(string(data))
	return FormateRes(data, getReq.Os, funcName)
}

func FormateRes(res []byte, os, funcName string) util.ResMsg {
	resData := &adres{}
	json.Unmarshal(res, resData)
	if resData.Code != 200 {
		mongo.LogNo(funcName, os)
		util.HttpRequestCount.WithLabelValues(funcName + "noerr").Inc()
		return util.ResMsg{}
	}
	if len(resData.Imp) == 0 {
		mongo.LogNo(funcName, os)
		util.HttpRequestCount.WithLabelValues(funcName + "noerr").Inc()
		return util.ResMsg{}
	}
	ad := resData.Imp[0]
	postData := util.ResMsg{
		Id:       "0",
		Weight:   0,
		State:    0,
		Title:    "",
		Content:  "",
		ImageUrl: "",
		Uri:      ad.Link,
	}
	for _, value := range ad.Word {
		if value.Type == 1 {
			postData.Title = value.Text
		}
		if value.Type == 2 {
			postData.Content = value.Text
		}
	}
	for _, value := range ad.Image {
		if value.Type > 300 {
			postData.ImageUrl = value.Iurl
		}
	}
	postData.Displayreport = ad.Imptk
	postData.Displayreport = append(postData.Displayreport, "http://120.26.247.23/ad/report.php?method="+funcName+"&event=displayreport&os="+os+"&t="+util.GetRandom())
	postData.Clickreport = ad.Clicktk
	postData.Clickreport = append(postData.Clickreport, "http://120.26.247.23/ad/report.php?method="+funcName+"&event=clickreport&os="+os+"&t="+util.GetRandom())

	return postData
}
