package huayuan

import (
	// "bytes"
	// "encoding/json"
	// "fmt"
	"io/ioutil"
	// "log"
	"net/http"
	"strconv"
	"strings"
	"time"
	// "unsafe"
	// "regexp"

	"ad/mongo"
	"ad/util"
	"github.com/pquerna/ffjson/ffjson"
)

const (
	PRO_URL                   = "http://ssp-show.huaat.com/ssp/bid/v1"
	DEV_URL                   = "http://hua.huaat.com:9521/ssp/netkitchen/bid"
	TOKEN_ANDROID             = "64f9deb06f884814bf6fbafa350285ff"
	ADSPACEID_ANDROID         = "20170714154931383" // "huaat_0021_301"
	ADSPACEID_ANDROID_FLOW    = "20170714154755041"
	ADSPACEID_ANDROID_STARTUP = "20170714154630450"
	TOKEN_IOS                 = "37806e77902c4143a8bd662746ebec4a"
	ADSPACEID_IOS             = "20170714154955729"
	ADSPACEID_IOS_FLOW        = "20170714154820240"
	ADSPACEID_IOS_STARTUP     = "20170714154717687"
)

func Huayuan(getReq *util.ReqMsg, funcName string) util.ResMsg {
	if getReq.Os == "1" {
		return base(getReq, funcName, ADSPACEID_ANDROID)
	}
	return base(getReq, funcName, ADSPACEID_IOS)
}

func HuayuanFlow(getReq *util.ReqMsg, funcName string) util.ResMsg {
	if getReq.Os == "1" {
		return base(getReq, funcName, ADSPACEID_ANDROID_FLOW)
	}
	return base(getReq, funcName, ADSPACEID_IOS_FLOW)
}

func HuayuanStartup(getReq *util.ReqMsg, funcName string) util.ResMsg {
	if getReq.Os == "1" {
		return base(getReq, funcName, ADSPACEID_ANDROID_STARTUP)
	}
	return base(getReq, funcName, ADSPACEID_IOS_STARTUP)
}

func base(getReq *util.ReqMsg, funcName, adid string) util.ResMsg {
	postData := Adreq{
		Data: []Reqata{
			{
				AdspaceId:       adid,
				DeviceId:        getReq.Imei,
				ScreenWidth:     0,
				ScreenHeight:    0,
				DensityDpi:      0,
				AppName:         "网上厨房",
				Os:              1,
				SimOperatorName: 1,
				Ip:              getReq.Ip,
				NetworkType:     1,
				AppPackage:      "cn.ecook",
				AndroidId:       getReq.Androidid,
				Brand:           getReq.Vendor,
			},
		},
	}
	reqData := &postData.Data[0]

	width, err := strconv.Atoi(getReq.Screenwidth)
	if err == nil {
		reqData.ScreenWidth = width
	}
	height, err := strconv.Atoi(getReq.Screenheight)
	if err == nil {
		reqData.ScreenHeight = height
	}
	sd, _ := strconv.Atoi(getReq.Sd)
	reqData.DensityDpi = sd / 160

	switch getReq.Imsi {
	case "1":
		reqData.SimOperatorName = 1
	case "2":
		reqData.SimOperatorName = 2
	case "3":
		reqData.SimOperatorName = 3
	}
	// conn := postData.Network.ConnectionType
	switch getReq.Network {
	case "wifi":
		reqData.NetworkType = 1
	case "2g":
		reqData.NetworkType = 2
	case "3g":
		reqData.NetworkType = 3
	case "4g":
		reqData.NetworkType = 4
	case "5g":
		reqData.NetworkType = 5
	default:
		reqData.NetworkType = 2
	}
	token := TOKEN_ANDROID
	if getReq.Os == "2" {
		reqData.Os = 2
		reqData.AppPackage = "cn.ecook.ecook"
		reqData.DeviceId = getReq.Idfa
		token = TOKEN_IOS
	}

	ma, err := ffjson.Marshal(&postData)
	if err != nil {
		mongo.LogTimeout(funcName, getReq.Os)
		return util.ResMsg{}
	}

	header := http.Header{}
	header.Set("Connection", "keep-alive")
	header.Set("Content-Type", "application/json;charset=utf-8")
	header.Set("Authorization", "Bearer "+token)
	resp, err := util.HttpPOST(PRO_URL, &header, ma)
	mongo.LogReq(funcName, getReq.Os)
	if err != nil {
		mongo.LogTimeout(funcName, getReq.Os)
		return util.ResMsg{}
	}
	data, err := ioutil.ReadAll(resp.Body)
	resp.Body.Close()
	if err != nil {
		mongo.LogNo(funcName, getReq.Os)
		return util.ResMsg{}
	}
	if resp.StatusCode != 200 {
		mongo.LogTimeout(funcName, getReq.Os)
		return util.ResMsg{}
	}
	return FormateRes(data, getReq.Os, funcName, getReq)
}

func FormateRes(res []byte, os, funcName string, getReq *util.ReqMsg) util.ResMsg {
	resData := Adres{}
	ffjson.Unmarshal(res, &resData)
	// fmt.Printf("%+v", resData)
	// fmt.Println(string(res))
	if resData.Code != 0 && resData.Code != 1 {
		mongo.LogNo(funcName, os)
		return util.ResMsg{}
	}
	if len(resData.Data) == 0 {
		mongo.LogNo(funcName, os)
		return util.ResMsg{}
	}
	ad := resData.Data[0]
	postData := util.ResMsg{
		Id:       "0",
		Weight:   0,
		State:    1,
		Title:    ad.Title,
		Content:  ad.Description,
		ImageUrl: ad.Multimedia1_file_url,
		Uri:      ad.Landpage,
	}
	postData.Displayreport = append(postData.Displayreport, ad.ShowUrl)
	thridShowUrl := ad.ThridShowUrl
	if ad.ThridShowUrl != "" {
		thridShowUrl = strings.Replace(thridShowUrl, "__OS__", os, -1)
		thridShowUrl = strings.Replace(thridShowUrl, "__IMEI__", getReq.Imei, -1)
		thridShowUrl = strings.Replace(thridShowUrl, "__IDFA__", getReq.Idfa, -1)
		thridShowUrl = strings.Replace(thridShowUrl, "__IP__", getReq.Ip, -1)
		thridShowUrl = strings.Replace(thridShowUrl, "__UA__", getReq.Ua, -1)
		thridShowUrl = strings.Replace(thridShowUrl, "__TS__", string(time.Now().Unix()), -1)
		postData.Displayreport = append(postData.Displayreport, thridShowUrl)
	}
	postData.Displayreport = append(postData.Displayreport, "http://120.26.247.23/ad/report.php?method="+funcName+"&event=displayreport&os="+os+"&t="+util.GetRandom())
	postData.Clickreport = append(postData.Clickreport, ad.ClickUrl, "http://120.26.247.23/ad/report.php?method="+funcName+"&event=clickreport&os="+os+"&t="+util.GetRandom())

	return postData
}
