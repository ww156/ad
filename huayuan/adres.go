package huayuan

type Adres struct {
	Code    int
	Message string
	Data    []Resdata
}

type Resdata struct {
	Action               int
	Landpage             string
	IsDeepLink           int
	DeeplinkUrl          string
	ShowUrl              string
	ClickUrl             string
	ThridShowUrl         string
	Package              string
	Creative_view        int
	Creative_spec        int
	Multimedia1_file_url string
	Multimedia2_file_url string
	Multimedia3_file_url string
	Multimedia4_file_url string
	Title                string
	Description          string
}
