package huayuan

type Adreq struct {
	Data []Reqata `json:"data"`
}

type Reqata struct {
	AdspaceId       string  `json:"adspaceId"`
	DeviceId        string  `json:"deviceId"`
	ScreenWidth     int     `json:"screenWidth"`
	ScreenHeight    int     `json:"screenHeight"`
	DensityDpi      int     `json:"densityDpi"`
	AppName         string  `json:"appName"`
	Os              int     `json:"os"`
	SimOperatorName int     `json:"simOperatorName"`
	Ip              string  `json:"ip"`
	NetworkType     int     `json:"networkType"`
	AppPackage      string  `json:"appPackage"`
	Latitude        float64 `json:"latitude"`
	Longitude       float64 `json:"longitude"`
	AndroidId       string  `json:"androidId"`
	Brand           string  `json:"brand"`
}
